﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int smallShirtAmount = 0;
        int mediumShirtAmount = 0;
        int largeShirtAmount = 0;
        double tax = 0;
        double subtotal = 0;
        double completeTotal = 0;
        double smallShirtPrice = 9.99;
        double mediumShirtPrice = 10.99;
        double largeShirtPrice = 11.99;
        double smallShirtSubtotal = 0;
        double mediumShirtSubtotal = 0;
        double largeShirtSubtotal = 0;

        private void smallShirtButton_Click(object sender, EventArgs e)
        {
            string shirtSize = "s";
            AddShirt(shirtSize);
            ShowInvoice();
        }

        private void mediumShirtButton_Click(object sender, EventArgs e)
        {
            string shirtSize = "m";
            AddShirt(shirtSize);
            ShowInvoice();
        }

        private void largeShirtButton_Click(object sender, EventArgs e)
        {
            string shirtSize = "l";
            AddShirt(shirtSize);
            ShowInvoice();
        }

        private void clearOrderButton_Click(object sender, EventArgs e)
        {
            ClearOrder();
            ShowInvoice();
        }

        public void AddShirt(string shirtSize)
        {
            if (shirtSize == "s")
            {
                ++smallShirtAmount;
                smallShirtsOrderedLabel.Text = Convert.ToString(smallShirtAmount);
                smallShirtSubtotal = smallShirtPrice * smallShirtAmount;
            }
            else if (shirtSize == "m")
            {
                ++mediumShirtAmount;
                mediumShirtsOrderedLabel.Text = Convert.ToString(mediumShirtAmount);
                mediumShirtSubtotal = mediumShirtPrice * mediumShirtAmount;
            }
            else if (shirtSize == "l")
            {
                ++largeShirtAmount;
                largeShirtsOrderedLabel.Text = Convert.ToString(largeShirtAmount);
                largeShirtSubtotal = largeShirtPrice * largeShirtAmount;
            }
            subtotal = smallShirtSubtotal + mediumShirtSubtotal + largeShirtSubtotal;
            tax = (subtotal*0.07);
            completeTotal = subtotal + tax;
        }

        public void ClearOrder()
        {
            smallShirtsOrderedLabel.Text = "";
            mediumShirtsOrderedLabel.Text = "";
            largeShirtsOrderedLabel.Text = "";
            smallShirtAmount = 0;
            mediumShirtAmount = 0;
            largeShirtAmount = 0;
            smallShirtSubtotal = 0;
            mediumShirtSubtotal = 0;
            largeShirtSubtotal = 0;
            subtotal = 0;
            tax = 0;
            completeTotal = 0;
        }

        public void ShowInvoice()
        {
            orderSummaryLabel.Text = String.Format($"{smallShirtAmount} Small @ {smallShirtPrice.ToString("C")} each {Environment.NewLine}" +
                $"{mediumShirtAmount} Medium @ {mediumShirtPrice.ToString("C")} each {Environment.NewLine}" +
                $"{largeShirtAmount} Large @ {largeShirtPrice.ToString("C")} each {Environment.NewLine} {Environment.NewLine}" +
                $"-------------------------------------- {Environment.NewLine}{Environment.NewLine}" +
                $"Subtotal: {subtotal.ToString("C")} {Environment.NewLine}" +
                $"Tax: {tax.ToString("C")} {Environment.NewLine}" +
                $"Total: {completeTotal.ToString("C")}");
        }
    }
}
