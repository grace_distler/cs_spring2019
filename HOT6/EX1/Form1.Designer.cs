﻿namespace EX1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.smallShirtButton = new System.Windows.Forms.Button();
            this.mediumShirtButton = new System.Windows.Forms.Button();
            this.largeShirtButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.orderSummaryLabel = new System.Windows.Forms.Label();
            this.clearOrderButton = new System.Windows.Forms.Button();
            this.smallShirtsOrderedLabel = new System.Windows.Forms.Label();
            this.mediumShirtsOrderedLabel = new System.Windows.Forms.Label();
            this.largeShirtsOrderedLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(39, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(185, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "What size shirts do you need?";
            // 
            // smallShirtButton
            // 
            this.smallShirtButton.Location = new System.Drawing.Point(42, 103);
            this.smallShirtButton.Name = "smallShirtButton";
            this.smallShirtButton.Size = new System.Drawing.Size(48, 23);
            this.smallShirtButton.TabIndex = 1;
            this.smallShirtButton.Text = "S";
            this.smallShirtButton.UseVisualStyleBackColor = true;
            this.smallShirtButton.Click += new System.EventHandler(this.smallShirtButton_Click);
            // 
            // mediumShirtButton
            // 
            this.mediumShirtButton.Location = new System.Drawing.Point(141, 103);
            this.mediumShirtButton.Name = "mediumShirtButton";
            this.mediumShirtButton.Size = new System.Drawing.Size(47, 23);
            this.mediumShirtButton.TabIndex = 2;
            this.mediumShirtButton.Text = "M";
            this.mediumShirtButton.UseVisualStyleBackColor = true;
            this.mediumShirtButton.Click += new System.EventHandler(this.mediumShirtButton_Click);
            // 
            // largeShirtButton
            // 
            this.largeShirtButton.Location = new System.Drawing.Point(242, 103);
            this.largeShirtButton.Name = "largeShirtButton";
            this.largeShirtButton.Size = new System.Drawing.Size(46, 23);
            this.largeShirtButton.TabIndex = 3;
            this.largeShirtButton.Text = "L";
            this.largeShirtButton.UseVisualStyleBackColor = true;
            this.largeShirtButton.Click += new System.EventHandler(this.largeShirtButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "$9.99";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(138, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "$10.99";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(239, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "$11.99";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.orderSummaryLabel);
            this.groupBox1.Location = new System.Drawing.Point(42, 197);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(246, 179);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Order Summary";
            // 
            // orderSummaryLabel
            // 
            this.orderSummaryLabel.AutoSize = true;
            this.orderSummaryLabel.Location = new System.Drawing.Point(23, 38);
            this.orderSummaryLabel.Name = "orderSummaryLabel";
            this.orderSummaryLabel.Size = new System.Drawing.Size(0, 13);
            this.orderSummaryLabel.TabIndex = 0;
            // 
            // clearOrderButton
            // 
            this.clearOrderButton.Location = new System.Drawing.Point(125, 398);
            this.clearOrderButton.Name = "clearOrderButton";
            this.clearOrderButton.Size = new System.Drawing.Size(75, 23);
            this.clearOrderButton.TabIndex = 8;
            this.clearOrderButton.Text = "Clear Order";
            this.clearOrderButton.UseVisualStyleBackColor = true;
            this.clearOrderButton.Click += new System.EventHandler(this.clearOrderButton_Click);
            // 
            // smallShirtsOrderedLabel
            // 
            this.smallShirtsOrderedLabel.AutoSize = true;
            this.smallShirtsOrderedLabel.Location = new System.Drawing.Point(42, 133);
            this.smallShirtsOrderedLabel.Name = "smallShirtsOrderedLabel";
            this.smallShirtsOrderedLabel.Size = new System.Drawing.Size(0, 13);
            this.smallShirtsOrderedLabel.TabIndex = 9;
            // 
            // mediumShirtsOrderedLabel
            // 
            this.mediumShirtsOrderedLabel.AutoSize = true;
            this.mediumShirtsOrderedLabel.Location = new System.Drawing.Point(141, 133);
            this.mediumShirtsOrderedLabel.Name = "mediumShirtsOrderedLabel";
            this.mediumShirtsOrderedLabel.Size = new System.Drawing.Size(0, 13);
            this.mediumShirtsOrderedLabel.TabIndex = 10;
            // 
            // largeShirtsOrderedLabel
            // 
            this.largeShirtsOrderedLabel.AutoSize = true;
            this.largeShirtsOrderedLabel.Location = new System.Drawing.Point(242, 133);
            this.largeShirtsOrderedLabel.Name = "largeShirtsOrderedLabel";
            this.largeShirtsOrderedLabel.Size = new System.Drawing.Size(0, 13);
            this.largeShirtsOrderedLabel.TabIndex = 11;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 459);
            this.Controls.Add(this.largeShirtsOrderedLabel);
            this.Controls.Add(this.mediumShirtsOrderedLabel);
            this.Controls.Add(this.smallShirtsOrderedLabel);
            this.Controls.Add(this.clearOrderButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.largeShirtButton);
            this.Controls.Add(this.mediumShirtButton);
            this.Controls.Add(this.smallShirtButton);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Super Custom Cool Shirts";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button smallShirtButton;
        private System.Windows.Forms.Button mediumShirtButton;
        private System.Windows.Forms.Button largeShirtButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button clearOrderButton;
        private System.Windows.Forms.Label orderSummaryLabel;
        private System.Windows.Forms.Label smallShirtsOrderedLabel;
        private System.Windows.Forms.Label mediumShirtsOrderedLabel;
        private System.Windows.Forms.Label largeShirtsOrderedLabel;
    }
}

