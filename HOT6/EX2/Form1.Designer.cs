﻿namespace EX2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.quantityEntryTextBox = new System.Windows.Forms.TextBox();
            this.discountCodeEntryTextBox = new System.Windows.Forms.TextBox();
            this.orderButton = new System.Windows.Forms.Button();
            this.discountCodeLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.summaryLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Quantity:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Discount Code:";
            // 
            // quantityEntryTextBox
            // 
            this.quantityEntryTextBox.Location = new System.Drawing.Point(170, 36);
            this.quantityEntryTextBox.Name = "quantityEntryTextBox";
            this.quantityEntryTextBox.Size = new System.Drawing.Size(191, 20);
            this.quantityEntryTextBox.TabIndex = 2;
            // 
            // discountCodeEntryTextBox
            // 
            this.discountCodeEntryTextBox.Location = new System.Drawing.Point(170, 75);
            this.discountCodeEntryTextBox.Name = "discountCodeEntryTextBox";
            this.discountCodeEntryTextBox.Size = new System.Drawing.Size(191, 20);
            this.discountCodeEntryTextBox.TabIndex = 3;
            // 
            // orderButton
            // 
            this.orderButton.Location = new System.Drawing.Point(415, 58);
            this.orderButton.Name = "orderButton";
            this.orderButton.Size = new System.Drawing.Size(75, 23);
            this.orderButton.TabIndex = 4;
            this.orderButton.Text = "Order";
            this.orderButton.UseVisualStyleBackColor = true;
            this.orderButton.Click += new System.EventHandler(this.orderButton_Click);
            // 
            // discountCodeLabel
            // 
            this.discountCodeLabel.AutoSize = true;
            this.discountCodeLabel.Location = new System.Drawing.Point(170, 117);
            this.discountCodeLabel.Name = "discountCodeLabel";
            this.discountCodeLabel.Size = new System.Drawing.Size(0, 13);
            this.discountCodeLabel.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.summaryLabel);
            this.groupBox1.Location = new System.Drawing.Point(39, 172);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(451, 161);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Order Summary";
            // 
            // summaryLabel
            // 
            this.summaryLabel.AutoSize = true;
            this.summaryLabel.Location = new System.Drawing.Point(42, 34);
            this.summaryLabel.Name = "summaryLabel";
            this.summaryLabel.Size = new System.Drawing.Size(0, 13);
            this.summaryLabel.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 359);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.discountCodeLabel);
            this.Controls.Add(this.orderButton);
            this.Controls.Add(this.discountCodeEntryTextBox);
            this.Controls.Add(this.quantityEntryTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Crazy Crank\'s T-Shirt Factory";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox quantityEntryTextBox;
        private System.Windows.Forms.TextBox discountCodeEntryTextBox;
        private System.Windows.Forms.Button orderButton;
        private System.Windows.Forms.Label discountCodeLabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label summaryLabel;
    }
}

