﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int quantity = 0;
        float discountPercent = 0;
        double shirtPrice = 0;
        double subtotal = 0;
        double tax = 0;
        double completeTotal = 0;
        string[] discountCodes = { "8264", "5679", "6483" };
        float[] discountPercentages = { 30, 20, 10 };

        private void orderButton_Click(object sender, EventArgs e)
        {
            string quantityEntry = quantityEntryTextBox.Text;
            string discountCodeEntry = discountCodeEntryTextBox.Text;
            if (quantityEntry == "")
            {
                discountCodeLabel.Text = "Please Enter a Quantity of T-Shirts";
            }
            else
            {
                CheckDiscountCode(discountCodeEntry);
                quantity = Convert.ToInt32(quantityEntry);
                ShowInvoice(quantity, discountPercent);
            }
        }

        public float CheckDiscountCode(string discountCode)
        {
            int discountIndex;
            for (discountIndex=0; discountIndex < discountCodes.Length; discountIndex++)
            {
                if (discountCode=="8264")
                {
                    discountPercent = discountPercentages[0];
                    discountCodeLabel.Text = String.Format($"{discountPercent}% Discount Applied");
                }
                else if(discountCode=="5679")
                {
                    discountPercent = discountPercentages[1];
                    discountCodeLabel.Text = String.Format($"{discountPercent}% Discount Applied");
                }
                else if(discountCode=="6483")
                {
                    discountPercent = discountPercentages[2];
                    discountCodeLabel.Text = String.Format($"{discountPercent}% Discount Applied");
                }
                else
                {
                    discountCodeLabel.Text = "* Invalid Discount Code";
                    discountPercent = 0;
                }
            }
            return discountPercent;
        }

        public void ShowInvoice(int quantity, float discountPercent)
        {
            //forloop searching through array
            if (discountPercent==0)
            {
                shirtPrice = 13.75;
                subtotal = quantity * shirtPrice;
                tax = subtotal * .08;
                completeTotal = subtotal + tax;
            }
            else if(discountPercent==discountPercentages[0])
            {
                shirtPrice = 9.625;
                subtotal = quantity * shirtPrice;
                tax = subtotal * .08;
                completeTotal = subtotal + tax;
            }
            else if (discountPercent == discountPercentages[1])
            {
                shirtPrice = 11;
                subtotal = quantity * shirtPrice;
                tax = subtotal * .08;
                completeTotal = subtotal + tax;
            }
            else if (discountPercent == discountPercentages[2])
            {
                shirtPrice = 12.375;
                subtotal = quantity * shirtPrice;
                tax = subtotal * .08;
                completeTotal = subtotal + tax;
            }

            summaryLabel.Text= String.Format($"{quantity} @ {shirtPrice.ToString("C")} each {Environment.NewLine} {Environment.NewLine}" +
                $"-------------------------------------- {Environment.NewLine}{Environment.NewLine}" +
                $"Subtotal: {subtotal.ToString("C")} {Environment.NewLine}" +
                $"Tax: {tax.ToString("C")} {Environment.NewLine}" +
                $"Total: {completeTotal.ToString("C")}");
        }
    }
}
