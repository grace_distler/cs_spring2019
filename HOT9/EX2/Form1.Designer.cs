﻿namespace EX2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnArithmetic = new System.Windows.Forms.Button();
            this.lblArithmeticError = new System.Windows.Forms.Label();
            this.lblFormatError = new System.Windows.Forms.Label();
            this.btnFormat = new System.Windows.Forms.Button();
            this.lblInvalidCast = new System.Windows.Forms.Label();
            this.btnInvalidCast = new System.Windows.Forms.Button();
            this.lblNullReference = new System.Windows.Forms.Label();
            this.btnNullReference = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnArithmetic
            // 
            this.btnArithmetic.Location = new System.Drawing.Point(30, 50);
            this.btnArithmetic.Name = "btnArithmetic";
            this.btnArithmetic.Size = new System.Drawing.Size(191, 31);
            this.btnArithmetic.TabIndex = 0;
            this.btnArithmetic.Text = "Arithmetic Exception";
            this.btnArithmetic.UseVisualStyleBackColor = true;
            this.btnArithmetic.Click += new System.EventHandler(this.btnArithmetic_Click);
            // 
            // lblArithmeticError
            // 
            this.lblArithmeticError.AutoSize = true;
            this.lblArithmeticError.Location = new System.Drawing.Point(27, 84);
            this.lblArithmeticError.Name = "lblArithmeticError";
            this.lblArithmeticError.Size = new System.Drawing.Size(0, 17);
            this.lblArithmeticError.TabIndex = 1;
            // 
            // lblFormatError
            // 
            this.lblFormatError.AutoSize = true;
            this.lblFormatError.Location = new System.Drawing.Point(27, 182);
            this.lblFormatError.Name = "lblFormatError";
            this.lblFormatError.Size = new System.Drawing.Size(0, 17);
            this.lblFormatError.TabIndex = 3;
            // 
            // btnFormat
            // 
            this.btnFormat.Location = new System.Drawing.Point(30, 148);
            this.btnFormat.Name = "btnFormat";
            this.btnFormat.Size = new System.Drawing.Size(191, 31);
            this.btnFormat.TabIndex = 2;
            this.btnFormat.Text = "Format Exception";
            this.btnFormat.UseVisualStyleBackColor = true;
            this.btnFormat.Click += new System.EventHandler(this.btnFormat_Click);
            // 
            // lblInvalidCast
            // 
            this.lblInvalidCast.AutoSize = true;
            this.lblInvalidCast.Location = new System.Drawing.Point(27, 279);
            this.lblInvalidCast.Name = "lblInvalidCast";
            this.lblInvalidCast.Size = new System.Drawing.Size(0, 17);
            this.lblInvalidCast.TabIndex = 5;
            // 
            // btnInvalidCast
            // 
            this.btnInvalidCast.Location = new System.Drawing.Point(30, 245);
            this.btnInvalidCast.Name = "btnInvalidCast";
            this.btnInvalidCast.Size = new System.Drawing.Size(191, 31);
            this.btnInvalidCast.TabIndex = 4;
            this.btnInvalidCast.Text = "Invalid Cast Exception";
            this.btnInvalidCast.UseVisualStyleBackColor = true;
            this.btnInvalidCast.Click += new System.EventHandler(this.btnInvalidCast_Click);
            // 
            // lblNullReference
            // 
            this.lblNullReference.AutoSize = true;
            this.lblNullReference.Location = new System.Drawing.Point(27, 370);
            this.lblNullReference.Name = "lblNullReference";
            this.lblNullReference.Size = new System.Drawing.Size(0, 17);
            this.lblNullReference.TabIndex = 7;
            // 
            // btnNullReference
            // 
            this.btnNullReference.Location = new System.Drawing.Point(30, 336);
            this.btnNullReference.Name = "btnNullReference";
            this.btnNullReference.Size = new System.Drawing.Size(191, 31);
            this.btnNullReference.TabIndex = 6;
            this.btnNullReference.Text = "Null Reference Exception";
            this.btnNullReference.UseVisualStyleBackColor = true;
            this.btnNullReference.Click += new System.EventHandler(this.btnNullReference_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 432);
            this.Controls.Add(this.lblNullReference);
            this.Controls.Add(this.btnNullReference);
            this.Controls.Add(this.lblInvalidCast);
            this.Controls.Add(this.btnInvalidCast);
            this.Controls.Add(this.lblFormatError);
            this.Controls.Add(this.btnFormat);
            this.Controls.Add(this.lblArithmeticError);
            this.Controls.Add(this.btnArithmetic);
            this.Name = "Form1";
            this.Text = "Exceptions";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnArithmetic;
        private System.Windows.Forms.Label lblArithmeticError;
        private System.Windows.Forms.Label lblFormatError;
        private System.Windows.Forms.Button btnFormat;
        private System.Windows.Forms.Label lblInvalidCast;
        private System.Windows.Forms.Button btnInvalidCast;
        private System.Windows.Forms.Label lblNullReference;
        private System.Windows.Forms.Button btnNullReference;
    }
}

