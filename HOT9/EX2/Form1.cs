﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnArithmetic_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch(ArithmeticException ex)
            {
                lblArithmeticError.ForeColor = Color.Red;
                lblArithmeticError.Text = "Incorrect Arithmetic";
            }
        }
       
        private void btnFormat_Click(object sender, EventArgs e)
        {
            int number = 0;
            try
            {
                number = Convert.ToInt32(txtEnter.Text);
            }
            catch (FormatException ex)
            {
                lblFormatError.ForeColor = Color.Red;
                lblFormatError.Text = "Incorrect Format";
            }
        }

        private void btnInvalidCast_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (InvalidCastException ex)
            {
                lblInvalidCast.ForeColor = Color.Red;
                lblInvalidCast.Text = "Invalid Cast";
            }
        }

        private void btnNullReference_Click(object sender, EventArgs e)
        {
            try
            {
                string name = null;
                if (name == null)
                {
                    new NullReferenceException();
                }
            }
            catch (NullReferenceException ex)
            {
                lblNullReference.ForeColor = Color.Red;
                lblNullReference.Text = "Null Reference";
            }
        }
    }
}
