﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            lblErrorMessage.Text = "";
            lblResults.Text = "";
        }
        TestStatistics test = new TestStatistics();

        private void btnEnter_Click(object sender, EventArgs e)
        {
            float entry;
            try
            {
                lblErrorMessage.Text = "";
                entry = float.Parse(txtScore.Text);
                test.AddScore(entry);
                lblResults.Text = string.Format("Number of scores: " + count +
                "\nSum of test scores: " + sum +
                "\nAverage test score: " + Math.Round(avg, 2) +
                "\nLowest test score: " + minScore +
                "\nHighest test score: " + maxScore);                
            }
            catch (Exception ex)
            {
                lblErrorMessage.ForeColor = Color.Red;
                lblErrorMessage.Text = ex.Message;
            }
        }
    }
}
