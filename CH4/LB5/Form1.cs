﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB5
{
    public partial class Form1 : Form
    {
        private int countComputer = 0;
        private int countPlayer = 0;
        private int countGame = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void rockButton_Click(object sender, EventArgs e)
        {
            Random rand = new Random();
            int num1 = rand.Next(1, 3);
            countGame++;
            switch (num1)
            {
                case 1:
                    computerOutputLabel.Text = "Computer pick Rock";
                    break;
                case 2:
                    computerOutputLabel.Text = "Computer pick Paper";
                    countComputer++;
                    break;
                case 3:
                    computerOutputLabel.Text = "Computer pick Scissors";
                    countPlayer++;
                    break;
            }
            currentScoreLabel.Text = "Player Score: " + countPlayer + "     Computer Score: " + countComputer;
            if (countGame == 3)
            {
                if (countPlayer > countComputer)
                    winnerOutputLabel.Text = "Player Wins!!!";
                else if (countComputer > countPlayer)
                    winnerOutputLabel.Text = "Computer Wins!!!";
                else
                    winnerOutputLabel.Text = "It's a tie!";


                rockButton.Enabled = false;
                paperButton.Enabled = false;
                scissorsButton.Enabled = false;
            }
        }

        private void paperButton_Click(object sender, EventArgs e)
        {
            Random rand = new Random();
            int num1 = rand.Next(1, 3);
            countGame++;
            switch (num1)
            {
                case 1:
                    computerOutputLabel.Text = "Computer pick Rock";
                    countPlayer++;
                    break;
                case 2:
                    computerOutputLabel.Text = "Computer pick Paper";
                    break;
                case 3:
                    computerOutputLabel.Text = "Computer pick Scissors";
                    countComputer++;
                    break;
            }
            currentScoreLabel.Text = "Player Score: " + countPlayer + "     Computer Score: " + countComputer;
            if (countGame == 3)
            {
                if (countPlayer > countComputer)
                    winnerOutputLabel.Text = "Player Wins!!!";
                else if (countComputer > countPlayer)
                    winnerOutputLabel.Text = "Computer Wins!!!";
                else
                    winnerOutputLabel.Text = "It's a tie!";


                rockButton.Enabled = false;
                paperButton.Enabled = false;
                scissorsButton.Enabled = false;
            }
        }

        private void scissorsButton_Click(object sender, EventArgs e)
        {
            Random rand = new Random();
            int num1 = rand.Next(1, 3);
            countGame++;
            switch (num1)
            {
                case 1:
                    computerOutputLabel.Text = "Computer pick Rock";
                    countComputer++;
                    break;
                case 2:
                    computerOutputLabel.Text = "Computer pick Paper";
                    countPlayer++;
                    break;
                case 3:
                    computerOutputLabel.Text = "Computer pick Scissors";
                    break;
            }
            currentScoreLabel.Text = "Player Score: " + countPlayer + "     Computer Score: " + countComputer;
            if (countGame == 3)
            {
                if (countPlayer > countComputer)
                    winnerOutputLabel.Text = "Player Wins!!!";
                else if (countComputer > countPlayer)
                    winnerOutputLabel.Text = "Computer Wins!!!";
                else
                    winnerOutputLabel.Text = "It's a tie!";


                rockButton.Enabled = false;
                paperButton.Enabled = false;
                scissorsButton.Enabled = false;
            }
        }
    }
}
