﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void admitButton_Click(object sender, EventArgs e)
        {
            double gpa, testScore;

            gpa = Convert.ToDouble(gpaTextBox.Text);
            testScore = Convert.ToDouble(testScoreTextBox.Text);

            if(gpa>=3.0 && testScore >= 60)
            {
                outputLabel.Text = ("Accept");
            } else if (gpa<3.0 && testScore >= 80)
            {
                outputLabel.Text = ("Accept");
            } else
            {
                outputLabel.Text = ("Reject");
            }
        }
    }
}
