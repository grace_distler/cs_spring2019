﻿namespace LB4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.windSpeedTextBox = new System.Windows.Forms.TextBox();
            this.estimateButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.categoryOutputTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Wind Speed";
            // 
            // windSpeedTextBox
            // 
            this.windSpeedTextBox.Location = new System.Drawing.Point(29, 52);
            this.windSpeedTextBox.Name = "windSpeedTextBox";
            this.windSpeedTextBox.Size = new System.Drawing.Size(119, 26);
            this.windSpeedTextBox.TabIndex = 1;
            // 
            // estimateButton
            // 
            this.estimateButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estimateButton.Location = new System.Drawing.Point(29, 113);
            this.estimateButton.Name = "estimateButton";
            this.estimateButton.Size = new System.Drawing.Size(119, 31);
            this.estimateButton.TabIndex = 2;
            this.estimateButton.Text = "Estimate";
            this.estimateButton.UseVisualStyleBackColor = true;
            this.estimateButton.Click += new System.EventHandler(this.estimateButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(217, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Category";
            // 
            // categoryOutputTextBox
            // 
            this.categoryOutputTextBox.Location = new System.Drawing.Point(221, 53);
            this.categoryOutputTextBox.Name = "categoryOutputTextBox";
            this.categoryOutputTextBox.ReadOnly = true;
            this.categoryOutputTextBox.Size = new System.Drawing.Size(211, 26);
            this.categoryOutputTextBox.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 184);
            this.Controls.Add(this.categoryOutputTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.estimateButton);
            this.Controls.Add(this.windSpeedTextBox);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Hurricane Speed";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox windSpeedTextBox;
        private System.Windows.Forms.Button estimateButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox categoryOutputTextBox;
    }
}

