﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void estimateButton_Click(object sender, EventArgs e)
        {
            double windSpeed;

            windSpeed = Convert.ToDouble(windSpeedTextBox.Text);

            if (windSpeed >=157)
            {
                categoryOutputTextBox.Text = "Category 5 Hurricane";
            } else if (windSpeed >= 130)
            {
                categoryOutputTextBox.Text = "Category 4 Hurricane";
            } else if (windSpeed >= 111)
            {
                categoryOutputTextBox.Text = "Category 3 Hurricane";
            } else if (windSpeed >= 96)
            {
                categoryOutputTextBox.Text = "Category 2 Hurricane";
            } else if (windSpeed >= 74)
            {
                categoryOutputTextBox.Text = "Category 1 Hurricane";
            } else {
                categoryOutputTextBox.Text = "Not a Hurricane";
            }
        }
    }
}
