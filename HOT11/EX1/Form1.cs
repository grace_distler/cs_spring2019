﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace EX1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnRainbowBrowse_Click(object sender, EventArgs e)
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;

            using (OpenFileDialog openFile = new OpenFileDialog())
            {
                openFile.InitialDirectory = "C:\\Users\\User\\Desktop\\cs_spring2019\\HOT11\\EX1\bin\\Debug";
                openFile.Filter = "csv files (*.csv)|*.csv";
                openFile.RestoreDirectory = true;

                if (openFile.ShowDialog() == DialogResult.OK)
                {
                    filePath = openFile.FileName;
                    txtRainbow.Text = filePath;

                    var fileStream = openFile.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        fileContent = reader.ReadToEnd();
                    }
                }
            }
            btnDumpedBrowse.Enabled = true;
        }

        private void btnDumpedBrowse_Click(object sender, EventArgs e)
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;

            using (OpenFileDialog openFile = new OpenFileDialog())
            {
                openFile.InitialDirectory = "C:\\Users\\User\\Desktop\\cs_spring2019\\HOT11\\EX1\bin\\Debug";
                openFile.Filter = "txt files (*.txt)|*.txt";
                openFile.RestoreDirectory = true;

                if (openFile.ShowDialog() == DialogResult.OK)
                {
                    filePath = openFile.FileName;
                    txtDumped.Text = filePath;

                    var fileStream = openFile.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        fileContent = reader.ReadToEnd();
                    }
                }
            }
            btnCrack.Enabled = true;
        }

        private void btnCrack_Click(object sender, EventArgs e)
        {
            var filePath = string.Empty;

            using (OpenFileDialog openFile = new OpenFileDialog())
            {
                filePath = openFile.FileName;
                
                CrackPassword(filePath);
                
            }
        }

        public string CrackPassword(string hash)
        {
            using (StreamReader rainbowReader = new StreamReader("rainbow.csv"))
            using (StreamReader dumpReader = new StreamReader("dump.txt"))
            {
                while (true)
                {
                    if (rainbowReader.EndOfStream || dumpReader.EndOfStream)
                    {
                        break;
                    }

                    string rainbowHash = rainbowReader.ReadLine();
                    string dumpedHash = dumpReader.ReadLine();

                    if (dumpedHash.Equals(rainbowHash))
                    {
                        lblOutput.Text += $"\n{dumpedHash.GetHashCode()} {rainbowHash.GetHashCode()}";
                    }
                    else if (!dumpedHash.Equals(rainbowHash))
                    {
                        lblOutput.Text += $"\n{dumpedHash.GetHashCode()}   * FAIL *";
                    }
                }

                string hashOutput = rainbowReader.GetHashCode().ToString();
                lblOutput.Text += hashOutput;

                return hashOutput;
            }
        }
    }
}
