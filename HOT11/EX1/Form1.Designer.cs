﻿namespace EX1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtRainbow = new System.Windows.Forms.TextBox();
            this.btnRainbowBrowse = new System.Windows.Forms.Button();
            this.btnDumpedBrowse = new System.Windows.Forms.Button();
            this.txtDumped = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCrack = new System.Windows.Forms.Button();
            this.lblOutput = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Rainbow Table";
            // 
            // txtRainbow
            // 
            this.txtRainbow.Location = new System.Drawing.Point(140, 29);
            this.txtRainbow.Name = "txtRainbow";
            this.txtRainbow.ReadOnly = true;
            this.txtRainbow.Size = new System.Drawing.Size(535, 22);
            this.txtRainbow.TabIndex = 0;
            // 
            // btnRainbowBrowse
            // 
            this.btnRainbowBrowse.Location = new System.Drawing.Point(714, 27);
            this.btnRainbowBrowse.Name = "btnRainbowBrowse";
            this.btnRainbowBrowse.Size = new System.Drawing.Size(91, 27);
            this.btnRainbowBrowse.TabIndex = 0;
            this.btnRainbowBrowse.Text = "Browse";
            this.btnRainbowBrowse.UseVisualStyleBackColor = true;
            this.btnRainbowBrowse.Click += new System.EventHandler(this.btnRainbowBrowse_Click);
            // 
            // btnDumpedBrowse
            // 
            this.btnDumpedBrowse.Enabled = false;
            this.btnDumpedBrowse.Location = new System.Drawing.Point(714, 78);
            this.btnDumpedBrowse.Name = "btnDumpedBrowse";
            this.btnDumpedBrowse.Size = new System.Drawing.Size(91, 27);
            this.btnDumpedBrowse.TabIndex = 1;
            this.btnDumpedBrowse.Text = "Browse";
            this.btnDumpedBrowse.UseVisualStyleBackColor = true;
            this.btnDumpedBrowse.Click += new System.EventHandler(this.btnDumpedBrowse_Click);
            // 
            // txtDumped
            // 
            this.txtDumped.Location = new System.Drawing.Point(140, 80);
            this.txtDumped.Name = "txtDumped";
            this.txtDumped.ReadOnly = true;
            this.txtDumped.Size = new System.Drawing.Size(535, 22);
            this.txtDumped.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Dumped Hashes";
            // 
            // btnCrack
            // 
            this.btnCrack.Enabled = false;
            this.btnCrack.Location = new System.Drawing.Point(140, 136);
            this.btnCrack.Name = "btnCrack";
            this.btnCrack.Size = new System.Drawing.Size(535, 47);
            this.btnCrack.TabIndex = 2;
            this.btnCrack.Text = "Crack!";
            this.btnCrack.UseVisualStyleBackColor = true;
            this.btnCrack.Click += new System.EventHandler(this.btnCrack_Click);
            // 
            // lblOutput
            // 
            this.lblOutput.Location = new System.Drawing.Point(137, 202);
            this.lblOutput.Name = "lblOutput";
            this.lblOutput.Size = new System.Drawing.Size(538, 117);
            this.lblOutput.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 347);
            this.Controls.Add(this.lblOutput);
            this.Controls.Add(this.btnCrack);
            this.Controls.Add(this.btnDumpedBrowse);
            this.Controls.Add(this.txtDumped);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnRainbowBrowse);
            this.Controls.Add(this.txtRainbow);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Password Cracker";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtRainbow;
        private System.Windows.Forms.Button btnRainbowBrowse;
        private System.Windows.Forms.Button btnDumpedBrowse;
        private System.Windows.Forms.TextBox txtDumped;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCrack;
        private System.Windows.Forms.Label lblOutput;
    }
}

