﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB2
{
    public partial class Form1 : Form
    {
        Course c1;

        public Form1()
        {
            InitializeComponent();

            c1 = new Course();
            List<Student> initialStudents = new List<Student>();
            initialStudents.Add(new Student { FirstName = "Bob", LastName = "Smith", EmailAddress = "BobSmith@gmail.com", StudentId = 1111 });
            initialStudents.Add(new Student { FirstName = "Grace", LastName = "Distler", EmailAddress = "grace.distler@gmail.com", StudentId = 1112 });
            initialStudents.Add(new Student { FirstName = "Joe", LastName = "Dart", EmailAddress = "joeDart1@gmail.com", StudentId = 1113 });
            initialStudents.Add(new Student { FirstName = "Kate", LastName = "Richie", EmailAddress = "k.richie@gmail.com", StudentId = 1114 });
            initialStudents.Add(new Student { FirstName = "Mike", LastName = "Long", EmailAddress = "mlong@gmail.com", StudentId = 1115 });
            c1.AddStudents(initialStudents);

            foreach (Student stu in c1)
            {
                rtbCourseStudents.Text += stu.ToString() + "\n";
                cmbStudents.Items.Add(stu.StudentId);
            }
        }

        private void btnAddStudent_Click(object sender, EventArgs e)
        {
            Student s1 = new Student()
            {
                FirstName = txtFirstName.Text,
                LastName = txtLastName.Text,
                StudentId = Convert.ToInt32(txtStudentID.Text),
                EmailAddress = txtEmail.Text
            };

            c1.AddStudent(s1);

            rtbCourseStudents.Text += c1[s1.StudentId];
            cmbStudents.Items.Add(s1.StudentId);

            txtStudentID.Text = "";
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtEmail.Text = "";
        }

        private void btnRemoveStudent_Click(object sender, EventArgs e)
        {
            if (cmbStudents.SelectedItem != null)
            {
                int studentId = Convert.ToInt32(cmbStudents.SelectedItem);
                c1.RemoveStudent(studentId);
            }

            cmbStudents.Items.Clear();
            rtbCourseStudents.Text = "";

            foreach (Student stu in c1)
            {
                rtbCourseStudents.Text += stu.ToString() + "\n";
                cmbStudents.Items.Add(stu.StudentId);
            }
        }

        private void btnSearchStudent_Click(object sender, EventArgs e)
        {
            try
            {
                Student s = c1[Convert.ToInt32(txtIDSearch.Text)];
                lblStudentByID.Text = s.ToString();
            }
            catch(KeyNotFoundException)
            {
                lblStudentByID.Text = "Student Not Found";
            }

            
        }
    }
}
