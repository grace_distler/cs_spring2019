﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB2
{
    public class Course : IEnumerable<Student>
    {
        public string CourseName { get; set; }

        private Dictionary<int, Student> _students = new Dictionary<int, Student>();

        //No arg constructor
        public Course()
        {

        }

        public void AddStudent(Student s)
        {
            _students.Add(s.StudentId, s);
        }

        public void RemoveStudent(int id)
        {
            _students.Remove(id);
        }

        public void AddStudents(IEnumerable<Student> students)
        {
            foreach (Student stu in students)
            {
                _students.Add(stu.StudentId, stu);
            }
        }

        public IEnumerator<Student> GetEnumerator()
        {
            foreach (Student stu in _students.Values)
            {
                yield return stu;
            }
        }

        public IEnumerator<Student> Take(int count)
        {
            int i = 0;
            foreach (Student stu in _students.Values)
            {
                yield return stu;
                ++i;
                if (i >= count) { break; }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Student this[int id]
        {
            get
            {
                //foreach (Student student in _students)
                //{
                //    if (student.StudentId == id)
                //    {
                //        return student;
                //    }
                //}
                //return null;
                return _students[id];
            }
        }
    }
}

