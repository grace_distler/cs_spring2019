﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB1
{
    public class Course : IEnumerable<Student>
    {
        public string CourseName { get; set; }
        
        //instance field for List of students
        private List<Student> _students = new List<Student>();
        
        //No arg constructor
        public Course()
        {

        }

        public void AddStudent(Student s)
        {
            _students.Add(s);
        }

        public void RemoveStudent(int id)
        {
            foreach (Student stu in _students)
            {
                if (stu.StudentId == id)
                {
                    _students.Remove(stu);
                    break;
                }
            }
        }

        public void AddStudents(IEnumerable<Student> students)
        {
            _students.AddRange(students);
        }

        public IEnumerator<Student> GetEnumerator()
        {
            foreach (Student stu in _students)
            {
                yield return stu;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Student this[int id]
        {
            get
            {
                foreach (Student student in _students)
                {
                    if(student.StudentId == id)
                    {
                        return student;
                    }
                }
                return null;
            }
        }
    }
}
