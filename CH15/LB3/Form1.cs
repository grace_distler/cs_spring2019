﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB3
{ 
    public partial class Form1 : Form
    {
        Course[] _courses = new Course[2];
        int courseIndex;

        public Form1()
        {
            _courses[0] = new Course();
            _courses[1] = new Course();

            InitializeComponent();

            List<Student> courseOneStudents = new List<Student>();
            courseOneStudents.Add(new Student { FirstName = "Bob", LastName = "Smith", EmailAddress = "BobSmith@gmail.com", StudentId = 1111 });
            courseOneStudents.Add(new Student { FirstName = "Grace", LastName = "Distler", EmailAddress = "grace.distler@gmail.com", StudentId = 1112 });
            courseOneStudents.Add(new Student { FirstName = "Joe", LastName = "Dart", EmailAddress = "joeDart1@gmail.com", StudentId = 1113 });
            courseOneStudents.Add(new Student { FirstName = "Kate", LastName = "Richie", EmailAddress = "k.richie@gmail.com", StudentId = 1114 });
            courseOneStudents.Add(new Student { FirstName = "Mike", LastName = "Long", EmailAddress = "mlong@gmail.com", StudentId = 1115 });
            _courses[0].AddStudents(courseOneStudents);
            _courses[0].CourseName = "AWD 1000 Web Development Fundamentals";

            List<Student> courseTwoStudents = new List<Student>();
            courseTwoStudents.Add(new Student { FirstName = "Justin", LastName = "Bieber", EmailAddress = "jbiebs@gmail.com", StudentId = 1116 });
            courseTwoStudents.Add(new Student { FirstName = "Ariana", LastName = "Grande", EmailAddress = "ari.grande@gmail.com", StudentId = 1117 });
            courseTwoStudents.Add(new Student { FirstName = "Lady", LastName = "Gaga", EmailAddress = "ladygaga@gmail.com", StudentId = 1118 });
            courseTwoStudents.Add(new Student { FirstName = "Post", LastName = "Malone", EmailAddress = "posty@gmail.com", StudentId = 1119 });
            courseTwoStudents.Add(new Student { FirstName = "Billie", LastName = "Eilish", EmailAddress = "beilish@gmail.com", StudentId = 1120 });
            _courses[1].AddStudents(courseTwoStudents);
            _courses[1].CourseName = "AWD 1100 C# Programming and Fundementals";

            cmbCourses.Items.AddRange(_courses);
        }

        private void btnAddStudent_Click_1(object sender, EventArgs e)
        {
            Student s1 = new Student()
            {
                FirstName = txtFirstName.Text,
                LastName = txtLastName.Text,
                StudentId = Convert.ToInt32(txtStudentID.Text),
                EmailAddress = txtEmail.Text
            };

            _courses[courseIndex].AddStudent(s1);

            rtbCourseStudents.Text += s1.ToString();
            cmbStudents.Items.Add(s1.StudentId);

            txtStudentID.Text = "";
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtEmail.Text = "";
        }

        private void btnRemoveStudent_Click_1(object sender, EventArgs e)
        {
            if (cmbStudents.SelectedItem != null)
            {
                int studentId = Convert.ToInt32(cmbStudents.SelectedItem);
                _courses[courseIndex].RemoveStudent(studentId);
            }

            cmbStudents.Items.Clear();
            rtbCourseStudents.Text = "";

            foreach (Student stu in _courses[courseIndex])
            {
                rtbCourseStudents.Text += stu.ToString() + "\n";
                cmbStudents.Items.Add(stu.StudentId);
            }
        }

        private void cmbCourses_SelectedIndexChanged(object sender, EventArgs e)
        {
            rtbCourseStudents.Text = "";
            cmbStudents.Items.Clear();
            courseIndex = cmbCourses.SelectedIndex;

            foreach (Student stu in _courses[cmbCourses.SelectedIndex])
            {
                rtbCourseStudents.Text += stu.ToString() + "\n";
                cmbStudents.Items.Add(stu.StudentId);
            }
        }
    }
}
