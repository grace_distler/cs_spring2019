﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB4
{
    public class MaximizeAbility : IAbility
    {
        public void Execute(PokemonForm form)
        {
            form.WindowState = FormWindowState.Maximized;
        }

        public string GetName()
        {
            return "Maximize";
        }
    }
}
