﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB4
{
    public interface IAbility
    {
        string GetName();

        void Execute(PokemonForm form);
    }
}
