﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace LB4
{
    public class Pokemon
    {
        private string _name;
        private Image _image;
        private IAbility _ability;

        public Pokemon(string name, Image image, IAbility ability)
        {
            _name = name;
            _image = image;
            _ability= ability;
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }
        public Image Image
        {
            get
            {
                return _image;
            }
        }
        public IAbility Ability
        {
            get
            {
                return _ability;
            }
        }
    }
}
