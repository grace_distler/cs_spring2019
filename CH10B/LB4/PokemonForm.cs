﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB4
{
    public partial class PokemonForm : Form
    {
        Pokemon rando;
        Random r1 = new Random();

        string[] pokeNames = {"Balbasaur",
        "Jigglypuff",
        "Pikachu",
        "Squirtle"};

        Image[] pokeImg =
        {
            Properties.Resources.bulbasaur,
            Properties.Resources.jigglypuff,
            Properties.Resources.pikachu,
            Properties.Resources.squirtle
        };

        IAbility[] abilities = { new MaximizeAbility(), new MinimizeAbility() };

        public PokemonForm()
        {
            InitializeComponent();
            Respawn();
        }

        private void btnAbility_Click(object sender, EventArgs e)
        {
            rando.Ability.Execute(this);
        }
        
        public void Respawn()
        {
            int pokeNum = r1.Next(0, 4);
            int abilityNum = r1.Next(0, 2);

            rando = new Pokemon(pokeNames[pokeNum],pokeImg[pokeNum], abilities[abilityNum]);
            picPokemon.Image = rando.Image;
            lblName.Text = rando.Name;
            btnAbility.Text = rando.Ability.GetName();
        }
        
        private void btnRespwan_Click(object sender, EventArgs e)
        {
            Respawn();
        }
    }
}
