﻿namespace LB4
{
    partial class PokemonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAbility = new System.Windows.Forms.Button();
            this.btnRespawn = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.picPokemon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picPokemon)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAbility
            // 
            this.btnAbility.Location = new System.Drawing.Point(689, 80);
            this.btnAbility.Name = "btnAbility";
            this.btnAbility.Size = new System.Drawing.Size(101, 33);
            this.btnAbility.TabIndex = 0;
            this.btnAbility.Text = "Maximize";
            this.btnAbility.UseVisualStyleBackColor = true;
            this.btnAbility.Click += new System.EventHandler(this.btnAbility_Click);
            // 
            // btnRespawn
            // 
            this.btnRespawn.Location = new System.Drawing.Point(689, 489);
            this.btnRespawn.Name = "btnRespawn";
            this.btnRespawn.Size = new System.Drawing.Size(101, 33);
            this.btnRespawn.TabIndex = 3;
            this.btnRespawn.Text = "Respawn";
            this.btnRespawn.UseVisualStyleBackColor = true;
            this.btnRespawn.Click += new System.EventHandler(this.btnRespwan_Click);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(307, 497);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(0, 17);
            this.lblName.TabIndex = 4;
            // 
            // picPokemon
            // 
            this.picPokemon.Location = new System.Drawing.Point(73, 46);
            this.picPokemon.Name = "picPokemon";
            this.picPokemon.Size = new System.Drawing.Size(558, 448);
            this.picPokemon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPokemon.TabIndex = 5;
            this.picPokemon.TabStop = false;
            // 
            // PokemonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 605);
            this.Controls.Add(this.picPokemon);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.btnRespawn);
            this.Controls.Add(this.btnAbility);
            this.Name = "PokemonForm";
            this.Text = "Polymorph";
            ((System.ComponentModel.ISupportInitialize)(this.picPokemon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAbility;
        private System.Windows.Forms.Button btnRespawn;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.PictureBox picPokemon;
    }
}

