﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB3
{
    public class ShinyCard : ICard
    {
        private string _name;
        private Image _image;
        private double _value;
        private Font _font;
        private Color _backColor;
        private Color _foreColor;

        public ShinyCard(string name, Image image, double value, Font font, Color backColor, Color foreColor)
        {
            _name = name;
            _image = image;
            _value = value;
            _font = font;
            _backColor = backColor;
            _foreColor = foreColor;
        }


        public void ShowCard(PictureBox picture, Label label)
        {
            picture.Image = _image;
            picture.BackColor = _backColor;
            label.Text = _name;
            label.Font = _font;
            label.ForeColor = _foreColor;
        }

        public int CompareTo(Object obj)
        {
           ICard sc = (ICard)obj;

            int returnValue;

            if (this.Value > sc.Value)
            {
                returnValue = -1;
            }
            else if (this.Value < sc.Value)
            {
                returnValue = 1;
            }
            else
            {
                returnValue = 0;
            }
            return returnValue;
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public Image Image
        {
            get
            {
                return _image;
            }
        }

        public double Value
        {
            get
            {
                return _value;
            }
        }

        
    }
}
