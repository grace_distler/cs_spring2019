﻿namespace LB3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picCard1 = new System.Windows.Forms.PictureBox();
            this.btnPackOpen = new System.Windows.Forms.Button();
            this.picCard3 = new System.Windows.Forms.PictureBox();
            this.picCard2 = new System.Windows.Forms.PictureBox();
            this.picCard4 = new System.Windows.Forms.PictureBox();
            this.lblPokemon1 = new System.Windows.Forms.Label();
            this.lblPokemon2 = new System.Windows.Forms.Label();
            this.lblPokemon3 = new System.Windows.Forms.Label();
            this.lblPokemon4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCard3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCard2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCard4)).BeginInit();
            this.SuspendLayout();
            // 
            // picCard1
            // 
            this.picCard1.Location = new System.Drawing.Point(24, 39);
            this.picCard1.Name = "picCard1";
            this.picCard1.Size = new System.Drawing.Size(192, 190);
            this.picCard1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCard1.TabIndex = 0;
            this.picCard1.TabStop = false;
            // 
            // btnPackOpen
            // 
            this.btnPackOpen.Location = new System.Drawing.Point(345, 272);
            this.btnPackOpen.Name = "btnPackOpen";
            this.btnPackOpen.Size = new System.Drawing.Size(192, 35);
            this.btnPackOpen.TabIndex = 1;
            this.btnPackOpen.Text = "Open Another Pack";
            this.btnPackOpen.UseVisualStyleBackColor = true;
            this.btnPackOpen.Click += new System.EventHandler(this.btnPackOpen_Click);
            // 
            // picCard3
            // 
            this.picCard3.Location = new System.Drawing.Point(458, 39);
            this.picCard3.Name = "picCard3";
            this.picCard3.Size = new System.Drawing.Size(192, 190);
            this.picCard3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCard3.TabIndex = 2;
            this.picCard3.TabStop = false;
            // 
            // picCard2
            // 
            this.picCard2.Location = new System.Drawing.Point(240, 39);
            this.picCard2.Name = "picCard2";
            this.picCard2.Size = new System.Drawing.Size(192, 190);
            this.picCard2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCard2.TabIndex = 3;
            this.picCard2.TabStop = false;
            // 
            // picCard4
            // 
            this.picCard4.Location = new System.Drawing.Point(676, 39);
            this.picCard4.Name = "picCard4";
            this.picCard4.Size = new System.Drawing.Size(192, 190);
            this.picCard4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCard4.TabIndex = 4;
            this.picCard4.TabStop = false;
            // 
            // lblPokemon1
            // 
            this.lblPokemon1.AutoSize = true;
            this.lblPokemon1.Location = new System.Drawing.Point(60, 232);
            this.lblPokemon1.Name = "lblPokemon1";
            this.lblPokemon1.Size = new System.Drawing.Size(0, 17);
            this.lblPokemon1.TabIndex = 5;
            // 
            // lblPokemon2
            // 
            this.lblPokemon2.AutoSize = true;
            this.lblPokemon2.Location = new System.Drawing.Point(280, 232);
            this.lblPokemon2.Name = "lblPokemon2";
            this.lblPokemon2.Size = new System.Drawing.Size(0, 17);
            this.lblPokemon2.TabIndex = 6;
            // 
            // lblPokemon3
            // 
            this.lblPokemon3.AutoSize = true;
            this.lblPokemon3.Location = new System.Drawing.Point(497, 232);
            this.lblPokemon3.Name = "lblPokemon3";
            this.lblPokemon3.Size = new System.Drawing.Size(0, 17);
            this.lblPokemon3.TabIndex = 7;
            // 
            // lblPokemon4
            // 
            this.lblPokemon4.AutoSize = true;
            this.lblPokemon4.Location = new System.Drawing.Point(717, 232);
            this.lblPokemon4.Name = "lblPokemon4";
            this.lblPokemon4.Size = new System.Drawing.Size(0, 17);
            this.lblPokemon4.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 336);
            this.Controls.Add(this.lblPokemon4);
            this.Controls.Add(this.lblPokemon3);
            this.Controls.Add(this.lblPokemon2);
            this.Controls.Add(this.lblPokemon1);
            this.Controls.Add(this.picCard4);
            this.Controls.Add(this.picCard2);
            this.Controls.Add(this.picCard3);
            this.Controls.Add(this.btnPackOpen);
            this.Controls.Add(this.picCard1);
            this.Name = "Form1";
            this.Text = "Booster Pack";
            ((System.ComponentModel.ISupportInitialize)(this.picCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCard3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCard2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCard4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picCard1;
        private System.Windows.Forms.Button btnPackOpen;
        private System.Windows.Forms.PictureBox picCard3;
        private System.Windows.Forms.PictureBox picCard2;
        private System.Windows.Forms.PictureBox picCard4;
        private System.Windows.Forms.Label lblPokemon1;
        private System.Windows.Forms.Label lblPokemon2;
        private System.Windows.Forms.Label lblPokemon3;
        private System.Windows.Forms.Label lblPokemon4;
    }
}

