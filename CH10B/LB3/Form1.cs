﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnPackOpen_Click(object sender, EventArgs e)
        {
            ICard[] tenPack = new ICard[4];

            FontFamily fontfamily = new FontFamily("Comic Sans MS");

            Font font = new Font(
                fontfamily,
                18,
                FontStyle.Bold,
                GraphicsUnit.Pixel);

            Random r1 = new Random();

            for (int i = 0; i < tenPack.Length; i++)
            {
                int pokeNum = r1.Next(0, 10);
                int lucky = r1.Next(0, 5);

                if (lucky == 4)
                {
                    switch (pokeNum)
                    {
                        case 0:
                            tenPack[i] = new ShinyCard("Bulbasaur", Properties.Resources.bulbasaur, 17, font, Color.Gold,Color.LightSteelBlue);
                            break;
                        case 1:
                            tenPack[i] = new ShinyCard("Caterpie", Properties.Resources.caterpie, 11, font, Color.Gold, Color.YellowGreen);
                            break;
                        case 2:
                            tenPack[i] = new ShinyCard("Charmander", Properties.Resources.charmander, 16, font, Color.Gold, Color.Orange);
                            break;
                        case 3:
                            tenPack[i] = new ShinyCard("Dragonair", Properties.Resources.dragonair_shiny_, 17, font, Color.Gold, Color.Pink);
                            break;
                        case 4:
                            tenPack[i] = new ShinyCard("Dragonair", Properties.Resources.dragonair, 14, font, Color.Gold, Color.CadetBlue);
                            break;
                        case 5:
                            tenPack[i] = new ShinyCard("Eevee", Properties.Resources.eevee_shiny_, 19, font, Color.Gold, Color.CornflowerBlue);
                            break;
                        case 6:
                            tenPack[i] = new ShinyCard("Eevee", Properties.Resources.eevee, 18, font, Color.Gold, Color.RosyBrown);
                            break;
                        case 7:
                            tenPack[i] = new ShinyCard("Jigglypuff", Properties.Resources.jigglypuff, 12, font, Color.Gold, Color.LightPink);
                            break;
                        case 8:
                            tenPack[i] = new ShinyCard("Pikachu", Properties.Resources.pikachu, 20, font, Color.Gold, Color.DarkGoldenrod);
                            break;
                        case 9:
                            tenPack[i] = new ShinyCard("Squirtle", Properties.Resources.squirtle, 15, font, Color.Gold, Color.LightBlue);
                            break;
                    }
                }
                else
                {
                    switch (pokeNum)
                    {
                        case 0:
                            tenPack[i] = new NormalCard("Bulbasaur", Properties.Resources.bulbasaur, 7);
                            break;
                        case 1:
                            tenPack[i] = new NormalCard("Caterpie", Properties.Resources.caterpie, 1);
                            break;
                        case 2:
                            tenPack[i] = new NormalCard("Charmander", Properties.Resources.charmander, 6);
                            break;
                        case 3:
                            tenPack[i] = new NormalCard("Dragonair", Properties.Resources.dragonair_shiny_, 7);
                            break;
                        case 4:
                            tenPack[i] = new NormalCard("Dragonair", Properties.Resources.dragonair, 4);
                            break;
                        case 5:
                            tenPack[i] = new NormalCard("Eevee", Properties.Resources.eevee_shiny_, 9);
                            break;
                        case 6:
                            tenPack[i] = new NormalCard("Eevee", Properties.Resources.eevee, 8);
                            break;
                        case 7:
                            tenPack[i] = new NormalCard("Jigglypuff", Properties.Resources.jigglypuff, 2);
                            break;
                        case 8:
                            tenPack[i] = new NormalCard("Pikachu", Properties.Resources.pikachu, 10);
                            break;
                        case 9:
                            tenPack[i] = new NormalCard("Squirtle", Properties.Resources.squirtle, 5);
                            break;
                    }
                }
            }
            Array.Sort(tenPack);

            tenPack[0].ShowCard(picCard1, lblPokemon1);
            tenPack[1].ShowCard(picCard2, lblPokemon2);
            tenPack[2].ShowCard(picCard3, lblPokemon3);
            tenPack[3].ShowCard(picCard4, lblPokemon4);
        }
    }
}
