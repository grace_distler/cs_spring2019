﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB3
{
    public class NormalCard : ICard
    {
        private string _name;
        private Image _image;
        private double _value;

        public NormalCard(string name, Image image, double value)
        {
            _name = name;
            _image = image;
            _value = value;
        }

        public void ShowCard(PictureBox picture, Label label)
        {
            picture.Image = _image;
            label.Text = _name;

            picture.BackColor = Control.DefaultBackColor;

            FontFamily fontfamily = new FontFamily("Microsoft Sans Serif");

            Font font = new Font(
                fontfamily,
                16,
                FontStyle.Regular,
                GraphicsUnit.Pixel);

            label.Font = font;
            label.ForeColor = Color.Black;
        }

        public int CompareTo(Object obj)
        {
            ICard nc = (ICard)obj;

            int returnValue;

            if(this.Value > nc.Value)
            {
                returnValue = -1;
            }
            else if (this.Value < nc.Value)
            {
                returnValue = 1;
            }
            else
            {
                returnValue = 0;
            }
            return returnValue;
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }
        public Image Image
        {
            get
            {
                return _image;
            }
        }
        public double Value
        {
            get
            {
                return _value;
            }
        }
    }
}
