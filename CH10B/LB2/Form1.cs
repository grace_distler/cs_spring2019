﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            if (unframedRad.Checked)
            {
                Photo p1 = new Photo(Convert.ToSingle(widthTextBox.Text), Convert.ToSingle(heightTextBox.Text));
                priceLabel.Text = "Cost: " + p1.Price.ToString("C");
                outputLabel.Text = p1.ToString();
            }
            else if (mattedRad.Checked)
            {
                Color color;

                if (blackRad.Checked)
                {
                    color = Color.BLACK;
                }
                else if (redRad.Checked)
                {
                    color = Color.RED;
                }
                else if (greenRad.Checked)
                {
                    color = Color.GREEN;
                }
                else if (blueRad.Checked)
                {
                    color = Color.BLUE;
                }
                else if(whiteRad.Checked)
                {
                    color = Color.WHITE;
                }
                else
                {
                    outputLabel.Text = "Please select a color for your matted photo";
                    return;
                }

                MattedPhoto mp = new MattedPhoto(Convert.ToSingle(widthTextBox.Text), Convert.ToSingle(heightTextBox.Text), color);
                priceLabel.Text = "Cost: " + mp.Price.ToString("C");
                outputLabel.Text = mp.ToString();
            }
            else if (framedRad.Checked)
            {
                Material material;
                Style style;

                if (pineRad.Checked)
                {
                    material = Material.PINE;
                }
                else if (oakRad.Checked)
                {
                    material = Material.OAK;
                }
                else if (steelRad.Checked)
                {
                    material = Material.STEEL;
                }
                else if (silverRad.Checked)
                {
                    material = Material.SILVER;
                }
                else if (goldRad.Checked)
                {
                    material = Material.GOLD;
                }
                else
                {
                    outputLabel.Text = "Please select a material for your framed photo";
                    return;
                }

                if (simpleRad.Checked)
                {
                    style = Style.SIMPLE;
                }
                else if (modernRad.Checked)
                {
                    style = Style.MODERN;
                }
                else if (antiqueRad.Checked)
                {
                    style = Style.ANTIQUE;
                }
                else if (vintageRad.Checked)
                {
                    style = Style.VINTAGE;
                }
                else if (eclecticRad.Checked)
                {
                    style = Style.ECLECTIC;
                }
                else
                {
                    outputLabel.Text = "Please select a style for your framed photo";
                    return;
                }

                FramedPhoto fp = new FramedPhoto(Convert.ToSingle(widthTextBox.Text), Convert.ToSingle(heightTextBox.Text), material, style);
                priceLabel.Text = "Cost: " + fp.Price.ToString("C");
                outputLabel.Text = fp.ToString();
            }
        }

        private void unframedRad_Click(object sender, EventArgs e)
        {
            grpColor.Enabled = false;
            grpMaterial.Enabled = false;
            grpStyle.Enabled = false;
        }

        private void mattedRad_Click(object sender, EventArgs e)
        {
            grpColor.Enabled = true;
            grpMaterial.Enabled = false;
            grpStyle.Enabled = false;
        }

        private void framedRad_Click(object sender, EventArgs e)
        {
            grpColor.Enabled = false;
            grpMaterial.Enabled = true;
            grpStyle.Enabled = true;
        }
    }
}
