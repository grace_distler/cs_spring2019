﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB2
{
    public class MattedPhoto : Photo
    {
        protected Color _color;

        public MattedPhoto(float width, float height, Color color) : base(width, height)
        {
            this._color = color;
        }

        public Color Color
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
            }
        }

        public override float Price
        {
            get
            {
                return base.Price + 10;
            }
        }

        public override string ToString()
        {
            return _width + " x" + _height + " MattedPhoto (" + _color + ")";
        }
    }
}
