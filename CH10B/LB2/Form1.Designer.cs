﻿namespace LB2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.widthTextBox = new System.Windows.Forms.TextBox();
            this.heightTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.framedRad = new System.Windows.Forms.RadioButton();
            this.mattedRad = new System.Windows.Forms.RadioButton();
            this.unframedRad = new System.Windows.Forms.RadioButton();
            this.grpColor = new System.Windows.Forms.GroupBox();
            this.whiteRad = new System.Windows.Forms.RadioButton();
            this.blueRad = new System.Windows.Forms.RadioButton();
            this.greenRad = new System.Windows.Forms.RadioButton();
            this.redRad = new System.Windows.Forms.RadioButton();
            this.blackRad = new System.Windows.Forms.RadioButton();
            this.grpMaterial = new System.Windows.Forms.GroupBox();
            this.goldRad = new System.Windows.Forms.RadioButton();
            this.silverRad = new System.Windows.Forms.RadioButton();
            this.steelRad = new System.Windows.Forms.RadioButton();
            this.oakRad = new System.Windows.Forms.RadioButton();
            this.pineRad = new System.Windows.Forms.RadioButton();
            this.grpStyle = new System.Windows.Forms.GroupBox();
            this.eclecticRad = new System.Windows.Forms.RadioButton();
            this.vintageRad = new System.Windows.Forms.RadioButton();
            this.antiqueRad = new System.Windows.Forms.RadioButton();
            this.modernRad = new System.Windows.Forms.RadioButton();
            this.simpleRad = new System.Windows.Forms.RadioButton();
            this.calculateButton = new System.Windows.Forms.Button();
            this.priceLabel = new System.Windows.Forms.Label();
            this.outputLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.grpColor.SuspendLayout();
            this.grpMaterial.SuspendLayout();
            this.grpStyle.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Width (in)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Height (in)";
            // 
            // widthTextBox
            // 
            this.widthTextBox.Location = new System.Drawing.Point(135, 26);
            this.widthTextBox.Name = "widthTextBox";
            this.widthTextBox.Size = new System.Drawing.Size(337, 22);
            this.widthTextBox.TabIndex = 1;
            // 
            // heightTextBox
            // 
            this.heightTextBox.Location = new System.Drawing.Point(135, 85);
            this.heightTextBox.Name = "heightTextBox";
            this.heightTextBox.Size = new System.Drawing.Size(337, 22);
            this.heightTextBox.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.framedRad);
            this.groupBox1.Controls.Add(this.mattedRad);
            this.groupBox1.Controls.Add(this.unframedRad);
            this.groupBox1.Location = new System.Drawing.Point(31, 134);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(441, 123);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Frame";
            // 
            // framedRad
            // 
            this.framedRad.AutoSize = true;
            this.framedRad.Location = new System.Drawing.Point(26, 82);
            this.framedRad.Name = "framedRad";
            this.framedRad.Size = new System.Drawing.Size(77, 21);
            this.framedRad.TabIndex = 5;
            this.framedRad.TabStop = true;
            this.framedRad.Text = "Framed";
            this.framedRad.UseVisualStyleBackColor = true;
            this.framedRad.Click += new System.EventHandler(this.framedRad_Click);
            // 
            // mattedRad
            // 
            this.mattedRad.AutoSize = true;
            this.mattedRad.Location = new System.Drawing.Point(26, 55);
            this.mattedRad.Name = "mattedRad";
            this.mattedRad.Size = new System.Drawing.Size(72, 21);
            this.mattedRad.TabIndex = 1;
            this.mattedRad.TabStop = true;
            this.mattedRad.Text = "Matted";
            this.mattedRad.UseVisualStyleBackColor = true;
            this.mattedRad.Click += new System.EventHandler(this.mattedRad_Click);
            // 
            // unframedRad
            // 
            this.unframedRad.AutoSize = true;
            this.unframedRad.Location = new System.Drawing.Point(26, 29);
            this.unframedRad.Name = "unframedRad";
            this.unframedRad.Size = new System.Drawing.Size(91, 21);
            this.unframedRad.TabIndex = 0;
            this.unframedRad.TabStop = true;
            this.unframedRad.Text = "Unframed";
            this.unframedRad.UseVisualStyleBackColor = true;
            this.unframedRad.Click += new System.EventHandler(this.unframedRad_Click);
            // 
            // grpColor
            // 
            this.grpColor.Controls.Add(this.whiteRad);
            this.grpColor.Controls.Add(this.blueRad);
            this.grpColor.Controls.Add(this.greenRad);
            this.grpColor.Controls.Add(this.redRad);
            this.grpColor.Controls.Add(this.blackRad);
            this.grpColor.Location = new System.Drawing.Point(32, 295);
            this.grpColor.Name = "grpColor";
            this.grpColor.Size = new System.Drawing.Size(131, 242);
            this.grpColor.TabIndex = 4;
            this.grpColor.TabStop = false;
            this.grpColor.Text = "Color";
            // 
            // whiteRad
            // 
            this.whiteRad.AutoSize = true;
            this.whiteRad.Location = new System.Drawing.Point(27, 193);
            this.whiteRad.Name = "whiteRad";
            this.whiteRad.Size = new System.Drawing.Size(65, 21);
            this.whiteRad.TabIndex = 4;
            this.whiteRad.TabStop = true;
            this.whiteRad.Text = "White";
            this.whiteRad.UseVisualStyleBackColor = true;
            // 
            // blueRad
            // 
            this.blueRad.AutoSize = true;
            this.blueRad.Location = new System.Drawing.Point(27, 154);
            this.blueRad.Name = "blueRad";
            this.blueRad.Size = new System.Drawing.Size(57, 21);
            this.blueRad.TabIndex = 3;
            this.blueRad.TabStop = true;
            this.blueRad.Text = "Blue";
            this.blueRad.UseVisualStyleBackColor = true;
            // 
            // greenRad
            // 
            this.greenRad.AutoSize = true;
            this.greenRad.Location = new System.Drawing.Point(27, 116);
            this.greenRad.Name = "greenRad";
            this.greenRad.Size = new System.Drawing.Size(69, 21);
            this.greenRad.TabIndex = 2;
            this.greenRad.TabStop = true;
            this.greenRad.Text = "Green";
            this.greenRad.UseVisualStyleBackColor = true;
            // 
            // redRad
            // 
            this.redRad.AutoSize = true;
            this.redRad.Location = new System.Drawing.Point(27, 77);
            this.redRad.Name = "redRad";
            this.redRad.Size = new System.Drawing.Size(55, 21);
            this.redRad.TabIndex = 1;
            this.redRad.TabStop = true;
            this.redRad.Text = "Red";
            this.redRad.UseVisualStyleBackColor = true;
            // 
            // blackRad
            // 
            this.blackRad.AutoSize = true;
            this.blackRad.Location = new System.Drawing.Point(27, 38);
            this.blackRad.Name = "blackRad";
            this.blackRad.Size = new System.Drawing.Size(63, 21);
            this.blackRad.TabIndex = 0;
            this.blackRad.TabStop = true;
            this.blackRad.Text = "Black";
            this.blackRad.UseVisualStyleBackColor = true;
            // 
            // grpMaterial
            // 
            this.grpMaterial.Controls.Add(this.goldRad);
            this.grpMaterial.Controls.Add(this.silverRad);
            this.grpMaterial.Controls.Add(this.steelRad);
            this.grpMaterial.Controls.Add(this.oakRad);
            this.grpMaterial.Controls.Add(this.pineRad);
            this.grpMaterial.Location = new System.Drawing.Point(187, 295);
            this.grpMaterial.Name = "grpMaterial";
            this.grpMaterial.Size = new System.Drawing.Size(131, 242);
            this.grpMaterial.TabIndex = 5;
            this.grpMaterial.TabStop = false;
            this.grpMaterial.Text = "Material";
            // 
            // goldRad
            // 
            this.goldRad.AutoSize = true;
            this.goldRad.Location = new System.Drawing.Point(27, 193);
            this.goldRad.Name = "goldRad";
            this.goldRad.Size = new System.Drawing.Size(59, 21);
            this.goldRad.TabIndex = 4;
            this.goldRad.TabStop = true;
            this.goldRad.Text = "Gold";
            this.goldRad.UseVisualStyleBackColor = true;
            // 
            // silverRad
            // 
            this.silverRad.AutoSize = true;
            this.silverRad.Location = new System.Drawing.Point(27, 154);
            this.silverRad.Name = "silverRad";
            this.silverRad.Size = new System.Drawing.Size(64, 21);
            this.silverRad.TabIndex = 3;
            this.silverRad.TabStop = true;
            this.silverRad.Text = "Silver";
            this.silverRad.UseVisualStyleBackColor = true;
            // 
            // steelRad
            // 
            this.steelRad.AutoSize = true;
            this.steelRad.Location = new System.Drawing.Point(27, 116);
            this.steelRad.Name = "steelRad";
            this.steelRad.Size = new System.Drawing.Size(61, 21);
            this.steelRad.TabIndex = 2;
            this.steelRad.TabStop = true;
            this.steelRad.Text = "Steel";
            this.steelRad.UseVisualStyleBackColor = true;
            // 
            // oakRad
            // 
            this.oakRad.AutoSize = true;
            this.oakRad.Location = new System.Drawing.Point(27, 77);
            this.oakRad.Name = "oakRad";
            this.oakRad.Size = new System.Drawing.Size(55, 21);
            this.oakRad.TabIndex = 1;
            this.oakRad.TabStop = true;
            this.oakRad.Text = "Oak";
            this.oakRad.UseVisualStyleBackColor = true;
            // 
            // pineRad
            // 
            this.pineRad.AutoSize = true;
            this.pineRad.Location = new System.Drawing.Point(27, 38);
            this.pineRad.Name = "pineRad";
            this.pineRad.Size = new System.Drawing.Size(57, 21);
            this.pineRad.TabIndex = 0;
            this.pineRad.TabStop = true;
            this.pineRad.Text = "Pine";
            this.pineRad.UseVisualStyleBackColor = true;
            // 
            // grpStyle
            // 
            this.grpStyle.Controls.Add(this.eclecticRad);
            this.grpStyle.Controls.Add(this.vintageRad);
            this.grpStyle.Controls.Add(this.antiqueRad);
            this.grpStyle.Controls.Add(this.modernRad);
            this.grpStyle.Controls.Add(this.simpleRad);
            this.grpStyle.Location = new System.Drawing.Point(341, 295);
            this.grpStyle.Name = "grpStyle";
            this.grpStyle.Size = new System.Drawing.Size(131, 242);
            this.grpStyle.TabIndex = 6;
            this.grpStyle.TabStop = false;
            this.grpStyle.Text = "Style";
            // 
            // eclecticRad
            // 
            this.eclecticRad.AutoSize = true;
            this.eclecticRad.Location = new System.Drawing.Point(27, 193);
            this.eclecticRad.Name = "eclecticRad";
            this.eclecticRad.Size = new System.Drawing.Size(77, 21);
            this.eclecticRad.TabIndex = 4;
            this.eclecticRad.TabStop = true;
            this.eclecticRad.Text = "Eclectic";
            this.eclecticRad.UseVisualStyleBackColor = true;
            // 
            // vintageRad
            // 
            this.vintageRad.AutoSize = true;
            this.vintageRad.Location = new System.Drawing.Point(27, 154);
            this.vintageRad.Name = "vintageRad";
            this.vintageRad.Size = new System.Drawing.Size(77, 21);
            this.vintageRad.TabIndex = 3;
            this.vintageRad.TabStop = true;
            this.vintageRad.Text = "Vintage";
            this.vintageRad.UseVisualStyleBackColor = true;
            // 
            // antiqueRad
            // 
            this.antiqueRad.AutoSize = true;
            this.antiqueRad.Location = new System.Drawing.Point(27, 116);
            this.antiqueRad.Name = "antiqueRad";
            this.antiqueRad.Size = new System.Drawing.Size(77, 21);
            this.antiqueRad.TabIndex = 2;
            this.antiqueRad.TabStop = true;
            this.antiqueRad.Text = "Antique";
            this.antiqueRad.UseVisualStyleBackColor = true;
            // 
            // modernRad
            // 
            this.modernRad.AutoSize = true;
            this.modernRad.Location = new System.Drawing.Point(27, 77);
            this.modernRad.Name = "modernRad";
            this.modernRad.Size = new System.Drawing.Size(77, 21);
            this.modernRad.TabIndex = 1;
            this.modernRad.TabStop = true;
            this.modernRad.Text = "Modern";
            this.modernRad.UseVisualStyleBackColor = true;
            // 
            // simpleRad
            // 
            this.simpleRad.AutoSize = true;
            this.simpleRad.Location = new System.Drawing.Point(27, 38);
            this.simpleRad.Name = "simpleRad";
            this.simpleRad.Size = new System.Drawing.Size(71, 21);
            this.simpleRad.TabIndex = 0;
            this.simpleRad.TabStop = true;
            this.simpleRad.Text = "Simple";
            this.simpleRad.UseVisualStyleBackColor = true;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(41, 583);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(107, 36);
            this.calculateButton.TabIndex = 7;
            this.calculateButton.Text = "Calculate";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priceLabel.Location = new System.Drawing.Point(336, 583);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(0, 29);
            this.priceLabel.TabIndex = 9;
            // 
            // outputLabel
            // 
            this.outputLabel.AutoSize = true;
            this.outputLabel.Location = new System.Drawing.Point(38, 642);
            this.outputLabel.Name = "outputLabel";
            this.outputLabel.Size = new System.Drawing.Size(0, 17);
            this.outputLabel.TabIndex = 10;
            // 
            // Form1
            // 
            this.AcceptButton = this.calculateButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 680);
            this.Controls.Add(this.outputLabel);
            this.Controls.Add(this.priceLabel);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.grpStyle);
            this.Controls.Add(this.grpMaterial);
            this.Controls.Add(this.grpColor);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.heightTextBox);
            this.Controls.Add(this.widthTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Photoshop";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpColor.ResumeLayout(false);
            this.grpColor.PerformLayout();
            this.grpMaterial.ResumeLayout(false);
            this.grpMaterial.PerformLayout();
            this.grpStyle.ResumeLayout(false);
            this.grpStyle.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox widthTextBox;
        private System.Windows.Forms.TextBox heightTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton framedRad;
        private System.Windows.Forms.RadioButton mattedRad;
        private System.Windows.Forms.RadioButton unframedRad;
        private System.Windows.Forms.GroupBox grpColor;
        private System.Windows.Forms.RadioButton blackRad;
        private System.Windows.Forms.RadioButton whiteRad;
        private System.Windows.Forms.RadioButton blueRad;
        private System.Windows.Forms.RadioButton greenRad;
        private System.Windows.Forms.RadioButton redRad;
        private System.Windows.Forms.GroupBox grpMaterial;
        private System.Windows.Forms.RadioButton goldRad;
        private System.Windows.Forms.RadioButton silverRad;
        private System.Windows.Forms.RadioButton steelRad;
        private System.Windows.Forms.RadioButton oakRad;
        private System.Windows.Forms.RadioButton pineRad;
        private System.Windows.Forms.GroupBox grpStyle;
        private System.Windows.Forms.RadioButton eclecticRad;
        private System.Windows.Forms.RadioButton vintageRad;
        private System.Windows.Forms.RadioButton antiqueRad;
        private System.Windows.Forms.RadioButton modernRad;
        private System.Windows.Forms.RadioButton simpleRad;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label priceLabel;
        private System.Windows.Forms.Label outputLabel;
    }
}

