﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB1
{
    class CertifiedLetter : Letter
    {
        protected string _trackingNumber;

        public CertifiedLetter(string recipient, DateTime sentDate, string trackingNumber) : base(recipient, sentDate)
        {
            this._trackingNumber = trackingNumber;
        }

        public string TrackingNumber
        {
            get
            {
                return _trackingNumber;
            }
        }

        public override double Price
        {
            get
            {
                return .65;
            }
        }

        public override string ToString()
        {
            return base.ToString() + ", " + _trackingNumber;
        }
    }
}
