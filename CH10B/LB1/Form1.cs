﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB1
{
    public partial class Form1 : Form
    {
        Letter[] theLetters;
        int letterIndex = 0;

        public Form1()
        {
            InitializeComponent();
            theLetters = new Letter[20];
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            string trackingNum = trackingNumberTextBox.Text;

            if (trackingNum.Equals(""))
            {
                theLetters[letterIndex] = new Letter(recipientTextBox.Text, sentDateDTP.Value);
            }
            else
            {
                theLetters[letterIndex] = new CertifiedLetter(recipientTextBox.Text, sentDateDTP.Value, trackingNumberTextBox.Text);
            }
            DisplayOutput(letterIndex);
            letterIndex++;
        }

        private void DisplayOutput(int index)
        {
            string currentOutput = outputRTB.Text;
            outputRTB.Text = theLetters[index].ToString() + "\n" + currentOutput;
        }
    }
}
