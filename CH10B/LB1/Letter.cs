﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB1
{
    class Letter
    {
        protected string _recipient;
        protected DateTime _sentDate;

        public Letter(string recipient, DateTime sentDate)
        {
            this._recipient = recipient;
            this._sentDate = sentDate;
        }

        public string Recipient
        {
            get
            {
                return _recipient;
            }
        }

        public DateTime SentDate
        {
            get
            {
                return _sentDate;
            }
        }

        public virtual double Price
        {
            get
            {
                return .5;
            }
        }

        public override string ToString()
        {
            return _sentDate.ToString("dMMMyyyy").ToUpper() + ", " + _recipient + ", " + Price.ToString("C");
        }
    }
}
