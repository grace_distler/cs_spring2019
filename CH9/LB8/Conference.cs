﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB8
{
    class Conference : IComparable
    {
        private string groupName;
        private string startingDate;
        private int numberOfAttendees;

        public Conference()
        {
            this.groupName = GroupName;
            this.startingDate = StartingDate;
            this.numberOfAttendees = NumberOfAttendees;
        }

        public string GroupName
        {
            get
            {
                return groupName;
            }
            set
            {
                groupName = value;
            }
        }
        public string StartingDate
        {
            get
            {
                return startingDate;
            }
            set
            {
                startingDate = value;
            }
        }
        public int NumberOfAttendees
        {
            get
            {
                return numberOfAttendees;
            }
            set
            {
                numberOfAttendees = value;
            }
        }

        int IComparable.CompareTo(Object o)
        {
            int returnValue;
            Conference temp = (Conference)o;

            if (this.NumberOfAttendees > temp.NumberOfAttendees)
            {
                returnValue = 1;
            }
            else if (this.NumberOfAttendees < temp.NumberOfAttendees)
            {
                returnValue = -1;
            }
            else
            {
                returnValue = 0;
            }
            return returnValue;
        }

    }
}
