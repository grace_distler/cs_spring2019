﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB8
{
    class ConferencesDemo
    {
        static void Main(string[] args)
        {
            Conference[] conferences = new Conference[5];
            int x;

            for(x = 0; x < conferences.Length; ++x)
            {
                Console.Write("Enter Group Name {0}: ", (x + 1));
                string conferenceName = Console.ReadLine();

                Console.Write("Enter the start date of the conference: ");
                string startDate = Console.ReadLine();

                Console.Write("Enter the number of attendees: ", (x + 1));
                int attendance = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();

                conferences[x] = new Conference();
                conferences[x].GroupName = conferenceName;
                conferences[x].StartingDate = startDate;
                conferences[x].NumberOfAttendees = attendance;
            }
            

            Array.Sort(conferences);
            Console.WriteLine();

            Console.WriteLine("Sorted Conferences: ");
            for(x = 0; x < conferences.Length; x++)
            {
                Console.WriteLine("Group Name: {0}, Start Date: {1}, Number of Attendees: {2}", 
                    conferences[x].GroupName, conferences[x].StartingDate, conferences[x].NumberOfAttendees);
            }
            Console.WriteLine();
            Console.ReadLine();
        }
    }
}
