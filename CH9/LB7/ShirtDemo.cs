﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB7
{
    class ShirtDemo
    {
        static void Main(string[] args)
        {
            Shirt[] shirtsArray = new Shirt[5];

            shirtsArray[0] = new Shirt("Cotton", "Black", "M");
            shirtsArray[1] = new Shirt("Cotton", "White", "L");
            shirtsArray[2] = new Shirt("Silk", "Pink", "L");
            shirtsArray[3] = new Shirt("Nylon", "Navy", "S");
            shirtsArray[4] = new Shirt("Silk", "Carolina Blue", "XL");

            DisplayShirts(shirtsArray);
            DisplayShirts(shirtsArray[0], shirtsArray[1]);
        }

        public static void DisplayShirts(params Shirt[] array)
        {
            foreach(Shirt x in array)
            {
                Console.WriteLine("Material: " + x.Material);
                Console.WriteLine("Color: " + x.Color);
                Console.WriteLine("Size: " + x.Size);
                Console.WriteLine();
            }
        }
    }
}
