﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB7
{
    class Shirt
    {
        public Shirt(string material, string color, string size)
        {
            this.Material = material;
            this.Color = color;
            this.Size = size;
        }

        public string Material { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
    }
}
