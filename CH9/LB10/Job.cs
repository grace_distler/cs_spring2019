﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB10
{
    class Job
    {
        private string description;
        private double hoursWorked;
        private double rate;
        private double totalFee;
        private double weightedTotal;

        public Job()
        {

        }

        public Job(string description, double hoursWorked, double rate)
        {
            this.description = description;
            this.hoursWorked = hoursWorked;
            this.rate = rate;
            totalFee = CalculateTotalFee();
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public double HoursWorked
        {
            get
            {
                return hoursWorked;
            }
            set
            {
                hoursWorked = value;
                totalFee = CalculateTotalFee();
            }
        }

        public double Rate
        {
            get
            {
                return rate;
            }
            set
            {
                rate = value;
                totalFee = CalculateTotalFee();
            }
        }

        public double TotalFee
        {
            get
            {
                this.totalFee = CalculateTotalFee();
                return this.totalFee;
            }
        }

        public double WeightedTotal
        {
            get
            {
                return weightedTotal;
            }
            set
            {
                weightedTotal = value;
            }
        }

        public static Job operator +(Job job1, Job job2)
        {
            Job combinedJob = new Job
            {
                Description = job1.Description + " and " + job2.Description,
                HoursWorked = job1.HoursWorked + job2.HoursWorked,
                Rate = (job1.Rate + job2.Rate) / 2,
                WeightedTotal = (job1.TotalFee + job2.TotalFee) / (job1.HoursWorked + job2.HoursWorked)
            };
            return combinedJob;
        }

        private double CalculateTotalFee()
        {
            return (hoursWorked * rate);
        }

        private double WeightedTotalFee()
        {
            return WeightedTotal / HoursWorked;
        }
    }
}
