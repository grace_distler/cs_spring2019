﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB10
{
    class DemoJobs
    {
        static void Main(string[] args)
        {
            Job job1 = new Job("Window Washing", 3.5, 25.00);
            Console.WriteLine("JOB 1:");
            Console.WriteLine(" Description: {0}", job1.Description);
            Console.WriteLine(" Time in Hours to Complete: {0}", job1.HoursWorked);
            Console.WriteLine(" Per-Hour Rate: {0}", job1.Rate.ToString("C"));
            Console.WriteLine(" Total Fee: {0}", job1.TotalFee.ToString("C"));
            Console.WriteLine();

            Job job2 = new Job("House Painting", 10, 100.00);
            Console.WriteLine("JOB 2:");
            Console.WriteLine(" Description: {0}", job2.Description);
            Console.WriteLine(" Time in Hours to Complete: {0}", job2.HoursWorked);
            Console.WriteLine(" Per-Hour Rate: {0}", job2.Rate.ToString("C"));
            Console.WriteLine(" Total Fee: {0}", job2.TotalFee.ToString("C"));
            Console.WriteLine();

            Job combinedJob = job1 + job2;

            Console.WriteLine();
            Console.WriteLine("Combined Jobs:");
            Console.WriteLine(" Description: {0}", combinedJob.Description);
            Console.WriteLine(" Time in Hours to Complete: {0}", combinedJob.HoursWorked);
            Console.WriteLine(" Per-Hour Rate: {0}", combinedJob.Rate.ToString("C"));
            Console.WriteLine(" Total Fee: {0}", combinedJob.TotalFee.ToString("C"));
            Console.WriteLine();
            Console.WriteLine("Weighted Total: {0}", combinedJob.WeightedTotal.ToString("C"));
            Console.WriteLine();
        }
    }
}
