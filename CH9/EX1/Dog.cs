﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX1
{
    class Dog
    {
        //Instance Fields
        private string name;
        private int age;

        //Creating my own constructor
        public Dog(string name)
        {
            this.name = name; //this(talks about the item up top) instance field == parameter
        }

        public Dog(string name, int age)
        {
            this.name = name;
            this.age = age;
        }

        public Dog()
        {

        }

        //Properties
        public String Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public int Age
        {
            get
            {
                return age;
            }
            set
            {
                if(value < 0 || value > 100)
                {
                    //bad data
                }
                else
                {
                    //good data
                    age = value;
                }
            }
        }

        //Auto-Implemented Property
        public string Breed { get; set; }

        //Instance Methods
        //(Non-Static) has parameters
        public string Bark(int numSeconds)
        {
            //When a variable is declared in a method it's called a local variable
            return "This dog is barking for " + numSeconds + " seconds";

        }
    }
}
