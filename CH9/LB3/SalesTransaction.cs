﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB3
{
    class SalesTransaction
    {
        private string salespersonName;
        private double salesAmount;
        private double commission;
        private readonly double commissionRate;

        public SalesTransaction(string name, double amount, double rate)
        {
            this.salespersonName = name;
            this.salesAmount = amount;
            this.commissionRate = rate;
            commission = salesAmount * (commissionRate / 100);
        }

        public SalesTransaction(string name, double amount) : this(name,amount,0)
        {
            //this.salespersonName = name;
            //this.salesAmount = amount;
            //this.commissionRate = 0;
            //this.commission = salesAmount;
        }

        public SalesTransaction(string name) : this(name,0,0)
        {
            //this.salespersonName = name;
            //this.salesAmount = 0;
            //this.commissionRate = 0;
            //this.commission = 0;
        }

        public string SalespersonName
        {
            get
            {
                return salespersonName;
            }
            set
            {
                salespersonName = value;
            }
        }
        public double SalesAmount
        {
            get
            {
                return salesAmount;
            }
            set
            {
                salesAmount = value;
            }
        }
        
        public static double operator+(SalesTransaction s1, SalesTransaction s2)
        {
            double saleValueSum = s1.SalesAmount + s2.SalesAmount;
            return saleValueSum;
        }
    }
}
