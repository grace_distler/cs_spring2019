﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB3
{
    class SalesTransactionDemo
    {
        static void Main(string[] args)
        {
            SalesTransaction transaction1 = new SalesTransaction("Tom", 10500, 6);
            SalesTransaction transaction2 = new SalesTransaction("Laura", 25000);
            //SalesTransaction t3 = new SalesTransaction("Gudmestad");

            Console.WriteLine();
            Console.WriteLine("The value from {0} is: {1}", transaction1.SalespersonName, (transaction1.SalesAmount).ToString("C"));
            Console.WriteLine("The value from {0} is: {1}", transaction2.SalespersonName, (transaction2.SalesAmount).ToString("C"));
            Console.WriteLine("The total in sales from {0} and {1} is: {2}", transaction1.SalespersonName, transaction2.SalespersonName, (transaction1 + transaction2).ToString("C"));
            Console.WriteLine();


            SalesTransaction transaction3 = new SalesTransaction("Ben");
            SalesTransaction transaction4 = new SalesTransaction("Jessica", 45000, 3);

            Console.WriteLine("The value from {0} is: {1}", transaction3.SalespersonName, (transaction3.SalesAmount).ToString("C"));
            Console.WriteLine("The value from {0} is: {1}", transaction4.SalespersonName, (transaction4.SalesAmount).ToString("C"));
            Console.WriteLine("The total in sales from {0} and {1} is: {2}", transaction3.SalespersonName, transaction4.SalespersonName, (transaction3 + transaction4).ToString("C"));
            Console.WriteLine();
        }
    }
}
