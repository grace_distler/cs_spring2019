﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB5
{
    class Car
    {
        private string model;
        private double milesPerGallon;

        public Car(string model, double mpg)
        {
            this.model = model;
            this.milesPerGallon = mpg;
        }

        public Car(string model)
        {
            this.model = model;
            this.milesPerGallon = 20;
        }

        public string Model
        {
            get
            {
                return model;
            }
            set
            {
                model = value;
            }
        }
        public double MilesPerGallon
        {
            get
            {
                return milesPerGallon;
            }
            set
            {
                milesPerGallon = value;
            }
        }

        public static Car operator++(Car car){
            car.MilesPerGallon += 1;
            return car;
        }
    }
}
