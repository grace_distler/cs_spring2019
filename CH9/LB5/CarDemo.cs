﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB5
{
    class CarDemo
    {
        static void Main(string[] args)
        {
            Car car1 = new Car("Audi", 25);
            Car car2 = new Car("Range Rover");

            Console.WriteLine("The car's model: " + car1.Model + "  and has: " + car1.MilesPerGallon + " miles per gallon");
            Console.WriteLine("The car's model: " + car2.Model + "  and has: " + car2.MilesPerGallon + " miles per gallon");
            Console.WriteLine();

            ++car1;
            ++car2;

            Console.WriteLine("The car's model: " + car1.Model + "  and has: " + car1.MilesPerGallon + " miles per gallon after incrementation");
            Console.WriteLine("The car's model: " + car2.Model + "  and has: " + car2.MilesPerGallon + " miles per gallon after incrementation");
            Console.WriteLine();
        }
    }
}
