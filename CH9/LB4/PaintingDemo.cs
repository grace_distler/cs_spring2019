﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB4
{
    class PaintingDemo
    {
        static void Main(string[] args)
        {
            Room[] roomNumber = new Room[8];

            roomNumber[0] = new Room(12, 10, 9);
            roomNumber[1] = new Room(6, 5, 8);
            roomNumber[2] = new Room(9, 8, 12);
            roomNumber[3] = new Room(12, 5, 8);
            roomNumber[4] = new Room(6, 8, 6);
            roomNumber[5] = new Room(7, 9, 9);
            roomNumber[6] = new Room(10, 5, 12);
            roomNumber[7] = new Room(6, 7, 10);

            for (int i = 0; i < roomNumber.Length; i++)
            {
                Console.WriteLine(" {0} gallons of paint are required for a {1} x {2} room, with {3}-foot ceilings. There are {4} square feet of wall space.",
                    roomNumber[i].Gallons, roomNumber[i].Length, roomNumber[i].Width, roomNumber[i].Height, roomNumber[i].Area);
                Console.WriteLine();
            }
        }
    }
}
