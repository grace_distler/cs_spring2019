﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB4
{
    class Room
    {
        private int length;
        private int width;
        private int height;
        private int area;
        private int gallons;

        public Room(int length, int width, int height)
        {
            this.length = length;
            this.width = width;
            this.height = height;

            CalculateArea();
            CalculateGallons();
        }

        public int Length
        {
            get
            {
                return length;
            }
        }

        public int Width
        {
            get
            {
                return width;
            }
        }

        public int Height
        {
            get
            {
                return height;
            }
        }

        public int Area
        {
            get
            {
                return area;
            }
        }

        public int Gallons
        {
            get
            {
                return gallons;
            }
        }

        private void CalculateArea()
        {
            area = ((length * height * 2) + (width * height * 2));
        }

        private void CalculateGallons()
        {
            if (area <= 350)
            {
                gallons = 1;
            }
            else
            {
                gallons = 2;
            }
        }
    }
}
