﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB1
{
    class SoccerPlayer
    {
        private string playerName;
        private int jerseyNumber;
        private int goalsScored;
        private int assists;

        public SoccerPlayer(string name, int jersey, int goals, int assist)
        {
            this.playerName = name;
            this.jerseyNumber = jersey;
            this.goalsScored = goals;
            this.assists = assist;
        }

        public SoccerPlayer()
        {

        }

        public string PlayerName
        {
            get
            {
                return playerName;
            }
            set
            {
                playerName = value;
            }
        }
        public int JerseyNumber
        {
            get
            {
                return jerseyNumber;
            }
            set
            {
                jerseyNumber = value;
            }
        }
        public int GoalsScored
        {
            get
            {
                return goalsScored;
            }
            set
            {
                goalsScored = value;
            }
        }
        public int Assists
        {
            get
            {
                return assists;
            }
            set
            {
                assists = value;
            }
        }
    }
}
