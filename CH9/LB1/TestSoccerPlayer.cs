﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB1
{
    class TestSoccerPlayer
    {
        static void Main(string[] args)
        {
            SoccerPlayer player = new SoccerPlayer("Distler", 7, 32, 48);

            Console.WriteLine("Player's Name: " + player.PlayerName);
            Console.WriteLine("Jersey Number: " + player.JerseyNumber);
            Console.WriteLine("Goals Scored: " + player.GoalsScored);
            Console.WriteLine("Assists: " + player.Assists);
            Console.WriteLine();
        }
    }
}
