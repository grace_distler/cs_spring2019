﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB9
{
    class Relative : IComparable
    {
        public string RelativeName { get; set; }
        public string RelativeRelationship { get; set; }
        public int RelativeBirthMonth { get; set; }
        public int RelativeBirthDay { get; set; }
        public int RelativeBirthYear { get; set; }

        int IComparable.CompareTo(Object o)
        {
            int returnValue;
            Relative temp = (Relative)o;

            returnValue = this.RelativeName.CompareTo(temp.RelativeName);
            return returnValue;
        }
    }
}
