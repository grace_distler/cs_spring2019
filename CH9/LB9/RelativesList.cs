﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB9
{
    class RelativesList
    {
        const int numberOfRelatives = 3;

        static void Main(string[] args)
        {
            Relative[] relatives = new Relative[numberOfRelatives];
            int x;
            int temp;

            for (x = 0; x < numberOfRelatives; ++x)
            {
                relatives[x] = new Relative();

                Console.Write("Enter Relative #{0} First Name: ", (x + 1));
                relatives[x].RelativeName = Console.ReadLine();

                Console.Write("Enter the relative's relationship to you: ");
                relatives[x].RelativeRelationship = Console.ReadLine();

                Console.Write("Enter the relative's birth month: ");
                int.TryParse(Console.ReadLine(), out temp);
                relatives[x].RelativeBirthMonth = temp;

                Console.Write("Enter the relative's birth day: ");
                int.TryParse(Console.ReadLine(), out temp);
                relatives[x].RelativeBirthDay = temp;

                Console.Write("Enter the relative's birth year: ");
                int.TryParse(Console.ReadLine(), out temp);
                relatives[x].RelativeBirthYear = temp;
                Console.WriteLine();
            }

            Array.Sort(relatives);
            Console.WriteLine();

            Console.WriteLine("Alphabetically Sorted Relatives: ");
            for (x = 0; x < numberOfRelatives; x++)
            {
                Console.WriteLine(" {0,-15} {1,-15}  {2}/{3}/{4}",
                    relatives[x].RelativeName, relatives[x].RelativeRelationship, relatives[x].RelativeBirthMonth, relatives[x].RelativeBirthDay, relatives[x].RelativeBirthYear);
            }
            Console.WriteLine();


            //RELATIVESBIRTHDAY
            Console.Write("Enter the relative's name you are looking for: ");
            string search = Console.ReadLine();

            bool found = false;
            for(int index = 0; index < numberOfRelatives; index++)
            {
                if (relatives[index].RelativeName.Contains(search))
                {
                    Console.WriteLine(" {0,-15} {1,-15} {2}/{3}/{4}",
                    relatives[index].RelativeName, relatives[index].RelativeRelationship, relatives[index].RelativeBirthMonth, 
                    relatives[index].RelativeBirthDay, relatives[index].RelativeBirthYear);
                    found = true;
                    break;
                }
            }

           if(found == false)
            {
                Console.WriteLine("The relative you entered cannot be found.");
            }
            Console.WriteLine();
        }
    }
}
