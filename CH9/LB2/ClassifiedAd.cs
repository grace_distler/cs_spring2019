﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB2
{
    class ClassifiedAd
    {
        private string category;
        private double wordCount;
        private double price;

        public ClassifiedAd(string category, double wordCount)
        {
            this.category = category;
            this.wordCount = wordCount;
            CalcPrice();
        }

        public string Category
        {
            get
            {
                return category;
            }
            set
            {
                category = value;
            }
        }
        public double WordCount
        {
            get
            {
                return wordCount;
            }
            set
            {
                wordCount = value;
                CalcPrice();
            }
        }
        public double Price
        {
            get
            {
                return price;
            }
        } 

        private void CalcPrice()
        {
            price = wordCount * .09;
        }
    }
}
