﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB2
{
    class TestClassifiedAd
    {
        static void Main(string[] args)
        {
            ClassifiedAd ad1 = new ClassifiedAd("Used Cars", 78);
            ClassifiedAd ad2 = new ClassifiedAd("New Cars", 53);

            Console.WriteLine("The first ad's category is " + ad1.Category);
            Console.WriteLine("It has " + ad1.WordCount + " words");
            Console.WriteLine("And it's price is " + ad1.Price.ToString("C"));
            Console.WriteLine();

            Console.WriteLine("The second ad's category is " + ad2.Category);
            Console.WriteLine("It has " + ad2.WordCount + " words");
            Console.WriteLine("And it's price is " + ad2.Price.ToString("C"));
            Console.WriteLine();

        }
    }
}
