﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB6
{
    class SalesTaxDemo
    {
        const int numberOfObjects = 2;

        static void Main(string[] args)
        {
            Sale[] sale = new Sale[numberOfObjects];
            int x;
            string userInput;

            for (x = 0; x < numberOfObjects; x++)
            {
                sale[x] = new Sale();

                Console.Write("Enter the inventory number for sale #{0}: ", (x + 1));
                sale[x].InventoryNumber = Console.ReadLine();

                Console.Write("Enter the sale amount for sale #{0}: ", (x + 1));
                userInput = Console.ReadLine();
                sale[x].AmountOfSale = Convert.ToDouble(userInput);
                Console.WriteLine();
                Console.WriteLine();
            }

            Console.WriteLine("Sales Data:");
            for (x = 0; x < numberOfObjects; x++)
            {
                Console.Write(" Sale #{0} \tInventory Number: {1} \tAmount of Sale: {2}\t", (x+1), sale[x].InventoryNumber,sale[x].AmountOfSale.ToString("C"));
                Console.WriteLine("  Tax Owed: {0} ", sale[x].TaxOwed.ToString("C"));
                Console.WriteLine();
            }
            Console.WriteLine();
            Array.Sort(sale);

            Console.WriteLine("Sorted Sales Data:");
            for (x = 0; x < numberOfObjects; x++)
            {
                Console.Write(" Sale #{0} \tInventory Number: {1} \tAmount of Sale: {2}\t", (x + 1), sale[x].InventoryNumber, sale[x].AmountOfSale.ToString("C"));
                Console.WriteLine("  Tax Owed: {0} ", sale[x].TaxOwed.ToString("C"));
                Console.WriteLine();
            }
        }
    }
}
