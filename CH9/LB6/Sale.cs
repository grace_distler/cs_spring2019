﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB6
{
    class Sale : IComparable
    {
        public const double limit = 100;
        public const double lowRate = 0.08;
        public const double highRate = 0.06;

        //instance variables
        private string inventoryNumber;
        private double amountOfSale;
        private double taxOwed;

        public Sale()
        {

        }

        public Sale(string inventory)
        {
            this.inventoryNumber = InventoryNumber;
            this.amountOfSale = AmountOfSale;
            this.taxOwed = TaxOwed;
        }

        public string InventoryNumber
        {
            get
            {
                return inventoryNumber;
            }
            set
            {
                inventoryNumber = value;
            }
        }

        public double AmountOfSale
        {
            get
            {
                return amountOfSale;
            }
            set
            {
                amountOfSale = value;
                CalculateTax();
            }
        }

        public double TaxOwed
        {
            get
            {
                return taxOwed;
            }
        }

        private void CalculateTax()
        {
            if (amountOfSale < limit)
            {
                taxOwed = amountOfSale * lowRate;
            }
            else
            {
                taxOwed = amountOfSale * highRate;
            }
        }

        public int CompareTo(object obj)
        {
            return taxOwed.CompareTo((obj as Sale).taxOwed);
        }
    }
}
