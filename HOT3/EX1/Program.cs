﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace EX1
{
    class Program
    {
        static void Main(string[] args)
        {
            int row, col, charLength;

            WriteLine("How many rows should the table have?");
            row = Convert.ToInt16(ReadLine());

            WriteLine("How many columns should the table have?");
            col = Convert.ToInt16(ReadLine());

            charLength = ((col + 1) * 7) - 1;
            Write("{0, 6}", "|");

            for (int i=1; i<=col; i++)
            {
                Write("{0, 6}|", i);
            }
            WriteLine();

            for (int i = 1; i <= charLength; i++)
            {
                Write("-");

            }
            WriteLine();
            
            for (int i = 1; i <= row; i++)
            {
                Write("{0, 5|", i);
                for (int j = 1; j <= col; j++)
                {
                    Write("{0, 6}|", (i*j));
                }
                WriteLine("");
            }
            ReadLine();
        }
        
    }
}
