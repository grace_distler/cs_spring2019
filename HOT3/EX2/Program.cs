﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace EX2
{
    class Program
    {
        static void Main(string[] args)
        {
            double price=1;
            double numberOfItems = -1;
            double avg = 0;
            double subtotal = 0;
            const double tax = 0.08;
            double totalTax = 0;
            double grandTotal = 0;

            while (price != 0)
            {
                Write("What is the item price? ");
                price = Convert.ToDouble(ReadLine());

                if ((price < 0))
                {
                    if (price != 0)
                    {
                        WriteLine("INVALID PRICE");
                    }
                } else
                {
                    numberOfItems++;
                    subtotal += price;
                    totalTax = (tax * subtotal);
                    grandTotal = (totalTax + subtotal);
                }
            }

            avg = subtotal / numberOfItems;
            WriteLine();
            WriteLine("---------------------------------------");
            WriteLine("Items Purchased: {0}", numberOfItems);
            WriteLine("Average Item Price: {0}", avg.ToString("C"));
            WriteLine("Subtotal: {0}", subtotal.ToString("C"));
            WriteLine("Tax: {0}", totalTax.ToString("C"));
            WriteLine("Total: {0}", grandTotal.ToString("C"));
            ReadLine();
        }
    }
}
