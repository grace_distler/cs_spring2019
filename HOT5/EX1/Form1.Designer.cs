﻿namespace EX1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.authorEntryTextBox = new System.Windows.Forms.TextBox();
            this.isbnEntryTextBox = new System.Windows.Forms.TextBox();
            this.keywordEntryTextBox = new System.Windows.Forms.TextBox();
            this.authorSearchButton = new System.Windows.Forms.Button();
            this.isbnSearchButton = new System.Windows.Forms.Button();
            this.keywordSearchButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.nameOutputLabel = new System.Windows.Forms.Label();
            this.descriptionOutputLabel = new System.Windows.Forms.Label();
            this.authorOutputLabel = new System.Windows.Forms.Label();
            this.isbnOutputLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Author:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "ISBN:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Keyword:";
            // 
            // authorEntryTextBox
            // 
            this.authorEntryTextBox.Location = new System.Drawing.Point(124, 20);
            this.authorEntryTextBox.Name = "authorEntryTextBox";
            this.authorEntryTextBox.Size = new System.Drawing.Size(186, 20);
            this.authorEntryTextBox.TabIndex = 3;
            // 
            // isbnEntryTextBox
            // 
            this.isbnEntryTextBox.Location = new System.Drawing.Point(124, 58);
            this.isbnEntryTextBox.Name = "isbnEntryTextBox";
            this.isbnEntryTextBox.Size = new System.Drawing.Size(186, 20);
            this.isbnEntryTextBox.TabIndex = 5;
            // 
            // keywordEntryTextBox
            // 
            this.keywordEntryTextBox.Location = new System.Drawing.Point(124, 95);
            this.keywordEntryTextBox.Name = "keywordEntryTextBox";
            this.keywordEntryTextBox.Size = new System.Drawing.Size(186, 20);
            this.keywordEntryTextBox.TabIndex = 7;
            // 
            // authorSearchButton
            // 
            this.authorSearchButton.Location = new System.Drawing.Point(362, 23);
            this.authorSearchButton.Name = "authorSearchButton";
            this.authorSearchButton.Size = new System.Drawing.Size(75, 23);
            this.authorSearchButton.TabIndex = 4;
            this.authorSearchButton.Text = "Search";
            this.authorSearchButton.UseVisualStyleBackColor = true;
            this.authorSearchButton.Click += new System.EventHandler(this.authorSearchButton_Click);
            // 
            // isbnSearchButton
            // 
            this.isbnSearchButton.Location = new System.Drawing.Point(362, 61);
            this.isbnSearchButton.Name = "isbnSearchButton";
            this.isbnSearchButton.Size = new System.Drawing.Size(75, 23);
            this.isbnSearchButton.TabIndex = 6;
            this.isbnSearchButton.Text = "Search";
            this.isbnSearchButton.UseVisualStyleBackColor = true;
            this.isbnSearchButton.Click += new System.EventHandler(this.isbnSearchButton_Click);
            // 
            // keywordSearchButton
            // 
            this.keywordSearchButton.Location = new System.Drawing.Point(362, 98);
            this.keywordSearchButton.Name = "keywordSearchButton";
            this.keywordSearchButton.Size = new System.Drawing.Size(75, 23);
            this.keywordSearchButton.TabIndex = 8;
            this.keywordSearchButton.Text = "Search";
            this.keywordSearchButton.UseVisualStyleBackColor = true;
            this.keywordSearchButton.Click += new System.EventHandler(this.keywordSearchButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(35, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(35, 389);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "ISBN-13";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(35, 319);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 20);
            this.label6.TabIndex = 11;
            this.label6.Text = "Author";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(35, 245);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 20);
            this.label7.TabIndex = 12;
            this.label7.Text = "Description";
            // 
            // nameOutputLabel
            // 
            this.nameOutputLabel.AutoSize = true;
            this.nameOutputLabel.Location = new System.Drawing.Point(38, 194);
            this.nameOutputLabel.Name = "nameOutputLabel";
            this.nameOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.nameOutputLabel.TabIndex = 13;
            // 
            // descriptionOutputLabel
            // 
            this.descriptionOutputLabel.AutoSize = true;
            this.descriptionOutputLabel.Location = new System.Drawing.Point(39, 269);
            this.descriptionOutputLabel.Name = "descriptionOutputLabel";
            this.descriptionOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.descriptionOutputLabel.TabIndex = 14;
            // 
            // authorOutputLabel
            // 
            this.authorOutputLabel.AutoSize = true;
            this.authorOutputLabel.Location = new System.Drawing.Point(38, 343);
            this.authorOutputLabel.Name = "authorOutputLabel";
            this.authorOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.authorOutputLabel.TabIndex = 15;
            // 
            // isbnOutputLabel
            // 
            this.isbnOutputLabel.AutoSize = true;
            this.isbnOutputLabel.Location = new System.Drawing.Point(42, 413);
            this.isbnOutputLabel.Name = "isbnOutputLabel";
            this.isbnOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.isbnOutputLabel.TabIndex = 16;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 463);
            this.Controls.Add(this.isbnOutputLabel);
            this.Controls.Add(this.authorOutputLabel);
            this.Controls.Add(this.descriptionOutputLabel);
            this.Controls.Add(this.nameOutputLabel);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.keywordSearchButton);
            this.Controls.Add(this.isbnSearchButton);
            this.Controls.Add(this.authorSearchButton);
            this.Controls.Add(this.keywordEntryTextBox);
            this.Controls.Add(this.isbnEntryTextBox);
            this.Controls.Add(this.authorEntryTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Books and Antiques";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox authorEntryTextBox;
        private System.Windows.Forms.TextBox isbnEntryTextBox;
        private System.Windows.Forms.TextBox keywordEntryTextBox;
        private System.Windows.Forms.Button authorSearchButton;
        private System.Windows.Forms.Button isbnSearchButton;
        private System.Windows.Forms.Button keywordSearchButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label nameOutputLabel;
        private System.Windows.Forms.Label descriptionOutputLabel;
        private System.Windows.Forms.Label authorOutputLabel;
        private System.Windows.Forms.Label isbnOutputLabel;
    }
}

