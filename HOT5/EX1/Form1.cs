﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string[] bookNames = { "The Great Gatsby", "War and Peace", "Moby-Dick", "Hamlet", "Pride and Prejudice" };
        string[] authorNames = { "F. Scott Fitzgerald", "Leo Tolstoy", "Herman Melville", "William Shakespeare", "Jane Austen" };
        string[] isbnNumbers = { "978-1847496140", "978-1400079988", "978-1503280786", "978-1973844402", "978-0141439518" };
        string[] bookKeywords = {
            "The story of eccentric millionaire Jay Gatsby and his pursuit of his lost love.",
            "A fictional story about the 1812 French invasion of Russia.",
            "The sttory of a sailor's relentless hunt for a white whale.",
            "A Shakespearean tragedy about a young man coming home from college after the murder of his father.",
            "A comedic story of love and life in Old England."
        };

        private void authorSearchButton_Click(object sender, EventArgs e)
        {
            string authorName = authorEntryTextBox.Text;
            ShowBook(SearchByAuthor(authorName));
        }

        private void isbnSearchButton_Click(object sender, EventArgs e)
        {
            string bookName = isbnEntryTextBox.Text;
            ShowBook(SearchByIsbn(bookName));
        }

        private void keywordSearchButton_Click(object sender, EventArgs e)
        {
            string keywordNames = keywordEntryTextBox.Text;
            ShowBook(SearchByKeyword(keywordNames));
        }

        public void ShowBook( int bookIndex)
        {            
            if (bookIndex >=0 && bookIndex <= 4)
            {
                nameOutputLabel.Text = bookNames[bookIndex];
                authorOutputLabel.Text = authorNames[bookIndex];
                isbnOutputLabel.Text = isbnNumbers[bookIndex];
                descriptionOutputLabel.Text = bookKeywords[bookIndex];
            }
            else
            {
                nameOutputLabel.Text = "Item Not Found";
                authorOutputLabel.Text = "Item Not Found";
                isbnOutputLabel.Text = "Item Not Found";
                descriptionOutputLabel.Text = "Item Not Found";
            }
        }

        public int SearchByAuthor(string nameOfAuthor)
        {
            int bookIndex;
            for (bookIndex = 0; bookIndex < bookNames.Length; bookIndex++)
            {
                if (authorNames[bookIndex].ToLower().Contains(nameOfAuthor) && nameOfAuthor.Length > 2)
                {
                    break;
                }
            }
            return bookIndex;
        }

        public int SearchByIsbn(string nameOfBook)
        {
            int bookIndex;
            for (bookIndex = 0; bookIndex < bookNames.Length; bookIndex++)
            {
                if (isbnNumbers[bookIndex].ToLower().Contains(nameOfBook))
                {
                    break;
                }
            }
            return bookIndex;
        }

        public int SearchByKeyword(string keywordEntry)
        {
            int bookIndex;
            for (bookIndex = 0; bookIndex < bookNames.Length; bookIndex++)
            {
                if (bookKeywords[bookIndex].ToLower().Contains(keywordEntry) || authorNames[bookIndex].ToLower().Contains(keywordEntry))
                {
                    break;
                }
            }
            return bookIndex;
        }
    }
}

        
