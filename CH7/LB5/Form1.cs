﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            string phrase= phraseEntryTextBox.Text.ToLower();
            int results = CountVowels(phrase);
            resultLabel.Text = "There are " + results + " vowels in your phrase.";
        }

        public int CountVowels(string phrase)
        {
            phrase = phraseEntryTextBox.Text.ToLower();
            int vowelCount = 0;

            for (int j = 0; j < phrase.Length; j++)
            {
                string check = phrase.Substring(j, 1);
                switch (check)
                {
                    case "a":
                        ++vowelCount;
                        break;
                    case "e":
                        ++vowelCount;
                        break;
                   case "i":
                        ++vowelCount;
                        break;
                    case "o":
                        ++vowelCount;
                        break;
                    case "u":
                        ++vowelCount;
                        break;
                }
            }
            return vowelCount;
        }
    }
}