﻿namespace LB9
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.accountNumberTextBox = new System.Windows.Forms.TextBox();
            this.pinNumberTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.welcomeNameLabel = new System.Windows.Forms.Label();
            this.accountBalanceLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.depositEntryTextBox = new System.Windows.Forms.TextBox();
            this.depositButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.withdrawalEntryTextBox = new System.Windows.Forms.TextBox();
            this.withdrawalButton = new System.Windows.Forms.Button();
            this.logoutButton = new System.Windows.Forms.Button();
            this.logoutReturnLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Account #:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Pin #:";
            // 
            // accountNumberTextBox
            // 
            this.accountNumberTextBox.Location = new System.Drawing.Point(96, 27);
            this.accountNumberTextBox.Name = "accountNumberTextBox";
            this.accountNumberTextBox.Size = new System.Drawing.Size(151, 20);
            this.accountNumberTextBox.TabIndex = 2;
            this.accountNumberTextBox.Click += new System.EventHandler(this.accountNumberTextBox_TextChanged);
            this.accountNumberTextBox.TextChanged += new System.EventHandler(this.accountNumberTextBox_TextChanged);
            // 
            // pinNumberTextBox
            // 
            this.pinNumberTextBox.Location = new System.Drawing.Point(96, 61);
            this.pinNumberTextBox.Name = "pinNumberTextBox";
            this.pinNumberTextBox.Size = new System.Drawing.Size(151, 20);
            this.pinNumberTextBox.TabIndex = 3;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(278, 42);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 4;
            this.calculateButton.Text = "Login";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // welcomeNameLabel
            // 
            this.welcomeNameLabel.AutoSize = true;
            this.welcomeNameLabel.Location = new System.Drawing.Point(96, 107);
            this.welcomeNameLabel.Name = "welcomeNameLabel";
            this.welcomeNameLabel.Size = new System.Drawing.Size(0, 13);
            this.welcomeNameLabel.TabIndex = 5;
            // 
            // accountBalanceLabel
            // 
            this.accountBalanceLabel.AutoSize = true;
            this.accountBalanceLabel.Location = new System.Drawing.Point(93, 129);
            this.accountBalanceLabel.Name = "accountBalanceLabel";
            this.accountBalanceLabel.Size = new System.Drawing.Size(0, 13);
            this.accountBalanceLabel.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 178);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(170, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Would you like to make a deposit?";
            // 
            // depositEntryTextBox
            // 
            this.depositEntryTextBox.Location = new System.Drawing.Point(13, 195);
            this.depositEntryTextBox.Name = "depositEntryTextBox";
            this.depositEntryTextBox.Size = new System.Drawing.Size(170, 20);
            this.depositEntryTextBox.TabIndex = 8;
            // 
            // depositButton
            // 
            this.depositButton.Location = new System.Drawing.Point(210, 193);
            this.depositButton.Name = "depositButton";
            this.depositButton.Size = new System.Drawing.Size(75, 23);
            this.depositButton.TabIndex = 9;
            this.depositButton.Text = "Deposit";
            this.depositButton.UseVisualStyleBackColor = true;
            this.depositButton.Click += new System.EventHandler(this.depositButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 246);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(180, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Would you like to make a withdrawl?";
            // 
            // withdrawalEntryTextBox
            // 
            this.withdrawalEntryTextBox.Location = new System.Drawing.Point(13, 263);
            this.withdrawalEntryTextBox.Name = "withdrawalEntryTextBox";
            this.withdrawalEntryTextBox.Size = new System.Drawing.Size(170, 20);
            this.withdrawalEntryTextBox.TabIndex = 11;
            // 
            // withdrawalButton
            // 
            this.withdrawalButton.Location = new System.Drawing.Point(210, 261);
            this.withdrawalButton.Name = "withdrawalButton";
            this.withdrawalButton.Size = new System.Drawing.Size(75, 23);
            this.withdrawalButton.TabIndex = 12;
            this.withdrawalButton.Text = "Withdraw";
            this.withdrawalButton.UseVisualStyleBackColor = true;
            this.withdrawalButton.Click += new System.EventHandler(this.withdrawalButton_Click);
            // 
            // logoutButton
            // 
            this.logoutButton.Location = new System.Drawing.Point(155, 327);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(75, 23);
            this.logoutButton.TabIndex = 13;
            this.logoutButton.Text = "Logout";
            this.logoutButton.UseVisualStyleBackColor = true;
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // logoutReturnLabel
            // 
            this.logoutReturnLabel.AutoSize = true;
            this.logoutReturnLabel.Location = new System.Drawing.Point(142, 351);
            this.logoutReturnLabel.Name = "logoutReturnLabel";
            this.logoutReturnLabel.Size = new System.Drawing.Size(0, 13);
            this.logoutReturnLabel.TabIndex = 14;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(366, 385);
            this.Controls.Add(this.logoutReturnLabel);
            this.Controls.Add(this.logoutButton);
            this.Controls.Add(this.withdrawalButton);
            this.Controls.Add(this.withdrawalEntryTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.depositButton);
            this.Controls.Add(this.depositEntryTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.accountBalanceLabel);
            this.Controls.Add(this.welcomeNameLabel);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.pinNumberTextBox);
            this.Controls.Add(this.accountNumberTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Weyland Bank";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox accountNumberTextBox;
        private System.Windows.Forms.TextBox pinNumberTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label welcomeNameLabel;
        private System.Windows.Forms.Label accountBalanceLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox depositEntryTextBox;
        private System.Windows.Forms.Button depositButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox withdrawalEntryTextBox;
        private System.Windows.Forms.Button withdrawalButton;
        private System.Windows.Forms.Button logoutButton;
        private System.Windows.Forms.Label logoutReturnLabel;
    }
}

