﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB9
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string account = "123-456-789";
        string pin = "1234";
        decimal accountBalance = 10000;

        private void calculateButton_Click(object sender, EventArgs e)
        {
            bool result = Login(account, pin);
            if(result == true)
            {
                welcomeNameLabel.Text = "Hello Mr. Smith!";
                accountBalanceLabel.Text = "Your account balance is " + accountBalance.ToString("C");
            }
            else
            {
                accountNumberTextBox.Text = "";
                pinNumberTextBox.Text = "";
                welcomeNameLabel.Text = "User Not Found. Please try again.";
                accountBalanceLabel.Text = "";
                depositEntryTextBox.Text = "";
                withdrawalEntryTextBox.Text = "";
            }
        }
        public bool Login(string accountNumber, string pinNumber)
        {
            accountNumber = accountNumberTextBox.Text;
            pinNumber = pinNumberTextBox.Text;

            if (accountNumber == account && pinNumber == pin)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            bool result = Logout();
            if (result == true)
            {
                accountNumberTextBox.Text = "";
                pinNumberTextBox.Text = "";
                welcomeNameLabel.Text = "";
                accountBalanceLabel.Text = "";
                depositEntryTextBox.Text = "";
                withdrawalEntryTextBox.Text = "";
                logoutReturnLabel.Text = "You have been logged out.";
            }
        }
        public bool Logout()
        {
            return true;
        }

        private void depositButton_Click(object sender, EventArgs e)
        {
            if (Login(account, pin) == true)
            {
                decimal depositAmount= Convert.ToDecimal(depositEntryTextBox.Text);
                decimal deposit = MakeDeposit(depositAmount);
                accountBalanceLabel.Text = "Your account balance is " + accountBalance.ToString("C");
            }
        }
        public decimal MakeDeposit(decimal depositAmount)
        {
            depositAmount = Convert.ToDecimal(depositEntryTextBox.Text);
            accountBalance += depositAmount;           
            return accountBalance;
        }

        private void withdrawalButton_Click(object sender, EventArgs e)
        {
            if (Login(account, pin) == true)
            {
                decimal withdrawnAmount = Convert.ToDecimal(withdrawalEntryTextBox.Text);
                decimal withdraw = MakeWithdrawal(withdrawnAmount);
                accountBalanceLabel.Text = "Your account balance is " + accountBalance.ToString("C");
            }
        }
        public decimal MakeWithdrawal(decimal withdrawAmount)
        {
            withdrawAmount = Convert.ToDecimal(withdrawalEntryTextBox.Text);
            accountBalance -= withdrawAmount;
            return accountBalance;
        }

        private void accountNumberTextBox_TextChanged(object sender, EventArgs e)
        {
            logoutReturnLabel.Text = "";
        }
    }
}
