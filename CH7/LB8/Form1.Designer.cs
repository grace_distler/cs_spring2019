﻿namespace LB8
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.woodEntryTextBox = new System.Windows.Forms.TextBox();
            this.drawerEntryTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.woodCostOutputLabel = new System.Windows.Forms.Label();
            this.drawerCostOutputLabel = new System.Windows.Forms.Label();
            this.totalCostOutputLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Type of Wood:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(200, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Number of Drawers:";
            // 
            // woodEntryTextBox
            // 
            this.woodEntryTextBox.Location = new System.Drawing.Point(33, 66);
            this.woodEntryTextBox.Name = "woodEntryTextBox";
            this.woodEntryTextBox.Size = new System.Drawing.Size(141, 20);
            this.woodEntryTextBox.TabIndex = 2;
            // 
            // drawerEntryTextBox
            // 
            this.drawerEntryTextBox.Location = new System.Drawing.Point(203, 66);
            this.drawerEntryTextBox.Name = "drawerEntryTextBox";
            this.drawerEntryTextBox.Size = new System.Drawing.Size(156, 20);
            this.drawerEntryTextBox.TabIndex = 3;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(33, 115);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 4;
            this.calculateButton.Text = "Estimate";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 184);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Cost of Wood:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(186, 184);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Cost of Drawers:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(352, 184);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Total Cost:";
            // 
            // woodCostOutputLabel
            // 
            this.woodCostOutputLabel.AutoSize = true;
            this.woodCostOutputLabel.Location = new System.Drawing.Point(36, 201);
            this.woodCostOutputLabel.Name = "woodCostOutputLabel";
            this.woodCostOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.woodCostOutputLabel.TabIndex = 8;
            // 
            // drawerCostOutputLabel
            // 
            this.drawerCostOutputLabel.AutoSize = true;
            this.drawerCostOutputLabel.Location = new System.Drawing.Point(189, 201);
            this.drawerCostOutputLabel.Name = "drawerCostOutputLabel";
            this.drawerCostOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.drawerCostOutputLabel.TabIndex = 9;
            // 
            // totalCostOutputLabel
            // 
            this.totalCostOutputLabel.AutoSize = true;
            this.totalCostOutputLabel.Location = new System.Drawing.Point(355, 201);
            this.totalCostOutputLabel.Name = "totalCostOutputLabel";
            this.totalCostOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.totalCostOutputLabel.TabIndex = 10;
            // 
            // Form1
            // 
            this.AcceptButton = this.calculateButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 252);
            this.Controls.Add(this.totalCostOutputLabel);
            this.Controls.Add(this.drawerCostOutputLabel);
            this.Controls.Add(this.woodCostOutputLabel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.drawerEntryTextBox);
            this.Controls.Add(this.woodEntryTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "My Custom Desk";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox woodEntryTextBox;
        private System.Windows.Forms.TextBox drawerEntryTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label woodCostOutputLabel;
        private System.Windows.Forms.Label drawerCostOutputLabel;
        private System.Windows.Forms.Label totalCostOutputLabel;
    }
}

