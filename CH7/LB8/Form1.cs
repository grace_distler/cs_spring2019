﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB8
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        string woodChoice;
        int numberOfDrawers;
        double woodPrice;
        double costOfDrawers;
        double totalCost;

        private void calculateButton_Click(object sender, EventArgs e)
        {
            woodChoice = GetWood();
            numberOfDrawers = GetDrawers();

            CalculateWoodCost(woodChoice);
            woodCostOutputLabel.Text = woodPrice.ToString("C");

            CalculateDrawerCost(numberOfDrawers);
            drawerCostOutputLabel.Text = costOfDrawers.ToString("C");

            CalculateTotalCost(woodChoice, numberOfDrawers);
            totalCostOutputLabel.Text = totalCost.ToString("C");
        }

        public string GetWood()
        {
            string[] woodTypes = { "Mahogany", "Oak", "Pine", "Other" };
            woodChoice = woodEntryTextBox.Text;
            woodChoice = woodChoice.ToLower();

            if (woodChoice.Contains("m"))
            {
                woodChoice = woodTypes[0];
            }
            else if (woodChoice.Contains("o"))
            {
                woodChoice = woodTypes[1];
            }
            else if (woodChoice.Contains("p"))
            {
                woodChoice = woodTypes[2];
            }
            else
            {
                woodChoice = woodTypes[3];
            }
            return woodChoice;
        }

        public int GetDrawers()
        {
            numberOfDrawers = Convert.ToInt32(drawerEntryTextBox.Text);
            return numberOfDrawers;
        }

        public double CalculateWoodCost(string woodChoice)
        {
            woodChoice = GetWood();
            string[] woodTypes = { "Mahogany", "Oak", "Pine", "Other" };
            double[] woodCost = { 200, 140, 100, 180 };

            if (woodChoice == woodTypes[0])
            {
                woodPrice = woodCost[0];
            }
            else if (woodChoice == woodTypes[1])
            {
                woodPrice = woodCost[1];
            }
            else if (woodChoice == woodTypes[2])
            {
                woodPrice = woodCost[2];
            }
            else
            {
                woodPrice = woodCost[3];
            }
            return woodPrice;
        }

        public double CalculateDrawerCost(int numberOfDrawers)
        {
            numberOfDrawers = GetDrawers();
            costOfDrawers = numberOfDrawers * 30;
            return costOfDrawers;
        }

        public double CalculateTotalCost(string woodChoice, int numberOfDrawers)
        {
            woodPrice= CalculateWoodCost(woodChoice);
            costOfDrawers = CalculateDrawerCost(numberOfDrawers);
            totalCost = woodPrice + costOfDrawers;
            return totalCost;
        }
    }
}
