﻿namespace LB3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.calculateButton = new System.Windows.Forms.Button();
            this.firstFortuneLabel = new System.Windows.Forms.Label();
            this.secondFortuneLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(34, 29);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(113, 37);
            this.calculateButton.TabIndex = 0;
            this.calculateButton.Text = "Open";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // firstFortuneLabel
            // 
            this.firstFortuneLabel.AutoSize = true;
            this.firstFortuneLabel.Location = new System.Drawing.Point(34, 132);
            this.firstFortuneLabel.Name = "firstFortuneLabel";
            this.firstFortuneLabel.Size = new System.Drawing.Size(0, 13);
            this.firstFortuneLabel.TabIndex = 1;
            // 
            // secondFortuneLabel
            // 
            this.secondFortuneLabel.AutoSize = true;
            this.secondFortuneLabel.Location = new System.Drawing.Point(34, 181);
            this.secondFortuneLabel.Name = "secondFortuneLabel";
            this.secondFortuneLabel.Size = new System.Drawing.Size(0, 13);
            this.secondFortuneLabel.TabIndex = 2;
            // 
            // Form1
            // 
            this.AcceptButton = this.calculateButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(385, 249);
            this.Controls.Add(this.secondFortuneLabel);
            this.Controls.Add(this.firstFortuneLabel);
            this.Controls.Add(this.calculateButton);
            this.Name = "Form1";
            this.Text = "Fortune Cookie";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label firstFortuneLabel;
        private System.Windows.Forms.Label secondFortuneLabel;
    }
}

