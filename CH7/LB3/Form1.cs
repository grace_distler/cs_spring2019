﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            string[] resultStrings = GenerateFortune();
            firstFortuneLabel.Text = resultStrings[0];
            secondFortuneLabel.Text = resultStrings[1];
        }

        public string[] GenerateFortune()
        {
            string[] fortunes =
            {
                "Birthdays are like friends. The more you have the better.",
                "Your smile is a treasure to all who know you.",
                "You never hesitate to tackle the most difficult problems.",
                "The most obvious solution is not always the best.",
                "If you want the rainbow, you will have to tolerate the rain.",
                "The early bird gets the worm, but the second mouse gets the cheese.",
                "From listening comes wisdom and from speaking repentance.",
                "A smile is your personal welcome mat."
            };

            string[] randomStrings = new string[2];
            int randomNumber1, randomNumber2;

            do
            {
                Random someNumber = new Random();
                randomNumber1 = someNumber.Next(0, 8);
                randomNumber2 = someNumber.Next(0, 8);
            } while (randomNumber1 == randomNumber2);
            

            randomStrings[0] = fortunes[randomNumber1];
            randomStrings[1] = fortunes[randomNumber2];

            return randomStrings;
        }
    }
}
