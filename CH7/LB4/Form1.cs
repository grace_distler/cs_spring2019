﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            double gpa = Convert.ToDouble(gpaInputTextBox.Text);
            int testScore = Convert.ToInt16(testScoreInputTextBox.Text);
            bool result = IsStudentAccepted(gpa, testScore); ;
            if (result == true)
            {
                resultLabel.Text = "Accept";
            }
            else
            {
                resultLabel.Text = "Reject";
            }
        }

        public bool IsStudentAccepted(double gpa, int testScore)
        {
            gpa = Convert.ToDouble(gpaInputTextBox.Text);
            testScore = Convert.ToInt16(testScoreInputTextBox.Text);

            if (gpa >= 3.0 && testScore >= 60)
            {
                return true;
            }
            else if (gpa < 3.0 && testScore >= 80)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
