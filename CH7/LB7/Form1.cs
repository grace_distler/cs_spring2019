﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB7
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string userEntry;
        string replacementCharacter = "X";
        int lastDigits = 4;

        private void calculateButton_Click(object sender, EventArgs e)
        {
            userEntry = creditCardEntryTextBox.Text;
            maskedNumberLabel.Text = MaskNumber(userEntry, replacementCharacter, lastDigits);
        }

        public string MaskNumber(string userEntry, string replacementCharacter, int lastDigits )
        {
            userEntry = creditCardEntryTextBox.Text;
            string maskedCardNumber = "";
            char[] arr;
            arr = userEntry.ToCharArray();           
            
            for (int i = 0; i < arr.Length; i++)
            {
                char digit = arr[i];
                if (i >= arr.Length - lastDigits && char.IsDigit(digit))
                {
                    maskedCardNumber += digit.ToString();
                }
                else if (char.IsSeparator(digit))
                {
                    maskedCardNumber += digit.ToString();
                }
                else if (char.IsNumber(digit))
                {
                    maskedCardNumber += replacementCharacter;
                }
                else if (char.IsLetter(digit))
                {
                    maskedCardNumber += replacementCharacter;
                }
                else
                {
                    maskedCardNumber += digit.ToString();
                }
            }
            return maskedCardNumber;
        }
    }
}
