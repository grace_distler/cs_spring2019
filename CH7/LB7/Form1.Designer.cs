﻿namespace LB7
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.creditCardEntryTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.maskedNumberLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // creditCardEntryTextBox
            // 
            this.creditCardEntryTextBox.Location = new System.Drawing.Point(39, 35);
            this.creditCardEntryTextBox.Name = "creditCardEntryTextBox";
            this.creditCardEntryTextBox.Size = new System.Drawing.Size(258, 20);
            this.creditCardEntryTextBox.TabIndex = 0;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(341, 33);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 1;
            this.calculateButton.Text = "Mask";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // maskedNumberLabel
            // 
            this.maskedNumberLabel.AutoSize = true;
            this.maskedNumberLabel.Location = new System.Drawing.Point(39, 92);
            this.maskedNumberLabel.Name = "maskedNumberLabel";
            this.maskedNumberLabel.Size = new System.Drawing.Size(0, 13);
            this.maskedNumberLabel.TabIndex = 2;
            // 
            // Form1
            // 
            this.AcceptButton = this.calculateButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 148);
            this.Controls.Add(this.maskedNumberLabel);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.creditCardEntryTextBox);
            this.Name = "Form1";
            this.Text = "Credit Card Masker";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox creditCardEntryTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label maskedNumberLabel;
    }
}

