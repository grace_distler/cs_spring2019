﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB10
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            string[] answersString = ShowAnswer();
            answerOutputLabel.Text = answersString[0];
        }

        public string[] ShowAnswer()
        {
            string[] answers = {
                "Tomorrow",
                "Today",
                "Yesterday",
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday",
                "Sunday",
                "I Don't Know" };
            string userInput = questionEntryTextBox.Text;
            string[] randomStrings = new string[1];
            int randomAnswer;

            if (userInput != "")
            {
                Random randomNumber = new Random();
                randomAnswer = randomNumber.Next(0,11);
                randomStrings[0] = answers[randomAnswer];
            }
            else
            {
                randomStrings[0]= answers[10];
            }
            return randomStrings;
        }
    }
}
