﻿namespace LB6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lengthInputTextBox = new System.Windows.Forms.TextBox();
            this.widthInputTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.totalAreaOutputLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.totalCostOutputLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Length (ft) :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(159, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Width (ft) :";
            // 
            // lengthInputTextBox
            // 
            this.lengthInputTextBox.Location = new System.Drawing.Point(33, 41);
            this.lengthInputTextBox.Name = "lengthInputTextBox";
            this.lengthInputTextBox.Size = new System.Drawing.Size(100, 20);
            this.lengthInputTextBox.TabIndex = 2;
            // 
            // widthInputTextBox
            // 
            this.widthInputTextBox.Location = new System.Drawing.Point(162, 41);
            this.widthInputTextBox.Name = "widthInputTextBox";
            this.widthInputTextBox.Size = new System.Drawing.Size(100, 20);
            this.widthInputTextBox.TabIndex = 3;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(33, 85);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 4;
            this.calculateButton.Text = "Calculate";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 159);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Total Area (sq ft):";
            // 
            // totalAreaOutputLabel
            // 
            this.totalAreaOutputLabel.AutoSize = true;
            this.totalAreaOutputLabel.Location = new System.Drawing.Point(36, 176);
            this.totalAreaOutputLabel.Name = "totalAreaOutputLabel";
            this.totalAreaOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.totalAreaOutputLabel.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(36, 220);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Total Cost ($):";
            // 
            // totalCostOutputLabel
            // 
            this.totalCostOutputLabel.AutoSize = true;
            this.totalCostOutputLabel.Location = new System.Drawing.Point(36, 237);
            this.totalCostOutputLabel.Name = "totalCostOutputLabel";
            this.totalCostOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.totalCostOutputLabel.TabIndex = 8;
            // 
            // Form1
            // 
            this.AcceptButton = this.calculateButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(338, 310);
            this.Controls.Add(this.totalCostOutputLabel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.totalAreaOutputLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.widthInputTextBox);
            this.Controls.Add(this.lengthInputTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Painting Estimate";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox lengthInputTextBox;
        private System.Windows.Forms.TextBox widthInputTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label totalAreaOutputLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label totalCostOutputLabel;
    }
}

