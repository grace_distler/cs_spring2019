﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        double length = 0;
        double width = 0;
        double totalArea;
        double totalCost;

        private void calculateButton_Click(object sender, EventArgs e)
        {
            length = Convert.ToDouble(lengthInputTextBox.Text);
            width = Convert.ToDouble(widthInputTextBox.Text);
            totalArea=CalculateTotalArea(length, width);
            totalAreaOutputLabel.Text = totalArea.ToString("F1");
            totalCost =CalculatePaintEstimate(totalArea);
            totalCostOutputLabel.Text = totalCost.ToString("C");
        }

        public double CalculateTotalArea(double length, double width)
        {
            length = Convert.ToDouble(lengthInputTextBox.Text);
            width = Convert.ToDouble(widthInputTextBox.Text);
            totalArea = ((length*9)*2) + ((width*9)*2);            
            return totalArea;
        }

        public double CalculatePaintEstimate(double totalArea)
        {
            length = Convert.ToDouble(lengthInputTextBox.Text);
            width = Convert.ToDouble(widthInputTextBox.Text);
            totalArea = CalculateTotalArea(length, width);
            double pricePerSquareFoot = 6;
            totalCost = totalArea * pricePerSquareFoot;            
            return totalCost;
        }
    }
}
