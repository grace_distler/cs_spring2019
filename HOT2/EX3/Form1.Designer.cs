﻿namespace EX3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lengthTextBox = new System.Windows.Forms.TextBox();
            this.widthTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.areaOutputLabel = new System.Windows.Forms.Label();
            this.weeklyFeeOutputLabel = new System.Windows.Forms.Label();
            this.seasonLengthOutputLabel = new System.Windows.Forms.Label();
            this.totalCostLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Length (ft)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(207, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Width (ft)";
            // 
            // lengthTextBox
            // 
            this.lengthTextBox.Location = new System.Drawing.Point(32, 59);
            this.lengthTextBox.Name = "lengthTextBox";
            this.lengthTextBox.Size = new System.Drawing.Size(100, 20);
            this.lengthTextBox.TabIndex = 2;
            // 
            // widthTextBox
            // 
            this.widthTextBox.Location = new System.Drawing.Point(210, 59);
            this.widthTextBox.Name = "widthTextBox";
            this.widthTextBox.Size = new System.Drawing.Size(100, 20);
            this.widthTextBox.TabIndex = 3;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(112, 112);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(117, 27);
            this.calculateButton.TabIndex = 4;
            this.calculateButton.Text = "Calculate";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // areaOutputLabel
            // 
            this.areaOutputLabel.AutoSize = true;
            this.areaOutputLabel.Location = new System.Drawing.Point(32, 177);
            this.areaOutputLabel.Name = "areaOutputLabel";
            this.areaOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.areaOutputLabel.TabIndex = 5;
            // 
            // weeklyFeeOutputLabel
            // 
            this.weeklyFeeOutputLabel.AutoSize = true;
            this.weeklyFeeOutputLabel.Location = new System.Drawing.Point(32, 204);
            this.weeklyFeeOutputLabel.Name = "weeklyFeeOutputLabel";
            this.weeklyFeeOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.weeklyFeeOutputLabel.TabIndex = 6;
            // 
            // seasonLengthOutputLabel
            // 
            this.seasonLengthOutputLabel.AutoSize = true;
            this.seasonLengthOutputLabel.Location = new System.Drawing.Point(32, 231);
            this.seasonLengthOutputLabel.Name = "seasonLengthOutputLabel";
            this.seasonLengthOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.seasonLengthOutputLabel.TabIndex = 7;
            // 
            // totalCostLabel
            // 
            this.totalCostLabel.AutoSize = true;
            this.totalCostLabel.Location = new System.Drawing.Point(32, 258);
            this.totalCostLabel.Name = "totalCostLabel";
            this.totalCostLabel.Size = new System.Drawing.Size(0, 13);
            this.totalCostLabel.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 313);
            this.Controls.Add(this.totalCostLabel);
            this.Controls.Add(this.seasonLengthOutputLabel);
            this.Controls.Add(this.weeklyFeeOutputLabel);
            this.Controls.Add(this.areaOutputLabel);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.widthTextBox);
            this.Controls.Add(this.lengthTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Greg\'s Lawncare";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox lengthTextBox;
        private System.Windows.Forms.TextBox widthTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label areaOutputLabel;
        private System.Windows.Forms.Label weeklyFeeOutputLabel;
        private System.Windows.Forms.Label seasonLengthOutputLabel;
        private System.Windows.Forms.Label totalCostLabel;
    }
}

