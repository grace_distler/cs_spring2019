﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            const double SEASON_LENGTH = 20;
            double length, width, lotArea, weeklyFee, totalCost;

            length = Convert.ToDouble(lengthTextBox.Text);
            width = Convert.ToDouble(widthTextBox.Text);

            lotArea = length * width;
            if (lotArea >= 600)
            {
                weeklyFee = 50;
            }
            else if (lotArea > 399)
            {
                weeklyFee = 35;
            }
            else
            {
                weeklyFee = 25;
            }

            totalCost = weeklyFee * SEASON_LENGTH;

            areaOutputLabel.Text = "Area: " + lotArea + " sq feet";
            weeklyFeeOutputLabel.Text = "Weekly Fee: " + weeklyFee.ToString("C");
            seasonLengthOutputLabel.Text = "Season Length: " + SEASON_LENGTH +" weeks";
            totalCostLabel.Text = "Total Cost: " + totalCost.ToString("C");

        }
    }
}
