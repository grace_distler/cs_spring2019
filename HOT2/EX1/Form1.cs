﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            const double CM_PER_IN = 2.54;
            double inches, centimeters;

            inches = Convert.ToDouble(inchesInputTextBox.Text);

            centimeters = inches * CM_PER_IN;

            cmOutputLabel.Text = inches + " inches is " + centimeters + " centimeters";
        }
    }
}
