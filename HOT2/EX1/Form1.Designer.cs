﻿namespace EX1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.inchesInputTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.cmOutputLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Distance in Inches:";
            // 
            // inchesInputTextBox
            // 
            this.inchesInputTextBox.Location = new System.Drawing.Point(32, 64);
            this.inchesInputTextBox.Name = "inchesInputTextBox";
            this.inchesInputTextBox.Size = new System.Drawing.Size(100, 20);
            this.inchesInputTextBox.TabIndex = 1;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(32, 124);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(100, 26);
            this.calculateButton.TabIndex = 2;
            this.calculateButton.Text = "Convert to cm";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // cmOutputLabel
            // 
            this.cmOutputLabel.AutoSize = true;
            this.cmOutputLabel.Location = new System.Drawing.Point(32, 200);
            this.cmOutputLabel.Name = "cmOutputLabel";
            this.cmOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.cmOutputLabel.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 284);
            this.Controls.Add(this.cmOutputLabel);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.inchesInputTextBox);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Distance Converter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox inchesInputTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label cmOutputLabel;
    }
}

