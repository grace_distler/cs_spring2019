﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            double gpa;
            string letterGrade;

            gpa = Convert.ToDouble(gpaInputTextBox.Text);

            if (gpa > 3.0)
            {
                letterGrade = "A";
            } else if (gpa > 2.0)
            {
                letterGrade = "B";
            } else if (gpa > 1.0)
            {
                letterGrade = "C";
            } else if (gpa > 0.0)
            {
                letterGrade = "D";
            } else
            {
                letterGrade = "F";
            }

            gradeOutputLabel.Text = "Your Grade: " + letterGrade;
        }
    }
}
