﻿namespace EX2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.gpaInputTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.gradeOutputLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Numeric Grade:";
            // 
            // gpaInputTextBox
            // 
            this.gpaInputTextBox.Location = new System.Drawing.Point(32, 63);
            this.gpaInputTextBox.Name = "gpaInputTextBox";
            this.gpaInputTextBox.Size = new System.Drawing.Size(113, 20);
            this.gpaInputTextBox.TabIndex = 1;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(32, 114);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(113, 23);
            this.calculateButton.TabIndex = 2;
            this.calculateButton.Text = "Show Letter Grade";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // gradeOutputLabel
            // 
            this.gradeOutputLabel.AutoSize = true;
            this.gradeOutputLabel.Location = new System.Drawing.Point(32, 164);
            this.gradeOutputLabel.Name = "gradeOutputLabel";
            this.gradeOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.gradeOutputLabel.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(319, 215);
            this.Controls.Add(this.gradeOutputLabel);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.gpaInputTextBox);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Letter Grade";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox gpaInputTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label gradeOutputLabel;
    }
}

