﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB2
{
    public partial class Form1 : Form
    {
        EmployeeDB data = new EmployeeDB();
        int page = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            page = 0;
            ShowSearchResults();
        }
        private void ShowSearchResults()
        {
            lblResults.Text = "";
            IEnumerable<Employee> employees = data.GetEmployees();
            string search = txtSearch.Text;

            if (!string.IsNullOrWhiteSpace(search))
            {
                //filtering results
                employees = employees.Where(x => x.City.Contains(search));
            }

            employees = employees.OrderBy(x => x.LastName)
                                 .ThenBy(x => x.FirstName)
                                 .ThenBy(x => x.SSN);
            //paging
            int numberOfResults = employees.Count();
            int numberOfPages = (numberOfResults + 4) / 5;

            btnPage1.Visible = true;
            btnPage2.Visible = (numberOfPages >= 2);
            btnPage3.Visible = (numberOfPages >= 3);
            btnPage4.Visible = (numberOfPages >= 4);

            //Skip() and Take()
            //skip tells how many of the records to skip over
            //take tells how many results to use

            employees = employees.Skip(page * 5).Take(5);

            foreach (var item in employees)
            {
                lblResults.Text += item.ToString() + "\n";
            }
        }

        private void btnPage1_Click(object sender, EventArgs e)
        {
            page = 0;
            ShowSearchResults();
        }

        private void btnPage2_Click(object sender, EventArgs e)
        {
            page = 1;
            ShowSearchResults();
        }

        private void btnPage3_Click(object sender, EventArgs e)
        {
            page = 2;
            ShowSearchResults();
        }

        private void btnPage4_Click(object sender, EventArgs e)
        {
            page = 3;
            ShowSearchResults();
        }
    }
}
