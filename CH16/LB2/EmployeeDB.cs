﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB2
{
    public class EmployeeDB
    {
        private List<Employee> _employees = new List<Employee>();

        public EmployeeDB()
        {
            //#1
            _employees.Add(new Employee
            {
                FirstName = "Evan",
                LastName = "Gudmestad",
                SSN = "111-11-1111",
                StreetAddress = "4431 Finney Ave",
                City = "Belleville",
                State = "IL",
                Zip = "62223"
            });

            //#2
            _employees.Add(new Employee
            {
                FirstName = "Ashley",
                LastName = "Reddick",
                SSN = "111-11-1112",
                StreetAddress = "4432 Finney Ave",
                City = "Belleville",
                State = "IL",
                Zip = "62223"
            });

            //#3
            _employees.Add(new Employee
            {
                FirstName = "Charles",
                LastName = "Corrigan",
                SSN = "111-11-1113",
                StreetAddress = "4433 Finney Ave",
                City = "Belleville",
                State = "IL",
                Zip = "62223"
            });

            //#4
            _employees.Add(new Employee
            {
                FirstName = "William",
                LastName = "Webber",
                SSN = "111-11-1114",
                StreetAddress = "4434 Finney Ave",
                City = "St. Louis",
                State = "MO",
                Zip = "63113"
            });

            //#5
            _employees.Add(new Employee
            {
                FirstName = "Kasey",
                LastName = "Poettker",
                SSN = "111-11-1115",
                StreetAddress = "4435 Finney Ave",
                City = "Highland",
                State = "IL",
                Zip = "62223"
            });

            //#6
            _employees.Add(new Employee
            {
                FirstName = "John",
                LastName = "Smith",
                SSN = "111-11-1116",
                StreetAddress = "4436 Finney Ave",
                City = "St. Louis",
                State = "MO",
                Zip = "63113"
            });

            //#7
            _employees.Add(new Employee
            {
                FirstName = "Bob",
                LastName = "Burgese",
                SSN = "111-11-1117",
                StreetAddress = "4437 Finney Ave",
                City = "Chesterfield",
                State = "MO",
                Zip = "63011"
            });

            //#8
            _employees.Add(new Employee
            {
                FirstName = "Mike",
                LastName = "McMann",
                SSN = "111-11-1118",
                StreetAddress = "4438 Finney Ave",
                City = "Chesterfield",
                State = "MO",
                Zip = "63011"
            });

            //#9
            _employees.Add(new Employee
            {
                FirstName = "Shay",
                LastName = "Holmes",
                SSN = "111-11-1119",
                StreetAddress = "4439 Finney Ave",
                City = "Highland",
                State = "IL",
                Zip = "62223"
            });

            //#10
            _employees.Add(new Employee
            {
                FirstName = "Maya",
                LastName = "Gonzales",
                SSN = "111-11-1120",
                StreetAddress = "4440 Finney Ave",
                City = "St. Louis",
                State = "MO",
                Zip = "63113"
            });

            //#11
            _employees.Add(new Employee
            {
                FirstName = "Michelle",
                LastName = "Williams",
                SSN = "111-11-1121",
                StreetAddress = "4441 Finney Ave",
                City = "Chesterfield",
                State = "MO",
                Zip = "63011"
            });

            //#12
            _employees.Add(new Employee
            {
                FirstName = "Carol",
                LastName = "Lang",
                SSN = "111-11-1122",
                StreetAddress = "4442 Finney Ave",
                City = "Belleville",
                State = "IL",
                Zip = "62223"
            });

            //#13
            _employees.Add(new Employee
            {
                FirstName = "Pete",
                LastName = "Davidson",
                SSN = "111-11-1123",
                StreetAddress = "4443 Finney Ave",
                City = "St. Louis",
                State = "MO",
                Zip = "63113"
            });

            //#14
            _employees.Add(new Employee
            {
                FirstName = "Justin",
                LastName = "Timberlake",
                SSN = "111-11-1124",
                StreetAddress = "4444 Finney Ave",
                City = "Ballwin",
                State = "MO",
                Zip = "63021"
            });

            //#15
            _employees.Add(new Employee
            {
                FirstName = "Eric",
                LastName = "Bomber",
                SSN = "111-11-1125",
                StreetAddress = "4445 Finney Ave",
                City = "Ballwin",
                State = "MO",
                Zip = "63021"
            });

            //#16
            _employees.Add(new Employee
            {
                FirstName = "Jacob",
                LastName = "Sitze",
                SSN = "111-11-1126",
                StreetAddress = "4446 Finney Ave",
                City = "Belleville",
                State = "IL",
                Zip = "62223"
            });

            //#17
            _employees.Add(new Employee
            {
                FirstName = "Mark",
                LastName = "Cuban",
                SSN = "111-11-1127",
                StreetAddress = "4447 Finney Ave",
                City = "Belleville",
                State = "IL",
                Zip = "62223"
            });

            //#18
            _employees.Add(new Employee
            {
                FirstName = "Jesse",
                LastName = "Prick",
                SSN = "111-11-1128",
                StreetAddress = "4448 Finney Ave",
                City = "Highland",
                State = "IL",
                Zip = "62223"
            });

            //#19
            _employees.Add(new Employee
            {
                FirstName = "Kelly",
                LastName = "Rowland",
                SSN = "111-11-1129",
                StreetAddress = "4449 Finney Ave",
                City = "Ballwin",
                State = "MO",
                Zip = "63021"
            });

            //#20
            _employees.Add(new Employee
            {
                FirstName = "Grace",
                LastName = "Distler",
                SSN = "111-11-1130",
                StreetAddress = "853 Castle Pines Drive",
                City = "Ballwin",
                State = "MO",
                Zip = "63021"
            });
        }
        //accessor method to return the list of employees
        public List<Employee> GetEmployees()
        {
            return _employees;
        }
    }
}
