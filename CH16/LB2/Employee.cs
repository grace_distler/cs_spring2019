﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB2
{
    public class Employee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SSN { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        public string Address
        {
            get
            {
                return $"{StreetAddress}, {City}, {State} {Zip}";
            }
        }

        public override string ToString()
        {
            return $"{FirstName} {LastName}'s SSN is {SSN} and lives at {Address} ";
        }
    }
}
