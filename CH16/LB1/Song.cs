﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB1
{
    public class Song
    {
        public string Name { get; set; }
        public string Artist { get; set; }
        public string Genre { get; set; }

        public override string ToString()
        {
            return $"{Name} was performed by {Artist} and is classified as {Genre} ";
        }
    }
}
