﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB1
{
    public partial class Form1 : Form
    {
        private List<Song> myCollection;

        public Form1()
        {
            InitializeComponent();
            myCollection = new List<Song>();

            myCollection.Add(new Song() { Name = "Sorry", Artist = "Justin Bieber", Genre = "Pop" });
            myCollection.Add(new Song() { Name = "Where Are U Now?", Artist = "Justin Bieber", Genre = "Pop" });
            myCollection.Add(new Song() { Name = "Friends", Artist = "Justin Bieber", Genre = "Pop" });
            myCollection.Add(new Song() { Name = "I'm the One", Artist = "Justin Bieber", Genre = "Pop" });
 
            myCollection.Add(new Song() { Name = "bad guy", Artist = "Billie Eilish", Genre = "Alternative" });
            myCollection.Add(new Song() { Name = "when the party's over", Artist = "Billie Eilish", Genre = "Alternative" });
            myCollection.Add(new Song() { Name = "you should see me in a crown", Artist = "Billie Eilish", Genre = "Alternative" });
            myCollection.Add(new Song() { Name = "bury a friend", Artist = "Billie Eilish", Genre = "Alternative" });

            myCollection.Add(new Song() { Name = "Stronger", Artist = "Kanye West", Genre = "Rap" });
            myCollection.Add(new Song() { Name = "POWER", Artist = "Kanye West", Genre = "Rap" });
            myCollection.Add(new Song() { Name = "Gold Digger", Artist = "Kanye West", Genre = "Rap" });
            myCollection.Add(new Song() { Name = "Heartless", Artist = "Kanye West", Genre = "Rap" });
        }

        private void btnSongSearch_Click(object sender, EventArgs e)
        {
            var songsByName = myCollection.Where(x => x.Name.Equals(txtSongName.Text)).ToList();
            lstResults.DataSource = songsByName;
        }

        private void btnArtistSearch_Click(object sender, EventArgs e)
        {
            var songsByArtist = myCollection.Where(x => x.Artist == txtArtisitName.Text).ToList();
            lstResults.DataSource = songsByArtist;
        }

        private void btnGenreSearch_Click(object sender, EventArgs e)
        {
            var songsByGenre = myCollection.Where(x => x.Genre.Equals(txtGenre.Text)).ToList();
            lstResults.DataSource = songsByGenre;
        }

        private void btnSearchAll_Click(object sender, EventArgs e)
        {
            var songsBySearch = myCollection.Where(x => (x.Name == txtSearchAny.Text) ||
                                                        (x.Artist == txtSearchAny.Text) ||
                                                        (x.Genre == txtSearchAny.Text)).ToList();
            lstResults.DataSource = songsBySearch;
        }
    }
}
