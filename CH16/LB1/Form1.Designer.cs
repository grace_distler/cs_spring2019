﻿namespace LB1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtSongName = new System.Windows.Forms.TextBox();
            this.txtArtisitName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtGenre = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSongSearch = new System.Windows.Forms.Button();
            this.btnGenreSearch = new System.Windows.Forms.Button();
            this.btnArtistSearch = new System.Windows.Forms.Button();
            this.lstResults = new System.Windows.Forms.ListBox();
            this.btnSearchAll = new System.Windows.Forms.Button();
            this.txtSearchAny = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Search by Song Name:";
            // 
            // txtSongName
            // 
            this.txtSongName.Location = new System.Drawing.Point(258, 39);
            this.txtSongName.Name = "txtSongName";
            this.txtSongName.Size = new System.Drawing.Size(209, 22);
            this.txtSongName.TabIndex = 0;
            // 
            // txtArtisitName
            // 
            this.txtArtisitName.Location = new System.Drawing.Point(258, 100);
            this.txtArtisitName.Name = "txtArtisitName";
            this.txtArtisitName.Size = new System.Drawing.Size(209, 22);
            this.txtArtisitName.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Search by Artist Name:";
            // 
            // txtGenre
            // 
            this.txtGenre.Location = new System.Drawing.Point(258, 163);
            this.txtGenre.Name = "txtGenre";
            this.txtGenre.Size = new System.Drawing.Size(209, 22);
            this.txtGenre.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 166);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Search by Genre:";
            // 
            // btnSongSearch
            // 
            this.btnSongSearch.Location = new System.Drawing.Point(506, 29);
            this.btnSongSearch.Name = "btnSongSearch";
            this.btnSongSearch.Size = new System.Drawing.Size(119, 43);
            this.btnSongSearch.TabIndex = 1;
            this.btnSongSearch.Text = "Song Search";
            this.btnSongSearch.UseVisualStyleBackColor = true;
            this.btnSongSearch.Click += new System.EventHandler(this.btnSongSearch_Click);
            // 
            // btnGenreSearch
            // 
            this.btnGenreSearch.Location = new System.Drawing.Point(506, 153);
            this.btnGenreSearch.Name = "btnGenreSearch";
            this.btnGenreSearch.Size = new System.Drawing.Size(119, 43);
            this.btnGenreSearch.TabIndex = 5;
            this.btnGenreSearch.Text = "Genre Search";
            this.btnGenreSearch.UseVisualStyleBackColor = true;
            this.btnGenreSearch.Click += new System.EventHandler(this.btnGenreSearch_Click);
            // 
            // btnArtistSearch
            // 
            this.btnArtistSearch.Location = new System.Drawing.Point(506, 90);
            this.btnArtistSearch.Name = "btnArtistSearch";
            this.btnArtistSearch.Size = new System.Drawing.Size(119, 43);
            this.btnArtistSearch.TabIndex = 3;
            this.btnArtistSearch.Text = "Artist Search";
            this.btnArtistSearch.UseVisualStyleBackColor = true;
            this.btnArtistSearch.Click += new System.EventHandler(this.btnArtistSearch_Click);
            // 
            // lstResults
            // 
            this.lstResults.FormattingEnabled = true;
            this.lstResults.ItemHeight = 16;
            this.lstResults.Location = new System.Drawing.Point(43, 349);
            this.lstResults.Name = "lstResults";
            this.lstResults.Size = new System.Drawing.Size(582, 228);
            this.lstResults.TabIndex = 9;
            // 
            // btnSearchAll
            // 
            this.btnSearchAll.Location = new System.Drawing.Point(506, 215);
            this.btnSearchAll.Name = "btnSearchAll";
            this.btnSearchAll.Size = new System.Drawing.Size(119, 43);
            this.btnSearchAll.TabIndex = 12;
            this.btnSearchAll.Text = "Search";
            this.btnSearchAll.UseVisualStyleBackColor = true;
            this.btnSearchAll.Click += new System.EventHandler(this.btnSearchAll_Click);
            // 
            // txtSearchAny
            // 
            this.txtSearchAny.Location = new System.Drawing.Point(258, 225);
            this.txtSearchAny.Name = "txtSearchAny";
            this.txtSearchAny.Size = new System.Drawing.Size(209, 22);
            this.txtSearchAny.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 228);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(218, 17);
            this.label4.TabIndex = 11;
            this.label4.Text = "Search by Song, Artist, or Genre:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(673, 609);
            this.Controls.Add(this.btnSearchAll);
            this.Controls.Add(this.txtSearchAny);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lstResults);
            this.Controls.Add(this.btnArtistSearch);
            this.Controls.Add(this.btnGenreSearch);
            this.Controls.Add(this.btnSongSearch);
            this.Controls.Add(this.txtGenre);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtArtisitName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtSongName);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSongName;
        private System.Windows.Forms.TextBox txtArtisitName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtGenre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSongSearch;
        private System.Windows.Forms.Button btnGenreSearch;
        private System.Windows.Forms.Button btnArtistSearch;
        private System.Windows.Forms.ListBox lstResults;
        private System.Windows.Forms.Button btnSearchAll;
        private System.Windows.Forms.TextBox txtSearchAny;
        private System.Windows.Forms.Label label4;
    }
}

