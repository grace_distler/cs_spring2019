﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB3
{
    public class Assignment
    {
        public string AssignmentName { get; set; }
        public string AssignmentType { get; set; }
        public string AssignedStudent { get; set; }
        public float Grade { get; set; }

        public override string ToString()
        {
            return $"{AssignmentName}, {AssignmentType}, {AssignedStudent}, {Grade}";
        }
    }
}
