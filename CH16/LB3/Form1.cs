﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace LB3
{
    public partial class Form1 : Form
    {
        FileStream inFile;
        StreamReader reader;

        List<Assignment> _assignments = new List<Assignment>();

        public Form1()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            lblStudentName.Text = "";
            lblLabGrade.Text = "";
            lblTestGrade.Text = "";
            lblOverallGrade.Text = "";

            inFile = new FileStream("students.csv", FileMode.Open, FileAccess.Read);
            reader = new StreamReader(inFile);

            //read a record from the file
            string recordIn = reader.ReadLine();
            
            //to store individual pieces of data
            string[] fields;

            Assignment anAssignment;

            while(recordIn != null)
            {
                anAssignment = new Assignment();

                //split the record into fields
                fields = recordIn.Split(',');
                anAssignment.AssignmentName = fields[0];
                anAssignment.AssignmentType = fields[1];
                anAssignment.AssignedStudent = fields[2];
                anAssignment.Grade = Convert.ToSingle(fields[3]);

                _assignments.Add(anAssignment);
                recordIn = reader.ReadLine();
            }

            //read the student name from the form
            string studentName = txtStudentName.Text.ToLower();

            //LINQ filter
            var studentAssignments = _assignments.Where(x => x.AssignedStudent.ToLower().Contains(studentName));

            //filter by lab
            var studentLabs = _assignments.Where(x => x.AssignedStudent.ToLower().Contains(studentName) && x.AssignmentType.Equals("Lab"));

            //filter by tests
            var studentTests = studentAssignments.Where(x => x.AssignmentType.Equals("Hands-On-Test"));

            //determine lab
            float labAverage = Convert.ToSingle(studentLabs.Average(x => x.Grade));
            //output lab
            lblLabGrade.Text = labAverage.ToString();

            //determine test
            float testAverage = Convert.ToSingle(studentTests.Average(x => x.Grade));
            //output test
            lblTestGrade.Text = testAverage.ToString();

            //calc overall grade
            float overallGrade = (labAverage * .4F) + (testAverage * .6F);
            //output overall grade
            lblOverallGrade.Text = overallGrade.ToString("N2");

            //output name
            lblStudentName.Text = studentAssignments.FirstOrDefault().AssignedStudent;

            reader.Close();
            inFile.Close();
        }
    }
}
