﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB6
{
    class Job
    {
        private const double TRIP_FEE = 35;
        private string _description;
        private float _hoursToComplete;
        private float _hourlyFee;
        private float _totalFee;

        public Job(string description, float hoursToComplete, float hourlyFee)
        {
            _description = description;
            _hoursToComplete = hoursToComplete;
            _hourlyFee = hourlyFee;
            _totalFee = (_hourlyFee * _hoursToComplete) + (float)TRIP_FEE;
        }

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        public double HoursToComplete
        {
            get
            {
                return _hoursToComplete;
            }
            set
            {
                _hoursToComplete = (float)value;
            }
        }

        public double HourlyFee
        {
            get
            {
                return _hourlyFee;
            }
            set
            {
                _hourlyFee = (float)value;
            }
        }

        public double TotalFee
        {
            get
            {
                return _totalFee;
            }
        }
    }
}
