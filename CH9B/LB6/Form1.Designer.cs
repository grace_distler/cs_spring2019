﻿namespace LB6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.descriptionEntryTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.hoursToCompleteEntryTextBox = new System.Windows.Forms.TextBox();
            this.hourlyRateEntryTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.descriptionCurrentValueLabel = new System.Windows.Forms.Label();
            this.hoursToCompleteCurrentValueLabel = new System.Windows.Forms.Label();
            this.hourlyRateCurrentValueLabel = new System.Windows.Forms.Label();
            this.totalFeeCurrentValueLabel = new System.Windows.Forms.Label();
            this.updateButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Description";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(53, 185);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Hourly Rate";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(53, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Hours to Complete";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(53, 244);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Total Fee";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(222, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "New Value";
            // 
            // descriptionEntryTextBox
            // 
            this.descriptionEntryTextBox.Location = new System.Drawing.Point(225, 69);
            this.descriptionEntryTextBox.Name = "descriptionEntryTextBox";
            this.descriptionEntryTextBox.Size = new System.Drawing.Size(244, 22);
            this.descriptionEntryTextBox.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(545, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Current Value";
            // 
            // hoursToCompleteEntryTextBox
            // 
            this.hoursToCompleteEntryTextBox.Location = new System.Drawing.Point(225, 129);
            this.hoursToCompleteEntryTextBox.Name = "hoursToCompleteEntryTextBox";
            this.hoursToCompleteEntryTextBox.Size = new System.Drawing.Size(244, 22);
            this.hoursToCompleteEntryTextBox.TabIndex = 2;
            // 
            // hourlyRateEntryTextBox
            // 
            this.hourlyRateEntryTextBox.Location = new System.Drawing.Point(225, 182);
            this.hourlyRateEntryTextBox.Name = "hourlyRateEntryTextBox";
            this.hourlyRateEntryTextBox.Size = new System.Drawing.Size(244, 22);
            this.hourlyRateEntryTextBox.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(222, 244);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 17);
            this.label7.TabIndex = 9;
            this.label7.Text = "---";
            // 
            // descriptionCurrentValueLabel
            // 
            this.descriptionCurrentValueLabel.AutoSize = true;
            this.descriptionCurrentValueLabel.Location = new System.Drawing.Point(545, 72);
            this.descriptionCurrentValueLabel.Name = "descriptionCurrentValueLabel";
            this.descriptionCurrentValueLabel.Size = new System.Drawing.Size(0, 17);
            this.descriptionCurrentValueLabel.TabIndex = 10;
            // 
            // hoursToCompleteCurrentValueLabel
            // 
            this.hoursToCompleteCurrentValueLabel.AutoSize = true;
            this.hoursToCompleteCurrentValueLabel.Location = new System.Drawing.Point(545, 129);
            this.hoursToCompleteCurrentValueLabel.Name = "hoursToCompleteCurrentValueLabel";
            this.hoursToCompleteCurrentValueLabel.Size = new System.Drawing.Size(0, 17);
            this.hoursToCompleteCurrentValueLabel.TabIndex = 11;
            // 
            // hourlyRateCurrentValueLabel
            // 
            this.hourlyRateCurrentValueLabel.AutoSize = true;
            this.hourlyRateCurrentValueLabel.Location = new System.Drawing.Point(545, 185);
            this.hourlyRateCurrentValueLabel.Name = "hourlyRateCurrentValueLabel";
            this.hourlyRateCurrentValueLabel.Size = new System.Drawing.Size(0, 17);
            this.hourlyRateCurrentValueLabel.TabIndex = 12;
            // 
            // totalFeeCurrentValueLabel
            // 
            this.totalFeeCurrentValueLabel.AutoSize = true;
            this.totalFeeCurrentValueLabel.Location = new System.Drawing.Point(545, 244);
            this.totalFeeCurrentValueLabel.Name = "totalFeeCurrentValueLabel";
            this.totalFeeCurrentValueLabel.Size = new System.Drawing.Size(0, 17);
            this.totalFeeCurrentValueLabel.TabIndex = 13;
            // 
            // updateButton
            // 
            this.updateButton.Location = new System.Drawing.Point(225, 297);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(137, 31);
            this.updateButton.TabIndex = 14;
            this.updateButton.Text = "Update";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // Form1
            // 
            this.AcceptButton = this.updateButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 363);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.totalFeeCurrentValueLabel);
            this.Controls.Add(this.hourlyRateCurrentValueLabel);
            this.Controls.Add(this.hoursToCompleteCurrentValueLabel);
            this.Controls.Add(this.descriptionCurrentValueLabel);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.hourlyRateEntryTextBox);
            this.Controls.Add(this.hoursToCompleteEntryTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.descriptionEntryTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Harold\'s Home Services";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox descriptionEntryTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox hoursToCompleteEntryTextBox;
        private System.Windows.Forms.TextBox hourlyRateEntryTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label descriptionCurrentValueLabel;
        private System.Windows.Forms.Label hoursToCompleteCurrentValueLabel;
        private System.Windows.Forms.Label hourlyRateCurrentValueLabel;
        private System.Windows.Forms.Label totalFeeCurrentValueLabel;
        private System.Windows.Forms.Button updateButton;
    }
}

