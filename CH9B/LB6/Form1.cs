﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Job service;

        private void updateButton_Click(object sender, EventArgs e)
        {
            string description = descriptionEntryTextBox.Text;
            float hoursToComplete = float.Parse(hoursToCompleteEntryTextBox.Text);
            float hourlyRate = float.Parse(hourlyRateEntryTextBox.Text);

            if (service == null)
            {
                service = new Job(description, hoursToComplete, hourlyRate);
            }
            else
            {
                service.Description = description;
                service.HoursToComplete = hoursToComplete;
                service.HourlyFee = hourlyRate;
            }
            descriptionCurrentValueLabel.Text = service.Description;
            hoursToCompleteCurrentValueLabel.Text = String.Format("{0:0.00}", service.HoursToComplete) + " hours";
            hourlyRateCurrentValueLabel.Text = String.Format("{0}", service.HourlyFee.ToString("C"));
            totalFeeCurrentValueLabel.Text= String.Format("{0}", service.TotalFee.ToString("C"));
        }
    }
}
