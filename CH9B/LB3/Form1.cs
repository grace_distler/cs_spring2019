﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB3
{
    public partial class Form1 : Form
    {
        private Account[] accounts;
        private int count = 5;
        private string accountNumber;
        private string pinNumber;
        private int userAccount;

        public Form1()
        {
            InitializeComponent();

            accounts = new Account[count];
            accounts[0] = new Account("123-456-789", "1234", "Mr. Smith", 10000);
            accounts[1] = new Account("999-999-999", "9037", "Ms. Lawernce", 5000);
            accounts[2] = new Account("987-654-321", "4321", "Mr. Brown", 3500);
            accounts[3] = new Account("222-222-222", "2217", "Mr. Marion", 25000);
            accounts[4] = new Account("314-635-150", "5150", "Ms. Distler", 16500);
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            accountNumber = accountNumberTextBox.Text;
            pinNumber = pinNumberTextBox.Text;
            Login(accountNumber, pinNumber);
        }

        public bool Login(string accountNumber, string pinNumber)
        {
            bool found = false;

            for(int i = 0; i < count; i++)
            {
                if (accountNumber.Equals(accounts[i].GetAccountNumber()) && pinNumber.Equals(accounts[i].GetPin()))
                {
                    found = true;
                    userAccount = i;
                    welcomeLabel.Text = "Welcome " + accounts[i].GetName() + "!";
                    accountBalanceLabel.Text = "Your account balance is " + accounts[i].GetBalance().ToString("C");
                }
            }

            if (found == false)
            {
                accountNumberTextBox.Text = "";
                pinNumberTextBox.Text = "";
                welcomeLabel.Text = "User Not Found. Please try again.";
                accountBalanceLabel.Text = "";
                depositEntryTextBox.Text = "";
                withdrawalEntryTextBox.Text = "";
            }
            return found;
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            Logout();
        }

        public void Logout()
        {
            accountNumberTextBox.Text = "";
            pinNumberTextBox.Text = "";
            welcomeLabel.Text = "";
            accountBalanceLabel.Text = "";
            depositEntryTextBox.Text = "";
            withdrawalEntryTextBox.Text = "";
            logoutReturnLabel.Text = "You have been logged out.";
        }

        private void accountNumberTextBox_TextChanged(object sender, EventArgs e)
        {
            logoutReturnLabel.Text = "";
        }

        private void depositButton_Click(object sender, EventArgs e)
        {
            if (Login(accountNumber, pinNumber) == true)
            {
                decimal depositAmount = Convert.ToDecimal(depositEntryTextBox.Text);
                MakeDeposit(depositAmount);
            }
            else
            {
                accountBalanceLabel.Text = "You must login to make a deposit.";
            }
        }

        public void MakeDeposit(decimal depositAmount)
        {
            accounts[userAccount].MakeDeposit(depositAmount);
            accountBalanceLabel.Text = "Your account balance is " + accounts[userAccount].GetBalance().ToString("C");
        }

        private void withdrawButton_Click(object sender, EventArgs e)
        {
            if (Login(accountNumber, pinNumber) == true)
            {
                decimal withdrawalAmount = Convert.ToDecimal(withdrawalEntryTextBox.Text);
                MakeWithdrawal(withdrawalAmount);
            }
            else
            {
                accountBalanceLabel.Text = "You must login to make a withdrawal.";
            }
        }

        public void MakeWithdrawal(decimal withdrawalAmount)
        {
            accounts[userAccount].MakeWithdrawal(withdrawalAmount);
            accountBalanceLabel.Text = "Your account balance is " + accounts[userAccount].GetBalance().ToString("C");
        }
    }
}
