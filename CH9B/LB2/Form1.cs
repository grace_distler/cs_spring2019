﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB2
{
    public partial class Form1 : Form
    {
        private static int count = 20;
        private Item[] storeItems = new Item[count];
        private int sizeStoreItems = 10;
        private int itemIndex;

        public Form1()
        {
            InitializeComponent();
            storeItems = new Item[count];
            storeItems[0] = new Item("Bottle Rocket", "1234-5678", 15, 100, 50, "Black Cat");
            storeItems[1] = new Item("G.I. Joe", "1111-1111", 28, 168, 30, "Toynk Toys");
            storeItems[2] = new Item("Slinky", "2222-2222", 3, 180, 100, "SLINKY");
            storeItems[3] = new Item("Etch A Sketch", "3333-3333", 13.49, 85, 25, "Spin-Master");
            storeItems[4] = new Item("Play-Doh", "4444-4444", 1.99, 120, 175, "Hasbro");
            storeItems[5] = new Item("Mr. Potato Head", "5555-5555", 7.99, 50, 25, "Playskool");
            storeItems[6] = new Item("Lite-Brite", "6666-6666", 5.99, 36, 15, "Basic Fun");
            storeItems[7] = new Item("Magic 8-Ball", "7777-7777", 13, 80, 60, "Mattel");
            storeItems[8] = new Item("Lincoln Log", "8888-8888", 14.97, 95, 20, "Lincoln Logs");
            storeItems[9] = new Item("Barbie", "9999-9999", 9.99, 75, 40, "Mattel");
            storeItems[10] = new Item();
            storeItems[11] = new Item();
            storeItems[12] = new Item();
            storeItems[13] = new Item();
            storeItems[14] = new Item();
            storeItems[15] = new Item();
            storeItems[16] = new Item();
            storeItems[17] = new Item();
            storeItems[18] = new Item();
            storeItems[19] = new Item();
        }

        private void searchByNameButton_Click(object sender, EventArgs e)
        {
            string itemName = searchByNameTextBox.Text;
            SearchByName(itemName);
        }

        public void SearchByName(string itemName)
        {
            bool found = false;
            itemName = searchByNameTextBox.Text.ToLower();
            for (int i = 0; i < count; i++)
            {
                if (itemName.Equals(storeItems[i].GetName().ToLower()))
                {
                    found = true;
                    itemIndex = i;
                    nameLabel.Text = "Name: " + storeItems[i].GetName();
                    upcLabel.Text = "UPC: " + storeItems[i].GetUPC();
                    priceLabel.Text = "Store Price: " + storeItems[i].GetStorePrice().ToString("C");
                    costPerCaseLabel.Text = "Cost Per Case: " + storeItems[i].GetCostPerCase().ToString("C");
                    unitsPerCaseLabel.Text = "Units Per Case: " + storeItems[i].GetUnitsPerCase().ToString("C");
                    distributorLabel.Text = "Distributor: " + storeItems[i].GetDistributor();
                }
            }
            if (found == false)
            {
                nameLabel.Text = "Name: Item Not Found";
                upcLabel.Text = "UPC: Item Not Found";
                priceLabel.Text = "Store Price: ";
                costPerCaseLabel.Text = "Cost Per Case: ";
                unitsPerCaseLabel.Text = "Units Per Case: ";
                distributorLabel.Text = "Distributor: ";
            }
        }

        private void searchByUPCButton_Click(object sender, EventArgs e)
        {
            string itemUPC = searchByUPCTextBox.Text;
            SearchByUPC(itemUPC);
        }

        public void SearchByUPC(string itemUPC)
        {
            bool found = false;
            itemUPC = searchByUPCTextBox.Text;
            for (int i = 0; i < count; i++)
            {
                if (itemUPC.Equals(storeItems[i].GetUPC()))
                {
                    found = true;
                    itemIndex = i;
                    nameLabel.Text = "Name: " + storeItems[i].GetName();
                    upcLabel.Text = "UPC: " + storeItems[i].GetUPC();
                    priceLabel.Text = "Store Price: " + storeItems[i].GetStorePrice().ToString("C");
                    costPerCaseLabel.Text = "Cost Per Case: " + storeItems[i].GetCostPerCase().ToString("C");
                    unitsPerCaseLabel.Text = "Units Per Case: " + storeItems[i].GetUnitsPerCase().ToString("C");
                    distributorLabel.Text = "Distributor: " + storeItems[i].GetDistributor();
                }
            }
            if (found == false)
            {
                nameLabel.Text = "Name: Item Not Found";
                upcLabel.Text = "UPC: Item Not Found";
                priceLabel.Text = "Store Price: ";
                costPerCaseLabel.Text = "Cost Per Case: ";
                unitsPerCaseLabel.Text = "Units Per Case: ";
                distributorLabel.Text = "Distributor: ";
            }
        }

        private void updatePriceButton_Click(object sender, EventArgs e)
        {
            string accessKey = priceChangeAccessKeyTextBox.Text;
            string newPrice = newPriceTextBox.Text;
            UpdatePrice(accessKey, newPrice);
        }

        public void UpdatePrice(string accessKey, string newPrice)
        {
            if (accessKey.Equals("1234"))
            {
                double priceAsDouble = 0;
                priceAsDouble = Convert.ToDouble(newPrice);
                storeItems[itemIndex].SetStorePrice(priceAsDouble);
                changePriceErrorLabel.Text = "Price Updated";

                nameLabel.Text = "Name: ";
                upcLabel.Text = "UPC: ";
                priceLabel.Text = "Store Price: ";
                costPerCaseLabel.Text = "Cost Per Case: ";
                unitsPerCaseLabel.Text = "Units Per Case: ";
                distributorLabel.Text = "Distributor: ";
                deleteItemErrorLabel.Text = "";
                addItemErrorLabel.Text = "";
                addItemNameTextBox.Text = "";
                addItemPriceTextBox.Text = "";
                addItemAccessKeyTextBox.Text = "";
                addItemCostPerCaseTextBox.Text = "";
                addItemUnitsPerCaseTextBox.Text = "";
                addItemDistributorTextBox.Text = "";
            }
            else
            {
                changePriceErrorLabel.Text = "Access Key Incorrect";
            }

        }

        private void deleteItemButton_Click(object sender, EventArgs e)
        {
            string upc = verifyUPCTextBox.Text;
            string accessKey = deleteItemAccessKeyTextBox.Text;
            DeleteItem(accessKey, upc);
        }

        public void DeleteItem(string accessKey, string upc)
        {
            upc = verifyUPCTextBox.Text;
            accessKey = deleteItemAccessKeyTextBox.Text;
            if (!accessKey.Equals("1234"))
            {
                deleteItemErrorLabel.Text = "Access Key Incorrect";
            }
            else if (!storeItems[itemIndex].GetUPC().Equals(upc))
            {
                deleteItemErrorLabel.Text = "UPC Incorrect";
            }
            else
            {
                for (int i = itemIndex; i < count - 1; i++)
                {
                    storeItems[itemIndex] = storeItems[itemIndex + 1];
                }
                sizeStoreItems--;

                if (!storeItems[itemIndex].GetUPC().Equals(upc))
                {
                    deleteItemErrorLabel.Text = "Item Deleted";
                    nameLabel.Text = "Name: ";
                    upcLabel.Text = "UPC: ";
                    priceLabel.Text = "Store Price: ";
                    costPerCaseLabel.Text = "Cost Per Case: ";
                    unitsPerCaseLabel.Text = "Units Per Case: ";
                    distributorLabel.Text = "Distributor: ";
                    changePriceErrorLabel.Text = "";
                    addItemErrorLabel.Text = "";
                    addItemNameTextBox.Text = "";
                    addItemPriceTextBox.Text = "";
                    addItemAccessKeyTextBox.Text = "";
                    addItemCostPerCaseTextBox.Text = "";
                    addItemUnitsPerCaseTextBox.Text = "";
                    addItemDistributorTextBox.Text = "";
                }
            }
        }

        private void addItemButton_Click(object sender, EventArgs e)
        {
            string name = "", upc = "", accessKey = "", distributor = "";
            double storePrice = 0, costPerCase = 0, unitPerCase = 0;
            name = addItemNameTextBox.Text;
            upc = addItemUPCTextBox.Text;
            storePrice = Convert.ToDouble(addItemPriceTextBox.Text);
            accessKey = addItemAccessKeyTextBox.Text;
            costPerCase = Convert.ToDouble(addItemCostPerCaseTextBox.Text);
            unitPerCase = Convert.ToDouble(addItemUnitsPerCaseTextBox.Text);
            distributor = addItemDistributorTextBox.Text;
            AddItem(name, upc, storePrice, accessKey, costPerCase, unitPerCase, distributor);
        }

        public void AddItem(string name, string upc, double storePrice, string accessKey, double costPerCase, double unitsPerCase, string distributor)
        {
            if (!accessKey.Equals("1234"))
            {
                addItemErrorLabel.Text = "Access Key Incorrect";
            }
            else if (sizeStoreItems > count)
            {
                addItemErrorLabel.Text = "Cannot Add Another Item";
            }
            else
            {
                storeItems[sizeStoreItems].SetName(name);
                storeItems[sizeStoreItems].SetUPC(upc);
                storeItems[sizeStoreItems].SetStorePrice(storePrice);
                storeItems[sizeStoreItems].SetCostPerCase(costPerCase);
                storeItems[sizeStoreItems].SetUnitsPerCase(unitsPerCase);
                storeItems[sizeStoreItems].SetDistributor(distributor);
                sizeStoreItems++;

                addItemErrorLabel.Text = "Item Added";

                nameLabel.Text = "Name: ";
                upcLabel.Text = "UPC: ";
                priceLabel.Text = "Store Price: ";
                costPerCaseLabel.Text = "Cost Per Case: ";
                unitsPerCaseLabel.Text = "Units Per Case: ";
                distributorLabel.Text = "Distributor: ";
                deleteItemErrorLabel.Text = "";
                changePriceErrorLabel.Text = "";
                addItemNameTextBox.Text = "";
                addItemUPCTextBox.Text = "";
                addItemPriceTextBox.Text = "";
                addItemAccessKeyTextBox.Text = "";
                addItemCostPerCaseTextBox.Text = "";
                addItemUnitsPerCaseTextBox.Text = "";
                addItemDistributorTextBox.Text = "";
                addItemErrorLabel.Text = "Item Added";

            }
        }

        private void newPriceTextBox_TextChanged(object sender, EventArgs e)
        {
            changePriceErrorLabel.Text = "";
        }

        private void verifyUPCTextBox_TextChanged(object sender, EventArgs e)
        {
            deleteItemErrorLabel.Text = "";
        }

        private void addItemAccessKeyTextBox_TextChanged(object sender, EventArgs e)
        {
            addItemErrorLabel.Text = "";
        }

        private void searchByUPCTextBox_TextChanged(object sender, EventArgs e)
        {
            nameLabel.Text = "Name: ";
            upcLabel.Text = "UPC: ";
            priceLabel.Text = "Store Price: ";
            costPerCaseLabel.Text = "Cost Per Case: ";
            unitsPerCaseLabel.Text = "Units Per Case: ";
            distributorLabel.Text = "Distributor: ";
            searchByNameTextBox.Text = "";
            changePriceErrorLabel.Text = "";
            newPriceTextBox.Text = "";
            priceChangeAccessKeyTextBox.Text = "";
            deleteItemErrorLabel.Text = "";
            verifyUPCTextBox.Text = "";
            deleteItemAccessKeyTextBox.Text = "";
            addItemNameTextBox.Text = "";
            addItemPriceTextBox.Text = "";
            addItemAccessKeyTextBox.Text = "";
            addItemCostPerCaseTextBox.Text = "";
            addItemUnitsPerCaseTextBox.Text = "";
            addItemDistributorTextBox.Text = "";
        }

        private void searchByNameTextBox_TextChanged(object sender, EventArgs e)
        {
            nameLabel.Text = "Name: ";
            upcLabel.Text = "UPC: ";
            priceLabel.Text = "Store Price: ";
            costPerCaseLabel.Text = "Cost Per Case: ";
            unitsPerCaseLabel.Text = "Units Per Case: ";
            distributorLabel.Text = "Distributor: ";
            searchByUPCTextBox.Text = "";
            changePriceErrorLabel.Text = "";
            newPriceTextBox.Text = "";
            priceChangeAccessKeyTextBox.Text = "";
            deleteItemErrorLabel.Text = "";
            verifyUPCTextBox.Text = "";
            deleteItemAccessKeyTextBox.Text = "";
            addItemNameTextBox.Text = "";
            addItemPriceTextBox.Text = "";
            addItemAccessKeyTextBox.Text = "";
            addItemCostPerCaseTextBox.Text = "";
            addItemUnitsPerCaseTextBox.Text = "";
            addItemDistributorTextBox.Text = "";
        }
    }
}
