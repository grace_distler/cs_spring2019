﻿namespace LB2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.searchByNameTextBox = new System.Windows.Forms.TextBox();
            this.searchByNameButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.searchByUPCTextBox = new System.Windows.Forms.TextBox();
            this.searchByUPCButton = new System.Windows.Forms.Button();
            this.nameLabel = new System.Windows.Forms.Label();
            this.upcLabel = new System.Windows.Forms.Label();
            this.priceLabel = new System.Windows.Forms.Label();
            this.costPerCaseLabel = new System.Windows.Forms.Label();
            this.distributorLabel = new System.Windows.Forms.Label();
            this.unitsPerCaseLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.changePriceErrorLabel = new System.Windows.Forms.Label();
            this.updatePriceButton = new System.Windows.Forms.Button();
            this.priceChangeAccessKeyTextBox = new System.Windows.Forms.TextBox();
            this.newPriceTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.deleteItemErrorLabel = new System.Windows.Forms.Label();
            this.deleteItemButton = new System.Windows.Forms.Button();
            this.deleteItemAccessKeyTextBox = new System.Windows.Forms.TextBox();
            this.verifyUPCTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.addItemErrorLabel = new System.Windows.Forms.Label();
            this.addItemButton = new System.Windows.Forms.Button();
            this.addItemDistributorTextBox = new System.Windows.Forms.TextBox();
            this.addItemUnitsPerCaseTextBox = new System.Windows.Forms.TextBox();
            this.addItemCostPerCaseTextBox = new System.Windows.Forms.TextBox();
            this.addItemAccessKeyTextBox = new System.Windows.Forms.TextBox();
            this.addItemPriceTextBox = new System.Windows.Forms.TextBox();
            this.addItemUPCTextBox = new System.Windows.Forms.TextBox();
            this.addItemNameTextBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Search by Name:";
            // 
            // searchByNameTextBox
            // 
            this.searchByNameTextBox.Location = new System.Drawing.Point(43, 51);
            this.searchByNameTextBox.Name = "searchByNameTextBox";
            this.searchByNameTextBox.Size = new System.Drawing.Size(199, 22);
            this.searchByNameTextBox.TabIndex = 1;
            this.searchByNameTextBox.TextChanged += new System.EventHandler(this.searchByNameTextBox_TextChanged);
            // 
            // searchByNameButton
            // 
            this.searchByNameButton.Location = new System.Drawing.Point(248, 49);
            this.searchByNameButton.Name = "searchByNameButton";
            this.searchByNameButton.Size = new System.Drawing.Size(104, 26);
            this.searchByNameButton.TabIndex = 2;
            this.searchByNameButton.Text = "Search";
            this.searchByNameButton.UseVisualStyleBackColor = true;
            this.searchByNameButton.Click += new System.EventHandler(this.searchByNameButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(432, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Search by UPC:";
            // 
            // searchByUPCTextBox
            // 
            this.searchByUPCTextBox.Location = new System.Drawing.Point(435, 51);
            this.searchByUPCTextBox.Name = "searchByUPCTextBox";
            this.searchByUPCTextBox.Size = new System.Drawing.Size(199, 22);
            this.searchByUPCTextBox.TabIndex = 4;
            this.searchByUPCTextBox.TextChanged += new System.EventHandler(this.searchByUPCTextBox_TextChanged);
            // 
            // searchByUPCButton
            // 
            this.searchByUPCButton.Location = new System.Drawing.Point(640, 49);
            this.searchByUPCButton.Name = "searchByUPCButton";
            this.searchByUPCButton.Size = new System.Drawing.Size(104, 26);
            this.searchByUPCButton.TabIndex = 5;
            this.searchByUPCButton.Text = "Search";
            this.searchByUPCButton.UseVisualStyleBackColor = true;
            this.searchByUPCButton.Click += new System.EventHandler(this.searchByUPCButton_Click);
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(51, 108);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(53, 17);
            this.nameLabel.TabIndex = 6;
            this.nameLabel.Text = "Name: ";
            // 
            // upcLabel
            // 
            this.upcLabel.AutoSize = true;
            this.upcLabel.Location = new System.Drawing.Point(51, 139);
            this.upcLabel.Name = "upcLabel";
            this.upcLabel.Size = new System.Drawing.Size(44, 17);
            this.upcLabel.TabIndex = 7;
            this.upcLabel.Text = "UPC: ";
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Location = new System.Drawing.Point(51, 168);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(86, 17);
            this.priceLabel.TabIndex = 8;
            this.priceLabel.Text = "Store Price: ";
            // 
            // costPerCaseLabel
            // 
            this.costPerCaseLabel.AutoSize = true;
            this.costPerCaseLabel.Location = new System.Drawing.Point(432, 108);
            this.costPerCaseLabel.Name = "costPerCaseLabel";
            this.costPerCaseLabel.Size = new System.Drawing.Size(106, 17);
            this.costPerCaseLabel.TabIndex = 9;
            this.costPerCaseLabel.Text = "Cost Per Case: ";
            // 
            // distributorLabel
            // 
            this.distributorLabel.AutoSize = true;
            this.distributorLabel.Location = new System.Drawing.Point(432, 168);
            this.distributorLabel.Name = "distributorLabel";
            this.distributorLabel.Size = new System.Drawing.Size(81, 17);
            this.distributorLabel.TabIndex = 10;
            this.distributorLabel.Text = "Distributor: ";
            // 
            // unitsPerCaseLabel
            // 
            this.unitsPerCaseLabel.AutoSize = true;
            this.unitsPerCaseLabel.Location = new System.Drawing.Point(432, 139);
            this.unitsPerCaseLabel.Name = "unitsPerCaseLabel";
            this.unitsPerCaseLabel.Size = new System.Drawing.Size(110, 17);
            this.unitsPerCaseLabel.TabIndex = 11;
            this.unitsPerCaseLabel.Text = "Units Per Case: ";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.changePriceErrorLabel);
            this.panel1.Controls.Add(this.updatePriceButton);
            this.panel1.Controls.Add(this.priceChangeAccessKeyTextBox);
            this.panel1.Controls.Add(this.newPriceTextBox);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(43, 233);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(277, 297);
            this.panel1.TabIndex = 12;
            // 
            // changePriceErrorLabel
            // 
            this.changePriceErrorLabel.AutoSize = true;
            this.changePriceErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.changePriceErrorLabel.Location = new System.Drawing.Point(72, 253);
            this.changePriceErrorLabel.Name = "changePriceErrorLabel";
            this.changePriceErrorLabel.Size = new System.Drawing.Size(0, 17);
            this.changePriceErrorLabel.TabIndex = 6;
            // 
            // updatePriceButton
            // 
            this.updatePriceButton.Location = new System.Drawing.Point(75, 210);
            this.updatePriceButton.Name = "updatePriceButton";
            this.updatePriceButton.Size = new System.Drawing.Size(124, 25);
            this.updatePriceButton.TabIndex = 5;
            this.updatePriceButton.Text = "Update Price";
            this.updatePriceButton.UseVisualStyleBackColor = true;
            this.updatePriceButton.Click += new System.EventHandler(this.updatePriceButton_Click);
            // 
            // priceChangeAccessKeyTextBox
            // 
            this.priceChangeAccessKeyTextBox.Location = new System.Drawing.Point(120, 141);
            this.priceChangeAccessKeyTextBox.Name = "priceChangeAccessKeyTextBox";
            this.priceChangeAccessKeyTextBox.PasswordChar = '*';
            this.priceChangeAccessKeyTextBox.Size = new System.Drawing.Size(119, 22);
            this.priceChangeAccessKeyTextBox.TabIndex = 4;
            // 
            // newPriceTextBox
            // 
            this.newPriceTextBox.Location = new System.Drawing.Point(120, 71);
            this.newPriceTextBox.Name = "newPriceTextBox";
            this.newPriceTextBox.Size = new System.Drawing.Size(119, 22);
            this.newPriceTextBox.TabIndex = 3;
            this.newPriceTextBox.TextChanged += new System.EventHandler(this.newPriceTextBox_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "Access Key: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "New Price: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(68, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(149, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Change Store Price";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.deleteItemErrorLabel);
            this.panel2.Controls.Add(this.deleteItemButton);
            this.panel2.Controls.Add(this.deleteItemAccessKeyTextBox);
            this.panel2.Controls.Add(this.verifyUPCTextBox);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Location = new System.Drawing.Point(435, 233);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(277, 297);
            this.panel2.TabIndex = 13;
            // 
            // deleteItemErrorLabel
            // 
            this.deleteItemErrorLabel.AutoSize = true;
            this.deleteItemErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.deleteItemErrorLabel.Location = new System.Drawing.Point(78, 257);
            this.deleteItemErrorLabel.Name = "deleteItemErrorLabel";
            this.deleteItemErrorLabel.Size = new System.Drawing.Size(0, 17);
            this.deleteItemErrorLabel.TabIndex = 13;
            // 
            // deleteItemButton
            // 
            this.deleteItemButton.Location = new System.Drawing.Point(81, 214);
            this.deleteItemButton.Name = "deleteItemButton";
            this.deleteItemButton.Size = new System.Drawing.Size(124, 25);
            this.deleteItemButton.TabIndex = 12;
            this.deleteItemButton.Text = "Delete Item";
            this.deleteItemButton.UseVisualStyleBackColor = true;
            this.deleteItemButton.Click += new System.EventHandler(this.deleteItemButton_Click);
            // 
            // deleteItemAccessKeyTextBox
            // 
            this.deleteItemAccessKeyTextBox.Location = new System.Drawing.Point(126, 145);
            this.deleteItemAccessKeyTextBox.Name = "deleteItemAccessKeyTextBox";
            this.deleteItemAccessKeyTextBox.PasswordChar = '*';
            this.deleteItemAccessKeyTextBox.Size = new System.Drawing.Size(119, 22);
            this.deleteItemAccessKeyTextBox.TabIndex = 11;
            // 
            // verifyUPCTextBox
            // 
            this.verifyUPCTextBox.Location = new System.Drawing.Point(126, 75);
            this.verifyUPCTextBox.Name = "verifyUPCTextBox";
            this.verifyUPCTextBox.Size = new System.Drawing.Size(119, 22);
            this.verifyUPCTextBox.TabIndex = 10;
            this.verifyUPCTextBox.TextChanged += new System.EventHandler(this.verifyUPCTextBox_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 148);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 17);
            this.label7.TabIndex = 9;
            this.label7.Text = "Access Key: ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(31, 78);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 17);
            this.label8.TabIndex = 8;
            this.label8.Text = "Verify UPC: ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(100, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 17);
            this.label9.TabIndex = 7;
            this.label9.Text = "Delete Item";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.addItemErrorLabel);
            this.panel3.Controls.Add(this.addItemButton);
            this.panel3.Controls.Add(this.addItemDistributorTextBox);
            this.panel3.Controls.Add(this.addItemUnitsPerCaseTextBox);
            this.panel3.Controls.Add(this.addItemCostPerCaseTextBox);
            this.panel3.Controls.Add(this.addItemAccessKeyTextBox);
            this.panel3.Controls.Add(this.addItemPriceTextBox);
            this.panel3.Controls.Add(this.addItemUPCTextBox);
            this.panel3.Controls.Add(this.addItemNameTextBox);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Location = new System.Drawing.Point(43, 583);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(669, 286);
            this.panel3.TabIndex = 14;
            // 
            // addItemErrorLabel
            // 
            this.addItemErrorLabel.AutoSize = true;
            this.addItemErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.addItemErrorLabel.Location = new System.Drawing.Point(64, 246);
            this.addItemErrorLabel.Name = "addItemErrorLabel";
            this.addItemErrorLabel.Size = new System.Drawing.Size(0, 17);
            this.addItemErrorLabel.TabIndex = 27;
            // 
            // addItemButton
            // 
            this.addItemButton.Location = new System.Drawing.Point(467, 206);
            this.addItemButton.Name = "addItemButton";
            this.addItemButton.Size = new System.Drawing.Size(124, 25);
            this.addItemButton.TabIndex = 26;
            this.addItemButton.Text = "Add Item";
            this.addItemButton.UseVisualStyleBackColor = true;
            this.addItemButton.Click += new System.EventHandler(this.addItemButton_Click);
            // 
            // addItemDistributorTextBox
            // 
            this.addItemDistributorTextBox.Location = new System.Drawing.Point(473, 149);
            this.addItemDistributorTextBox.Name = "addItemDistributorTextBox";
            this.addItemDistributorTextBox.Size = new System.Drawing.Size(178, 22);
            this.addItemDistributorTextBox.TabIndex = 25;
            // 
            // addItemUnitsPerCaseTextBox
            // 
            this.addItemUnitsPerCaseTextBox.Location = new System.Drawing.Point(473, 111);
            this.addItemUnitsPerCaseTextBox.Name = "addItemUnitsPerCaseTextBox";
            this.addItemUnitsPerCaseTextBox.Size = new System.Drawing.Size(178, 22);
            this.addItemUnitsPerCaseTextBox.TabIndex = 24;
            // 
            // addItemCostPerCaseTextBox
            // 
            this.addItemCostPerCaseTextBox.Location = new System.Drawing.Point(473, 66);
            this.addItemCostPerCaseTextBox.Name = "addItemCostPerCaseTextBox";
            this.addItemCostPerCaseTextBox.Size = new System.Drawing.Size(178, 22);
            this.addItemCostPerCaseTextBox.TabIndex = 23;
            // 
            // addItemAccessKeyTextBox
            // 
            this.addItemAccessKeyTextBox.Location = new System.Drawing.Point(120, 200);
            this.addItemAccessKeyTextBox.Name = "addItemAccessKeyTextBox";
            this.addItemAccessKeyTextBox.PasswordChar = '*';
            this.addItemAccessKeyTextBox.Size = new System.Drawing.Size(178, 22);
            this.addItemAccessKeyTextBox.TabIndex = 22;
            this.addItemAccessKeyTextBox.TextChanged += new System.EventHandler(this.addItemAccessKeyTextBox_TextChanged);
            // 
            // addItemPriceTextBox
            // 
            this.addItemPriceTextBox.Location = new System.Drawing.Point(120, 151);
            this.addItemPriceTextBox.Name = "addItemPriceTextBox";
            this.addItemPriceTextBox.Size = new System.Drawing.Size(178, 22);
            this.addItemPriceTextBox.TabIndex = 21;
            // 
            // addItemUPCTextBox
            // 
            this.addItemUPCTextBox.Location = new System.Drawing.Point(120, 111);
            this.addItemUPCTextBox.Name = "addItemUPCTextBox";
            this.addItemUPCTextBox.Size = new System.Drawing.Size(178, 22);
            this.addItemUPCTextBox.TabIndex = 20;
            // 
            // addItemNameTextBox
            // 
            this.addItemNameTextBox.Location = new System.Drawing.Point(120, 66);
            this.addItemNameTextBox.Name = "addItemNameTextBox";
            this.addItemNameTextBox.Size = new System.Drawing.Size(178, 22);
            this.addItemNameTextBox.TabIndex = 19;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(25, 203);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(89, 17);
            this.label16.TabIndex = 18;
            this.label16.Text = "Access Key: ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(343, 114);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(110, 17);
            this.label10.TabIndex = 17;
            this.label10.Text = "Units Per Case: ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(347, 154);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 17);
            this.label11.TabIndex = 16;
            this.label11.Text = "Distributor: ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(347, 69);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(106, 17);
            this.label12.TabIndex = 15;
            this.label12.Text = "Cost Per Case: ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(25, 154);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 17);
            this.label13.TabIndex = 14;
            this.label13.Text = "Store Price: ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(25, 114);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 17);
            this.label14.TabIndex = 13;
            this.label14.Text = "UPC: ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(25, 69);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 17);
            this.label15.TabIndex = 12;
            this.label15.Text = "Name: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(72, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Add New Item";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 913);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.unitsPerCaseLabel);
            this.Controls.Add(this.distributorLabel);
            this.Controls.Add(this.costPerCaseLabel);
            this.Controls.Add(this.priceLabel);
            this.Controls.Add(this.upcLabel);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.searchByUPCButton);
            this.Controls.Add(this.searchByUPCTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.searchByNameButton);
            this.Controls.Add(this.searchByNameTextBox);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Store Inventory & Price Inquiry";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox searchByNameTextBox;
        private System.Windows.Forms.Button searchByNameButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox searchByUPCTextBox;
        private System.Windows.Forms.Button searchByUPCButton;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label upcLabel;
        private System.Windows.Forms.Label priceLabel;
        private System.Windows.Forms.Label costPerCaseLabel;
        private System.Windows.Forms.Label distributorLabel;
        private System.Windows.Forms.Label unitsPerCaseLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label changePriceErrorLabel;
        private System.Windows.Forms.Button updatePriceButton;
        private System.Windows.Forms.TextBox priceChangeAccessKeyTextBox;
        private System.Windows.Forms.TextBox newPriceTextBox;
        private System.Windows.Forms.Label deleteItemErrorLabel;
        private System.Windows.Forms.Button deleteItemButton;
        private System.Windows.Forms.TextBox deleteItemAccessKeyTextBox;
        private System.Windows.Forms.TextBox verifyUPCTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label addItemErrorLabel;
        private System.Windows.Forms.Button addItemButton;
        private System.Windows.Forms.TextBox addItemDistributorTextBox;
        private System.Windows.Forms.TextBox addItemUnitsPerCaseTextBox;
        private System.Windows.Forms.TextBox addItemCostPerCaseTextBox;
        private System.Windows.Forms.TextBox addItemAccessKeyTextBox;
        private System.Windows.Forms.TextBox addItemPriceTextBox;
        private System.Windows.Forms.TextBox addItemUPCTextBox;
        private System.Windows.Forms.TextBox addItemNameTextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label6;
    }
}

