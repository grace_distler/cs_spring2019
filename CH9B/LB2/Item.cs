﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB2
{
    class Item
    {
        private string _name;
        private string _upc;
        private double _storePrice;
        private double _costPerCase;
        private double _unitsPerCase;
        private string _distributor;

        public Item(string name, string upc, double storePrice, double costPerCase, double unitsPerCase, string distributor)
        {
            _name = name;
            _upc = upc;
            _storePrice = storePrice;
            _costPerCase = costPerCase;
            _unitsPerCase = unitsPerCase;
            _distributor = distributor;
        }

        public Item()
        {
            _name = "";
            _upc = "";
            _storePrice = 0;
            _costPerCase = 0;
            _unitsPerCase = 0;
            _distributor = "";
        }

        public string GetName()
        {
            return _name;
        }
        public void SetName(string name)
        {
            _name = name;
        }

        public string GetUPC()
        {
            return _upc;
        }
        public void SetUPC(string upc)
        {
            _upc = upc;
        }

        public double GetStorePrice()
        {
            return _storePrice;
        }
        public void SetStorePrice(double price)
        {
            _storePrice = price;
        }

        public double GetCostPerCase()
        {
            return _costPerCase;
        }
        public void SetCostPerCase(double cost)
        {
            _costPerCase = cost;
        }

        public double GetUnitsPerCase()
        {
            return _unitsPerCase;
        }
        public void SetUnitsPerCase(double units)
        {
            _unitsPerCase = units;
        }

        public string GetDistributor()
        {
            return _distributor;
        }
        public void SetDistributor(string distributor)
        {
            _distributor = distributor;
        }
    }
}
