﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB7
{
    public partial class Form1 : Form
    {
        Room[] roomsInHouse = new Room[20];
        int count = 0;
        int totalBoxes = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (count < 20)
            {
                Room room = new Room(roomNameTextBox.Text, Convert.ToInt32(widthEntryTextBox.Text), Convert.ToInt32(lengthEntryTextBos.Text));
                roomsInHouse[count] = room;
                count++;
                totalBoxes += room.Boxes;

                amountOfRoomsOutputLabel.Text = "Rooms: " + count + "/" + 20;
                boxesNeededOutputLabel.Text = "Total Boxes Needed: " + totalBoxes;

                string result = "";
                for (int i = 0; i < count; i++)
                {
                    result += roomsInHouse[i].Display() + "\n";
                }

                summaryLabel.Text = result;
                roomNameTextBox.Text = "";
                widthEntryTextBox.Text = "";
                lengthEntryTextBos.Text = "";
                roomNameTextBox.Focus();
            }
            else
            {
                amountOfRoomsOutputLabel.Text = "YOU CANNOT ADD ANY MORE ROOMS. \nONLY 20 ROOMS ALLOWED!";
                boxesNeededOutputLabel.Text = "";
                roomNameTextBox.Text = "";
                widthEntryTextBox.Text = "";
                lengthEntryTextBos.Text = "";
                roomNameTextBox.Focus();
            }
        }
    }
}
