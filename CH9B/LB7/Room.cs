﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB7
{
    class Room
    {
        private string _name;
        private int _width;
        private int _length;
        private int _area;
        private int _boxes;

        public Room(string name, int width, int length)
        {
            _name = name;
            _width = width;
            _length = length;
            _area = _width * _length;
            _boxes = _area / 12;

            if (_area - (12 * _boxes) != 0)
            {
                _boxes += 2;
            }
            else
            {
                _boxes += 1;
            }
        }

        public string Display()
        {
            return _name +  " (" + _width + "*" + _length + ") needs " + _boxes + " boxes";
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public int Width
        {
            get
            {
                return _width;
            }
        }

        public int Length
        {
            get
            {
                return _length;
            }
        }

        public int Area
        {
            get
            {
                return _area;
            }
        }

        public int Boxes
        {
            get
            {
                return _boxes;
            }
        }
    }
}
