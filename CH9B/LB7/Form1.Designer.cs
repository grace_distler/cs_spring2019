﻿namespace LB7
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.roomNameTextBox = new System.Windows.Forms.TextBox();
            this.lengthEntryTextBos = new System.Windows.Forms.TextBox();
            this.widthEntryTextBox = new System.Windows.Forms.TextBox();
            this.addButton = new System.Windows.Forms.Button();
            this.amountOfRoomsOutputLabel = new System.Windows.Forms.Label();
            this.boxesNeededOutputLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.summaryLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(439, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Length:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(236, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Width:";
            // 
            // roomNameTextBox
            // 
            this.roomNameTextBox.Location = new System.Drawing.Point(38, 57);
            this.roomNameTextBox.Name = "roomNameTextBox";
            this.roomNameTextBox.Size = new System.Drawing.Size(152, 22);
            this.roomNameTextBox.TabIndex = 1;
            // 
            // lengthEntryTextBos
            // 
            this.lengthEntryTextBos.Location = new System.Drawing.Point(442, 57);
            this.lengthEntryTextBos.Name = "lengthEntryTextBos";
            this.lengthEntryTextBos.Size = new System.Drawing.Size(152, 22);
            this.lengthEntryTextBos.TabIndex = 3;
            // 
            // widthEntryTextBox
            // 
            this.widthEntryTextBox.Location = new System.Drawing.Point(239, 57);
            this.widthEntryTextBox.Name = "widthEntryTextBox";
            this.widthEntryTextBox.Size = new System.Drawing.Size(152, 22);
            this.widthEntryTextBox.TabIndex = 2;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(38, 97);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(98, 28);
            this.addButton.TabIndex = 4;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // amountOfRoomsOutputLabel
            // 
            this.amountOfRoomsOutputLabel.AutoSize = true;
            this.amountOfRoomsOutputLabel.Location = new System.Drawing.Point(49, 164);
            this.amountOfRoomsOutputLabel.Name = "amountOfRoomsOutputLabel";
            this.amountOfRoomsOutputLabel.Size = new System.Drawing.Size(60, 17);
            this.amountOfRoomsOutputLabel.TabIndex = 7;
            this.amountOfRoomsOutputLabel.Text = "Rooms: ";
            // 
            // boxesNeededOutputLabel
            // 
            this.boxesNeededOutputLabel.AutoSize = true;
            this.boxesNeededOutputLabel.Location = new System.Drawing.Point(49, 197);
            this.boxesNeededOutputLabel.Name = "boxesNeededOutputLabel";
            this.boxesNeededOutputLabel.Size = new System.Drawing.Size(140, 17);
            this.boxesNeededOutputLabel.TabIndex = 8;
            this.boxesNeededOutputLabel.Text = "Total Boxes Needed:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.summaryLabel);
            this.groupBox1.Location = new System.Drawing.Point(30, 237);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(564, 386);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Summary";
            // 
            // summaryLabel
            // 
            this.summaryLabel.AutoSize = true;
            this.summaryLabel.Location = new System.Drawing.Point(19, 35);
            this.summaryLabel.Name = "summaryLabel";
            this.summaryLabel.Size = new System.Drawing.Size(0, 17);
            this.summaryLabel.TabIndex = 0;
            // 
            // Form1
            // 
            this.AcceptButton = this.addButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(633, 649);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.boxesNeededOutputLabel);
            this.Controls.Add(this.amountOfRoomsOutputLabel);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.widthEntryTextBox);
            this.Controls.Add(this.lengthEntryTextBos);
            this.Controls.Add(this.roomNameTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Tiling Estimator";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox roomNameTextBox;
        private System.Windows.Forms.TextBox lengthEntryTextBos;
        private System.Windows.Forms.TextBox widthEntryTextBox;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Label amountOfRoomsOutputLabel;
        private System.Windows.Forms.Label boxesNeededOutputLabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label summaryLabel;
    }
}

