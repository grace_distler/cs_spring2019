﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB11
{
    public class Order
    {
        private string _name;
        private Sundae _sundae;
        private Soda _soda;

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public Sundae Sundae
        {
            get
            {
                return _sundae;
            }
        }

        public Soda Soda
        {
            get
            {
                return _soda;
            }
        }

        public double Price { get; }

        public Order(string name, bool hasSundae, bool hasSoda)
        {
            _name = name;

            if (hasSundae)
            {
                _sundae = new Sundae();
                Price += _sundae.Price;
            }

            if (hasSoda)
            {
                _soda = new Soda();
                Price += _soda.Price;
            }
        }
    }
}
