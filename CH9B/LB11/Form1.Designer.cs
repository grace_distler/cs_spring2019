﻿namespace LB11
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.nameEntryTextBox = new System.Windows.Forms.TextBox();
            this.requiredErrorLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.sundaeCheckBox = new System.Windows.Forms.CheckBox();
            this.sodaCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.orderTextBox = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.sprinklesCheckBox = new System.Windows.Forms.CheckBox();
            this.chocolateSyrupCheckBox = new System.Windows.Forms.CheckBox();
            this.nutsCheckBox = new System.Windows.Forms.CheckBox();
            this.peachCheckBox = new System.Windows.Forms.CheckBox();
            this.mangoCheckBox = new System.Windows.Forms.CheckBox();
            this.limeCheckBox = new System.Windows.Forms.CheckBox();
            this.mixinErrorLabel = new System.Windows.Forms.Label();
            this.toppingsErrorLabel = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.totalLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(34, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(285, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "What name do you want on the order?";
            // 
            // nameEntryTextBox
            // 
            this.nameEntryTextBox.Location = new System.Drawing.Point(37, 54);
            this.nameEntryTextBox.Name = "nameEntryTextBox";
            this.nameEntryTextBox.Size = new System.Drawing.Size(287, 22);
            this.nameEntryTextBox.TabIndex = 1;
            // 
            // requiredErrorLabel
            // 
            this.requiredErrorLabel.AutoSize = true;
            this.requiredErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.requiredErrorLabel.Location = new System.Drawing.Point(34, 90);
            this.requiredErrorLabel.Name = "requiredErrorLabel";
            this.requiredErrorLabel.Size = new System.Drawing.Size(0, 17);
            this.requiredErrorLabel.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(34, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(178, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Do you want a sundae?";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(286, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Do you want a soda?";
            // 
            // sundaeCheckBox
            // 
            this.sundaeCheckBox.AutoSize = true;
            this.sundaeCheckBox.Location = new System.Drawing.Point(37, 164);
            this.sundaeCheckBox.Name = "sundaeCheckBox";
            this.sundaeCheckBox.Size = new System.Drawing.Size(54, 21);
            this.sundaeCheckBox.TabIndex = 5;
            this.sundaeCheckBox.Text = "Yes";
            this.sundaeCheckBox.UseVisualStyleBackColor = true;
            // 
            // sodaCheckBox
            // 
            this.sodaCheckBox.AutoSize = true;
            this.sodaCheckBox.Location = new System.Drawing.Point(289, 164);
            this.sodaCheckBox.Name = "sodaCheckBox";
            this.sodaCheckBox.Size = new System.Drawing.Size(54, 21);
            this.sodaCheckBox.TabIndex = 6;
            this.sodaCheckBox.Text = "Yes";
            this.sodaCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.orderTextBox);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(537, 34);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(507, 352);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Order Summary";
            // 
            // orderTextBox
            // 
            this.orderTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderTextBox.Location = new System.Drawing.Point(32, 36);
            this.orderTextBox.Name = "orderTextBox";
            this.orderTextBox.Size = new System.Drawing.Size(446, 290);
            this.orderTextBox.TabIndex = 0;
            this.orderTextBox.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(34, 225);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Sundae Toppings:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(286, 225);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Soda Mixins:";
            // 
            // sprinklesCheckBox
            // 
            this.sprinklesCheckBox.AutoSize = true;
            this.sprinklesCheckBox.Location = new System.Drawing.Point(37, 245);
            this.sprinklesCheckBox.Name = "sprinklesCheckBox";
            this.sprinklesCheckBox.Size = new System.Drawing.Size(88, 21);
            this.sprinklesCheckBox.TabIndex = 10;
            this.sprinklesCheckBox.Text = "Sprinkles";
            this.sprinklesCheckBox.UseVisualStyleBackColor = true;
            // 
            // chocolateSyrupCheckBox
            // 
            this.chocolateSyrupCheckBox.AutoSize = true;
            this.chocolateSyrupCheckBox.Location = new System.Drawing.Point(37, 299);
            this.chocolateSyrupCheckBox.Name = "chocolateSyrupCheckBox";
            this.chocolateSyrupCheckBox.Size = new System.Drawing.Size(134, 21);
            this.chocolateSyrupCheckBox.TabIndex = 11;
            this.chocolateSyrupCheckBox.Text = "Chocolate Syrup";
            this.chocolateSyrupCheckBox.UseVisualStyleBackColor = true;
            // 
            // nutsCheckBox
            // 
            this.nutsCheckBox.AutoSize = true;
            this.nutsCheckBox.Location = new System.Drawing.Point(37, 272);
            this.nutsCheckBox.Name = "nutsCheckBox";
            this.nutsCheckBox.Size = new System.Drawing.Size(59, 21);
            this.nutsCheckBox.TabIndex = 12;
            this.nutsCheckBox.Text = "Nuts";
            this.nutsCheckBox.UseVisualStyleBackColor = true;
            // 
            // peachCheckBox
            // 
            this.peachCheckBox.AutoSize = true;
            this.peachCheckBox.Location = new System.Drawing.Point(289, 272);
            this.peachCheckBox.Name = "peachCheckBox";
            this.peachCheckBox.Size = new System.Drawing.Size(113, 21);
            this.peachCheckBox.TabIndex = 15;
            this.peachCheckBox.Text = "Peach Flavor";
            this.peachCheckBox.UseVisualStyleBackColor = true;
            // 
            // mangoCheckBox
            // 
            this.mangoCheckBox.AutoSize = true;
            this.mangoCheckBox.Location = new System.Drawing.Point(289, 299);
            this.mangoCheckBox.Name = "mangoCheckBox";
            this.mangoCheckBox.Size = new System.Drawing.Size(116, 21);
            this.mangoCheckBox.TabIndex = 14;
            this.mangoCheckBox.Text = "Mango Flavor";
            this.mangoCheckBox.UseVisualStyleBackColor = true;
            // 
            // limeCheckBox
            // 
            this.limeCheckBox.AutoSize = true;
            this.limeCheckBox.Location = new System.Drawing.Point(289, 245);
            this.limeCheckBox.Name = "limeCheckBox";
            this.limeCheckBox.Size = new System.Drawing.Size(103, 21);
            this.limeCheckBox.TabIndex = 13;
            this.limeCheckBox.Text = "Lime Flavor";
            this.limeCheckBox.UseVisualStyleBackColor = true;
            // 
            // mixinErrorLabel
            // 
            this.mixinErrorLabel.AutoSize = true;
            this.mixinErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.mixinErrorLabel.Location = new System.Drawing.Point(286, 343);
            this.mixinErrorLabel.Name = "mixinErrorLabel";
            this.mixinErrorLabel.Size = new System.Drawing.Size(0, 17);
            this.mixinErrorLabel.TabIndex = 16;
            // 
            // toppingsErrorLabel
            // 
            this.toppingsErrorLabel.AutoSize = true;
            this.toppingsErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.toppingsErrorLabel.Location = new System.Drawing.Point(34, 343);
            this.toppingsErrorLabel.Name = "toppingsErrorLabel";
            this.toppingsErrorLabel.Size = new System.Drawing.Size(0, 17);
            this.toppingsErrorLabel.TabIndex = 17;
            // 
            // addButton
            // 
            this.addButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addButton.Location = new System.Drawing.Point(37, 393);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(175, 40);
            this.addButton.TabIndex = 18;
            this.addButton.Text = "Add Item to Order";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // totalLabel
            // 
            this.totalLabel.AutoSize = true;
            this.totalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalLabel.Location = new System.Drawing.Point(845, 400);
            this.totalLabel.Name = "totalLabel";
            this.totalLabel.Size = new System.Drawing.Size(68, 24);
            this.totalLabel.TabIndex = 19;
            this.totalLabel.Text = "Total: ";
            // 
            // Form1
            // 
            this.AcceptButton = this.addButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1092, 464);
            this.Controls.Add(this.totalLabel);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.toppingsErrorLabel);
            this.Controls.Add(this.mixinErrorLabel);
            this.Controls.Add(this.peachCheckBox);
            this.Controls.Add(this.mangoCheckBox);
            this.Controls.Add(this.limeCheckBox);
            this.Controls.Add(this.nutsCheckBox);
            this.Controls.Add(this.chocolateSyrupCheckBox);
            this.Controls.Add(this.sprinklesCheckBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.sodaCheckBox);
            this.Controls.Add(this.sundaeCheckBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.requiredErrorLabel);
            this.Controls.Add(this.nameEntryTextBox);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Food Order";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nameEntryTextBox;
        private System.Windows.Forms.Label requiredErrorLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox sundaeCheckBox;
        private System.Windows.Forms.CheckBox sodaCheckBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox sprinklesCheckBox;
        private System.Windows.Forms.CheckBox chocolateSyrupCheckBox;
        private System.Windows.Forms.CheckBox nutsCheckBox;
        private System.Windows.Forms.CheckBox peachCheckBox;
        private System.Windows.Forms.CheckBox mangoCheckBox;
        private System.Windows.Forms.CheckBox limeCheckBox;
        private System.Windows.Forms.Label mixinErrorLabel;
        private System.Windows.Forms.Label toppingsErrorLabel;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Label totalLabel;
        private System.Windows.Forms.RichTextBox orderTextBox;
    }
}

