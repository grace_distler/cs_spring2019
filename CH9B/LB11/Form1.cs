﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB11
{
    public partial class Form1 : Form
    {
        double orderTotal = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            toppingsErrorLabel.Text = "";
            mixinErrorLabel.Text = "";
            requiredErrorLabel.Text = "";

            string orderName = nameEntryTextBox.Text;

            bool hasSundae = sundaeCheckBox.Checked;
            bool hasSoda = sodaCheckBox.Checked;

            Order anOrder = new Order(orderName, hasSundae, hasSoda);

            //Sundae
            if (hasSundae)
            {
                if (sprinklesCheckBox.Checked)
                {
                    anOrder.Sundae.AddTopping(SundaeTopping.SPRINKLES);
                }
                if (nutsCheckBox.Checked)
                {
                    anOrder.Sundae.AddTopping(SundaeTopping.NUTS);
                }
                if (chocolateSyrupCheckBox.Checked)
                {
                    anOrder.Sundae.AddTopping(SundaeTopping.CHOCOLATE_SYRUP);
                }
            }

            //Soda
            if (hasSoda)
            {
                int numFlavors = 0;

                if (limeCheckBox.Checked)
                {
                    numFlavors++;
                }
                if (peachCheckBox.Checked)
                {
                    numFlavors++;
                }
                if (mangoCheckBox.Checked)
                {
                    numFlavors++;
                }

                if (numFlavors > 1)
                {
                    mixinErrorLabel.Text = "Only 1 mixin allowed";
                    return;
                }

                if (limeCheckBox.Checked)
                {
                    anOrder.Soda.AddFlavor(SodaFlavor.LIME);
                }
                if (peachCheckBox.Checked)
                {
                    anOrder.Soda.AddFlavor(SodaFlavor.PEACH);
                }
                if (mangoCheckBox.Checked)
                {
                    anOrder.Soda.AddFlavor(SodaFlavor.MANGO);
                }
            }

            //Order Output

            string nameInfo = null;
           
            if (nameEntryTextBox.Text != "")
            {
                nameInfo += "\n" + anOrder.Name + "\n------------------------------\n";
            }
            else
            {
                requiredErrorLabel.Text = "Name Required";
            }

            bool nameWritten = false;

            if (hasSundae)
            {
                if (anOrder.Sundae.ToppingCount == 0)
                {
                    orderTextBox.Text += nameInfo + "Sundae - NONE " + anOrder.Sundae.Price + "\n";
                    orderTotal += anOrder.Sundae.Price;
                    nameWritten = true;
                }
                else if (anOrder.Sundae.ToppingCount == 1)
                {
                    orderTextBox.Text += nameInfo + "Sundae - " + anOrder.Sundae.GetTopping(0) + " - " + anOrder.Sundae.Price + "\n";
                    orderTotal += anOrder.Sundae.Price;
                    nameWritten = true;
                }
                else if (anOrder.Sundae.ToppingCount == 2)
                {
                    orderTextBox.Text += nameInfo + "Sundae - " + anOrder.Sundae.GetTopping(0) + " " + anOrder.Sundae.GetTopping(1) + " - " + anOrder.Sundae.Price + "\n";
                    orderTotal += anOrder.Sundae.Price;
                    nameWritten = true;
                 }
                else if (anOrder.Sundae.ToppingCount == 3)
                {
                    toppingsErrorLabel.Text = "Only 2 toppings allowed";
                    return;
                }
            }

            if(hasSoda)
            {
                if(!nameWritten)
                {
                    orderTextBox.Text += nameInfo;
                }
                orderTextBox.Text += "Soda - " + anOrder.Soda.Flavor + " - " + anOrder.Soda.Price + "\n";
                orderTotal += anOrder.Soda.Price;
            }
            
            totalLabel.Text = "Total: " + orderTotal.ToString("C");
        }
    }
}
