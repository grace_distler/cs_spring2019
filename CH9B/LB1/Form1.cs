﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB1_FlashCards
{
    public partial class Form1 : Form
    {
        private FlashCard[] myCards;
        private int count = 5;

        public Form1()
        {
            InitializeComponent();

            myCards = new FlashCard[20];
            myCards[0] = new FlashCard("HTML", "Hypertext Markup Language");
            myCards[1] = new FlashCard("CSS", "Cascading Style Sheet");
            myCards[2] = new FlashCard("JS", "JavaScript");
            myCards[3] = new FlashCard("C#", "C Sharp Programming");
            myCards[4] = new FlashCard("VB", "Visual Basic");
        }

        private void defineButton_Click(object sender, EventArgs e)
        {
            successLabel.Text = "";
            string input = termEntryTextBox.Text;
            bool found = false;
            for (int i = 0; i < count; i++)
            {
                if (input.ToUpper().Equals(myCards[i].GetTerm()))
                {
                    definitionOutputLabel.Text = myCards[i].GetDefinition();
                    found = true;
                }
            }

            if (found == false)
            {
                unknownLabel.Text = "I don't know that term yet. What does it mean?";
                definitionEntryTextBox.Visible = true;
                addButton.Visible = true;
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            myCards[count] = new FlashCard(termEntryTextBox.Text.ToUpper(), definitionEntryTextBox.Text);
            count++;
            successLabel.Text = "Flash Card Added!";
            unknownLabel.Text = "";
            definitionEntryTextBox.Visible = false;
            addButton.Visible = false;
        }
    }
}
