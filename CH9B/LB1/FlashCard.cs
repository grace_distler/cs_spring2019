﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB1_FlashCards
{
    class FlashCard
    {
        //Instance Fields
        private string _term;
        private string _definition;

        //2 Parameter Constructor
        public FlashCard(string term, string definition)
        {
            _term = term;
            _definition = definition;
        }

        public string GetTerm()
        {
            return _term;
        }

        public string GetDefinition()
        {
            return _definition;
        }
    }
}
