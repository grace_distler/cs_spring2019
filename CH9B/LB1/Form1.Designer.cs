﻿namespace LB1_FlashCards
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.termEntryTextBox = new System.Windows.Forms.TextBox();
            this.defineButton = new System.Windows.Forms.Button();
            this.definitionOutputLabel = new System.Windows.Forms.Label();
            this.definitionEntryTextBox = new System.Windows.Forms.TextBox();
            this.addButton = new System.Windows.Forms.Button();
            this.unknownLabel = new System.Windows.Forms.Label();
            this.successLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(243, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "What term do you want me to define?";
            // 
            // termEntryTextBox
            // 
            this.termEntryTextBox.Location = new System.Drawing.Point(44, 61);
            this.termEntryTextBox.Name = "termEntryTextBox";
            this.termEntryTextBox.Size = new System.Drawing.Size(234, 22);
            this.termEntryTextBox.TabIndex = 1;
            // 
            // defineButton
            // 
            this.defineButton.Location = new System.Drawing.Point(297, 59);
            this.defineButton.Name = "defineButton";
            this.defineButton.Size = new System.Drawing.Size(91, 27);
            this.defineButton.TabIndex = 2;
            this.defineButton.Text = "Define";
            this.defineButton.UseVisualStyleBackColor = true;
            this.defineButton.Click += new System.EventHandler(this.defineButton_Click);
            // 
            // definitionOutputLabel
            // 
            this.definitionOutputLabel.AutoSize = true;
            this.definitionOutputLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.definitionOutputLabel.Location = new System.Drawing.Point(40, 102);
            this.definitionOutputLabel.Name = "definitionOutputLabel";
            this.definitionOutputLabel.Size = new System.Drawing.Size(0, 24);
            this.definitionOutputLabel.TabIndex = 3;
            // 
            // definitionEntryTextBox
            // 
            this.definitionEntryTextBox.Location = new System.Drawing.Point(44, 184);
            this.definitionEntryTextBox.Name = "definitionEntryTextBox";
            this.definitionEntryTextBox.Size = new System.Drawing.Size(234, 22);
            this.definitionEntryTextBox.TabIndex = 4;
            this.definitionEntryTextBox.Visible = false;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(297, 182);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(82, 27);
            this.addButton.TabIndex = 5;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Visible = false;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // unknownLabel
            // 
            this.unknownLabel.AutoSize = true;
            this.unknownLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unknownLabel.Location = new System.Drawing.Point(40, 148);
            this.unknownLabel.Name = "unknownLabel";
            this.unknownLabel.Size = new System.Drawing.Size(0, 24);
            this.unknownLabel.TabIndex = 6;
            // 
            // successLabel
            // 
            this.successLabel.AutoSize = true;
            this.successLabel.Location = new System.Drawing.Point(41, 227);
            this.successLabel.Name = "successLabel";
            this.successLabel.Size = new System.Drawing.Size(0, 17);
            this.successLabel.TabIndex = 7;
            // 
            // Form1
            // 
            this.AcceptButton = this.defineButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 281);
            this.Controls.Add(this.successLabel);
            this.Controls.Add(this.unknownLabel);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.definitionEntryTextBox);
            this.Controls.Add(this.definitionOutputLabel);
            this.Controls.Add(this.defineButton);
            this.Controls.Add(this.termEntryTextBox);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Flash Cards";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox termEntryTextBox;
        private System.Windows.Forms.Button defineButton;
        private System.Windows.Forms.Label definitionOutputLabel;
        private System.Windows.Forms.TextBox definitionEntryTextBox;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Label unknownLabel;
        private System.Windows.Forms.Label successLabel;
    }
}

