﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB4
{
    public partial class Form1 : Form
    {
        private Game[] videoGames;
        private int count = 5;

        public Form1()
        {
            InitializeComponent();

            videoGames = new Game[count];
            videoGames[0] = new Game("Player Unknown's Battlegrounds (PUBG)", "Bluehole", 35);
            videoGames[1] = new Game("League of Legends", "Riot Games", 0);
            videoGames[2] = new Game("Call of Duty: Black Ops III", "Activision", 60);
            videoGames[3] = new Game("Battlefield 4", "Electronic Arts (EA)", 20);
            videoGames[4] = new Game("Super Mario Odyssey", "Nintendo", 60);
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            string entry;
            entry = gameInputTextBox.Text.ToLower();
            ShowGameInfo(SearchForGame(entry));
        }
        
        public int SearchForGame(string entry)
        {
            for(int i = 0; i < count; i++)
            {
                if(entry.Contains(videoGames[i].GetName().ToLower()) || entry.Contains(videoGames[i].GetPublisher().ToLower()))
                {
                    return i;
                    //break;
                }
            }
            return -1;
        }

        public void ShowGameInfo(int gameIndex)
        {
            string entry = gameInputTextBox.Text;

            if(SearchForGame(entry) >= 0)
            {
                nameOutputLabel.Text = videoGames[gameIndex].GetName();
                publisherOutputLabel.Text = videoGames[gameIndex].GetPublisher();
                priceOutputLabel.Text = videoGames[gameIndex].GetPrice().ToString("C");
            }
            else
            {
                nameOutputLabel.Text = "Game Not Found";
                publisherOutputLabel.Text = "Game Not Found";
                priceOutputLabel.Text = "Game Not Found";
            }
        }
    }
}
