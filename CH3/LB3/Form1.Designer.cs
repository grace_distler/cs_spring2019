﻿namespace LB3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.milesLabel = new System.Windows.Forms.Label();
            this.kilometersLabel = new System.Windows.Forms.Label();
            this.milesTextBox = new System.Windows.Forms.TextBox();
            this.kilometersTextBox = new System.Windows.Forms.TextBox();
            this.convertMilesButton = new System.Windows.Forms.Button();
            this.convertKilometersButton = new System.Windows.Forms.Button();
            this.outputLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // milesLabel
            // 
            this.milesLabel.AutoSize = true;
            this.milesLabel.Font = new System.Drawing.Font("Franklin Gothic Book", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.milesLabel.Location = new System.Drawing.Point(12, 42);
            this.milesLabel.Name = "milesLabel";
            this.milesLabel.Size = new System.Drawing.Size(102, 17);
            this.milesLabel.TabIndex = 0;
            this.milesLabel.Text = "Distance in Miles";
            // 
            // kilometersLabel
            // 
            this.kilometersLabel.AutoSize = true;
            this.kilometersLabel.Font = new System.Drawing.Font("Franklin Gothic Book", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kilometersLabel.Location = new System.Drawing.Point(151, 42);
            this.kilometersLabel.Name = "kilometersLabel";
            this.kilometersLabel.Size = new System.Drawing.Size(132, 17);
            this.kilometersLabel.TabIndex = 1;
            this.kilometersLabel.Text = "Distance in Kilometers";
            // 
            // milesTextBox
            // 
            this.milesTextBox.Location = new System.Drawing.Point(15, 84);
            this.milesTextBox.Name = "milesTextBox";
            this.milesTextBox.Size = new System.Drawing.Size(100, 20);
            this.milesTextBox.TabIndex = 2;
            // 
            // kilometersTextBox
            // 
            this.kilometersTextBox.Location = new System.Drawing.Point(154, 84);
            this.kilometersTextBox.Name = "kilometersTextBox";
            this.kilometersTextBox.Size = new System.Drawing.Size(100, 20);
            this.kilometersTextBox.TabIndex = 3;
            // 
            // convertMilesButton
            // 
            this.convertMilesButton.Font = new System.Drawing.Font("Franklin Gothic Book", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.convertMilesButton.Location = new System.Drawing.Point(15, 140);
            this.convertMilesButton.Name = "convertMilesButton";
            this.convertMilesButton.Size = new System.Drawing.Size(100, 25);
            this.convertMilesButton.TabIndex = 4;
            this.convertMilesButton.Text = "Convert to km";
            this.convertMilesButton.UseVisualStyleBackColor = true;
            this.convertMilesButton.Click += new System.EventHandler(this.convertMilesButton_Click);
            // 
            // convertKilometersButton
            // 
            this.convertKilometersButton.Font = new System.Drawing.Font("Franklin Gothic Book", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.convertKilometersButton.Location = new System.Drawing.Point(154, 140);
            this.convertKilometersButton.Name = "convertKilometersButton";
            this.convertKilometersButton.Size = new System.Drawing.Size(100, 25);
            this.convertKilometersButton.TabIndex = 5;
            this.convertKilometersButton.Text = "Convert to miles";
            this.convertKilometersButton.UseVisualStyleBackColor = true;
            this.convertKilometersButton.Click += new System.EventHandler(this.convertKilometersButton_Click);
            // 
            // outputLabel
            // 
            this.outputLabel.AutoSize = true;
            this.outputLabel.Location = new System.Drawing.Point(63, 210);
            this.outputLabel.Name = "outputLabel";
            this.outputLabel.Size = new System.Drawing.Size(0, 13);
            this.outputLabel.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(295, 269);
            this.Controls.Add(this.outputLabel);
            this.Controls.Add(this.convertKilometersButton);
            this.Controls.Add(this.convertMilesButton);
            this.Controls.Add(this.kilometersTextBox);
            this.Controls.Add(this.milesTextBox);
            this.Controls.Add(this.kilometersLabel);
            this.Controls.Add(this.milesLabel);
            this.Name = "Form1";
            this.Text = "Distance Converter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label milesLabel;
        private System.Windows.Forms.Label kilometersLabel;
        private System.Windows.Forms.TextBox milesTextBox;
        private System.Windows.Forms.TextBox kilometersTextBox;
        private System.Windows.Forms.Button convertMilesButton;
        private System.Windows.Forms.Button convertKilometersButton;
        private System.Windows.Forms.Label outputLabel;
    }
}

