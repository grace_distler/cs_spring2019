﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Console;

namespace LB3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void convertMilesButton_Click(object sender, EventArgs e)
        {
            const double KM_PER_MILE = 1.6;
            double miles;
            double convertToKM;

            miles = Convert.ToDouble(milesTextBox.Text);
            convertToKM = miles * KM_PER_MILE;

            outputLabel.Text = miles.ToString("N") + " miles is " + convertToKM + " kilometers";
        }

        private void convertKilometersButton_Click(object sender, EventArgs e)
        {
            const double KM_PER_MILE = 1.6;
            double kilometers;
            double convertToMiles;

            kilometers = Convert.ToDouble(kilometersTextBox.Text);
            convertToMiles = kilometers / KM_PER_MILE;

            outputLabel.Text = kilometers + " kilometers is " + convertToMiles.ToString("N") + " miles";
        }
    }
}
