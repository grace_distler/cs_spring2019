﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            int chicken1Eggs, chicken2Eggs, chicken3Eggs, chicken4Eggs;
            int calculation, dozens, remainder;

            chicken1Eggs = Convert.ToInt32(eggsLaid1TextBox.Text);
            chicken2Eggs = Convert.ToInt32(eggsLaid2TextBox.Text);
            chicken3Eggs = Convert.ToInt32(eggsLaid3TextBox.Text);
            chicken4Eggs = Convert.ToInt32(eggsLaid4TextBox.Text);

            calculation = chicken1Eggs + chicken2Eggs + chicken3Eggs + chicken4Eggs;
            dozens = calculation / 12;
            remainder = calculation % 12;

            outputLabel.Text = calculation + " eggs total, or " + dozens + " dozen and " + remainder + " eggs";
        }
    }
}
