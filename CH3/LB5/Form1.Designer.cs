﻿namespace LB5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.eggsLaid1TextBox = new System.Windows.Forms.TextBox();
            this.eggsLaid2TextBox = new System.Windows.Forms.TextBox();
            this.eggsLaid3TextBox = new System.Windows.Forms.TextBox();
            this.eggsLaid4TextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.outputLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(137, 45);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(364, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Enter number of eggs laid by each chicken:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // eggsLaid1TextBox
            // 
            this.eggsLaid1TextBox.Location = new System.Drawing.Point(57, 102);
            this.eggsLaid1TextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.eggsLaid1TextBox.Name = "eggsLaid1TextBox";
            this.eggsLaid1TextBox.Size = new System.Drawing.Size(132, 26);
            this.eggsLaid1TextBox.TabIndex = 1;
            // 
            // eggsLaid2TextBox
            // 
            this.eggsLaid2TextBox.Location = new System.Drawing.Point(224, 102);
            this.eggsLaid2TextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.eggsLaid2TextBox.Name = "eggsLaid2TextBox";
            this.eggsLaid2TextBox.Size = new System.Drawing.Size(132, 26);
            this.eggsLaid2TextBox.TabIndex = 2;
            // 
            // eggsLaid3TextBox
            // 
            this.eggsLaid3TextBox.Location = new System.Drawing.Point(393, 102);
            this.eggsLaid3TextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.eggsLaid3TextBox.Name = "eggsLaid3TextBox";
            this.eggsLaid3TextBox.Size = new System.Drawing.Size(132, 26);
            this.eggsLaid3TextBox.TabIndex = 3;
            // 
            // eggsLaid4TextBox
            // 
            this.eggsLaid4TextBox.Location = new System.Drawing.Point(567, 102);
            this.eggsLaid4TextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.eggsLaid4TextBox.Name = "eggsLaid4TextBox";
            this.eggsLaid4TextBox.Size = new System.Drawing.Size(132, 26);
            this.eggsLaid4TextBox.TabIndex = 4;
            // 
            // calculateButton
            // 
            this.calculateButton.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calculateButton.Location = new System.Drawing.Point(297, 181);
            this.calculateButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(141, 40);
            this.calculateButton.TabIndex = 5;
            this.calculateButton.Text = "Calculate";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // outputLabel
            // 
            this.outputLabel.AutoSize = true;
            this.outputLabel.Location = new System.Drawing.Point(251, 271);
            this.outputLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.outputLabel.Name = "outputLabel";
            this.outputLabel.Size = new System.Drawing.Size(0, 21);
            this.outputLabel.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 351);
            this.Controls.Add(this.outputLabel);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.eggsLaid4TextBox);
            this.Controls.Add(this.eggsLaid3TextBox);
            this.Controls.Add(this.eggsLaid2TextBox);
            this.Controls.Add(this.eggsLaid1TextBox);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Franklin Gothic Book", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Chicken Farmer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox eggsLaid1TextBox;
        private System.Windows.Forms.TextBox eggsLaid2TextBox;
        private System.Windows.Forms.TextBox eggsLaid3TextBox;
        private System.Windows.Forms.TextBox eggsLaid4TextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label outputLabel;
    }
}

