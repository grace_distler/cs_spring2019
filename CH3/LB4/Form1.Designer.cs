﻿namespace LB4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.employee1GroupBox = new System.Windows.Forms.GroupBox();
            this.salary1TextBox = new System.Windows.Forms.TextBox();
            this.salary1Label = new System.Windows.Forms.Label();
            this.name1TextBox = new System.Windows.Forms.TextBox();
            this.name1Label = new System.Windows.Forms.Label();
            this.employee2GroupBox = new System.Windows.Forms.GroupBox();
            this.salary2TextBox = new System.Windows.Forms.TextBox();
            this.salary2Label = new System.Windows.Forms.Label();
            this.name2TextBox = new System.Windows.Forms.TextBox();
            this.name2Label = new System.Windows.Forms.Label();
            this.employee3GroupBox = new System.Windows.Forms.GroupBox();
            this.salary3TextBox = new System.Windows.Forms.TextBox();
            this.salary3Label = new System.Windows.Forms.Label();
            this.name3TextBox = new System.Windows.Forms.TextBox();
            this.name3Label = new System.Windows.Forms.Label();
            this.calculateButton = new System.Windows.Forms.Button();
            this.yearLabel = new System.Windows.Forms.Label();
            this.label2018 = new System.Windows.Forms.Label();
            this.label2019 = new System.Windows.Forms.Label();
            this.label2020 = new System.Windows.Forms.Label();
            this.employee1NameLabel = new System.Windows.Forms.Label();
            this.employee2NameLabel = new System.Windows.Forms.Label();
            this.employee3NameLabel = new System.Windows.Forms.Label();
            this.employee1SalaryOutput = new System.Windows.Forms.Label();
            this.employee2SalaryOutput = new System.Windows.Forms.Label();
            this.employee3SalaryOutput = new System.Windows.Forms.Label();
            this.employee1Raise1Output = new System.Windows.Forms.Label();
            this.employee2Raise1Output = new System.Windows.Forms.Label();
            this.employee3Raise1Output = new System.Windows.Forms.Label();
            this.employee1Raise2Output = new System.Windows.Forms.Label();
            this.employee2Raise2Output = new System.Windows.Forms.Label();
            this.employee3Raise2Output = new System.Windows.Forms.Label();
            this.employee1GroupBox.SuspendLayout();
            this.employee2GroupBox.SuspendLayout();
            this.employee3GroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // employee1GroupBox
            // 
            this.employee1GroupBox.Controls.Add(this.salary1TextBox);
            this.employee1GroupBox.Controls.Add(this.salary1Label);
            this.employee1GroupBox.Controls.Add(this.name1TextBox);
            this.employee1GroupBox.Controls.Add(this.name1Label);
            this.employee1GroupBox.Location = new System.Drawing.Point(31, 29);
            this.employee1GroupBox.Name = "employee1GroupBox";
            this.employee1GroupBox.Size = new System.Drawing.Size(177, 221);
            this.employee1GroupBox.TabIndex = 0;
            this.employee1GroupBox.TabStop = false;
            this.employee1GroupBox.Text = "Employee 1";
            // 
            // salary1TextBox
            // 
            this.salary1TextBox.Location = new System.Drawing.Point(32, 153);
            this.salary1TextBox.Name = "salary1TextBox";
            this.salary1TextBox.Size = new System.Drawing.Size(100, 20);
            this.salary1TextBox.TabIndex = 3;
            // 
            // salary1Label
            // 
            this.salary1Label.AutoSize = true;
            this.salary1Label.Location = new System.Drawing.Point(29, 137);
            this.salary1Label.Name = "salary1Label";
            this.salary1Label.Size = new System.Drawing.Size(36, 13);
            this.salary1Label.TabIndex = 2;
            this.salary1Label.Text = "Salary";
            // 
            // name1TextBox
            // 
            this.name1TextBox.Location = new System.Drawing.Point(32, 64);
            this.name1TextBox.Name = "name1TextBox";
            this.name1TextBox.Size = new System.Drawing.Size(100, 20);
            this.name1TextBox.TabIndex = 1;
            // 
            // name1Label
            // 
            this.name1Label.AutoSize = true;
            this.name1Label.Location = new System.Drawing.Point(29, 47);
            this.name1Label.Name = "name1Label";
            this.name1Label.Size = new System.Drawing.Size(35, 13);
            this.name1Label.TabIndex = 0;
            this.name1Label.Text = "Name";
            // 
            // employee2GroupBox
            // 
            this.employee2GroupBox.Controls.Add(this.salary2TextBox);
            this.employee2GroupBox.Controls.Add(this.salary2Label);
            this.employee2GroupBox.Controls.Add(this.name2TextBox);
            this.employee2GroupBox.Controls.Add(this.name2Label);
            this.employee2GroupBox.Location = new System.Drawing.Point(229, 29);
            this.employee2GroupBox.Name = "employee2GroupBox";
            this.employee2GroupBox.Size = new System.Drawing.Size(185, 221);
            this.employee2GroupBox.TabIndex = 1;
            this.employee2GroupBox.TabStop = false;
            this.employee2GroupBox.Text = "Employee 2";
            // 
            // salary2TextBox
            // 
            this.salary2TextBox.Location = new System.Drawing.Point(40, 153);
            this.salary2TextBox.Name = "salary2TextBox";
            this.salary2TextBox.Size = new System.Drawing.Size(100, 20);
            this.salary2TextBox.TabIndex = 3;
            // 
            // salary2Label
            // 
            this.salary2Label.AutoSize = true;
            this.salary2Label.Location = new System.Drawing.Point(37, 137);
            this.salary2Label.Name = "salary2Label";
            this.salary2Label.Size = new System.Drawing.Size(36, 13);
            this.salary2Label.TabIndex = 2;
            this.salary2Label.Text = "Salary";
            // 
            // name2TextBox
            // 
            this.name2TextBox.Location = new System.Drawing.Point(37, 64);
            this.name2TextBox.Name = "name2TextBox";
            this.name2TextBox.Size = new System.Drawing.Size(100, 20);
            this.name2TextBox.TabIndex = 1;
            // 
            // name2Label
            // 
            this.name2Label.AutoSize = true;
            this.name2Label.Location = new System.Drawing.Point(34, 47);
            this.name2Label.Name = "name2Label";
            this.name2Label.Size = new System.Drawing.Size(35, 13);
            this.name2Label.TabIndex = 0;
            this.name2Label.Text = "Name";
            // 
            // employee3GroupBox
            // 
            this.employee3GroupBox.Controls.Add(this.salary3TextBox);
            this.employee3GroupBox.Controls.Add(this.salary3Label);
            this.employee3GroupBox.Controls.Add(this.name3TextBox);
            this.employee3GroupBox.Controls.Add(this.name3Label);
            this.employee3GroupBox.Location = new System.Drawing.Point(433, 29);
            this.employee3GroupBox.Name = "employee3GroupBox";
            this.employee3GroupBox.Size = new System.Drawing.Size(189, 221);
            this.employee3GroupBox.TabIndex = 2;
            this.employee3GroupBox.TabStop = false;
            this.employee3GroupBox.Text = "Employee 3";
            // 
            // salary3TextBox
            // 
            this.salary3TextBox.Location = new System.Drawing.Point(41, 152);
            this.salary3TextBox.Name = "salary3TextBox";
            this.salary3TextBox.Size = new System.Drawing.Size(100, 20);
            this.salary3TextBox.TabIndex = 3;
            // 
            // salary3Label
            // 
            this.salary3Label.AutoSize = true;
            this.salary3Label.Location = new System.Drawing.Point(38, 137);
            this.salary3Label.Name = "salary3Label";
            this.salary3Label.Size = new System.Drawing.Size(36, 13);
            this.salary3Label.TabIndex = 2;
            this.salary3Label.Text = "Salary";
            // 
            // name3TextBox
            // 
            this.name3TextBox.Location = new System.Drawing.Point(38, 64);
            this.name3TextBox.Name = "name3TextBox";
            this.name3TextBox.Size = new System.Drawing.Size(100, 20);
            this.name3TextBox.TabIndex = 1;
            // 
            // name3Label
            // 
            this.name3Label.AutoSize = true;
            this.name3Label.Location = new System.Drawing.Point(35, 47);
            this.name3Label.Name = "name3Label";
            this.name3Label.Size = new System.Drawing.Size(35, 13);
            this.name3Label.TabIndex = 0;
            this.name3Label.Text = "Name";
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(31, 269);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(132, 28);
            this.calculateButton.TabIndex = 3;
            this.calculateButton.Text = "Calculate";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // yearLabel
            // 
            this.yearLabel.AutoSize = true;
            this.yearLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yearLabel.Location = new System.Drawing.Point(63, 336);
            this.yearLabel.Name = "yearLabel";
            this.yearLabel.Size = new System.Drawing.Size(41, 16);
            this.yearLabel.TabIndex = 4;
            this.yearLabel.Text = "Year";
            // 
            // label2018
            // 
            this.label2018.AutoSize = true;
            this.label2018.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2018.Location = new System.Drawing.Point(63, 373);
            this.label2018.Name = "label2018";
            this.label2018.Size = new System.Drawing.Size(39, 15);
            this.label2018.TabIndex = 5;
            this.label2018.Text = "2018";
            // 
            // label2019
            // 
            this.label2019.AutoSize = true;
            this.label2019.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2019.Location = new System.Drawing.Point(63, 407);
            this.label2019.Name = "label2019";
            this.label2019.Size = new System.Drawing.Size(39, 15);
            this.label2019.TabIndex = 6;
            this.label2019.Text = "2019";
            // 
            // label2020
            // 
            this.label2020.AutoSize = true;
            this.label2020.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2020.Location = new System.Drawing.Point(65, 440);
            this.label2020.Name = "label2020";
            this.label2020.Size = new System.Drawing.Size(39, 15);
            this.label2020.TabIndex = 7;
            this.label2020.Text = "2020";
            // 
            // employee1NameLabel
            // 
            this.employee1NameLabel.AutoSize = true;
            this.employee1NameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employee1NameLabel.Location = new System.Drawing.Point(162, 336);
            this.employee1NameLabel.Name = "employee1NameLabel";
            this.employee1NameLabel.Size = new System.Drawing.Size(0, 16);
            this.employee1NameLabel.TabIndex = 8;
            // 
            // employee2NameLabel
            // 
            this.employee2NameLabel.AutoSize = true;
            this.employee2NameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employee2NameLabel.Location = new System.Drawing.Point(266, 336);
            this.employee2NameLabel.Name = "employee2NameLabel";
            this.employee2NameLabel.Size = new System.Drawing.Size(0, 16);
            this.employee2NameLabel.TabIndex = 9;
            // 
            // employee3NameLabel
            // 
            this.employee3NameLabel.AutoSize = true;
            this.employee3NameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employee3NameLabel.Location = new System.Drawing.Point(361, 336);
            this.employee3NameLabel.Name = "employee3NameLabel";
            this.employee3NameLabel.Size = new System.Drawing.Size(0, 16);
            this.employee3NameLabel.TabIndex = 10;
            // 
            // employee1SalaryOutput
            // 
            this.employee1SalaryOutput.AutoSize = true;
            this.employee1SalaryOutput.Location = new System.Drawing.Point(162, 375);
            this.employee1SalaryOutput.Name = "employee1SalaryOutput";
            this.employee1SalaryOutput.Size = new System.Drawing.Size(0, 13);
            this.employee1SalaryOutput.TabIndex = 11;
            // 
            // employee2SalaryOutput
            // 
            this.employee2SalaryOutput.AutoSize = true;
            this.employee2SalaryOutput.Location = new System.Drawing.Point(263, 375);
            this.employee2SalaryOutput.Name = "employee2SalaryOutput";
            this.employee2SalaryOutput.Size = new System.Drawing.Size(0, 13);
            this.employee2SalaryOutput.TabIndex = 12;
            // 
            // employee3SalaryOutput
            // 
            this.employee3SalaryOutput.AutoSize = true;
            this.employee3SalaryOutput.Location = new System.Drawing.Point(361, 375);
            this.employee3SalaryOutput.Name = "employee3SalaryOutput";
            this.employee3SalaryOutput.Size = new System.Drawing.Size(0, 13);
            this.employee3SalaryOutput.TabIndex = 13;
            // 
            // employee1Raise1Output
            // 
            this.employee1Raise1Output.AutoSize = true;
            this.employee1Raise1Output.Location = new System.Drawing.Point(162, 409);
            this.employee1Raise1Output.Name = "employee1Raise1Output";
            this.employee1Raise1Output.Size = new System.Drawing.Size(0, 13);
            this.employee1Raise1Output.TabIndex = 14;
            // 
            // employee2Raise1Output
            // 
            this.employee2Raise1Output.AutoSize = true;
            this.employee2Raise1Output.Location = new System.Drawing.Point(266, 409);
            this.employee2Raise1Output.Name = "employee2Raise1Output";
            this.employee2Raise1Output.Size = new System.Drawing.Size(0, 13);
            this.employee2Raise1Output.TabIndex = 15;
            // 
            // employee3Raise1Output
            // 
            this.employee3Raise1Output.AutoSize = true;
            this.employee3Raise1Output.Location = new System.Drawing.Point(361, 409);
            this.employee3Raise1Output.Name = "employee3Raise1Output";
            this.employee3Raise1Output.Size = new System.Drawing.Size(0, 13);
            this.employee3Raise1Output.TabIndex = 16;
            // 
            // employee1Raise2Output
            // 
            this.employee1Raise2Output.AutoSize = true;
            this.employee1Raise2Output.Location = new System.Drawing.Point(162, 442);
            this.employee1Raise2Output.Name = "employee1Raise2Output";
            this.employee1Raise2Output.Size = new System.Drawing.Size(0, 13);
            this.employee1Raise2Output.TabIndex = 17;
            // 
            // employee2Raise2Output
            // 
            this.employee2Raise2Output.AutoSize = true;
            this.employee2Raise2Output.Location = new System.Drawing.Point(266, 442);
            this.employee2Raise2Output.Name = "employee2Raise2Output";
            this.employee2Raise2Output.Size = new System.Drawing.Size(0, 13);
            this.employee2Raise2Output.TabIndex = 18;
            // 
            // employee3Raise2Output
            // 
            this.employee3Raise2Output.AutoSize = true;
            this.employee3Raise2Output.Location = new System.Drawing.Point(361, 442);
            this.employee3Raise2Output.Name = "employee3Raise2Output";
            this.employee3Raise2Output.Size = new System.Drawing.Size(0, 13);
            this.employee3Raise2Output.TabIndex = 19;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(653, 525);
            this.Controls.Add(this.employee3Raise2Output);
            this.Controls.Add(this.employee2Raise2Output);
            this.Controls.Add(this.employee1Raise2Output);
            this.Controls.Add(this.employee3Raise1Output);
            this.Controls.Add(this.employee2Raise1Output);
            this.Controls.Add(this.employee1Raise1Output);
            this.Controls.Add(this.employee3SalaryOutput);
            this.Controls.Add(this.employee2SalaryOutput);
            this.Controls.Add(this.employee1SalaryOutput);
            this.Controls.Add(this.employee3NameLabel);
            this.Controls.Add(this.employee2NameLabel);
            this.Controls.Add(this.employee1NameLabel);
            this.Controls.Add(this.label2020);
            this.Controls.Add(this.label2019);
            this.Controls.Add(this.label2018);
            this.Controls.Add(this.yearLabel);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.employee3GroupBox);
            this.Controls.Add(this.employee2GroupBox);
            this.Controls.Add(this.employee1GroupBox);
            this.ForeColor = System.Drawing.SystemColors.Desktop;
            this.Name = "Form1";
            this.Text = "Payroll Projections";
            this.employee1GroupBox.ResumeLayout(false);
            this.employee1GroupBox.PerformLayout();
            this.employee2GroupBox.ResumeLayout(false);
            this.employee2GroupBox.PerformLayout();
            this.employee3GroupBox.ResumeLayout(false);
            this.employee3GroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox employee1GroupBox;
        private System.Windows.Forms.TextBox salary1TextBox;
        private System.Windows.Forms.Label salary1Label;
        private System.Windows.Forms.TextBox name1TextBox;
        private System.Windows.Forms.Label name1Label;
        private System.Windows.Forms.GroupBox employee2GroupBox;
        private System.Windows.Forms.TextBox salary2TextBox;
        private System.Windows.Forms.Label salary2Label;
        private System.Windows.Forms.TextBox name2TextBox;
        private System.Windows.Forms.Label name2Label;
        private System.Windows.Forms.GroupBox employee3GroupBox;
        private System.Windows.Forms.TextBox salary3TextBox;
        private System.Windows.Forms.Label salary3Label;
        private System.Windows.Forms.TextBox name3TextBox;
        private System.Windows.Forms.Label name3Label;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label yearLabel;
        private System.Windows.Forms.Label label2018;
        private System.Windows.Forms.Label label2019;
        private System.Windows.Forms.Label label2020;
        private System.Windows.Forms.Label employee1NameLabel;
        private System.Windows.Forms.Label employee2NameLabel;
        private System.Windows.Forms.Label employee3NameLabel;
        private System.Windows.Forms.Label employee1SalaryOutput;
        private System.Windows.Forms.Label employee2SalaryOutput;
        private System.Windows.Forms.Label employee3SalaryOutput;
        private System.Windows.Forms.Label employee1Raise1Output;
        private System.Windows.Forms.Label employee2Raise1Output;
        private System.Windows.Forms.Label employee3Raise1Output;
        private System.Windows.Forms.Label employee1Raise2Output;
        private System.Windows.Forms.Label employee2Raise2Output;
        private System.Windows.Forms.Label employee3Raise2Output;
    }
}

