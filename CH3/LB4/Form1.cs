﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            const double PERCENT_RAISE = 0.04;
            string employee1, employee2, employee3;
            double employee1Salary, employee1Raise1, employee1Raise2, employee2Salary, employee2Raise1, employee2Raise2, employee3Salary, employee3Raise1, employee3Raise2;

            employee1 = name1TextBox.Text;
            employee2 = name2TextBox.Text;
            employee3 = name3TextBox.Text;

            employee1Salary = Convert.ToDouble(salary1TextBox.Text);
            employee2Salary = Convert.ToDouble(salary2TextBox.Text);
            employee3Salary = Convert.ToDouble(salary3TextBox.Text);

            employee1Raise1 = employee1Salary + (employee1Salary * PERCENT_RAISE);
            employee2Raise1 = employee2Salary + (employee2Salary * PERCENT_RAISE);
            employee3Raise1 = employee3Salary + (employee3Salary * PERCENT_RAISE);

            employee1Raise2 = employee1Raise1 + (employee1Raise1 * PERCENT_RAISE);
            employee2Raise2 = employee2Raise1 + (employee2Raise1 * PERCENT_RAISE);
            employee3Raise2 = employee3Raise1 + (employee3Raise1 * PERCENT_RAISE);

            employee1NameLabel.Text = employee1;
            employee2NameLabel.Text = employee2;
            employee3NameLabel.Text = employee3;

            employee1SalaryOutput.Text = employee1Salary.ToString("C");
            employee2SalaryOutput.Text = employee2Salary.ToString("C");
            employee3SalaryOutput.Text = employee3Salary.ToString("C");

            employee1Raise1Output.Text = employee1Raise1.ToString("C");
            employee2Raise1Output.Text = employee2Raise1.ToString("C");
            employee3Raise1Output.Text = employee3Raise1.ToString("C");

            employee1Raise2Output.Text = employee1Raise2.ToString("C");
            employee2Raise2Output.Text = employee2Raise2.ToString("C");
            employee3Raise2Output.Text = employee3Raise2.ToString("C");

        }
    }
}
