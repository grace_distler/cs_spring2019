﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            lblEncodeError.Text = "";
            lblDecodeError.Text = "";
            lblShiftError.Text = "";
        }

        private void btnEncode_Click(object sender, EventArgs e)
        {
            lblEncodeError.Text = "";
            lblDecodeError.Text = "";
            lblShiftError.Text = "";

            var ring = new DecoderRing();
            ring.Shift = int.Parse(txtShift.Text);

            try
            {
                txtDecode.Text = ring.Encode(txtEncode.Text);
            }
            catch(InvalidCharacterException ex)
            {
                lblEncodeError.Text = ex.Message;
            }
        }

        private void btnDecode_Click(object sender, EventArgs e)
        {
            lblEncodeError.Text = "";
            lblDecodeError.Text = "";
            lblShiftError.Text = "";

            var ring = new DecoderRing();
            ring.Shift = int.Parse(txtShift.Text);

            try
            {
                txtEncode.Text = ring.Decode(txtDecode.Text);
            }
            catch (InvalidCharacterException ex)
            {
                lblDecodeError.Text = ex.Message;
            }
        }
    }
}
