﻿using System;

namespace LB3
{
    public class InvalidCharacterException : Exception
    {
        public InvalidCharacterException() : base("INVALID CHARACTER")
        {

        }
        public InvalidCharacterException(string message) : base(message)
        {

        }
    }
}
