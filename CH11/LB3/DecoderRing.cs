﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB3
{
    public class DecoderRing
    {
        private int _shift = 0;
        private string _innerRing = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private string _outerRing = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public DecoderRing()
        {

        }
        public string Decode(string text)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < text.Length; ++i)
            {
                result.Append(DecodeChar(text[i]));
            }
            return result.ToString();
        }
        public string Encode(string text)
        {
            StringBuilder results = new StringBuilder();
            for (int i = 0; i < text.Length; ++i)
            {
                results.Append(EncodeChar(text[i]));
            }
            return results.ToString();
        }

        private char DecodeChar(char c)
        {
            if (char.IsWhiteSpace(c))
            {
                return c;
            }
            if (char.IsLetter(c))
            {
                for (int i = 0; i < _outerRing.Length; ++i)
                {
                    if (char.ToUpper(c) == _outerRing[i])
                    {
                        return _innerRing[(i + _shift) % 26];
                    }
                }
                throw new InvalidCharacterException($"{c} is invalid");
                //int index = char.ToUpper(c) - 'A';
                //return _outerRing[(index + 26 - _shift) % 26];
            }
            else
            {
                throw new InvalidCharacterException($"{c} is invalid");
            }
        }
        private char EncodeChar(char c)
        {
            if (char.IsWhiteSpace(c))
            {
                return c;
            }
            if (char.IsLetter(c))
            {
                for (int i = 0; i < _innerRing.Length; ++i)
                {
                    if (char.ToUpper(c) == _innerRing[i])
                    {
                        return _outerRing[(i + 26 - _shift) % 26];
                    }
                }
                throw new InvalidCharacterException($"{c} is invalid");
                //int index = char.ToUpper(c) - 'A';
                //return _outerRing[(index + 26 - _shift) % 26];
            }
            else
            {
                throw new InvalidCharacterException($"{c} is invalid");
            }
        }

        public int Shift
        {
            get
            {
                return _shift;
            }
            set
            {
                _shift = value;
            }
        }
    }
}
