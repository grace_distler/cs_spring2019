﻿namespace LB3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtEncode = new System.Windows.Forms.TextBox();
            this.lblEncodeError = new System.Windows.Forms.Label();
            this.btnDecode = new System.Windows.Forms.Button();
            this.lblDecodeError = new System.Windows.Forms.Label();
            this.txtDecode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtShift = new System.Windows.Forms.TextBox();
            this.btnEncode = new System.Windows.Forms.Button();
            this.lblShiftError = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtEncode
            // 
            this.txtEncode.Location = new System.Drawing.Point(46, 48);
            this.txtEncode.Name = "txtEncode";
            this.txtEncode.Size = new System.Drawing.Size(490, 22);
            this.txtEncode.TabIndex = 1;
            // 
            // lblEncodeError
            // 
            this.lblEncodeError.AutoSize = true;
            this.lblEncodeError.ForeColor = System.Drawing.Color.Red;
            this.lblEncodeError.Location = new System.Drawing.Point(43, 73);
            this.lblEncodeError.Name = "lblEncodeError";
            this.lblEncodeError.Size = new System.Drawing.Size(0, 17);
            this.lblEncodeError.TabIndex = 1;
            // 
            // btnDecode
            // 
            this.btnDecode.Location = new System.Drawing.Point(598, 116);
            this.btnDecode.Name = "btnDecode";
            this.btnDecode.Size = new System.Drawing.Size(75, 30);
            this.btnDecode.TabIndex = 5;
            this.btnDecode.Text = "Decode";
            this.btnDecode.UseVisualStyleBackColor = true;
            this.btnDecode.Click += new System.EventHandler(this.btnDecode_Click);
            // 
            // lblDecodeError
            // 
            this.lblDecodeError.AutoSize = true;
            this.lblDecodeError.ForeColor = System.Drawing.Color.Red;
            this.lblDecodeError.Location = new System.Drawing.Point(43, 141);
            this.lblDecodeError.Name = "lblDecodeError";
            this.lblDecodeError.Size = new System.Drawing.Size(0, 17);
            this.lblDecodeError.TabIndex = 4;
            // 
            // txtDecode
            // 
            this.txtDecode.Location = new System.Drawing.Point(46, 116);
            this.txtDecode.Name = "txtDecode";
            this.txtDecode.Size = new System.Drawing.Size(490, 22);
            this.txtDecode.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(747, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Shift";
            // 
            // txtShift
            // 
            this.txtShift.Location = new System.Drawing.Point(750, 74);
            this.txtShift.Name = "txtShift";
            this.txtShift.Size = new System.Drawing.Size(90, 22);
            this.txtShift.TabIndex = 3;
            // 
            // btnEncode
            // 
            this.btnEncode.Location = new System.Drawing.Point(598, 44);
            this.btnEncode.Name = "btnEncode";
            this.btnEncode.Size = new System.Drawing.Size(75, 30);
            this.btnEncode.TabIndex = 4;
            this.btnEncode.Text = "Encode";
            this.btnEncode.UseVisualStyleBackColor = true;
            this.btnEncode.Click += new System.EventHandler(this.btnEncode_Click);
            // 
            // lblShiftError
            // 
            this.lblShiftError.AutoSize = true;
            this.lblShiftError.ForeColor = System.Drawing.Color.Red;
            this.lblShiftError.Location = new System.Drawing.Point(747, 99);
            this.lblShiftError.Name = "lblShiftError";
            this.lblShiftError.Size = new System.Drawing.Size(0, 17);
            this.lblShiftError.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 195);
            this.Controls.Add(this.lblShiftError);
            this.Controls.Add(this.btnEncode);
            this.Controls.Add(this.txtShift);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnDecode);
            this.Controls.Add(this.lblDecodeError);
            this.Controls.Add(this.txtDecode);
            this.Controls.Add(this.lblEncodeError);
            this.Controls.Add(this.txtEncode);
            this.Name = "Form1";
            this.Text = "Decoder Ring";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtEncode;
        private System.Windows.Forms.Label lblEncodeError;
        private System.Windows.Forms.Button btnDecode;
        private System.Windows.Forms.Label lblDecodeError;
        private System.Windows.Forms.TextBox txtDecode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtShift;
        private System.Windows.Forms.Button btnEncode;
        private System.Windows.Forms.Label lblShiftError;
    }
}

