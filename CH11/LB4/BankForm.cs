﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB4
{
    public partial class BankForm : Form
    {
        Account[] accounts = new Account[5];

        public BankForm()
        {
            InitializeComponent();

            accounts[0] = new Account("gdistler", "abc123", "Grace", false);
            accounts[1] = new Account("egudmestad", "123abc", "Evan", false);
            accounts[2] = new Account("prsmith2", "prsmith2", "Smith", true);
            accounts[3] = new Account("prsmith", "987654", "Smith", false);
            accounts[4] = new Account("agrande", "3145", "Ariana", false);
        }

        public void Login(string username, string password)
        {
            btnLogin.Click += btnLogin_Click;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                string username = txtUsername.Text;
                string password = txtPassword.Text;
                if (username == "" || password == "")
                {
                    throw new NoUsernamePasswordException();
                }

                bool found = false;
                string nameFound = null;
                foreach (var item in accounts)
                {
                    if (item.Username == username && item.Password == password)
                    {
                        found = true;
                        if (item.IsDisabled == true)
                        {
                            throw new AccountDisabledException();
                        }
                        nameFound = item.FirstName;
                        break;
                    }
                }
                if (found == false)
                {
                    throw new IncorrectPasswordException();
                }

                lblMessage.ForeColor = Color.Black;
                lblMessage.Text = "Welcome, " + nameFound;

            }
            catch (NoUsernamePasswordException ec)
            {
                lblMessage.ForeColor = Color.Red;
                lblMessage.Text = "You must enter a username and password";
            }
            catch (IncorrectPasswordException ec)
            {
                lblMessage.ForeColor = Color.Red;
                lblMessage.Text = "Incorrect Username/Password";
            }
            catch (AccountDisabledException ec)
            {
                lblMessage.ForeColor = Color.Red;
                lblMessage.Text = "Account Disabled";
            }
        }
    }
}
