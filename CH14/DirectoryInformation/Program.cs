﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;
using System.IO;

namespace DirectoryInformation
{
    class Program
    {
        static void Main()
        {
            const string END = "end";
            string directoryName;
            string[] listOfFiles;

            Write("Enter a directory name >> ");
            directoryName = ReadLine();

            while (directoryName != END)
            {
                if (Directory.Exists(directoryName))
                {
                    WriteLine("\nDirectory Exists, and contains the following: ");
                    listOfFiles = Directory.GetFiles(directoryName);
                    for (int x = 0; x < listOfFiles.Length; ++x)
                    {
                        WriteLine("   {0}", listOfFiles[x]);
                    }
                }
                else
                {
                    WriteLine("\nDirectory Does Not Exist");
                }

                WriteLine("\n\nEnter a directory name.");
                Write("   (Enter " + END + " to quit) >> ");
                directoryName = ReadLine();
            }
        }
    }
}
