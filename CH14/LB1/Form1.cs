﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;

namespace LB1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            //string[] files = Directory.GetFiles(path);

            //foreach (var file in files)
            //{
            //    lblPath.Text += file.ToString() + "\n";
            //}
        }
        string path;

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog() { Description = "Select Your Path" };

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                path = fbd.SelectedPath;
                txtPath.Text = path;

                GetFiles();
            }
        }

        private void GetFiles()
        {
            lstFiles.Items.Clear();
            
            string[] files = Directory.GetFiles(path);

            for (int i = 0; i < files.Length; i++)
            {
                lstFiles.Items.Add(Path.GetFileName(files[i]));
            }
        }

        private void lstFiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            chkboxReadOnly.Visible = true;

            string fullPath = Path.Combine(path, lstFiles.SelectedItem.ToString());
            //MessageBox.Show(fullPath);

            lblFileInfo.Text = $"File Creation Time: {File.GetCreationTime(fullPath).ToString()}\n";
            lblFileInfo.Text += $"File Last Write Time: {File.GetLastWriteTime(fullPath).ToString()}\n";
            lblFileInfo.Text += $"File Last Access Time: {File.GetLastAccessTime(fullPath).ToString()}";

            if (File.GetAttributes(fullPath).ToString().Contains("ReadOnly"))
            {
                chkboxReadOnly.Checked = true;
            }
            else
            {
                chkboxReadOnly.Checked = false;
            }
            lblPath.Text = fullPath;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            GetFiles();
        }
    }
}
