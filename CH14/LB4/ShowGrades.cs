﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB4
{
    public partial class ShowGrades : Form
    {
        public ShowGrades()
        {
            InitializeComponent();

            FileStream inFile = new FileStream("grades.csv", FileMode.Open, FileAccess.Read);
            StreamReader fileReader = new StreamReader(inFile);

            int numAssignments = 0;
            double totalOfScores = 0;

            string rowOfData = fileReader.ReadLine();

            while (rowOfData != null)
            {
                string[] fields = rowOfData.Split(',');
                Assignment nextAssignment = new Assignment(fields[0], fields[1], Convert.ToDouble(fields[2]));
                numAssignments++;
                totalOfScores += nextAssignment.AssignmentGrade;

                //read newt line from file
                rowOfData = fileReader.ReadLine();

                //output assignments to form
                lblGrades.Text += $"{nextAssignment.AssignmentType,-10}{nextAssignment.AssignmentName,-15}{nextAssignment.AssignmentGrade,7}\n";
            }

            double avg = totalOfScores / numAssignments;
            lblAverage.Text = $"Average Grade: {avg}%";
        }
    }
}
