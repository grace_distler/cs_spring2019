﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB4
{
    public class Assignment
    {
        public Assignment(string type, string name, double grade)
        {
            if (String.IsNullOrEmpty(type) || String.IsNullOrEmpty(name) || grade < 0 || grade > 100)
            {
                throw new Exception();
            }
            AssignmentType = type;
            AssignmentName = name;
            AssignmentGrade = grade;
        }

        public string AssignmentType { get; set; }
        public string AssignmentName { get; set; }
        public double AssignmentGrade { get; set; }

        public override string ToString()
        {
            return $"{AssignmentType} {AssignmentName} {AssignmentGrade}";
        }
    }
}