﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            cmbAssignment.Items.AddRange(new string[] { "Homework", "Lab", "Test" });
            cmbAssignment.SelectedIndex = 0;
        }

        private void btnAppend_Click(object sender, EventArgs e)
        {
            //clear the error message
            lblError.Text = "";
            Assignment nextAssignment = null;

            try
            {
                //create an assignment
                nextAssignment = new Assignment(cmbAssignment.SelectedItem.ToString(), txtName.Text, Convert.ToDouble(txtGrade.Text));
            }
            catch(Exception ex)
            {
                //handle any errors in the assignment
                lblError.Text = "ERROR";
                return;
            }

            FileStream outFile = new FileStream("grades.csv", FileMode.Append, FileAccess.Write);
            StreamWriter fileWriter = new StreamWriter(outFile);

            fileWriter.WriteLine(nextAssignment.ToString());
            fileWriter.Close();
            outFile.Close();
            MessageBox.Show("Assignment Saved");
            Reset();
            cmbAssignment.Focus();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void Reset()
        {
            cmbAssignment.Focus();
            lblError.Text = "";
            txtName.Text = "";
            txtGrade.Text = "";
        }

        private void btnShowAll_Click(object sender, EventArgs e)
        {
            ShowGrades gradesForm = new ShowGrades();
            gradesForm.ShowDialog();
        }
    }
}
