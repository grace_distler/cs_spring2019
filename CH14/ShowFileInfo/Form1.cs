﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ShowFileInfo
{
    public partial class Form1 : Form
    {
        private string path;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //lstFiles.Items.AddRange(Directory.GetFiles(@"C:\Users\User\Desktop\cs_spring2019\CH14\ShowFileInfo"));
            lstFiles.Items.AddRange(Directory.GetFiles(@"C:\Users\User\Documents"));
        }

        private void lstFiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            path = lstFiles.SelectedItem.ToString();
            lblCreationTime.Text = $"Creation Time: {File.GetCreationTime(path).ToString()}";
        }

       
    }
}
