﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB2
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double BalanceOwed { get; set; }

        public override string ToString()
        {
            return $"{Id}, {Name}, {BalanceOwed}";
        }
    }
}
