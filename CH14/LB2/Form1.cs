﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace LB2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //instantiate a customer object and fill properties with data
            Customer c1 = new Customer() { Id = Convert.ToInt32(txtID.Text), Name = txtName.Text, BalanceOwed = Convert.ToDouble(txtBalanceOwed.Text) };

            //open file
            //FileStream outFile = new FileStream("customer.txt", FileMode.Create, FileAccess.Write);
            //StreamWriter fileWriter = new StreamWriter(outFile);

            SaveFileDialog save = new SaveFileDialog();

            //save.FileName = txtFileName.Text;
            save.Filter = "Text File | *.txt";

            if (save.ShowDialog() == DialogResult.OK)
            {
                StreamWriter fileWriter = new StreamWriter(save.OpenFile());
                fileWriter.WriteLine(c1.ToString());
                fileWriter.Close();
                MessageBox.Show("Customer Created");
            }
            else
            {
                MessageBox.Show("It Broke...");
            }

            //write to file
            //fileWriter.WriteLine(c1.ToString());

            ////close file
            //fileWriter.Close();
            //outFile.Close();

            txtID.Text = "";
            txtName.Text = "";
            txtBalanceOwed.Text = "";
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            //open file
            //FileStream inFile = new FileStream("Customer.txt", FileMode.Open, FileAccess.Read);
            OpenFileDialog ofd = new OpenFileDialog();
            StreamReader fileReader = null;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                fileReader = new StreamReader(ofd.FileName.ToString());
            }

            //read file
            string rowOfData = fileReader.ReadLine();

            //split the row into fields
            string[] fields = rowOfData.Split(',');

            //create a customer using data from file
            Customer c1 = new Customer() { Id = Convert.ToInt32(fields[0]), Name = fields[1], BalanceOwed = Convert.ToDouble(fields[2]) };

            //show data in the form labels
            txtID.Text = c1.Id.ToString();
            txtName.Text = c1.Name;
            txtBalanceOwed.Text = c1.BalanceOwed.ToString("C");
        }
    }
}
