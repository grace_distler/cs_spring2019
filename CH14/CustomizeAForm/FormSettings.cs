﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomizeAForm
{
    public class FormSettings
    {
        public FormSettings()
        {

        }

        public string BackColor { get; set; }
        public string Size { get; set; }
        //public int ItemWidth { get; set; }
        //public int ItemHeight { get; set; }
        public string Title { get; set; }

        public override string ToString()
        {
            return $"{BackColor} {Size} {Title}";
        }
    }
}
