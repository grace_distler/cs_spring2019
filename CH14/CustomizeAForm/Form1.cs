﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CustomizeAForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            cmbBackColor.Items.AddRange(GetColors());
            cmbBackColor.SelectedIndex = 0;

            cmbSize.Items.AddRange(GetSizes());
            cmbSize.SelectedIndex = 0;
        }


        private string[] GetColors()
        {
            return new string[]
            {
                    "Please Select Color",
                    "White",
                    "Gray",
                    "Peach"
            };
        }

        private string[] GetSizes()
        {
            return new string[]
            {
                    "Please Select Size",
                    "520 x 390",
                    "440 x 350",
                    "670 x 490"
            };
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            FileStream form = new FileStream("formSettings.txt", FileMode.Create, FileAccess.Write);
            StreamWriter stream = new StreamWriter(form);

            stream.WriteLine(this.BackColor);
            stream.WriteLine(this.Size);
            stream.WriteLine(txtTitle.Text);

            form.Close();
            stream.Close();

            MessageBox.Show("Form Settings Saved");
        }

        private void cmbBackColor_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = cmbBackColor.SelectedIndex;
            Control form = (Control)sender;
            if (index == 1)
            {
                form.BackColor = Color.White;
            }
            else if (index == 2)
            {
                form.BackColor = Color.Gray;
            }
            else if (index == 3)
            {
                form.BackColor = Color.PeachPuff;
            }
        }

        private void cmbSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = cmbSize.SelectedIndex;
            Control form = (Control)sender;
            if (index == 1)
            {
                int x = 520;
                int y = 390;

                form.Size = x,y;
            }
            else if (index == 2)
            {
                int x = 440;
                int y = 350;

                form.Size = System.Drawing.Size(x,y);
            }
            else if (index == 3)
            {
                int x = 670;
                int y = 490;

               form.Size = x,y;
            }
        }

        private void txtTitle_TextChanged(object sender, EventArgs e)
        {
            Control form = (Control)sender;
            form.Text = txtTitle.Text;
        }
    }
}