﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace HighScore
{
    public partial class Form1 : Form
    {
        int numRounds = 0;
        int userScore = 0;
        int highScore = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnGuess_Click(object sender, EventArgs e)
        {
            string userPick = txtUserGuess.Text.ToLower();
            DetermineWinner(userPick);
        }

        private void DetermineWinner(string userPick)
        {
            userPick = txtUserGuess.Text.ToLower();
            Random r1 = new Random();
            int pcPick = r1.Next(0, 3);
            //0 = A
            //1 = B
            //2 = C

            if (userPick.Equals("a"))
            {
                numRounds++;
                lblUserGuess.Text = "You Guessed A";
                if (pcPick == 0)
                {
                    lblPCGuess.Text = "PC Picked A";
                    userScore++;
                    lblScore.Text = $"Score: {userScore}";
                }
                else if (pcPick == 1)
                {
                    lblPCGuess.Text = "PC Picked B";
                }
                else
                {
                    lblPCGuess.Text = "PC Picked C";
                }
            }
            else if (userPick.Equals("b"))
            {
                numRounds++;
                lblUserGuess.Text = "You Guessed B";
                if (pcPick == 0)
                {
                    lblPCGuess.Text = "PC Picked A";
                }
                else if (pcPick == 1)
                {
                    lblPCGuess.Text = "PC Picked B";
                    userScore++;
                    lblScore.Text = $"Score: {userScore}";
                }
                else
                {
                    lblPCGuess.Text = "PC Picked C";
                }
            }
            else if (userPick.Equals("c"))
            {
                numRounds++;
                lblUserGuess.Text = "You Guessed C";
                if (pcPick == 0)
                {
                    lblPCGuess.Text = "PC Picked A";
                }
                else if (pcPick == 1)
                {
                    lblPCGuess.Text = "PC Picked B";
                }
                else
                {
                    lblPCGuess.Text = "PC Picked C";
                    userScore++;
                    lblScore.Text = $"Score: {userScore}";
                }
            }
            else
            {
                lblPCGuess.Text = "";
                lblUserGuess.Text = "Please Guess A, B, or C";
            }

            lblRound.Text = $"Round: {numRounds}/10";

            if (numRounds == 10)
            {
                MessageBox.Show("Game Over");
                txtUserGuess.Enabled = false;
                btnGuess.Enabled = false;

                if (userScore > highScore)
                {
                    FileStream outFile = new FileStream("highscore.txt", FileMode.Create, FileAccess.Write);
                    StreamWriter fileWriter = new StreamWriter(outFile);
                    fileWriter.WriteLine(userScore);
                    lblHighScore.Text = $"High Score: {highScore}";
                    fileWriter.Close();
                    outFile.Close();
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (File.Exists("highscore.txt"))
            {
                FileStream inFile = new FileStream("highscore.txt", FileMode.Open, FileAccess.Read);
                StreamReader fileReader = new StreamReader(inFile);
                highScore = Convert.ToInt32(fileReader.ReadLine());
                lblHighScore.Text = $"High Score: {highScore}";
                fileReader.Close();
                inFile.Close();
            }
        }
    }
}
