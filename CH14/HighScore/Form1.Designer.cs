﻿namespace HighScore
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtUserGuess = new System.Windows.Forms.TextBox();
            this.btnGuess = new System.Windows.Forms.Button();
            this.lblHighScore = new System.Windows.Forms.Label();
            this.lblRound = new System.Windows.Forms.Label();
            this.lblScore = new System.Windows.Forms.Label();
            this.lblUserGuess = new System.Windows.Forms.Label();
            this.lblPCGuess = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(260, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Guess the Computer\'s Choice  (A, B, C):";
            // 
            // txtUserGuess
            // 
            this.txtUserGuess.Location = new System.Drawing.Point(37, 67);
            this.txtUserGuess.Name = "txtUserGuess";
            this.txtUserGuess.Size = new System.Drawing.Size(257, 22);
            this.txtUserGuess.TabIndex = 1;
            // 
            // btnGuess
            // 
            this.btnGuess.Location = new System.Drawing.Point(316, 63);
            this.btnGuess.Name = "btnGuess";
            this.btnGuess.Size = new System.Drawing.Size(85, 31);
            this.btnGuess.TabIndex = 2;
            this.btnGuess.Text = "Guess";
            this.btnGuess.UseVisualStyleBackColor = true;
            this.btnGuess.Click += new System.EventHandler(this.btnGuess_Click);
            // 
            // lblHighScore
            // 
            this.lblHighScore.AutoSize = true;
            this.lblHighScore.Location = new System.Drawing.Point(34, 201);
            this.lblHighScore.Name = "lblHighScore";
            this.lblHighScore.Size = new System.Drawing.Size(82, 17);
            this.lblHighScore.TabIndex = 3;
            this.lblHighScore.Text = "High Score:";
            // 
            // lblRound
            // 
            this.lblRound.AutoSize = true;
            this.lblRound.Location = new System.Drawing.Point(34, 268);
            this.lblRound.Name = "lblRound";
            this.lblRound.Size = new System.Drawing.Size(58, 17);
            this.lblRound.TabIndex = 4;
            this.lblRound.Text = "Round: ";
            // 
            // lblScore
            // 
            this.lblScore.AutoSize = true;
            this.lblScore.Location = new System.Drawing.Point(34, 235);
            this.lblScore.Name = "lblScore";
            this.lblScore.Size = new System.Drawing.Size(49, 17);
            this.lblScore.TabIndex = 5;
            this.lblScore.Text = "Score:";
            // 
            // lblUserGuess
            // 
            this.lblUserGuess.AutoSize = true;
            this.lblUserGuess.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserGuess.Location = new System.Drawing.Point(34, 114);
            this.lblUserGuess.Name = "lblUserGuess";
            this.lblUserGuess.Size = new System.Drawing.Size(96, 18);
            this.lblUserGuess.TabIndex = 6;
            this.lblUserGuess.Text = "User Guess: ";
            // 
            // lblPCGuess
            // 
            this.lblPCGuess.AutoSize = true;
            this.lblPCGuess.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPCGuess.Location = new System.Drawing.Point(34, 146);
            this.lblPCGuess.Name = "lblPCGuess";
            this.lblPCGuess.Size = new System.Drawing.Size(81, 18);
            this.lblPCGuess.TabIndex = 7;
            this.lblPCGuess.Text = "PC Guess:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 314);
            this.Controls.Add(this.lblPCGuess);
            this.Controls.Add(this.lblUserGuess);
            this.Controls.Add(this.lblScore);
            this.Controls.Add(this.lblRound);
            this.Controls.Add(this.lblHighScore);
            this.Controls.Add(this.btnGuess);
            this.Controls.Add(this.txtUserGuess);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Guessing Game";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtUserGuess;
        private System.Windows.Forms.Button btnGuess;
        private System.Windows.Forms.Label lblHighScore;
        private System.Windows.Forms.Label lblRound;
        private System.Windows.Forms.Label lblScore;
        private System.Windows.Forms.Label lblUserGuess;
        private System.Windows.Forms.Label lblPCGuess;
    }
}

