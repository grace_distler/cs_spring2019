﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileComparison
{
    class Program
    {
        static void Main()
        {
            string FILE_NAME = "movieQuote";

            FileInfo txtFile = new FileInfo(@"C:\Users\User\Desktop\cs_spring2019\CH14\FileComparison\bin\Debug\movieQuote.txt");
            FileInfo docFile = new FileInfo(@"C:\Users\User\Desktop\cs_spring2019\CH14\FileComparison\bin\Debug\movieQuote.doc");

            long txtSize = txtFile.Length;
            long docSize = docFile.Length;

            float txtToDocRatio = (float)txtSize / docSize;
            float docToTxtRatio = (float)docSize / txtSize;

            //output
            Console.WriteLine("File Comparison:");
            Console.WriteLine("\n\t" + FILE_NAME + ".txt");
            Console.WriteLine("\tSize: " + txtSize);

            Console.WriteLine("\n\t" + FILE_NAME + ".doc");
            Console.WriteLine("\tSize: " + docSize);

            Console.WriteLine($"\n\tRatio of size DOC to TEXT (doc/txt): " + docToTxtRatio + "\n\n\tRatio of size TEXT to DOC (txt/doc): " + txtToDocRatio);
            Console.WriteLine("");
            Console.WriteLine("");
            Console.Write("Press any key to continue . . . ");
            Console.ReadKey(true);
        }
    }
}