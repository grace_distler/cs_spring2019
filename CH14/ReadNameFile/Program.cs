﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;
using System.IO;

class ReadNameFile
{
    static void Main()
    {
        FileStream file = new FileStream("Names.txt", FileMode.Open, FileAccess.Read);
        StreamReader reader = new StreamReader(file);
        int count = 1;
        string name;
    
        WriteLine("Displaying All Names\n");
        name = reader.ReadLine();
        while(name != null)
        {
            WriteLine("" + count + " " + name);
            name = reader.ReadLine();
            ++count;
        }
        ReadLine();
        reader.Close();
        file.Close();
    }
}
