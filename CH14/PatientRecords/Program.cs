﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace PatientRecords
{
    class Program
    {
        static void Main(string[] args)
        {
            //WritePatientRecords
            StreamWriter stream = new StreamWriter("patientRecords.txt");
            {
                Patient currentPatient = new Patient();
                Console.WriteLine("Patient Records:");
                Console.WriteLine("\tEnter patient ID:");
                Console.Write("\t(Enter 999 to exit)  >>>  ");
                currentPatient.Number = Convert.ToInt32(Console.ReadLine());

                while (currentPatient.Number != 999)
                {
                    stream.Write(currentPatient.Number + "");

                    Console.Write("\tEnter patient name: ");
                    currentPatient.Name = Console.ReadLine();
                    stream.Write(currentPatient.Name + "");

                    Console.Write("\tEnter patient balance: ");
                    currentPatient.Balance = Convert.ToDouble(Console.ReadLine());
                    stream.Write(currentPatient.Balance + "");

                    Console.WriteLine("\n\tEnter patient ID:");
                    Console.Write("\t(Enter 999 to exit)  >>> ");
                    currentPatient.Number = Convert.ToInt32(Console.ReadLine());
                }
            }
            Console.ReadKey();

            //ReadPatientRecords
            string readFile = "";

            try
            {
                StreamReader reader = new StreamReader("patientRecords.txt");
                {
                    while ((readFile = reader.ReadLine()) != null)
                    {
                        Console.WriteLine(readFile);
                    }
                }
            }
            catch
            {
                Console.WriteLine("ERROR! The file is invalid");
            }
            Console.ReadKey();

            //FindPatientRecords
            string file = "";
            int id;

            Console.Write("\n\tEnter ID to be searched >>> ");
            id = Convert.ToInt32(Console.ReadLine());

            try
            {
                StreamReader fileReader = new StreamReader("patientRecords.txt");
                {
                    while((file=fileReader.ReadLine()) != null)
                    {
                        string[] tokens = file.Split(',');

                        if (Convert.ToInt32(tokens[0]) == id)
                        {
                            Console.WriteLine(file);
                        }
                    }
                }
            }
            catch
            {
                Console.WriteLine("ERROR! The file is invalid");
            }
            Console.ReadKey();

            //FindPatientRecords2
            string files = "";
            int idNum;

            Console.Write("\n\tEnter minimum balance: >> ");
            idNum = Convert.ToInt32(Console.ReadLine());

            try
            {
                StreamReader streamRead = new StreamReader("patientRecords.txt");
                {
                    while ((files = streamRead.ReadLine()) != null)

                    {
                        string[] token = files.Split(' ');

                        if (Convert.ToInt32(token[2]) >= id)
                        {
                            Console.WriteLine(files);
                        }
                    }
                }
            }
            catch
            {
                Console.WriteLine("Exception!!!The file is invalid or no data found");
            }
            Console.ReadKey();
        }
    }
}
