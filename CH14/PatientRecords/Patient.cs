﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientRecords
{
    public class Patient
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public double Balance { get; set; }
    }
}
