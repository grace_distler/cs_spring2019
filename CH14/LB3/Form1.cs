﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB3
{
    public partial class Form1 : Form
    {
        int numUserWins = 0;
        int numRounds = 0;
        int highScore = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void picRock_Click(object sender, EventArgs e)
        {
            DetermineWinner("rock");
        }

        private void picPaper_Click(object sender, EventArgs e)
        {
            DetermineWinner("paper");
        }

        private void picScissors_Click(object sender, EventArgs e)
        {
            DetermineWinner("scissors");
        }

        private void DetermineWinner(string userPick)
        {
            Random r1 = new Random();
            int pcPick = r1.Next(0, 3);
            numRounds++;
            lblRound.Text = $"Round: {numRounds}/10";
            //0 = rock
            //1 = paper
            //2 = scissors

            if (userPick.Equals("rock"))
            {
                lblUserPick.Text = "You Picked Rock";
                if (pcPick == 0)
                {
                    lblPCPick.Text = "PC Picked Rock";
                    lblWinner.Text = "It's a TIE";
                }
                else if (pcPick == 1)
                {
                    lblPCPick.Text = "PC Picked Paper";
                    lblWinner.Text = "PC Wins";
                }
                else
                {
                    lblPCPick.Text = "PC Picked Scissors";
                    lblWinner.Text = "YOU Win";
                    numUserWins++;
                    lblScore.Text = $"Score: {numUserWins}";
                }
            }
            else if (userPick.Equals("paper"))
            {
                lblUserPick.Text = "You Picked Paper";
                if (pcPick == 0)
                {
                    lblPCPick.Text = "PC Picked Rock";
                    lblWinner.Text = "YOU Win";
                    numUserWins++;
                    lblScore.Text = $"Score: {numUserWins}";
                }
                else if (pcPick == 1)
                {
                    lblPCPick.Text = "PC Picked Paper";
                    lblWinner.Text = "It's a TIE";
                }
                else
                {
                    lblPCPick.Text = "PC Picked Scissors";
                    lblWinner.Text = "PC Wins";
                }
            }
            else if (userPick.Equals("scissors"))
            {
                lblUserPick.Text = "You Picked Scissors";
                if (pcPick == 0)
                {
                    lblPCPick.Text = "PC Picked Rock";
                    lblWinner.Text = "PC Wins";
                }
                else if (pcPick == 1)
                {
                    lblPCPick.Text = "PC Picked Paper";
                    lblWinner.Text = "YOU Win";
                    numUserWins++;
                    lblScore.Text = $"Score: {numUserWins}";
                }
                else
                {
                    lblPCPick.Text = "PC Picked Scissors";
                    lblWinner.Text = "It's a TIE";
                }
            }

            if (numRounds == 10)
            {
                MessageBox.Show("Game Over");
                picRock.Enabled = false;
                picPaper.Enabled = false;
                picScissors.Enabled = false;

                if (numUserWins > highScore)
                {
                    FileStream outFile = new FileStream("highscore.txt", FileMode.Create, FileAccess.Write);
                    StreamWriter fileWriter = new StreamWriter(outFile);
                    fileWriter.WriteLine(numUserWins);
                    lblHighScore.Text = $"High Score: {highScore}";
                    fileWriter.Close();
                    outFile.Close();
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (File.Exists("highscore.txt"))
            {
                FileStream inFile = new FileStream("highscore.txt", FileMode.Open, FileAccess.Read);
                StreamReader fileReader = new StreamReader(inFile);
                highScore = Convert.ToInt32(fileReader.ReadLine());
                lblHighScore.Text = $"High Score: {highScore}";
                fileReader.Close();
                inFile.Close();
            }
        }
    }
}
