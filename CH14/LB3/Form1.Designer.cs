﻿namespace LB3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblScore = new System.Windows.Forms.Label();
            this.lblRound = new System.Windows.Forms.Label();
            this.lblHighScore = new System.Windows.Forms.Label();
            this.lblUserPick = new System.Windows.Forms.Label();
            this.lblPCPick = new System.Windows.Forms.Label();
            this.lblWinner = new System.Windows.Forms.Label();
            this.picScissors = new System.Windows.Forms.PictureBox();
            this.picPaper = new System.Windows.Forms.PictureBox();
            this.picRock = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picScissors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaper)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRock)).BeginInit();
            this.SuspendLayout();
            // 
            // lblScore
            // 
            this.lblScore.AutoSize = true;
            this.lblScore.Location = new System.Drawing.Point(30, 34);
            this.lblScore.Name = "lblScore";
            this.lblScore.Size = new System.Drawing.Size(53, 17);
            this.lblScore.TabIndex = 0;
            this.lblScore.Text = "Score: ";
            // 
            // lblRound
            // 
            this.lblRound.AutoSize = true;
            this.lblRound.Location = new System.Drawing.Point(150, 34);
            this.lblRound.Name = "lblRound";
            this.lblRound.Size = new System.Drawing.Size(54, 17);
            this.lblRound.TabIndex = 1;
            this.lblRound.Text = "Round:";
            // 
            // lblHighScore
            // 
            this.lblHighScore.AutoSize = true;
            this.lblHighScore.Location = new System.Drawing.Point(490, 34);
            this.lblHighScore.Name = "lblHighScore";
            this.lblHighScore.Size = new System.Drawing.Size(82, 17);
            this.lblHighScore.TabIndex = 2;
            this.lblHighScore.Text = "High Score:";
            // 
            // lblUserPick
            // 
            this.lblUserPick.AutoSize = true;
            this.lblUserPick.Location = new System.Drawing.Point(248, 325);
            this.lblUserPick.Name = "lblUserPick";
            this.lblUserPick.Size = new System.Drawing.Size(0, 17);
            this.lblUserPick.TabIndex = 3;
            // 
            // lblPCPick
            // 
            this.lblPCPick.AutoSize = true;
            this.lblPCPick.Location = new System.Drawing.Point(248, 352);
            this.lblPCPick.Name = "lblPCPick";
            this.lblPCPick.Size = new System.Drawing.Size(0, 17);
            this.lblPCPick.TabIndex = 4;
            // 
            // lblWinner
            // 
            this.lblWinner.AutoSize = true;
            this.lblWinner.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWinner.Location = new System.Drawing.Point(246, 378);
            this.lblWinner.Name = "lblWinner";
            this.lblWinner.Size = new System.Drawing.Size(0, 29);
            this.lblWinner.TabIndex = 5;
            // 
            // picScissors
            // 
            this.picScissors.BackColor = System.Drawing.Color.White;
            this.picScissors.Image = global::LB3.Properties.Resources.scissors1;
            this.picScissors.Location = new System.Drawing.Point(418, 81);
            this.picScissors.Name = "picScissors";
            this.picScissors.Size = new System.Drawing.Size(154, 215);
            this.picScissors.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picScissors.TabIndex = 8;
            this.picScissors.TabStop = false;
            this.picScissors.Click += new System.EventHandler(this.picScissors_Click);
            // 
            // picPaper
            // 
            this.picPaper.Image = global::LB3.Properties.Resources.paper;
            this.picPaper.Location = new System.Drawing.Point(233, 81);
            this.picPaper.Name = "picPaper";
            this.picPaper.Size = new System.Drawing.Size(154, 215);
            this.picPaper.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picPaper.TabIndex = 7;
            this.picPaper.TabStop = false;
            this.picPaper.Click += new System.EventHandler(this.picPaper_Click);
            // 
            // picRock
            // 
            this.picRock.Image = global::LB3.Properties.Resources.rock;
            this.picRock.Location = new System.Drawing.Point(50, 81);
            this.picRock.Name = "picRock";
            this.picRock.Size = new System.Drawing.Size(154, 215);
            this.picRock.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picRock.TabIndex = 6;
            this.picRock.TabStop = false;
            this.picRock.Click += new System.EventHandler(this.picRock_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 440);
            this.Controls.Add(this.picScissors);
            this.Controls.Add(this.picPaper);
            this.Controls.Add(this.picRock);
            this.Controls.Add(this.lblWinner);
            this.Controls.Add(this.lblPCPick);
            this.Controls.Add(this.lblUserPick);
            this.Controls.Add(this.lblHighScore);
            this.Controls.Add(this.lblRound);
            this.Controls.Add(this.lblScore);
            this.Name = "Form1";
            this.Text = "Rock Paper Scissors";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picScissors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaper)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRock)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblScore;
        private System.Windows.Forms.Label lblRound;
        private System.Windows.Forms.Label lblHighScore;
        private System.Windows.Forms.Label lblUserPick;
        private System.Windows.Forms.Label lblPCPick;
        private System.Windows.Forms.Label lblWinner;
        private System.Windows.Forms.PictureBox picRock;
        private System.Windows.Forms.PictureBox picPaper;
        private System.Windows.Forms.PictureBox picScissors;
    }
}

