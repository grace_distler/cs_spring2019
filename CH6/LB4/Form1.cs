﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            string userEntry;
            userEntry = creditCardTextBox.Text;

            char[] arr;
            arr = userEntry.ToCharArray();
            string maskedCardNumber="";

            for (int i = 0; i < arr.Length; i++)
            {
                char digit = arr[i];
                if (i >= arr.Length - 4 && char.IsDigit(digit))
                {
                    maskedCardNumber+= digit.ToString(); // add digit to string
                }
                else if (char.IsSeparator(digit))
                {
                    maskedCardNumber += digit.ToString();
                }
                else if (char.IsNumber(digit))
                {
                    maskedCardNumber += "X";
                }
                else if (char.IsLetter(digit))
                {
                    maskedCardNumber += "X";
                }
                else
                {
                    maskedCardNumber = digit.ToString();

                }

            }
            maskedNumberOutputLabel.Text = maskedCardNumber;
        }
    }
}
