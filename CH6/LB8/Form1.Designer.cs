﻿namespace LB8
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.searchTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.movieNameOutputLabel = new System.Windows.Forms.Label();
            this.directorOutputLabel = new System.Windows.Forms.Label();
            this.descriptionOutputLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(202, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Enter the name of a movie or the director:";
            // 
            // searchTextBox
            // 
            this.searchTextBox.Location = new System.Drawing.Point(31, 40);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(278, 20);
            this.searchTextBox.TabIndex = 1;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(31, 78);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 2;
            this.calculateButton.Text = "Search";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Director:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 202);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Description:";
            // 
            // movieNameOutputLabel
            // 
            this.movieNameOutputLabel.AutoSize = true;
            this.movieNameOutputLabel.Location = new System.Drawing.Point(163, 146);
            this.movieNameOutputLabel.Name = "movieNameOutputLabel";
            this.movieNameOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.movieNameOutputLabel.TabIndex = 6;
            // 
            // directorOutputLabel
            // 
            this.directorOutputLabel.AutoSize = true;
            this.directorOutputLabel.Location = new System.Drawing.Point(163, 174);
            this.directorOutputLabel.Name = "directorOutputLabel";
            this.directorOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.directorOutputLabel.TabIndex = 7;
            // 
            // descriptionOutputLabel
            // 
            this.descriptionOutputLabel.AutoSize = true;
            this.descriptionOutputLabel.Location = new System.Drawing.Point(163, 202);
            this.descriptionOutputLabel.Name = "descriptionOutputLabel";
            this.descriptionOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.descriptionOutputLabel.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 247);
            this.Controls.Add(this.descriptionOutputLabel);
            this.Controls.Add(this.directorOutputLabel);
            this.Controls.Add(this.movieNameOutputLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.searchTextBox);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "MovieDatabase";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox searchTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label movieNameOutputLabel;
        private System.Windows.Forms.Label directorOutputLabel;
        private System.Windows.Forms.Label descriptionOutputLabel;
    }
}

