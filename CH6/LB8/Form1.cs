﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB8
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            string entry;
            string[] movieTitles = {"Lord of the Rings: Fellowship of the Ring", "Pirates of the Caribbean: The Curse of the Black Pearl", "Harry Potter and the Goblet of Fire", "Star Wars: Episode III-Revenge of the Sith" };
            string[] director = {"Peter Jackson", "Gore Verbinski", "Mike Newell", "George Lucas" };
            string[] description = {"An epic journey of hobits and magic rings", "The first movie of the series", "The fourth installment to the Harry Potter series", "The Star Wars saga finale" };

            entry = searchTextBox.Text;
            entry = entry.ToLower();

            if (entry.Contains("lord") || entry.Contains("ring") || entry.Contains("fellowship") || entry.Contains("peter") || entry.Contains("jackson"))
            {
                movieNameOutputLabel.Text = movieTitles[0];
                directorOutputLabel.Text = director[0];
                descriptionOutputLabel.Text = description[0];
            }
            else if (entry.Contains("pirates") || entry.Contains("caribbean") || entry.Contains("curse") || entry.Contains("pearl") || entry.Contains("gore") || entry.Contains("verbinski"))
            {
                movieNameOutputLabel.Text = movieTitles[1];
                directorOutputLabel.Text = director[1];
                descriptionOutputLabel.Text = description[1];
            }
            else if (entry.Contains("harry")|| entry.Contains("potter") || entry.Contains("goblet") || entry.Contains("fire") || entry.Contains("mike") || entry.Contains("newell"))
            {
                movieNameOutputLabel.Text = movieTitles[2];
                directorOutputLabel.Text = director[2];
                descriptionOutputLabel.Text = description[2];
            }
            else if (entry.Contains("star") || entry.Contains("wars") || entry.Contains("episode") || entry.Contains("revenge") || entry.Contains("sith") || entry.Contains("george") || entry.Contains("lucas"))
            {
                movieNameOutputLabel.Text = movieTitles[3];
                directorOutputLabel.Text = director[3];
                descriptionOutputLabel.Text = description[3];
            }
            else
            {
                movieNameOutputLabel.Text = "Movie Not Found";
                directorOutputLabel.Text = "Movie Not Found";
                descriptionOutputLabel.Text = "Movie Not Found";
            }

        }
    }
}
