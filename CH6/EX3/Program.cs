﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace EX3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] rents = { {400,600,800},
                             {100,1200,1400},
                             {1600,1800,2000}
            };

            for (int i=0; i<rents.GetLength(0); i++)
            {
                for (int x=0;x<rents.GetLength(1); x++)
                {
                    Write(rents[i, x] + " ");
                }
                WriteLine("");
            }
            ReadLine();
        }
    }
}
