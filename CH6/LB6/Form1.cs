﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            int nightsStayedInput;
            int[] rateRangeLowLimits = { 1, 3, 5, 8 };
            double[] nightlyRate = { 200, 180, 160, 145 };
            double payableNightlyRate;
            double totalCost;

            nightsStayedInput = Convert.ToInt32(nightsStayedTextBox.Text);

            int sub = rateRangeLowLimits.Length - 1;

            while (sub >= 0 && nightsStayedInput < rateRangeLowLimits[sub]) --sub;

            payableNightlyRate = nightlyRate[sub];
            nightlyCostOutputLabel.Text = payableNightlyRate.ToString("C");

            totalCost = payableNightlyRate * nightsStayedInput;
            totalCostOutputLabel.Text = totalCost.ToString("C");
        }
    }
}
