﻿namespace LB7
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userInputTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.calculateButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.gameNameOutputLabel = new System.Windows.Forms.Label();
            this.publisherNameOutputLabel = new System.Windows.Forms.Label();
            this.priceOutputLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // userInputTextBox
            // 
            this.userInputTextBox.Location = new System.Drawing.Point(36, 48);
            this.userInputTextBox.Name = "userInputTextBox";
            this.userInputTextBox.Size = new System.Drawing.Size(236, 20);
            this.userInputTextBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(236, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Enter the name of a video game or the publisher:";
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(36, 90);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 2;
            this.calculateButton.Text = "Search";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 149);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Name: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 173);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Publisher: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 196);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Price: ";
            // 
            // gameNameOutputLabel
            // 
            this.gameNameOutputLabel.AutoSize = true;
            this.gameNameOutputLabel.Location = new System.Drawing.Point(178, 149);
            this.gameNameOutputLabel.Name = "gameNameOutputLabel";
            this.gameNameOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.gameNameOutputLabel.TabIndex = 6;
            // 
            // publisherNameOutputLabel
            // 
            this.publisherNameOutputLabel.AutoSize = true;
            this.publisherNameOutputLabel.Location = new System.Drawing.Point(178, 173);
            this.publisherNameOutputLabel.Name = "publisherNameOutputLabel";
            this.publisherNameOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.publisherNameOutputLabel.TabIndex = 7;
            // 
            // priceOutputLabel
            // 
            this.priceOutputLabel.AutoSize = true;
            this.priceOutputLabel.Location = new System.Drawing.Point(178, 196);
            this.priceOutputLabel.Name = "priceOutputLabel";
            this.priceOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.priceOutputLabel.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 236);
            this.Controls.Add(this.priceOutputLabel);
            this.Controls.Add(this.publisherNameOutputLabel);
            this.Controls.Add(this.gameNameOutputLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.userInputTextBox);
            this.Name = "Form1";
            this.Text = "GameBreak";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox userInputTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label gameNameOutputLabel;
        private System.Windows.Forms.Label publisherNameOutputLabel;
        private System.Windows.Forms.Label priceOutputLabel;
    }
}

