﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB7
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            string entry;
            string[] videoGame = { "Player Unknown's Battlegrounds (PUBG)", "League of Legends", "Call of Duty: Black Ops III", "Battlefield 4", "Super Mario Odyssey" };
            string[] publisher = { "Bluehole", "Riot Games", "Activision", "Electronic Arts (EA)", "Nintendo" };
            int[] price = { 35, 0, 60, 20, 60 };
            
            entry = userInputTextBox.Text;
            entry = entry.ToLower();

            if (entry.Contains("pubg") || entry.Contains("player") || entry.Contains("battlegrounds") || entry.Contains("unknown") || entry.Contains("bluehole")){
                gameNameOutputLabel.Text = videoGame[0];
                publisherNameOutputLabel.Text = publisher[0];
                priceOutputLabel.Text = price[0].ToString("C");
            }
            else if (entry.Contains("league") || entry.Contains("legends") || entry.Contains("lol") || entry.Contains("riot") || entry.Contains("games"))
            {
                gameNameOutputLabel.Text = videoGame[1];
                publisherNameOutputLabel.Text = publisher[1];
                priceOutputLabel.Text = price[1].ToString("C");
            }
            else if (entry.Contains("call of duty") || entry.Contains("call") || entry.Contains("cod") || entry.Contains("duty") || entry.Contains("activision"))
            {
                gameNameOutputLabel.Text = videoGame[2];
                publisherNameOutputLabel.Text = publisher[2];
                priceOutputLabel.Text = price[2].ToString("C");
            }
            else if (entry.Contains("battlefield") || entry.Contains("ea") || entry.Contains("electronic arts"))
            {
                gameNameOutputLabel.Text = videoGame[3];
                publisherNameOutputLabel.Text = publisher[3];
                priceOutputLabel.Text = price[3].ToString("C");
            }
            else if (entry.Contains("mario") || entry.Contains("super") || entry.Contains("odyssey") || entry.Contains("nintendo"))
            {
                gameNameOutputLabel.Text = videoGame[4];
                publisherNameOutputLabel.Text = publisher[4];
                priceOutputLabel.Text = price[4].ToString("C");
            }
            else
            {
                gameNameOutputLabel.Text = "Game Not Found";
                publisherNameOutputLabel.Text = "Game Not Found";
                priceOutputLabel.Text = "Game Not Found";
            }
        }
    }
}
