﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace EX1
{
    class Program
    {
        static void Main(string[] args)
        {
            String[] studentNames = new string[2000];
            int numStudents = 0;

            for (int i=0; i< studentNames.Length; i++)
            {
                WriteLine("Enter the name for student #{0} or type 0 to quit", i + 1);
                string name = ReadLine();

                if (name == "0")
                {
                    break;
                }
                else
                {
                    studentNames[i] = name;
                    numStudents++;
                }                
            }

            for (int i=0; i <= numStudents; i++)
            {
                WriteLine(studentNames[i]);
            }

            ReadLine();
        }
    }
}
