﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            int x;
            int zipCodeInput;
            double deliveryFee = 0;
            bool isValidZipCode = false;
            int[] range = { 63101, 63103, 63105, 63109, 63113, 63118, 63130, 63133, 63136, 63137 };
            double[] charge = { 20, 12, 25, 15, 10, 23, 18, 20, 17, 12 };

            zipCodeInput = Convert.ToInt32(zipCodeTextBox.Text);
            x = 0;

            while (x < range.Length && zipCodeInput != range[x]) ++x;
            if(x != range.Length)
            {
                isValidZipCode = true;
                deliveryFee = charge[x];
            }

            if (isValidZipCode)
            {
                deliveryChargeOutputLabel.Text = deliveryFee.ToString("C");
            } else
            {
                deliveryChargeOutputLabel.Text = "Zip Code is not valid";
            }

        }
    }
}
