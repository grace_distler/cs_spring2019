﻿namespace EX1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.nameEntryTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.firstNameOutputLabel = new System.Windows.Forms.Label();
            this.lastNameOutputLabel = new System.Windows.Forms.Label();
            this.phoneNumberOutputLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "First Name or Last Name:";
            // 
            // nameEntryTextBox
            // 
            this.nameEntryTextBox.Location = new System.Drawing.Point(30, 39);
            this.nameEntryTextBox.Name = "nameEntryTextBox";
            this.nameEntryTextBox.Size = new System.Drawing.Size(176, 20);
            this.nameEntryTextBox.TabIndex = 1;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(228, 36);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 2;
            this.calculateButton.Text = "Search";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "First Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Last Name:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 162);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Phone #:";
            // 
            // firstNameOutputLabel
            // 
            this.firstNameOutputLabel.AutoSize = true;
            this.firstNameOutputLabel.Location = new System.Drawing.Point(171, 101);
            this.firstNameOutputLabel.Name = "firstNameOutputLabel";
            this.firstNameOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.firstNameOutputLabel.TabIndex = 6;
            // 
            // lastNameOutputLabel
            // 
            this.lastNameOutputLabel.AutoSize = true;
            this.lastNameOutputLabel.Location = new System.Drawing.Point(171, 132);
            this.lastNameOutputLabel.Name = "lastNameOutputLabel";
            this.lastNameOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.lastNameOutputLabel.TabIndex = 7;
            // 
            // phoneNumberOutputLabel
            // 
            this.phoneNumberOutputLabel.AutoSize = true;
            this.phoneNumberOutputLabel.Location = new System.Drawing.Point(171, 162);
            this.phoneNumberOutputLabel.Name = "phoneNumberOutputLabel";
            this.phoneNumberOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.phoneNumberOutputLabel.TabIndex = 8;
            // 
            // Form1
            // 
            this.AcceptButton = this.calculateButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(461, 204);
            this.Controls.Add(this.phoneNumberOutputLabel);
            this.Controls.Add(this.lastNameOutputLabel);
            this.Controls.Add(this.firstNameOutputLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.nameEntryTextBox);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Address Book";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nameEntryTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label firstNameOutputLabel;
        private System.Windows.Forms.Label lastNameOutputLabel;
        private System.Windows.Forms.Label phoneNumberOutputLabel;
    }
}

