﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            string entry;
            string[] firstName = { "Markel", "Luiza", "Bryony", "Giraldo", "Lowri" };
            string[] lastName = { "Diggory", "Gunnar", "Hester", "Addy", "Hari" };
            string[] phoneNumber = { "555-8390", "555-4618", "555-4440", "555-1687", "555-7763" };

            entry = nameEntryTextBox.Text;
            entry = entry.ToLower();

                if (entry.Contains("markel") || entry.Contains("diggory"))
                {
                    firstNameOutputLabel.Text = firstName[0];
                    lastNameOutputLabel.Text = lastName[0];
                    phoneNumberOutputLabel.Text = phoneNumber[0];
                }
                else if (entry.Contains("luiza") || entry.Contains("gunnar"))
                {
                    firstNameOutputLabel.Text = firstName[1];
                    lastNameOutputLabel.Text = lastName[1];
                    phoneNumberOutputLabel.Text = phoneNumber[1];
                }
                else if (entry.Contains("bryony") || entry.Contains("hester"))
                {
                    firstNameOutputLabel.Text = firstName[2];
                    lastNameOutputLabel.Text = lastName[2];
                    phoneNumberOutputLabel.Text = phoneNumber[2];
                }
                else if (entry.Contains("giraldo") || entry.Contains("addy"))
                {
                    firstNameOutputLabel.Text = firstName[3];
                    lastNameOutputLabel.Text = lastName[3];
                    phoneNumberOutputLabel.Text = phoneNumber[3];
                }
                else if (entry.Contains("lowri") || entry.Contains("hari"))
                {
                    firstNameOutputLabel.Text = firstName[4];
                    lastNameOutputLabel.Text = lastName[4];
                    phoneNumberOutputLabel.Text = phoneNumber[4];
                }
                else
                {
                    firstNameOutputLabel.Text = "Error";
                    lastNameOutputLabel.Text = "Error";
                    phoneNumberOutputLabel.Text = "Error";
                }               
                
            }

        }
    }
