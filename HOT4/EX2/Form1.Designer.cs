﻿namespace EX2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.creditCardTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.maskedNumberOutputLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // creditCardTextBox
            // 
            this.creditCardTextBox.Location = new System.Drawing.Point(33, 38);
            this.creditCardTextBox.Name = "creditCardTextBox";
            this.creditCardTextBox.Size = new System.Drawing.Size(261, 20);
            this.creditCardTextBox.TabIndex = 0;
            this.creditCardTextBox.Enter += new System.EventHandler(this.calculateButton_Click);
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(33, 65);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 1;
            this.calculateButton.Text = "Mask";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            this.calculateButton.Enter += new System.EventHandler(this.calculateButton_Click);
            // 
            // maskedNumberOutputLabel
            // 
            this.maskedNumberOutputLabel.AutoSize = true;
            this.maskedNumberOutputLabel.Location = new System.Drawing.Point(33, 127);
            this.maskedNumberOutputLabel.Name = "maskedNumberOutputLabel";
            this.maskedNumberOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.maskedNumberOutputLabel.TabIndex = 2;
            // 
            // Form1
            // 
            this.AcceptButton = this.calculateButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 182);
            this.Controls.Add(this.maskedNumberOutputLabel);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.creditCardTextBox);
            this.Name = "Form1";
            this.Text = "Credit Card Masker";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox creditCardTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label maskedNumberOutputLabel;
    }
}

