﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB1
{
    class LetterDemo
    {
        static void Main(string[] args)
        {
            Letter letter1 = new Letter("Bob", "March 8, 2019");
            Console.WriteLine(letter1);

            CertifiedLetter trackLetter = new CertifiedLetter("Karen", "Feburary 26, 2019", 123456789);
            Console.WriteLine(trackLetter);
        }
    }
}
