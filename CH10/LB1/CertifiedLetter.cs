﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB1
{
    class CertifiedLetter : Letter
    {
        public int TrackingNumber { get; set; }

        public CertifiedLetter(string name, string date, int track) : base(name, date)
        {
            TrackingNumber = track;
        }

        public override string ToString()
        {
            return GetType().ToString() + "\nClient Name: " + Name + "\nDate Mailed: " + Date + "\nTracking Number: " + TrackingNumber + "\n";
        }
    }
}
