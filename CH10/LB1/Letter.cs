﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB1
{
    class Letter
    {
        public string Name { get; set; }
        public string Date { get; set; }

        public Letter()
        {

        }

        public Letter(string clientName, string dateMailed)
        {
            Name = clientName;
            Date = dateMailed;
        }

        public override string ToString()
        {
            return GetType() + "\nSent to Client: " + Name + "\nDate Mailed: " + Date + "\n";
        }
    }
}
