﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB3
{
    class RushJob : Job, IComparable
    {
        public RushJob(int jobNumber, string customerName, string jobDescription, int extimatedHours) : base(jobNumber, customerName, jobDescription, extimatedHours)
        {
            JobPrice = 150 + JobPrice;
        }

        public int CompareTo(object obj)
        {
            RushJob rushJob = obj as RushJob;
        
            if (rushJob.JobNumber < JobNumber)
            {
                return 1;
            }
            if (rushJob.JobNumber > JobNumber)
            {
                return -1;
            }
            return 0;
        }
    }
}
