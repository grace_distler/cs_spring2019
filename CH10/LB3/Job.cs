﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace LB3
{
    class Job : System.Object
    {
        public int JobNumber { get; set; }
        public string CustomerName { get; set; }
        public string JobDescription { get; set; }
        public int EstimatedHours { get; set; }
        public int JobPrice { get; set; }

        public Job(int jobNumber, string customerName, string jobDescription, int extimatedHours)
        {
            JobNumber = jobNumber;
            CustomerName = customerName;
            JobDescription = jobDescription;
            EstimatedHours = extimatedHours;
            JobPrice = 45 * extimatedHours;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            Job job = obj as Job;
            if (job == null)
            {
                return false;
            }
            return JobNumber == job.JobNumber;
        }

        public override int GetHashCode()
        {
            return JobNumber;
        }
        public override string ToString()
        {
            return string.Format("JobNumber {0} , CustomerName {1}, JobDescription {2}, EstimatedHours {3}, JobPrice {4} ",
            JobNumber, CustomerName, JobDescription, EstimatedHours, JobPrice);
        }
    }
}
