﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB3
{
    class JobDemo
    {
        static void Main(string[] args)
        {
            //Part A
            Object job1 = new Job(121, "Robert", "Repair service for Robert", 2);
            Object job2 = new Job(121, "Joy", "Repair service for Joy", 3);

            if (job1.Equals(job2))
            {
                Console.WriteLine("Result from Equals = Same objects");
            }
            else
            {
                Console.WriteLine("Result from Equals = Different objects");
            }

            if (job1.GetHashCode() == job2.GetHashCode())
            {
                Console.WriteLine("Result from GetHashCode = Same ");
            }
            else
            {
                Console.WriteLine("Result from GetHashCode = Different ");
            }

            Console.WriteLine("To string result job1 object = {0}", job1.ToString());
            Console.WriteLine("To string result job2 object = {0}", job2.ToString());
            Console.WriteLine();

            //Part B
            Job[] jobArray = new Job[5];

            int i = 0;
            while (true)
            {
                Console.WriteLine("Enter the {0} user data", i + 1);
                Console.WriteLine("Enter the Job Number");
                int jobNumber = Convert.ToInt32(Console.ReadLine());

                if (i > 0)
                {
                    bool jobNumberFound = false;

                    for (int j = 0; j < i; j++)
                    {
                        if (jobArray[j].JobNumber == jobNumber)
                        {
                            jobNumberFound = true;
                            Console.WriteLine("Job Number is already entered.. enter it again");
                            break;
                        }
                    }

                    if (jobNumberFound)
                    {
                        continue;
                    }
                }

                Console.WriteLine("Enter the Customer Name");
                string customerName = Console.ReadLine();

                Console.WriteLine("Enter the Job Description");
                string jobDescription = Console.ReadLine();

                Console.WriteLine("Enter the Estimated hours");
                int extimatedHours = Convert.ToInt32(Console.ReadLine());

                jobArray[i] = new Job(jobNumber, customerName, jobDescription, extimatedHours);
                i++;

                if (i == 5)
                {
                    break;
                }
            }

            Console.WriteLine("======Entered Data For Rush Job ===========");
            int totalPrice = 0;

            for (int k = 0; k < 5; k++)
            {
                Console.WriteLine(jobArray[k].ToString());
                totalPrice = totalPrice + jobArray[k].JobPrice;
            }

            Console.WriteLine("Total Price = {0}", totalPrice);
            Console.WriteLine();

            //Part C
            Job[] rushJobArray = new RushJob[5];

            List<Job> rushJobList = new List<Job>(); // Part D
            int counter = 0;
            while (true)
            {
                Console.WriteLine("Enter the {0} user data", counter + 1);
                Console.WriteLine("Enter the Job Number");
                int jobNumber = Convert.ToInt32(Console.ReadLine());

                if (counter > 0)
                {
                    bool jobNumberFound = false;

                    for (int j = 0; j < counter; j++)
                    {
                        if (rushJobArray[j].JobNumber == jobNumber)
                        {
                            jobNumberFound = true;
                            Console.WriteLine("Job Number is already entered.. enter it again");
                            break;
                        }
                    }

                    if (jobNumberFound)
                    {
                        continue;
                    }
                }

                Console.WriteLine("Enter the Customer Name");
                string customerName = Console.ReadLine();

                Console.WriteLine("Enter the Job Description");
                string jobDescription = Console.ReadLine();

                Console.WriteLine("Enter the Estimated hours");
                int extimatedHours = Convert.ToInt32(Console.ReadLine());

                rushJobArray[counter] = new RushJob(jobNumber, customerName, jobDescription, extimatedHours);
                rushJobList.Add(rushJobArray[counter]);
                counter++;

                if (counter == 5)
                {
                    break;
                }
            }

            Console.WriteLine("======Entered Data ===========");
            int totalPriceRushJob = 0;

            for (int k = 0; k < 5; k++)
            {
                Console.WriteLine(rushJobArray[k].ToString());
                totalPriceRushJob = totalPriceRushJob + rushJobArray[k].JobPrice;
            }

            Console.WriteLine("Total Price = {0}", totalPriceRushJob);

            Console.WriteLine("Sorting the entered data based on job number");
            rushJobList.Sort();

            Console.WriteLine("Printing the sorted data");
            foreach (Job j in rushJobList)
            {
                Console.WriteLine(j.ToString());
            }
        }
    }
}
