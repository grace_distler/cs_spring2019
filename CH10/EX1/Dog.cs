﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX1
{
    class Dog : Animal
    {
        public string CoatColor { get; set; }

        public Dog(string name, int age, string cc) : base(name, age)
        {
            CoatColor = cc;
        }

        public override void Move()
        {
            base.Move();
            Console.WriteLine("A dog gallops");
        }

        public override string ToString()
        {
            return "My age is: " + Age + "\nMy name is: " + Name + "\nMy fur's color is: " + CoatColor + "\n\n";
        }
    }
}
