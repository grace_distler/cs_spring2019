﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Animal anAnimal = new Dog();
            //anAnimal.Move();

            Dog myDog = new Dog("Rover", 12, "Brown");

            Console.WriteLine(myDog);
            Console.ReadLine();

            //Snake aSnake = new Snake();
            //aSnake.Name = "Big Boss";
            //aSnake.Age = 1;
            //aSnake.Move();
        }

        public static void Display(Animal anAnimal)
        {
            anAnimal.Move();
        }
    }
}
