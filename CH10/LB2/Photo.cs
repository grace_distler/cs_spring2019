﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB2
{
    public class Photo
    {
        public int width;
        public int height;
        protected double price;

        public Photo(int w, int h)
        {
            this.width = w;
            this.height = h;

            if(width==8 && height == 10)
            {
                this.price = 3.99;
            }
            else if (width==10 && height == 12)
            {
                this.price = 5.99;
            }
            else
            {
                this.price = 9.99;
            }
        }

        public int GetWidth()
        {
            return width;
        }

        public int GetHeight()
        {
            return height;
        }

        public double GetPrice()
        {
            return price;
        }

        public string ToString()
        {
            return GetType() + "\nWidth: " + GetWidth() + "\nHeight: " + GetHeight() + "\nPrice: $" + GetPrice() + "\n"; ;
        }
    }
}
