﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB2
{
    public class MattedPhoto : Photo
    {
        public string color;

        public MattedPhoto(int w, int h, string c) : base(w, h)
        {
            width = w;
            height = h;
            color = c;

            if(width==8 && height == 10)
            {
                price = 3.99 + 10;
            }
            else if(width==10 && height == 12)
            {
                price = 5.99 + 10;
            }
            else
            {
                price = 9.99 + 10;
            }
        }

        public void SetColor(string color)
        {
            this.color = color;
        }

        public string GetColor()
        {
            return color;
        }

        public string ToString()
        {
            return GetType() + "\nWidth: " + GetWidth() + "\nHeight: " + GetHeight() + "\nPrice: $" + GetPrice() + "\nColor: " + GetColor() + "\n";
        }
    }
}
