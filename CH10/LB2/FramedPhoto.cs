﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB2
{
    public class FramedPhoto : Photo
    {
        public string material;
        public string style;

        public FramedPhoto(int w, int h, string m, string s) : base(w, h)
        {
            width = w;
            height = h;
            material = m;
            style = s;

            if (width == 8 && height == 10)
            {
                price = 3.99 + 25;
            }
            else if (width == 10 && height == 12)
            {
                price = 5.99 + 25;
            }
            else
            {
                price = 9.99 + 25;
            }
        }

        public string GetMaterial()
        {
            return material;
        }

        public string GetStyle()
        {
            return style;
        }

        public string ToString()
        {
            return GetType() + "\nWidth: " + GetWidth() + "\nHeight: " + GetHeight() + "\nPrice: $" + GetPrice() + "\nMaterial: " + GetMaterial() + "\nStyle: " + GetStyle() + "\n";

        }
    }
}
