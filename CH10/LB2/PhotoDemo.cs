﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB2
{
    class PhotoDemo
    {
        static void Main(string[] args)
        {
            Photo photo1 = new Photo(8, 10);
            Console.WriteLine(photo1.ToString());

            Photo photo2 = new Photo(10, 12);
            Console.WriteLine(photo2.ToString());

            Photo photo3 = new Photo(16, 20);
            Console.WriteLine(photo3.ToString());

            MattedPhoto photo4 = new MattedPhoto(8, 10, "Blue");
            Console.WriteLine(photo4.ToString());

            FramedPhoto photo5 = new FramedPhoto(8, 10, "Platinum", "Modern");
            Console.WriteLine(photo5.ToString());
        }
    }
}
