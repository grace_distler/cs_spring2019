﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB4
{
    class SpecialCookieOrder : CookieOrder
    {
        private string speciality;

        public SpecialCookieOrder(int orderNo, string reciepientName, string cookieType, int dozens, string speciality) : base(orderNo, reciepientName, cookieType, dozens)
        {
            this.speciality = speciality;
        }

        public override double computePrice()
        {
            double prc;

            if (Dozen <= 2)
            {
                prc = 15 * Dozen;
            }
            else
            {
                prc = 13 * Dozen;
            }

            if (prc <= 40)
            {
                prc = prc + 10;
            }
            else
            {
                prc = prc + 8;
            }
            return prc;
        }

        public string Speciality
        {
            get
            {
                return speciality;
            }
            set
            {
                speciality = value;
            }
        }
    }
}
