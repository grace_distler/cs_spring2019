﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB4
{
    class CookieDemo
    {
        static void Main(string[] args)
        {
            CookieOrder order1 = new CookieOrder(12345, "Fred Meyer", "Chocolate Chip", 2);
            Console.WriteLine("\nOrder Number: " + order1.OrderNum + " for " + order1.ReciepientName);
            Console.WriteLine(order1.CookieType);
            Console.WriteLine("How many dozen: " + order1.Dozen);
            Console.WriteLine("Total Price: $" + order1.computePrice());
            Console.WriteLine();

            CookieOrder order2 = new CookieOrder(23456, "Albertsons", "Peanut Butter", 5);
            Console.WriteLine("\nOrder Number: " + order2.OrderNum + " for " + order2.ReciepientName);
            Console.WriteLine(order2.CookieType);
            Console.WriteLine("How many dozen: " + order2.Dozen);
            Console.WriteLine("Total Price: $" + order2.computePrice());
            Console.WriteLine();

            CookieOrder order3 = new CookieOrder(34567, "Winco", "Oatmeal", 1);
            Console.WriteLine("\nOrder Number: " + order3.OrderNum + " for " + order3.ReciepientName);
            Console.WriteLine(order3.CookieType);
            Console.WriteLine("How many dozen: " + order3.Dozen);
            Console.WriteLine("Total Price: $" + order3.computePrice());
            Console.WriteLine();

            SpecialCookieOrder order4 = new SpecialCookieOrder(56789, "Sweet Valley Cookie Company", "Sugar", 1, "Iced");
            Console.WriteLine("\nOrder Number: " + order4.OrderNum + " for " + order4.ReciepientName);
            Console.WriteLine(order4.CookieType);
            Console.WriteLine("How many dozen: " + order4.Dozen);
            Console.WriteLine("Total Price: $" + order4.computePrice());
            Console.WriteLine("Special Order: " + order4.Speciality);
            Console.WriteLine();
        }
    }
}
