﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB4
{
    class CookieOrder
    {
        private int orderNum;
        private string reciepientName;
        private string cookieType;
        private int dozen;
        private double price;

        public CookieOrder(int orderNum, string reciepientName, string cookieType, int dozen)
        {
            this.orderNum = orderNum;
            this.reciepientName = reciepientName;
            this.cookieType = cookieType;
            this.dozen = dozen;
        }

        public int OrderNum
        {
            get
            {
                return orderNum;
            }
            set
            {
                orderNum = value;
            }
        }
        public string ReciepientName
        {
            get
            {
                return reciepientName;
            }
            set
            {
                reciepientName = value;
            }      
        }
        public string CookieType
        {
            get
            {
                return cookieType;
            }
            set
            {
                cookieType = value;
            }
        }
        public int Dozen
        {
            get
            {
                return dozen;
            }
            set
            {
                dozen = value;
            }          
        }

        public virtual double computePrice()
        {
            double prc;

            if (dozen <= 2)
            {
                prc = 15 * dozen;
            }
            else
            {
                prc = 30 + 13 * (dozen - 2);
            }
            return prc;
        }
    }
}
