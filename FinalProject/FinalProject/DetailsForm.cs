﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalProject
{
    public partial class DetailsForm : Form
    {
        public DetailsForm()
        {
            InitializeComponent();
        }

        private Book _book=new Book();

        private void BookDetails_Load(object sender, EventArgs e)
        {
            _book = SearchForm.detBook;
            picBooks.Image = _book.Img;
            lblTitle.Text = _book.BookName;
            lblISBN.Text = $"ISBN: " + _book.Isbn;
            lblAuthor.Text = $"By " + _book.AuthorName;
            
            for (int i = 0; i  < _book.Reviews.Count; i++)
            {
                lblPastReviews.Text += _book.Reviews[i].ReviewText + "\n";
            }            
        }
        
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            Review newReview = new Review(txtReview.Text);
            newReview.ReviewText = txtReview.Text;
            _book.Reviews.Add(newReview);
            SearchForm.detBook = _book;
            this.Close();
        }

        private void BookDetails_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
