﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    public class Review
    {
        public string ReviewText { get; set; }

        public Review(string text)
        {

        }
        public Review()
        {

        }

        public override string ToString()
        {
            return $"{ReviewText}\n";
        }
    }
}
