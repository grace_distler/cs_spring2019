﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalProject
{
    public partial class SearchForm : Form
    {
        public SearchForm()
        {
            InitializeComponent();
        }

        private List<Book> _books;
        private List<String> _authors;
        private int _pageNumber;
        private List<Book> _searchResults;

        public static Book detBook;
        int detailsEditIndex = -1;

        Label[] isbnLabels = new Label[5];
        Label[] nameLabels = new Label[5];
        Label[] authorLabels = new Label[5];
        Label[] yearLabels = new Label[5];
        Button[] detailsButtons = new Button[5];

        private void SearchForm_Load(object sender, EventArgs e)
        {
            _books = new List<Book>();

            _books.Add(new Book { Isbn = "978-0439023481", BookName = "The Hunger Games", AuthorName = "Suzanne Collins", PublishYear = 2008, Img = Properties.Resources.hunger_games });
            _books.Add(new Book { Isbn = "978-0439023498", BookName = "Catching Fire", AuthorName = "Suzanne Collins", PublishYear = 2009, Img = Properties.Resources.catching_fire });
            _books.Add(new Book { Isbn = "978-0439023511", BookName = "Mockingjay", AuthorName = "Suzanne Collins", PublishYear = 2010, Img = Properties.Resources.mockingjay });

            _books.Add(new Book { Isbn = "978-0590353403", BookName = "Harry Potter and the Sorcerer's Stone", AuthorName = "J.K. Rowling", PublishYear = 1998, Img = Properties.Resources.sorcerer_s_stone });
            _books.Add(new Book { Isbn = "978-0439064866", BookName = "Harry Potter and the Chamber of Secrets", AuthorName = "J.K. Rowling", PublishYear = 1999, Img = Properties.Resources.chamber_of_secrects });
            _books.Add(new Book { Isbn = "978-0439136358", BookName = "Harry Potter and the Prisoner of Azkaban", AuthorName = "J.K. Rowling", PublishYear = 1999, Img = Properties.Resources.prisoner_of_azkaban });
            _books.Add(new Book { Isbn = "978-0439139595", BookName = "Harry Potter and the Goblet of Fire", AuthorName = "J.K. Rowling", PublishYear = 2000, Img = Properties.Resources.goblet_of_fire });
            _books.Add(new Book { Isbn = "978-0439358064", BookName = "Harry Potter and the Order of the Phoenix", AuthorName = "J.K. Rowling", PublishYear = 2003, Img = Properties.Resources.order_of_the_phoenix });
            _books.Add(new Book { Isbn = "978-0439784542", BookName = "Harry Potter and the Half-Blood Prince", AuthorName = "J.K. Rowling", PublishYear = 2005, Img = Properties.Resources.half_blood_prince });
            _books.Add(new Book { Isbn = "978-0545010221", BookName = "Harry Potter and the Deathly Hallows", AuthorName = "J.K. Rowling", PublishYear = 2007, Img = Properties.Resources.deathly_hallows });

            _books.Add(new Book { Isbn = "978-0446676090", BookName = "The Notebook", AuthorName = "Nicholas Sparks", PublishYear = 1996, Img = Properties.Resources.the_notebook });
            _books.Add(new Book { Isbn = "978-0446693806", BookName = "A Walk to Remember", AuthorName = "Nicholas Sparks", PublishYear = 1999, Img = Properties.Resources.a_walk_to_remember });
            _books.Add(new Book { Isbn = "978-1455571635", BookName = "A Bend in the Road", AuthorName = "Nicholas Sparks", PublishYear = 2001, Img = Properties.Resources.a_bend_in_the_road });
            _books.Add(new Book { Isbn = "978-0446618151", BookName = "True Believer", AuthorName = "Nicholas Sparks", PublishYear = 2005, Img = Properties.Resources.true_believer });
            _books.Add(new Book { Isbn = "978-0446567336", BookName = "Dear John", AuthorName = "Nicholas Sparks", PublishYear = 2006, Img = Properties.Resources.dear_john });
            _books.Add(new Book { Isbn = "978-1455508969", BookName = "The Lucky One", AuthorName = "Nicholas Sparks", PublishYear = 2008, Img = Properties.Resources.the_lucky_one });
            _books.Add(new Book { Isbn = "978-0446570961", BookName = "The Last Song", AuthorName = "Nicholas Sparks", PublishYear = 2009, Img = Properties.Resources.the_last_song });
            _books.Add(new Book { Isbn = "978-1455523559", BookName = "Safe Haven", AuthorName = "Nicholas Sparks", PublishYear = 2010, Img = Properties.Resources.safe_haven });
            _books.Add(new Book { Isbn = "978-0446547635", BookName = "Best of Me", AuthorName = "Nicholas Sparks", PublishYear = 2010, Img = Properties.Resources.best_of_me });
            _books.Add(new Book { Isbn = "978-1455520633", BookName = "The Longest Ride", AuthorName = "Nicholas Sparks", PublishYear = 2013, Img = Properties.Resources.the_longest_ride });
            _books.Add(new Book { Isbn = "978-1455520602", BookName = "See Me", AuthorName = "Nicholas Sparks", PublishYear = 2015, Img = Properties.Resources.see_me });


            isbnLabels[0] = lblBookISBN1;
            isbnLabels[1] = lblBookISBN2;
            isbnLabels[2] = lblBookISBN3;
            isbnLabels[3] = lblBookISBN4;
            isbnLabels[4] = lblBookISBN5;

            nameLabels[0] = lblBookName1;
            nameLabels[1] = lblBookName2;
            nameLabels[2] = lblBookName3;
            nameLabels[3] = lblBookName4;
            nameLabels[4] = lblBookName5;

            authorLabels[0] = lblAuthor1;
            authorLabels[1] = lblAuthor2;
            authorLabels[2] = lblAuthor3;
            authorLabels[3] = lblAuthor4;
            authorLabels[4] = lblAuthor5;

            yearLabels[0] = lblYear1;
            yearLabels[1] = lblYear2;
            yearLabels[2] = lblYear3;
            yearLabels[3] = lblYear4;
            yearLabels[4] = lblYear5;

            detailsButtons[0] = btnDetails1;
            detailsButtons[1] = btnDetails2;
            detailsButtons[2] = btnDetails3;
            detailsButtons[3] = btnDetails4;
            detailsButtons[4] = btnDetails5;
            

            for (int i = 0; i < 5; i++)
            {
                isbnLabels[i].Visible = false;
                nameLabels[i].Visible = false;
                authorLabels[i].Visible = false;
                yearLabels[i].Visible = false;
                detailsButtons[i].Visible = false;
            }

            _authors = _books.Select(x => x.AuthorName).Distinct().ToList();
            for (int i = 0; i < _authors.Count; i++)
            {
                cmbAuthor.Items.Add(_authors[i]);
            }
            cmbAuthor.SelectedIndex = -1;

            btnPageOne.Enabled = false;
            btnPageTwo.Enabled = false;
            btnPageThree.Enabled = false;          
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            var booksByNameOrISBN = _books.Where(x => ((x.BookName.Contains(txtSearch.Text)) || (x.Isbn == txtSearch.Text))).ToList();
            _searchResults = booksByNameOrISBN;

            for (int i = 0; i < _searchResults.Count; i++)
            {
                if (cmbAuthor.Items.Contains(_searchResults[i].AuthorName))
                {
                    cmbAuthor.SelectedIndex = cmbAuthor.Items.IndexOf(_searchResults[i].AuthorName);
                }
            }

            //searches less than or equal to 5
            if (_searchResults.Count <= 5)
            {
                btnPageOne.Enabled = true;
                btnPageTwo.Enabled = false;
                btnPageThree.Enabled = false;
            }

            //searches between 5 and 10
            if(_searchResults.Count > 5 && _searchResults.Count <= 10)
            {
                btnPageOne.Enabled = true;
                btnPageTwo.Enabled = true;
                btnPageThree.Enabled = false;
            }

            //searches between 10 and 15
            if(_searchResults.Count > 10 && _searchResults.Count < 15)
            {
                btnPageOne.Enabled = true;
                btnPageTwo.Enabled = true;
                btnPageThree.Enabled = true;
            }
            btnPageOne_Click(null, null);
        }
        
        private void Display()
        {
            for (int i = 0; i < 5; i++)
            {
                isbnLabels[i].Visible = false;
                nameLabels[i].Visible = false;
                authorLabels[i].Visible = false;
                yearLabels[i].Visible = false;
                detailsButtons[i].Visible = false;
            }
            
            int numberOfObjectsPerPage = 5;
            var displayBooks = _searchResults.Skip(numberOfObjectsPerPage * _pageNumber).Take(numberOfObjectsPerPage).ToList();
            
            for (int i = 0; i < displayBooks.Count; i++)
            {
                isbnLabels[i].Visible = true;
                nameLabels[i].Visible = true;
                authorLabels[i].Visible = true;
                yearLabels[i].Visible = true;
                detailsButtons[i].Visible = true;

                isbnLabels[i].Text = displayBooks[i].Isbn;
                nameLabels[i].Text = displayBooks[i].BookName;
                authorLabels[i].Text = displayBooks[i].AuthorName;
                yearLabels[i].Text = displayBooks[i].PublishYear.ToString();
            }
        }
        private void btnPageOne_Click(object sender, EventArgs e)
        {
            _pageNumber = 0;
            Display();
        }

        private void btnPageTwo_Click(object sender, EventArgs e)
        {
            _pageNumber = 1;
            Display();
            
        }

        private void btnPageThree_Click(object sender, EventArgs e)
        {
            _pageNumber = 2;
            Display();
        }

        
        private void Details_Clicked(object sender, EventArgs e)
        {
            int selIndex = -1;
            for (int i = 0; i < detailsButtons.Length; i++)
            {
                if (detailsButtons[i] == sender)
                {
                    selIndex = i;
                }
            }

            for (int i = 0; i < _searchResults.Count; i++)
            {
                if (nameLabels[selIndex].Text == _searchResults[i].BookName && isbnLabels[selIndex].Text == _searchResults[i].Isbn && authorLabels[selIndex].Text == _searchResults[i].AuthorName && yearLabels[selIndex].Text == _searchResults[i].PublishYear.ToString())
                {
                    detBook = _searchResults[i];
                    detailsEditIndex = i;
                }
                    
            }
            DetailsForm detForm = new DetailsForm();
            detForm.Show();
        }

        private void SearchForm_Activated(object sender, EventArgs e)
        {
            if (detailsEditIndex != -1)
                _searchResults[detailsEditIndex] = detBook;
        }
    }
}
