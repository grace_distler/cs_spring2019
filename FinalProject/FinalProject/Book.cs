﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    public class Book
    {
        public string Isbn { get; set; }
        public string AuthorName { get; set; }
        public string BookName { get; set; }
        public int PublishYear { get; set; }
        public List<Review> Reviews { get; set; }
        public System.Drawing.Image Img { get; set; }

        public Book()
        {
            Reviews = new List<Review>();
        }

        public override string ToString()
        {
            return $"{Isbn}   {BookName}   {AuthorName}   {PublishYear}";
        }
    }
}
