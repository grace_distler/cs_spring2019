﻿namespace FinalProject
{
    partial class SearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblYear5 = new System.Windows.Forms.Label();
            this.lblYear1 = new System.Windows.Forms.Label();
            this.lblYear4 = new System.Windows.Forms.Label();
            this.lblYear2 = new System.Windows.Forms.Label();
            this.lblYear3 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblAuthor5 = new System.Windows.Forms.Label();
            this.lblAuthor3 = new System.Windows.Forms.Label();
            this.lblAuthor4 = new System.Windows.Forms.Label();
            this.lblAuthor1 = new System.Windows.Forms.Label();
            this.lblAuthor2 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnDetails5 = new System.Windows.Forms.Button();
            this.btnDetails4 = new System.Windows.Forms.Button();
            this.btnDetails2 = new System.Windows.Forms.Button();
            this.btnDetails3 = new System.Windows.Forms.Button();
            this.btnDetails1 = new System.Windows.Forms.Button();
            this.lblBookName1 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblBookName5 = new System.Windows.Forms.Label();
            this.lblBookName2 = new System.Windows.Forms.Label();
            this.lblBookName4 = new System.Windows.Forms.Label();
            this.lblBookName3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnPageThree = new System.Windows.Forms.Button();
            this.btnPageTwo = new System.Windows.Forms.Button();
            this.btnPageOne = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.cmbAuthor = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblBookISBN5 = new System.Windows.Forms.Label();
            this.lblBookISBN4 = new System.Windows.Forms.Label();
            this.lblBookISBN3 = new System.Windows.Forms.Label();
            this.lblBookISBN2 = new System.Windows.Forms.Label();
            this.lblBookISBN1 = new System.Windows.Forms.Label();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lblYear5);
            this.panel4.Controls.Add(this.lblYear1);
            this.panel4.Controls.Add(this.lblYear4);
            this.panel4.Controls.Add(this.lblYear2);
            this.panel4.Controls.Add(this.lblYear3);
            this.panel4.Location = new System.Drawing.Point(597, 115);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(67, 359);
            this.panel4.TabIndex = 23;
            // 
            // lblYear5
            // 
            this.lblYear5.AutoSize = true;
            this.lblYear5.Location = new System.Drawing.Point(7, 306);
            this.lblYear5.Name = "lblYear5";
            this.lblYear5.Size = new System.Drawing.Size(0, 17);
            this.lblYear5.TabIndex = 14;
            // 
            // lblYear1
            // 
            this.lblYear1.AutoSize = true;
            this.lblYear1.Location = new System.Drawing.Point(7, 37);
            this.lblYear1.Name = "lblYear1";
            this.lblYear1.Size = new System.Drawing.Size(0, 17);
            this.lblYear1.TabIndex = 10;
            // 
            // lblYear4
            // 
            this.lblYear4.AutoSize = true;
            this.lblYear4.Location = new System.Drawing.Point(7, 236);
            this.lblYear4.Name = "lblYear4";
            this.lblYear4.Size = new System.Drawing.Size(0, 17);
            this.lblYear4.TabIndex = 13;
            // 
            // lblYear2
            // 
            this.lblYear2.AutoSize = true;
            this.lblYear2.Location = new System.Drawing.Point(7, 103);
            this.lblYear2.Name = "lblYear2";
            this.lblYear2.Size = new System.Drawing.Size(0, 17);
            this.lblYear2.TabIndex = 11;
            // 
            // lblYear3
            // 
            this.lblYear3.AutoSize = true;
            this.lblYear3.Location = new System.Drawing.Point(7, 167);
            this.lblYear3.Name = "lblYear3";
            this.lblYear3.Size = new System.Drawing.Size(0, 17);
            this.lblYear3.TabIndex = 12;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblAuthor5);
            this.panel3.Controls.Add(this.lblAuthor3);
            this.panel3.Controls.Add(this.lblAuthor4);
            this.panel3.Controls.Add(this.lblAuthor1);
            this.panel3.Controls.Add(this.lblAuthor2);
            this.panel3.Location = new System.Drawing.Point(468, 115);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(124, 359);
            this.panel3.TabIndex = 24;
            // 
            // lblAuthor5
            // 
            this.lblAuthor5.AutoSize = true;
            this.lblAuthor5.Location = new System.Drawing.Point(7, 306);
            this.lblAuthor5.Name = "lblAuthor5";
            this.lblAuthor5.Size = new System.Drawing.Size(0, 17);
            this.lblAuthor5.TabIndex = 9;
            // 
            // lblAuthor3
            // 
            this.lblAuthor3.AutoSize = true;
            this.lblAuthor3.Location = new System.Drawing.Point(7, 167);
            this.lblAuthor3.Name = "lblAuthor3";
            this.lblAuthor3.Size = new System.Drawing.Size(0, 17);
            this.lblAuthor3.TabIndex = 7;
            // 
            // lblAuthor4
            // 
            this.lblAuthor4.AutoSize = true;
            this.lblAuthor4.Location = new System.Drawing.Point(7, 236);
            this.lblAuthor4.Name = "lblAuthor4";
            this.lblAuthor4.Size = new System.Drawing.Size(0, 17);
            this.lblAuthor4.TabIndex = 8;
            // 
            // lblAuthor1
            // 
            this.lblAuthor1.AutoSize = true;
            this.lblAuthor1.Location = new System.Drawing.Point(7, 37);
            this.lblAuthor1.Name = "lblAuthor1";
            this.lblAuthor1.Size = new System.Drawing.Size(0, 17);
            this.lblAuthor1.TabIndex = 5;
            // 
            // lblAuthor2
            // 
            this.lblAuthor2.AutoSize = true;
            this.lblAuthor2.Location = new System.Drawing.Point(7, 103);
            this.lblAuthor2.Name = "lblAuthor2";
            this.lblAuthor2.Size = new System.Drawing.Size(0, 17);
            this.lblAuthor2.TabIndex = 6;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnDetails5);
            this.panel5.Controls.Add(this.btnDetails4);
            this.panel5.Controls.Add(this.btnDetails2);
            this.panel5.Controls.Add(this.btnDetails3);
            this.panel5.Controls.Add(this.btnDetails1);
            this.panel5.Location = new System.Drawing.Point(670, 115);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(90, 359);
            this.panel5.TabIndex = 25;
            // 
            // btnDetails5
            // 
            this.btnDetails5.Location = new System.Drawing.Point(12, 299);
            this.btnDetails5.Name = "btnDetails5";
            this.btnDetails5.Size = new System.Drawing.Size(65, 30);
            this.btnDetails5.TabIndex = 4;
            this.btnDetails5.Text = "Details";
            this.btnDetails5.UseVisualStyleBackColor = true;
            this.btnDetails5.Click += new System.EventHandler(this.Details_Clicked);
            // 
            // btnDetails4
            // 
            this.btnDetails4.Location = new System.Drawing.Point(13, 229);
            this.btnDetails4.Name = "btnDetails4";
            this.btnDetails4.Size = new System.Drawing.Size(65, 30);
            this.btnDetails4.TabIndex = 3;
            this.btnDetails4.Text = "Details";
            this.btnDetails4.UseVisualStyleBackColor = true;
            this.btnDetails4.Click += new System.EventHandler(this.Details_Clicked);
            // 
            // btnDetails2
            // 
            this.btnDetails2.Location = new System.Drawing.Point(12, 96);
            this.btnDetails2.Name = "btnDetails2";
            this.btnDetails2.Size = new System.Drawing.Size(65, 30);
            this.btnDetails2.TabIndex = 2;
            this.btnDetails2.Text = "Details";
            this.btnDetails2.UseVisualStyleBackColor = true;
            this.btnDetails2.Click += new System.EventHandler(this.Details_Clicked);
            // 
            // btnDetails3
            // 
            this.btnDetails3.Location = new System.Drawing.Point(12, 160);
            this.btnDetails3.Name = "btnDetails3";
            this.btnDetails3.Size = new System.Drawing.Size(65, 30);
            this.btnDetails3.TabIndex = 1;
            this.btnDetails3.Text = "Details";
            this.btnDetails3.UseVisualStyleBackColor = true;
            this.btnDetails3.Click += new System.EventHandler(this.Details_Clicked);
            // 
            // btnDetails1
            // 
            this.btnDetails1.Location = new System.Drawing.Point(12, 30);
            this.btnDetails1.Name = "btnDetails1";
            this.btnDetails1.Size = new System.Drawing.Size(65, 30);
            this.btnDetails1.TabIndex = 0;
            this.btnDetails1.Text = "Details";
            this.btnDetails1.UseVisualStyleBackColor = true;
            this.btnDetails1.Click += new System.EventHandler(this.Details_Clicked);
            // 
            // lblBookName1
            // 
            this.lblBookName1.AutoSize = true;
            this.lblBookName1.Location = new System.Drawing.Point(9, 37);
            this.lblBookName1.Name = "lblBookName1";
            this.lblBookName1.Size = new System.Drawing.Size(0, 17);
            this.lblBookName1.TabIndex = 5;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.lblBookName5);
            this.panel6.Controls.Add(this.lblBookName2);
            this.panel6.Controls.Add(this.lblBookName4);
            this.panel6.Controls.Add(this.lblBookName1);
            this.panel6.Controls.Add(this.lblBookName3);
            this.panel6.Location = new System.Drawing.Point(166, 115);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(295, 359);
            this.panel6.TabIndex = 26;
            // 
            // lblBookName5
            // 
            this.lblBookName5.AutoSize = true;
            this.lblBookName5.Location = new System.Drawing.Point(9, 306);
            this.lblBookName5.Name = "lblBookName5";
            this.lblBookName5.Size = new System.Drawing.Size(0, 17);
            this.lblBookName5.TabIndex = 9;
            // 
            // lblBookName2
            // 
            this.lblBookName2.AutoSize = true;
            this.lblBookName2.Location = new System.Drawing.Point(9, 103);
            this.lblBookName2.Name = "lblBookName2";
            this.lblBookName2.Size = new System.Drawing.Size(0, 17);
            this.lblBookName2.TabIndex = 6;
            // 
            // lblBookName4
            // 
            this.lblBookName4.AutoSize = true;
            this.lblBookName4.Location = new System.Drawing.Point(9, 236);
            this.lblBookName4.Name = "lblBookName4";
            this.lblBookName4.Size = new System.Drawing.Size(0, 17);
            this.lblBookName4.TabIndex = 8;
            // 
            // lblBookName3
            // 
            this.lblBookName3.AutoSize = true;
            this.lblBookName3.Location = new System.Drawing.Point(9, 167);
            this.lblBookName3.Name = "lblBookName3";
            this.lblBookName3.Size = new System.Drawing.Size(0, 17);
            this.lblBookName3.TabIndex = 7;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(13, 72);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(747, 37);
            this.panel1.TabIndex = 21;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(591, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 18);
            this.label7.TabIndex = 3;
            this.label7.Text = "Year";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(461, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 18);
            this.label6.TabIndex = 2;
            this.label6.Text = "Author";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(162, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 18);
            this.label5.TabIndex = 1;
            this.label5.Text = "Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 18);
            this.label4.TabIndex = 0;
            this.label4.Text = "ISBN";
            // 
            // btnPageThree
            // 
            this.btnPageThree.Location = new System.Drawing.Point(140, 498);
            this.btnPageThree.Name = "btnPageThree";
            this.btnPageThree.Size = new System.Drawing.Size(33, 25);
            this.btnPageThree.TabIndex = 20;
            this.btnPageThree.Text = "3";
            this.btnPageThree.UseVisualStyleBackColor = true;
            this.btnPageThree.Click += new System.EventHandler(this.btnPageThree_Click);
            // 
            // btnPageTwo
            // 
            this.btnPageTwo.Location = new System.Drawing.Point(100, 498);
            this.btnPageTwo.Name = "btnPageTwo";
            this.btnPageTwo.Size = new System.Drawing.Size(33, 25);
            this.btnPageTwo.TabIndex = 19;
            this.btnPageTwo.Text = "2";
            this.btnPageTwo.UseVisualStyleBackColor = true;
            this.btnPageTwo.Click += new System.EventHandler(this.btnPageTwo_Click);
            // 
            // btnPageOne
            // 
            this.btnPageOne.Location = new System.Drawing.Point(61, 498);
            this.btnPageOne.Name = "btnPageOne";
            this.btnPageOne.Size = new System.Drawing.Size(33, 25);
            this.btnPageOne.TabIndex = 18;
            this.btnPageOne.Text = "1";
            this.btnPageOne.UseVisualStyleBackColor = true;
            this.btnPageOne.Click += new System.EventHandler(this.btnPageOne_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 502);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 17);
            this.label3.TabIndex = 17;
            this.label3.Text = "Page:";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(670, 23);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(90, 30);
            this.btnSearch.TabIndex = 16;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // cmbAuthor
            // 
            this.cmbAuthor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAuthor.FormattingEnabled = true;
            this.cmbAuthor.Location = new System.Drawing.Point(364, 27);
            this.cmbAuthor.Name = "cmbAuthor";
            this.cmbAuthor.Size = new System.Drawing.Size(261, 24);
            this.cmbAuthor.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(362, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 17);
            this.label2.TabIndex = 14;
            this.label2.Text = "Author:";
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(13, 27);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(315, 22);
            this.txtSearch.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "Search by Name or ISBN:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblBookISBN5);
            this.panel2.Controls.Add(this.lblBookISBN4);
            this.panel2.Controls.Add(this.lblBookISBN3);
            this.panel2.Controls.Add(this.lblBookISBN2);
            this.panel2.Controls.Add(this.lblBookISBN1);
            this.panel2.Location = new System.Drawing.Point(13, 115);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(147, 359);
            this.panel2.TabIndex = 22;
            // 
            // lblBookISBN5
            // 
            this.lblBookISBN5.AutoSize = true;
            this.lblBookISBN5.Location = new System.Drawing.Point(16, 306);
            this.lblBookISBN5.Name = "lblBookISBN5";
            this.lblBookISBN5.Size = new System.Drawing.Size(0, 17);
            this.lblBookISBN5.TabIndex = 4;
            // 
            // lblBookISBN4
            // 
            this.lblBookISBN4.AutoSize = true;
            this.lblBookISBN4.Location = new System.Drawing.Point(16, 236);
            this.lblBookISBN4.Name = "lblBookISBN4";
            this.lblBookISBN4.Size = new System.Drawing.Size(0, 17);
            this.lblBookISBN4.TabIndex = 3;
            // 
            // lblBookISBN3
            // 
            this.lblBookISBN3.AutoSize = true;
            this.lblBookISBN3.Location = new System.Drawing.Point(16, 167);
            this.lblBookISBN3.Name = "lblBookISBN3";
            this.lblBookISBN3.Size = new System.Drawing.Size(0, 17);
            this.lblBookISBN3.TabIndex = 2;
            // 
            // lblBookISBN2
            // 
            this.lblBookISBN2.AutoSize = true;
            this.lblBookISBN2.Location = new System.Drawing.Point(16, 103);
            this.lblBookISBN2.Name = "lblBookISBN2";
            this.lblBookISBN2.Size = new System.Drawing.Size(0, 17);
            this.lblBookISBN2.TabIndex = 1;
            // 
            // lblBookISBN1
            // 
            this.lblBookISBN1.AutoSize = true;
            this.lblBookISBN1.Location = new System.Drawing.Point(16, 37);
            this.lblBookISBN1.Name = "lblBookISBN1";
            this.lblBookISBN1.Size = new System.Drawing.Size(0, 17);
            this.lblBookISBN1.TabIndex = 0;
            // 
            // SearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(771, 538);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnPageThree);
            this.Controls.Add(this.btnPageTwo);
            this.Controls.Add(this.btnPageOne);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.cmbAuthor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel2);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "SearchForm";
            this.Text = "Barnes & Noble";
            this.Activated += new System.EventHandler(this.SearchForm_Activated);
            this.Load += new System.EventHandler(this.SearchForm_Load);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblYear5;
        private System.Windows.Forms.Label lblYear1;
        private System.Windows.Forms.Label lblYear4;
        private System.Windows.Forms.Label lblYear2;
        private System.Windows.Forms.Label lblYear3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblAuthor5;
        private System.Windows.Forms.Label lblAuthor3;
        private System.Windows.Forms.Label lblAuthor4;
        private System.Windows.Forms.Label lblAuthor1;
        private System.Windows.Forms.Label lblAuthor2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnDetails5;
        private System.Windows.Forms.Button btnDetails4;
        private System.Windows.Forms.Button btnDetails2;
        private System.Windows.Forms.Button btnDetails3;
        private System.Windows.Forms.Button btnDetails1;
        private System.Windows.Forms.Label lblBookName1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblBookName5;
        private System.Windows.Forms.Label lblBookName2;
        private System.Windows.Forms.Label lblBookName4;
        private System.Windows.Forms.Label lblBookName3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnPageThree;
        private System.Windows.Forms.Button btnPageTwo;
        private System.Windows.Forms.Button btnPageOne;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.ComboBox cmbAuthor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblBookISBN5;
        private System.Windows.Forms.Label lblBookISBN4;
        private System.Windows.Forms.Label lblBookISBN3;
        private System.Windows.Forms.Label lblBookISBN2;
        private System.Windows.Forms.Label lblBookISBN1;
    }
}

