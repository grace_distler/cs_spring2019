﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace LB7
{
    class PigLatin
    {
        static void Main(string[] args)
        {
            const string SPACE = " ";
            string pig = "";
            string first;
            string last;

            Write("Enter a word in English to convert it to Pig Latin... ");
            string txtEnglish = ReadLine();
            string[] words = txtEnglish.Split(' ');
            foreach (string word in words)
            {
                first = word.Substring(0, 1);
                last = word.Substring(1, word.Length - 1);
                pig += last + first + "ay" + SPACE;
                
            }
            WriteLine();
            WriteLine(pig);
            ReadKey();
        }
    }
}
