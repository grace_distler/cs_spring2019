﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace LB4
{
    class DistanceConverter
    {
        static void Main(string[] args)
        {
            const double KM_PER_MILE = 1.6;
            string milesAsString;
            double miles;
            double conversion;
            Write("Enter a distance in miles >> ");
            milesAsString = ReadLine();
            miles = Convert.ToDouble(milesAsString);
            conversion = miles * KM_PER_MILE;
            WriteLine("{0} miles is {1} kilometers ", miles.ToString("g"), conversion.ToString("g"));
            ReadLine();
        }
    }
}
