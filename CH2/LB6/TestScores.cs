﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace LB6
{
    class TestScores
    {
        static void Main(string[] args)
        {
            string testScore1, testScore2, testScore3, testScore4;
            int test1, test2, test3, test4, total;

            Write("Enter the first test score ");
            testScore1 = ReadLine();
            Write("Enter the second test score ");
            testScore2 = ReadLine();
            Write("Enter the third test score ");
            testScore3 = ReadLine();
            Write("Enter the fourth test score ");
            testScore4 = ReadLine();

            test1 = Convert.ToInt32(testScore1);
            test2 = Convert.ToInt32(testScore2);
            test3 = Convert.ToInt32(testScore3);
            test4 = Convert.ToInt32(testScore4);

            total = test1 + test2 + test3 + test4;
            double avg = total / 4;

            WriteLine();
            WriteLine("The average of the test scores is " + Math.Round(avg, 2));
            ReadLine();
        }
    }
}
