﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace LB5
{
    class ProjectedRaises
    {
        static void Main(string[] args)
        {
            const double PERCENT_RAISE = 0.04;
            string employee1, employee2, employee3;
            double employee1Salary, employee1Raise1, employee1Raise2, employee2Salary, employee2Raise1, employee2Raise2, employee3Salary, employee3Raise1, employee3Raise2;

            Write("Enter the first employee's name... ");
            employee1 = ReadLine();
            Write("Enter {0}'s salary... ", employee1);
            employee1Salary = Convert.ToDouble(ReadLine());
            WriteLine();

            Write("Enter the second employee's name... ");
            employee2 = ReadLine();
            Write("Enter {0}'s salary... ", employee2);
            employee2Salary = Convert.ToDouble(ReadLine());
            WriteLine();

            Write("Enter the third employee's name... ");
            employee3 = ReadLine();
            Write("Enter {0}'s salary... ", employee3);
            employee3Salary = Convert.ToDouble(ReadLine());
            WriteLine();

            employee1Raise1 = employee1Salary + (employee1Salary * PERCENT_RAISE);
            employee2Raise1 = employee2Salary + (employee2Salary * PERCENT_RAISE);
            employee3Raise1 = employee3Salary + (employee3Salary * PERCENT_RAISE);

            employee1Raise2 = employee1Raise1 + (employee1Raise1 * PERCENT_RAISE);
            employee2Raise2 = employee2Raise1 + (employee2Raise1 * PERCENT_RAISE);
            employee3Raise2 = employee3Raise1 + (employee3Raise1 * PERCENT_RAISE);

            WriteLine("+------------+------------+-----------+------------+");
            WriteLine("|    Year    |     {0}   |     {1}   |     {2}   |", employee1, employee2, employee3);
            WriteLine("+------------+------------+-----------+------------+");
            WriteLine("|    2018    |{0} |{1} |{2} |", employee1Salary.ToString("C"), employee2Salary.ToString("C"), employee3Salary.ToString("C"));
            WriteLine("|    2019    |{0} |{1} |{2} |", employee1Raise1.ToString("C"), employee2Raise1.ToString("C"), employee3Raise1.ToString("C"));
            WriteLine("|    2020    |{0} |{1} |{2} |", employee1Raise2.ToString("C"), employee2Raise2.ToString("C"), employee3Raise2.ToString("C"));
            WriteLine("+------------+------------+-----------+------------+");
            ReadLine();
        }
    }
}
