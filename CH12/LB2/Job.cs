﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB2
{
    public class Job
    {
        public string JobName { get; set; }
        public string JobURL { get; set; }

        public override string ToString()
        {
            return JobName;
        }

    }
}
