﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB2
{
    public partial class Form2 : Form
    {
        private Industry[] _selectedIndustries;

        public Form2(params Industry[] selectedIndustries)
        {
            InitializeComponent();
            _selectedIndustries = selectedIndustries;

            foreach(var industries in selectedIndustries)
            {
                foreach (var jobs in industries.CurrentJobs)
                {
                    LinkLabel nextLink = new LinkLabel();
                    nextLink.Text = jobs.JobName;
                    nextLink.Click += NextLink_Click;
                    nextLink.AutoSize = true;
                    flowLayoutPanel1.Controls.Add(nextLink);
                }
            }
        }

        private void NextLink_Click(object sender, EventArgs e)
        {
            LinkLabel thisLink = (LinkLabel)sender;
            foreach (var industries in _selectedIndustries)
            {
                foreach (var jobs in industries.CurrentJobs)
                {
                    if (jobs.JobName == thisLink.Text)
                    {
                        System.Diagnostics.Process.Start(jobs.JobURL);
                    }
                }
            }
        }
    }
}
