﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB2
{
    public class Industry
    {
        private string _industryName;
        private Job[] _currentJobs;

        public Industry()
        {

        }
        public Industry(string industryName, Job[] currentJobs)
        {
            _industryName = industryName;
            _currentJobs = currentJobs;
        }

        public string IndustryName
        {
            get
            {
                return _industryName;
            }
            set
            {
                _industryName = value;
            }
        }
        public Job[] CurrentJobs
        {
            get
            {
                return _currentJobs;
            }
            set
            {
                _currentJobs = value;
            }
        }

        public override string ToString()
        {
            return _industryName;
        }
    }
}
