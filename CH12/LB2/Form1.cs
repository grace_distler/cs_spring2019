﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.ListBox;

namespace LB2
{
    public partial class Form1 : Form
    {
        Industry[] industries = new Industry[1];
        Job[] itJobs = new Job[5];
        Job[] carpenterJobs = new Job[1];
        Job[] hvacJobs = new Job[3];
        Job[] autoJobs = new Job[2];
        Job[] electricJobs = new Job[4];
       
        public Form1()
        {
            InitializeComponent();

            itJobs[0] = new Job() { JobName = "Software Developer I", JobURL = "https://www.dice.com/jobs/detail/IT-Software-Developer-National-Security-Agency-St-Louis-MO-63101/10470536/20190104" };
            itJobs[1] = new Job() { JobName = "Entry Level C# Developer", JobURL = "https://www.dice.com/jobs/detail/Entry%26%2345Level-C%23-Software-Developer-Capital-Markets-Placement-St.-Louis-MO-63101/10120548/3625_33419925" };
            itJobs[2] = new Job() { JobName = "Android Developer", JobURL = "https://www.dice.com/jobs/detail/Android-Developer-Xoriant-Corporation-Saint-Louis-MO-63102/xorca001/5840202" };
            itJobs[3] = new Job() { JobName = "Front End Developer", JobURL = "https://www.dice.com/jobs/detail/Front-End-Developer-SAIC-O%27fallon-IL-62269/10111346/445885" };
            itJobs[4] = new Job() { JobName = "JavaScript Developer", JobURL = "https://www.dice.com/jobs/detail/JavaScript-Developer-WWT-%26%2347-Asynchrony-Labs-Saint-Louis-MO-63102/10111827a/583528" };

            carpenterJobs[0] = new Job() { JobName = "Carpenter Apprentice", JobURL = "https://explorethetrades.org/what-we-do/personal-guide-program/" };

            hvacJobs[0] = new Job() { JobName = "Mechanical HVAC Engineer", JobURL = "https://m.hays.co.uk/Job/Detail/mechanical-hvac-engineer-oxford-en-GB_3554794?q=HVAC&location=&applyId=JOB_1997506&jobSource=HaysGCJ&isSponsored=N&specialismId=&subSpecialismId=&jobName=projects/mineral-balm-174308/jobs/117317980309594822&lang=en" };
            hvacJobs[1] = new Job() { JobName = "HVAC Refrigeration Technician", JobURL = "http://jobs.jobvite.com/totalmechanicalservices/job/o3ZJ8fwD?utm_campaign=google_jobs_apply&utm_source=google_jobs_apply&utm_medium=organic" };
            hvacJobs[2] = new Job() { JobName = "HVAC Installer", JobURL = "https://www.gethvacjobs.com/hvac-installer-st-louis-missouri-160018628.htm?utm_campaign=google_jobs_apply&utm_source=google_jobs_apply&utm_medium=organic" };

            autoJobs[0] = new Job() { JobName = "Automotive Service Consultant", JobURL = "https://careers.hireology.com/m5managementservices/269657/description?utm_campaign=google_jobs_apply&utm_source=google_jobs_apply&utm_medium=organic" };
            autoJobs[0] = new Job() { JobName = "Automotive Technician", JobURL = "https://www.snagajob.com/jobs/521716015?utm_campaign=google_jobs_apply&utm_source=google_jobs_apply&utm_medium=organic" };

            electricJobs[0] = new Job() { JobName = "Maintenance Electrician", JobURL = "https://www.careerbuilder.com/job/J4R0R26CR7HCM4VHCD5?utm_campaign=google_jobs_apply&utm_source=google_jobs_apply&utm_medium=organic" };
            electricJobs[1] = new Job() { JobName = "Electrician (Technology)", JobURL = "https://www.nestlejobs.com/job/9168496/electrician-technology-site-services-saint-louis-mo/?utm_campaign=google_jobs_apply&utm_source=google_jobs_apply&utm_medium=organic" };
            electricJobs[2] = new Job() { JobName = "Facility Electrician", JobURL = "https://www.linkedin.com/jobs/view/facility-electrician-at-bistate-medical-review-1058798349/?utm_campaign=google_jobs_apply&utm_source=google_jobs_apply&utm_medium=organic" };
            electricJobs[3] = new Job() { JobName = "Electrical Inspector I", JobURL = "https://www.linkedin.com/jobs/view/electrical-inspector-i-at-city-of-st-louis-1099756618/?utm_campaign=google_jobs_apply&utm_source=google_jobs_apply&utm_medium=organic" };

            industries[0] = new Industry("Information Technology", itJobs);
            industries[1] = new Industry("Carpentery", carpenterJobs);
            industries[2] = new Industry("HVAC", hvacJobs);
            industries[3] = new Industry("Automotive", autoJobs);
            industries[4] = new Industry("Electrical", electricJobs);
            lstIndustries.Items.AddRange(industries);
            
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SelectedIndexCollection selectedIndicies = lstIndustries.SelectedIndices;

            Industry[] selectedIndustries;
            selectedIndustries = new Industry[selectedIndicies.Count];

            int indexCount = 0;

            foreach (var index in selectedIndicies)
            {
                selectedIndustries[indexCount] = industries[index];
                indexCount++;
            }

            SearchResults searchForm = new SearchResults(selectedIndustries);
            searchForm.ShowDialog();
            //SelectedIndexCollection selectedIndices = lstIndustries.SelectedIndices;

        }
    }
}
