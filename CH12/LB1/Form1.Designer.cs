﻿namespace LB1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbSuperHeroes = new System.Windows.Forms.ComboBox();
            this.lblSuperHeroName = new System.Windows.Forms.Label();
            this.lblFunFacts = new System.Windows.Forms.Label();
            this.lblBiography = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblLink = new System.Windows.Forms.LinkLabel();
            this.picSuperHeroPicture = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picSuperHeroPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbSuperHeroes
            // 
            this.cmbSuperHeroes.FormattingEnabled = true;
            this.cmbSuperHeroes.Location = new System.Drawing.Point(40, 46);
            this.cmbSuperHeroes.Name = "cmbSuperHeroes";
            this.cmbSuperHeroes.Size = new System.Drawing.Size(233, 24);
            this.cmbSuperHeroes.TabIndex = 0;
            this.cmbSuperHeroes.SelectedIndexChanged += new System.EventHandler(this.cmbSuperHeroes_SelectedIndexChanged);
            // 
            // lblSuperHeroName
            // 
            this.lblSuperHeroName.AutoSize = true;
            this.lblSuperHeroName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSuperHeroName.Location = new System.Drawing.Point(37, 108);
            this.lblSuperHeroName.Name = "lblSuperHeroName";
            this.lblSuperHeroName.Size = new System.Drawing.Size(85, 29);
            this.lblSuperHeroName.TabIndex = 1;
            this.lblSuperHeroName.Text = "label1";
            // 
            // lblFunFacts
            // 
            this.lblFunFacts.AutoSize = true;
            this.lblFunFacts.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFunFacts.Location = new System.Drawing.Point(39, 146);
            this.lblFunFacts.Name = "lblFunFacts";
            this.lblFunFacts.Size = new System.Drawing.Size(46, 18);
            this.lblFunFacts.TabIndex = 3;
            this.lblFunFacts.Text = "label1";
            // 
            // lblBiography
            // 
            this.lblBiography.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBiography.Location = new System.Drawing.Point(37, 307);
            this.lblBiography.Name = "lblBiography";
            this.lblBiography.Size = new System.Drawing.Size(328, 225);
            this.lblBiography.TabIndex = 5;
            this.lblBiography.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(35, 269);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Biography";
            // 
            // lblLink
            // 
            this.lblLink.AutoSize = true;
            this.lblLink.Location = new System.Drawing.Point(39, 515);
            this.lblLink.Name = "lblLink";
            this.lblLink.Size = new System.Drawing.Size(72, 17);
            this.lblLink.TabIndex = 6;
            this.lblLink.TabStop = true;
            this.lblLink.Text = "linkLabel1";
            this.lblLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblLink_LinkClicked);
            // 
            // picSuperHeroPicture
            // 
            this.picSuperHeroPicture.Location = new System.Drawing.Point(431, 46);
            this.picSuperHeroPicture.Name = "picSuperHeroPicture";
            this.picSuperHeroPicture.Size = new System.Drawing.Size(314, 385);
            this.picSuperHeroPicture.TabIndex = 2;
            this.picSuperHeroPicture.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 565);
            this.Controls.Add(this.lblLink);
            this.Controls.Add(this.lblBiography);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblFunFacts);
            this.Controls.Add(this.picSuperHeroPicture);
            this.Controls.Add(this.lblSuperHeroName);
            this.Controls.Add(this.cmbSuperHeroes);
            this.Name = "Form1";
            this.Text = "Super Hero Database";
            ((System.ComponentModel.ISupportInitialize)(this.picSuperHeroPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbSuperHeroes;
        private System.Windows.Forms.Label lblSuperHeroName;
        private System.Windows.Forms.PictureBox picSuperHeroPicture;
        private System.Windows.Forms.Label lblFunFacts;
        private System.Windows.Forms.Label lblBiography;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.LinkLabel lblLink;
    }
}

