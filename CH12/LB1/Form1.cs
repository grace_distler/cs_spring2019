﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB1
{
    public partial class Form1 : Form
    {
        SuperHero[] heroes = new SuperHero[5];
        public Form1()
        {
            InitializeComponent();
            cmbSuperHeroes.Items.Insert(0, "None");
            heroes[0] = new SuperHero() { HeroName = "Captain America", HeroLikes = "Beating Up Bad Guys", HeroDislikes = "Iron Man", Superpower = "Endurance", Biography = "Captain America is the alter ego of Steve Rogers, a frail young man enhanced to the peak of human perfection by an experimental serum to aid the United States government's efforts in World War II. Near the end of the war, he was trapped in ice and survived in suspended animation until he was revived in the present day.", ImageURL = Properties.Resources.CaptainAmerica, URL = "https://en.wikipedia.org/wiki/Captain_America" };
            heroes[1] = new SuperHero() { HeroName = "Hulk", HeroLikes = "Smash", HeroDislikes = "Captain America", Superpower = "Invulnerability", Biography = "Following his accidental exposure to gamma rays during the detonation of an experimental bomb, Dr. Robert Bruce Banner is physically transformed into the Hulk when subjected to emotional stress, at or against his will, often leading to destructive rampages and conflicts that complicate Banner's civilian life.",ImageURL= Properties.Resources.Hulk, URL = "https://en.wikipedia.org/wiki/Hulk" };
            heroes[2] = new SuperHero() { HeroName = "Iron Man", HeroLikes = "Technology", HeroDislikes = "Captain America", Superpower = "Genius", Biography = "The wealthy business magnate and ingenious scientist, Tony Stark, suffered a severe chest injury during a kidnapping in which his captors attempted to force him to build weapons of mass destruction for them. He instead created a power suit to save his life and escape captivity.", ImageURL = Properties.Resources.IronMan, URL = "https://en.wikipedia.org/wiki/Iron_Man" };
            heroes[3] = new SuperHero() { HeroName = "Spider Man", HeroLikes = "Mary Jane Watson", HeroDislikes = "The Green Goblin", Superpower = "Spider-sense ability", Biography = "Spider-Man is the alias of Peter Parker, an orphan raised by his Aunt May and Uncle Ben in New York City after his parents Richard and Mary Parker were killed in a plane crash. His origin story has him acquiring spider-related abilities after a bite from a radioactive spider; these include clinging to surfaces, shooting spider-webs from wrist-mounted devices, and detecting danger with his 'spider - sense'.", ImageURL = Properties.Resources.SpiderMan, URL = "https://en.wikipedia.org/wiki/Spider-Man" };
            heroes[4] = new SuperHero() { HeroName = "Thor", HeroLikes = "Family and Friends", HeroDislikes = "Frost Giants", Superpower = "Superhuman strength", Biography = "Thor is the Asgardian god of thunder who possesses the enchanted hammer, Mjolnir, which grants him the ability to fly and manipulate weather amongst his other superhuman attributes.", ImageURL = Properties.Resources.Thor, URL = "https://en.wikipedia.org/wiki/Thor_(Marvel_Comics)" };


           //cmbSuperHeroes.Items.Clear();
            cmbSuperHeroes.Items.AddRange(heroes);            
            cmbSuperHeroes.SelectedIndex = 0;
        }

        private void cmbSuperHeroes_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = cmbSuperHeroes.SelectedIndex;
            if (index == 0)
            {
                lblSuperHeroName.Text = "Please select a hero";
                lblFunFacts.Text = "";
                label2.Text = "";
                lblBiography.Text = "";
                lblLink.Text = "";
                picSuperHeroPicture.Image = null;
            }
            else
            {
                index--;
                lblSuperHeroName.Text = heroes[index].HeroName;
                lblFunFacts.Text= "Likes: " + heroes[index].HeroLikes +"\n\nDislikes: " + heroes[index].HeroDislikes + "\n\nSuperpower: " + heroes[index].Superpower;
                label2.Text = "Biography";
                lblBiography.Text = heroes[index].Biography;
                picSuperHeroPicture.Image = heroes[index].ImageURL;
                lblLink.Text = heroes[index].URL;
            }
        }

        private void lblLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            int index = cmbSuperHeroes.SelectedIndex - 1;
            string url = heroes[index].URL;
            System.Diagnostics.Process.Start(url);
        }
    }
}
