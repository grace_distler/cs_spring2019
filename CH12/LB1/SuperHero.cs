﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB1
{
    public class SuperHero
    {
        public string HeroName { get; set; }
        public string HeroLikes { get; set; }
        public string HeroDislikes { get; set; }
        public string Superpower { get; set; }
        public string Biography { get; set; }
        public Image ImageURL { get; set; }
        public string URL { get; set; }

        public override string ToString()
        {
            return HeroName;
        }
    }
}
