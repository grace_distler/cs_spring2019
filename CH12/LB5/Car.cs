﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB5
{
    public class Car
    {
        public Car()
        {
            Description = "Lorem ipsum dolor sit amet";
        }

        public string Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public System.Drawing.Image Image { get; set; }
        public string Description { get; set; }

        public string Name
        {
            get
            {
                return $"{Year} {Make} {Model}";
            }
        }

        public override string ToString()
        {
            return Model;
        }
    }
}
