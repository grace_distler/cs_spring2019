﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB4
{
    public class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Course { get; set; }
        public string CourseGrade { get; set; }
        public double Gpa { get; set; }

        public override string ToString()
        {
            return FirstName + " " + LastName + "\nCourse: " + Course + "\nCourse Grade: " + CourseGrade + "\nGPA: " + Gpa;
        }
    }
}
