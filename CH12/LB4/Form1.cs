﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB4
{
    public partial class FrmStudentList : Form
    {
        Student[] rankenStudents = new Student[5];

        public FrmStudentList()
        {
            InitializeComponent();

            rankenStudents[0] = new Student() { FirstName = "Grace", LastName = "Distler", Course = "AWD 1100", CourseGrade = "A+", Gpa = 4.0 };
            rankenStudents[1] = new Student() { FirstName = "Mike", LastName = "Ross", Course = "AWD 1020", CourseGrade = "A+", Gpa = 3.75 };
            rankenStudents[2] = new Student() { FirstName = "Bob", LastName = "Burgese", Course = "AWD 1113", CourseGrade = "C", Gpa = 2.0 };
            rankenStudents[3] = new Student() { FirstName = "Jan", LastName = "Scott", Course = "AWD 1000", CourseGrade = "B", Gpa = 3.3 };
            rankenStudents[4] = new Student() { FirstName = "Ashley", LastName = "Speare", Course = "AWD 1112", CourseGrade = "A", Gpa =3.5 };
        }

        private void txtFirstName_Enter(object sender, EventArgs e)
        {
            this.AcceptButton = btnSearchByFirstName;
        }

        private void txtLastName_Enter(object sender, EventArgs e)
        {
            this.AcceptButton = btnSearchByLastName;
        }

        private void btnSearchByFirstName_Click(object sender, EventArgs e)
        {
            string firstName = txtFirstName.Text;
            bool found = false;

            foreach (var student in rankenStudents)
            {
                if (student.FirstName.Equals(firstName))
                {
                    lblOutputSummary.Text = student.ToString();
                    found = true;
                }
            }

            if (!found)
            {
                lblOutputSummary.Text = "Student Not Found";
            }
        }

        private void btnSearchByLastName_Click(object sender, EventArgs e)
        {
            string lastName = txtLastName.Text;
            bool found = false;

            foreach (var student in rankenStudents)
            {
                if (student.FirstName.Equals(lastName))
                {
                    lblOutputSummary.Text = student.ToString();
                    found = true;
                }
            }

            if (!found)
            {
                lblOutputSummary.Text = "Student Not Found";
            }
        }
    }
}
