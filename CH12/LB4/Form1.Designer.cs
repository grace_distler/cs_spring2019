﻿namespace LB4
{
    partial class FrmStudentList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.btnSearchByFirstName = new System.Windows.Forms.Button();
            this.lblOutputSummary = new System.Windows.Forms.Label();
            this.btnSearchByLastName = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "First Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(329, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Last Name";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(40, 58);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(203, 22);
            this.txtFirstName.TabIndex = 2;
            this.txtFirstName.Enter += new System.EventHandler(this.txtFirstName_Enter);
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(332, 58);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(203, 22);
            this.txtLastName.TabIndex = 3;
            this.txtLastName.Enter += new System.EventHandler(this.txtLastName_Enter);
            // 
            // btnSearchByFirstName
            // 
            this.btnSearchByFirstName.Location = new System.Drawing.Point(40, 97);
            this.btnSearchByFirstName.Name = "btnSearchByFirstName";
            this.btnSearchByFirstName.Size = new System.Drawing.Size(160, 27);
            this.btnSearchByFirstName.TabIndex = 4;
            this.btnSearchByFirstName.Text = "Search by First Name";
            this.btnSearchByFirstName.UseVisualStyleBackColor = true;
            this.btnSearchByFirstName.Click += new System.EventHandler(this.btnSearchByFirstName_Click);
            // 
            // lblOutputSummary
            // 
            this.lblOutputSummary.AutoSize = true;
            this.lblOutputSummary.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutputSummary.Location = new System.Drawing.Point(37, 180);
            this.lblOutputSummary.Name = "lblOutputSummary";
            this.lblOutputSummary.Size = new System.Drawing.Size(0, 20);
            this.lblOutputSummary.TabIndex = 7;
            // 
            // btnSearchByLastName
            // 
            this.btnSearchByLastName.Location = new System.Drawing.Point(332, 97);
            this.btnSearchByLastName.Name = "btnSearchByLastName";
            this.btnSearchByLastName.Size = new System.Drawing.Size(160, 27);
            this.btnSearchByLastName.TabIndex = 8;
            this.btnSearchByLastName.Text = "Search by Last Name";
            this.btnSearchByLastName.UseVisualStyleBackColor = true;
            this.btnSearchByLastName.Click += new System.EventHandler(this.btnSearchByLastName_Click);
            // 
            // FrmStudentList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 329);
            this.Controls.Add(this.btnSearchByLastName);
            this.Controls.Add(this.lblOutputSummary);
            this.Controls.Add(this.btnSearchByFirstName);
            this.Controls.Add(this.txtLastName);
            this.Controls.Add(this.txtFirstName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmStudentList";
            this.Text = "Student List";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.Button btnSearchByFirstName;
        private System.Windows.Forms.Label lblOutputSummary;
        private System.Windows.Forms.Button btnSearchByLastName;
    }
}

