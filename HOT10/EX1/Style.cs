﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX1
{
    public class Style
    {
        public string Name { get; set; }
        public BadImageFormatException Image { get; set; }
        public double Price { get; set; }
        
        public Style()
        {
            
        }

        public override string ToString()
        {
            return Name + "(" + Price.ToString("C") + ")";
        }
    }
}
