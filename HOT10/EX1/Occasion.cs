﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX1
{
    public class Occasion
    {
        private string _occasionName;
        private Style[] _styles;

        public Occasion()
        {

        }
        public Occasion(string name, Style[] styles)
        {
            _occasionName = name;
            _styles = styles;
        }

        public string Name
        {
            get
            {
                return _occasionName;
            }
            set
            {
                _occasionName = value;
            }
        }
        public Style[] Styles
        {
            get
            {
                return _styles;
            }
            set
            {
                _styles = value;
            }
        }
    }
}
