﻿using EX1.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX1
{
    public partial class Form1 : Form
    {
        private Occasion chosenOccasion;
        private Style chosenStyle;
        private Order theOrder;

        public Form1()
        {
            InitializeComponent();

            //cmbOccasion.Items.Clear();
            cmbOccasion.Items.AddRange(GetOccasions());
            cmbOccasion.SelectedIndex = 0;
        }

        private Occasion[] GetOccasions()
        {
            Style[] birthdayStyles = new Style[2];
            birthdayStyles[0] = new Style { Name = "Balloons", Price = 2.25, Image = Resources.HappyBirthdayBalloons };
            birthdayStyles[1] = new Style { Name = "Primary Colors", Price = 1.75, Image = Resources.HappyBirthdayColor };

            Style[] getWellSoonStyles = new Style[2];
            getWellSoonStyles[0] = new Style { Name = "Bandaid", Price = 1.50, Image = Resources.GetWellSoonBandaid };
            getWellSoonStyles[1] = new Style { Name = "Bee", Price = 1.75, Image = Resources.GetWellSoonBee };

            Style[] thankYouStyles = new Style[2];
            thankYouStyles[0] = new Style { Name = "Elaborate", Price = 2.00, Image = Resources.ThankYouPink };
            thankYouStyles[1] = new Style { Name = "Simple", Price = 1.00, Image = Resources.ThankYouWhite };

            return{

            }
        }

       
               
                case "{Thank You} {Elaborate}":
                    return new Order[]
                    {
                        new Order{Occasion="Get Well Soon", Style="Bandaid", Cost=2.00,  Image=Properties.Resources.ThankYouPink}
                    };
                case "{Thank You} {Simple}":
                    return new Order[]
                    {
                        new Order{Occasion="Get Well Soon", Style="Bandaid", Cost=1.00,  Image=Properties.Resources.ThankYouWhite}
                    };
            }
        }

        private void cmbOccasion_SelectedIndexChanged(object sender, EventArgs e)
        {
            string occasion = (string)cmbOccasion.SelectedItem;

            cmbStyle.Items.Clear();
            cmbStyle.Items.AddRange(GetStyle(occasion));
            cmbStyle.SelectedIndex = 0;

        }

        private void cmbStyle_SelectedIndexChanged(object sender, EventArgs e)
        {
            Order o = (Order)cmbStyle.SelectedItem;
            lblCost.Text = o.Cost.ToString("C");
            imgCardImage.Image = o.Image;
        }

        private void cmbOccasion_Enter(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            c.BackColor = Color.LightGreen;
        }

        private void cmbOccasion_Leave(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            c.BackColor = Color.WhiteSmoke;
        }
    }
}
