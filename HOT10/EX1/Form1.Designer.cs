﻿namespace EX1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cmbOccasion = new System.Windows.Forms.ComboBox();
            this.cmbStyle = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chkEnvelope = new System.Windows.Forms.CheckBox();
            this.chkStamp = new System.Windows.Forms.CheckBox();
            this.chkMessage = new System.Windows.Forms.CheckBox();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.lblCost = new System.Windows.Forms.Label();
            this.imgCardImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgCardImage)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Location = new System.Drawing.Point(46, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Occasion";
            this.label1.Enter += new System.EventHandler(this.cmbOccasion_Enter);
            this.label1.Leave += new System.EventHandler(this.cmbOccasion_Leave);
            // 
            // cmbOccasion
            // 
            this.cmbOccasion.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cmbOccasion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOccasion.FormattingEnabled = true;
            this.cmbOccasion.Location = new System.Drawing.Point(49, 50);
            this.cmbOccasion.Name = "cmbOccasion";
            this.cmbOccasion.Size = new System.Drawing.Size(223, 24);
            this.cmbOccasion.TabIndex = 1;
            this.cmbOccasion.SelectedIndexChanged += new System.EventHandler(this.cmbOccasion_SelectedIndexChanged);
            this.cmbOccasion.Enter += new System.EventHandler(this.cmbOccasion_Enter);
            this.cmbOccasion.Leave += new System.EventHandler(this.cmbOccasion_Leave);
            // 
            // cmbStyle
            // 
            this.cmbStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cmbStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStyle.FormattingEnabled = true;
            this.cmbStyle.Location = new System.Drawing.Point(49, 103);
            this.cmbStyle.Name = "cmbStyle";
            this.cmbStyle.Size = new System.Drawing.Size(223, 24);
            this.cmbStyle.TabIndex = 3;
            this.cmbStyle.SelectedIndexChanged += new System.EventHandler(this.cmbStyle_SelectedIndexChanged);
            this.cmbStyle.Enter += new System.EventHandler(this.cmbOccasion_Enter);
            this.cmbStyle.Leave += new System.EventHandler(this.cmbOccasion_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Location = new System.Drawing.Point(46, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Style";
            this.label2.Enter += new System.EventHandler(this.cmbOccasion_Enter);
            this.label2.Leave += new System.EventHandler(this.cmbOccasion_Leave);
            // 
            // chkEnvelope
            // 
            this.chkEnvelope.AutoSize = true;
            this.chkEnvelope.BackColor = System.Drawing.Color.WhiteSmoke;
            this.chkEnvelope.Location = new System.Drawing.Point(49, 168);
            this.chkEnvelope.Name = "chkEnvelope";
            this.chkEnvelope.Size = new System.Drawing.Size(139, 21);
            this.chkEnvelope.TabIndex = 4;
            this.chkEnvelope.Text = "Envelope ($0.25)";
            this.chkEnvelope.UseVisualStyleBackColor = false;
            this.chkEnvelope.Enter += new System.EventHandler(this.cmbOccasion_Enter);
            this.chkEnvelope.Leave += new System.EventHandler(this.cmbOccasion_Leave);
            // 
            // chkStamp
            // 
            this.chkStamp.AutoSize = true;
            this.chkStamp.BackColor = System.Drawing.Color.WhiteSmoke;
            this.chkStamp.Location = new System.Drawing.Point(49, 195);
            this.chkStamp.Name = "chkStamp";
            this.chkStamp.Size = new System.Drawing.Size(120, 21);
            this.chkStamp.TabIndex = 5;
            this.chkStamp.Text = "Stamp ($0.50)";
            this.chkStamp.UseVisualStyleBackColor = false;
            this.chkStamp.Enter += new System.EventHandler(this.cmbOccasion_Enter);
            this.chkStamp.Leave += new System.EventHandler(this.cmbOccasion_Leave);
            // 
            // chkMessage
            // 
            this.chkMessage.AutoSize = true;
            this.chkMessage.BackColor = System.Drawing.Color.WhiteSmoke;
            this.chkMessage.Location = new System.Drawing.Point(49, 222);
            this.chkMessage.Name = "chkMessage";
            this.chkMessage.Size = new System.Drawing.Size(188, 21);
            this.chkMessage.TabIndex = 6;
            this.chkMessage.Text = "Custom Message ($0.25)";
            this.chkMessage.UseVisualStyleBackColor = false;
            this.chkMessage.Enter += new System.EventHandler(this.cmbOccasion_Enter);
            this.chkMessage.Leave += new System.EventHandler(this.cmbOccasion_Leave);
            // 
            // txtMessage
            // 
            this.txtMessage.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtMessage.Enabled = false;
            this.txtMessage.Location = new System.Drawing.Point(49, 261);
            this.txtMessage.Multiline = true;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(313, 191);
            this.txtMessage.TabIndex = 7;
            this.txtMessage.Enter += new System.EventHandler(this.cmbOccasion_Enter);
            this.txtMessage.Leave += new System.EventHandler(this.cmbOccasion_Leave);
            // 
            // lblCost
            // 
            this.lblCost.AutoSize = true;
            this.lblCost.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCost.Location = new System.Drawing.Point(44, 499);
            this.lblCost.Name = "lblCost";
            this.lblCost.Size = new System.Drawing.Size(0, 29);
            this.lblCost.TabIndex = 8;
            // 
            // imgCardImage
            // 
            this.imgCardImage.BackColor = System.Drawing.SystemColors.Control;
            this.imgCardImage.Location = new System.Drawing.Point(412, 50);
            this.imgCardImage.Name = "imgCardImage";
            this.imgCardImage.Size = new System.Drawing.Size(500, 500);
            this.imgCardImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgCardImage.TabIndex = 9;
            this.imgCardImage.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(939, 591);
            this.Controls.Add(this.imgCardImage);
            this.Controls.Add(this.lblCost);
            this.Controls.Add(this.txtMessage);
            this.Controls.Add(this.chkMessage);
            this.Controls.Add(this.chkStamp);
            this.Controls.Add(this.chkEnvelope);
            this.Controls.Add(this.cmbStyle);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbOccasion);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Card Designer";
            ((System.ComponentModel.ISupportInitialize)(this.imgCardImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbOccasion;
        private System.Windows.Forms.ComboBox cmbStyle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkEnvelope;
        private System.Windows.Forms.CheckBox chkStamp;
        private System.Windows.Forms.CheckBox chkMessage;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.Label lblCost;
        private System.Windows.Forms.PictureBox imgCardImage;
    }
}

