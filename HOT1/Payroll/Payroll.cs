﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Payroll
{
    class Payroll
    {
        static void Main(string[] args)
        {
            string firstNameEmployee1, lastNameEmployee1, firstNameEmployee2, lastNameEmployee2;
            double hoursWorkedEmployee1, hourlyRateEmployee1, hoursWorkedEmployee2, hourlyRateEmployee2;
            double grossPayEmployee1, grossPayEmployee2, totPayroll;

            Write("Please enter the first name for employee 1: ");
            firstNameEmployee1 = ReadLine();
            Write("Please enter the last name for employee 1: ");
            lastNameEmployee1 = ReadLine();
            Write("Please enter the hours worked for employee 1: ");
            hoursWorkedEmployee1 = Convert.ToDouble(ReadLine());
            Write("Please enter the hourly rate for employee 1: ");
            hourlyRateEmployee1 = Convert.ToDouble(ReadLine());
            WriteLine();

            grossPayEmployee1 = hoursWorkedEmployee1 * hourlyRateEmployee1;

            WriteLine("First Name: {0}", firstNameEmployee1);
            WriteLine("Last Name: {0}", lastNameEmployee1);
            WriteLine("Hours Worked: {0}", hoursWorkedEmployee1.ToString("F"));
            WriteLine("Hourly Rate: {0}", hourlyRateEmployee1.ToString("C"));
            WriteLine("Gross Pay: {0}", grossPayEmployee1.ToString("C"));
            WriteLine();

            Write("Please enter the first name for employee 2: ");
            firstNameEmployee2 = ReadLine();
            Write("Please enter the last name for employee 2: ");
            lastNameEmployee2 = ReadLine();
            Write("Please enter the hours worked for employee 2: ");
            hoursWorkedEmployee2 = Convert.ToDouble(ReadLine());
            Write("Please enter the hourly rate for employee 2: ");
            hourlyRateEmployee2 = Convert.ToDouble(ReadLine());
            WriteLine();

            grossPayEmployee2 = hoursWorkedEmployee2 * hourlyRateEmployee2;

            WriteLine("First Name: {0}", firstNameEmployee2);
            WriteLine("Last Name: {0}", lastNameEmployee2);
            WriteLine("Hours Worked: {0}", hoursWorkedEmployee2.ToString("F"));
            WriteLine("Hourly Rate: {0}", hourlyRateEmployee2.ToString("C"));
            WriteLine("Gross Pay: {0}", grossPayEmployee2.ToString("C"));
            WriteLine();

            totPayroll = grossPayEmployee1 + grossPayEmployee2;
            WriteLine("Total Gross Pay: {0}", totPayroll.ToString("C"));
            WriteLine("Press enter to continue... ");
            ReadLine();
        }
    }
}
