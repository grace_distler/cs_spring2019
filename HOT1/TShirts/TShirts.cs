﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace TShirts
{
    class TShirts
    {
        static void Main(string[] args)
        {
            const double tshirtPrice = 14.99;
            const double tax = 0.08;
            string name, address, city, state, zip;
            double quantity, subTotal, salesTax, amountDue;

            Write("Enter your name: ");
            name = ReadLine();

            Write("Street Address: ");
            address = ReadLine();

            Write("City: ");
            city = ReadLine();

            Write("State: ");
            state = ReadLine();

            Write("Zip: ");
            zip = ReadLine();

            Write("How many T-Shirts did you want to order? ");
            quantity = Convert.ToDouble(ReadLine());
            WriteLine();
            WriteLine();

            subTotal = quantity * 14.99;
            salesTax = subTotal * tax;
            //salesTax = Math.Round(salesTax, 2);
            amountDue = subTotal + salesTax;

            WriteLine("Receipt for:");
            WriteLine("{0}", name);
            WriteLine("{0}", address);
            WriteLine("{0}, {1}, {2}", city, state, zip);
            WriteLine();
            WriteLine("{0} T-Shirts ordered @ $14.99 each", quantity);
            WriteLine();
            WriteLine("Total: {0, 5}", subTotal.ToString("C"));
            WriteLine("Tax: {0, 7}", salesTax.ToString("C"));
            WriteLine("--------------------");
            WriteLine("Due: {0, 8}", amountDue.ToString("C"));
            WriteLine("Press enter to continue...");
            ReadLine();

        }

    }
}
