﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace LB5
{
    class Program
    {
        static void Main(string[] args)
        {
            int testScore = 0;
            int numberOfScores = 0;
            int sum = 0;

            int maxScore = 0;
            int minScore = 100;
            float avg = 0;
            while (testScore != 999)
            {
                Write("Please enter a test score:");
                testScore = Convert.ToInt32(ReadLine());
                if ((testScore > 100) || testScore < 0)
                {
                    if (testScore != 999)
                    {
                        WriteLine("Invalid Score");
                        continue;
                    }
                }
                else
                {
                    numberOfScores++;
                    sum = sum + testScore;
                    if (testScore > maxScore) maxScore = testScore;
                    if (testScore < minScore) minScore = testScore;
                }
            }
            avg = (float)sum / numberOfScores;
            WriteLine();
            WriteLine("Number of scores: {0}", numberOfScores);
            WriteLine("Sum of test scores: {0}", sum);
            WriteLine("Average test score: {0}", Math.Round(avg, 2));
            WriteLine("Lowest test score: {0}", minScore);
            WriteLine("Highest test score: {0}", maxScore);
            
            Write("Press any key to continue...");
            ReadLine();
        }
    };
}
