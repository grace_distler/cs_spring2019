﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace LB7
{
    class Program
    {
        static void Main(string[] args)
        {
            string initial = "";

            double dAmount = 0.00;
            double eAmount = 0.00;
            double fAmount = 0.00;
            double grandTotal = 0.00;

            while (initial != "z" && initial != "Z")
            {
                WriteLine("\nWho made the sale? ");
                initial = ReadLine().ToLower();

                if (initial == "d" || initial == "D" || initial == "e" || initial == "E" || initial == "f" || initial == "F")
                {
                    WriteLine("How much was the sale? ");
                    double salesAmount = Convert.ToDouble(ReadLine());

                    if (initial == "d" || initial == "D")
                        dAmount += salesAmount;
                    else if (initial == "e" || initial == "E")
                        eAmount += salesAmount;
                    else if (initial == "f" || initial == "F")
                        fAmount += salesAmount;

                    grandTotal = dAmount + eAmount + fAmount;
                } else 
                {
                    WriteLine("Invalid initial");
                }

            }

            WriteLine("\nDanielle's Amount: {0:c}"
                    + "\nEdward's Amount: {1:c}"
                    + "\nFrancis' Amount: {2:c}"
                    + "\n\nGrand Total: {3:c}", dAmount, eAmount, fAmount, grandTotal);
            ReadLine();

        }
    }
}
