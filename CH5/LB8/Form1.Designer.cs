﻿namespace LB8
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.saleTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.danielleButton = new System.Windows.Forms.Button();
            this.edwardButton = new System.Windows.Forms.Button();
            this.francisButton = new System.Windows.Forms.Button();
            this.danielleSalesOutputLabel = new System.Windows.Forms.Label();
            this.edwardSalesOutputLabel = new System.Windows.Forms.Label();
            this.francisSalesOutputLabel = new System.Windows.Forms.Label();
            this.totalSalesLabel = new System.Windows.Forms.Label();
            this.mostSalesLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "How much was the sale?";
            // 
            // saleTextBox
            // 
            this.saleTextBox.Location = new System.Drawing.Point(30, 43);
            this.saleTextBox.Name = "saleTextBox";
            this.saleTextBox.Size = new System.Drawing.Size(139, 20);
            this.saleTextBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Who made the sale?";
            // 
            // danielleButton
            // 
            this.danielleButton.Location = new System.Drawing.Point(30, 122);
            this.danielleButton.Name = "danielleButton";
            this.danielleButton.Size = new System.Drawing.Size(75, 23);
            this.danielleButton.TabIndex = 3;
            this.danielleButton.Text = "Danielle";
            this.danielleButton.UseVisualStyleBackColor = true;
            // 
            // edwardButton
            // 
            this.edwardButton.Location = new System.Drawing.Point(134, 122);
            this.edwardButton.Name = "edwardButton";
            this.edwardButton.Size = new System.Drawing.Size(75, 23);
            this.edwardButton.TabIndex = 4;
            this.edwardButton.Text = "Edward";
            this.edwardButton.UseVisualStyleBackColor = true;
            // 
            // francisButton
            // 
            this.francisButton.Location = new System.Drawing.Point(235, 122);
            this.francisButton.Name = "francisButton";
            this.francisButton.Size = new System.Drawing.Size(75, 23);
            this.francisButton.TabIndex = 5;
            this.francisButton.Text = "Francis";
            this.francisButton.UseVisualStyleBackColor = true;
            // 
            // danielleSalesOutputLabel
            // 
            this.danielleSalesOutputLabel.AutoSize = true;
            this.danielleSalesOutputLabel.Location = new System.Drawing.Point(30, 190);
            this.danielleSalesOutputLabel.Name = "danielleSalesOutputLabel";
            this.danielleSalesOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.danielleSalesOutputLabel.TabIndex = 6;
            // 
            // edwardSalesOutputLabel
            // 
            this.edwardSalesOutputLabel.AutoSize = true;
            this.edwardSalesOutputLabel.Location = new System.Drawing.Point(30, 219);
            this.edwardSalesOutputLabel.Name = "edwardSalesOutputLabel";
            this.edwardSalesOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.edwardSalesOutputLabel.TabIndex = 7;
            // 
            // francisSalesOutputLabel
            // 
            this.francisSalesOutputLabel.AutoSize = true;
            this.francisSalesOutputLabel.Location = new System.Drawing.Point(30, 248);
            this.francisSalesOutputLabel.Name = "francisSalesOutputLabel";
            this.francisSalesOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.francisSalesOutputLabel.TabIndex = 8;
            // 
            // totalSalesLabel
            // 
            this.totalSalesLabel.AutoSize = true;
            this.totalSalesLabel.Location = new System.Drawing.Point(30, 277);
            this.totalSalesLabel.Name = "totalSalesLabel";
            this.totalSalesLabel.Size = new System.Drawing.Size(0, 13);
            this.totalSalesLabel.TabIndex = 9;
            // 
            // mostSalesLabel
            // 
            this.mostSalesLabel.AutoSize = true;
            this.mostSalesLabel.Location = new System.Drawing.Point(30, 344);
            this.mostSalesLabel.Name = "mostSalesLabel";
            this.mostSalesLabel.Size = new System.Drawing.Size(0, 13);
            this.mostSalesLabel.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 389);
            this.Controls.Add(this.mostSalesLabel);
            this.Controls.Add(this.totalSalesLabel);
            this.Controls.Add(this.francisSalesOutputLabel);
            this.Controls.Add(this.edwardSalesOutputLabel);
            this.Controls.Add(this.danielleSalesOutputLabel);
            this.Controls.Add(this.francisButton);
            this.Controls.Add(this.edwardButton);
            this.Controls.Add(this.danielleButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.saleTextBox);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Home Sales";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox saleTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button danielleButton;
        private System.Windows.Forms.Button edwardButton;
        private System.Windows.Forms.Button francisButton;
        private System.Windows.Forms.Label danielleSalesOutputLabel;
        private System.Windows.Forms.Label edwardSalesOutputLabel;
        private System.Windows.Forms.Label francisSalesOutputLabel;
        private System.Windows.Forms.Label totalSalesLabel;
        private System.Windows.Forms.Label mostSalesLabel;
    }
}

