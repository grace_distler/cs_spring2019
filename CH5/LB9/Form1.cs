﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB9
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            if(phraseTextBox.Text != "")
            {
                string phrase = phraseTextBox.Text;
                int vowelCount = 0;

                for (int j = 0; j < phrase.Length; j++)
                {
                    string check = phrase.Substring(j, 1);

                    switch (check)
                    {
                        case "a":
                            ++vowelCount;
                            break;
                        case "A":
                            ++vowelCount;
                            break;
                        case "e":
                            ++vowelCount;
                            break;
                        case "E":
                            ++vowelCount;
                            break;
                        case "i":
                            ++vowelCount;
                            break;
                        case "I":
                            ++vowelCount;
                            break;
                        case "o":
                            ++vowelCount;
                            break;
                        case "O":
                            ++vowelCount;
                            break;
                        case "u":
                            ++vowelCount;
                            break;
                        case "U":
                            ++vowelCount;
                            break;
                    }
                }
                vowelOutputLabel.Text = "The number of vowels is: " + vowelCount;
            }
            else
            {
                vowelOutputLabel.Text=("Error! Please enter a phrase.");
            }
        }




    }
}
