﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB6
{
    public partial class Form1 : Form
    {
        List<int> testScoresList = new List<int>();

        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            int testScore;
            int numberOfScores = 0;
            int sum = 0;
            int maxScore = 0;
            int minScore = 100;
            float avg = 0;

            testScore = Convert.ToInt32(testScoreEntryTextBox.Text);

            if ((testScore != 999)&&(testScore < 100 || testScore < 0))
            {
                testScoresList.Add(testScore);

            }
            else if ((testScore > 100) || testScore < 0)
                {
                numberOfScoresLabel.Text = "Invalid Score";

                if (testScore == 999)
                {
                    foreach (int score in testScoresList)
                    {
                        numberOfScores++;
                        sum += score;
                        if (score > maxScore) maxScore = score;
                        if (score < minScore) minScore = score;

                    }

                    avg = (float)sum / numberOfScores;

                    numberOfScoresLabel.Text = "Number of scores: " + numberOfScores;
                    sumOutputLabel.Text = "Sum of test scores: " + sum;
                    averageOutputLabel.Text = "Average test score: " + Math.Round(avg, 2);
                    minScoreOutputLabel.Text = "Lowest test score: " + minScore;
                    maxScoreOuputLabel.Text = "Highest test score: " + maxScore;

                    testScoresList.Clear();
                }
            }

        }
    }
}
