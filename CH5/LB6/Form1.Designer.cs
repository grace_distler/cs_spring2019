﻿namespace LB6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.testScoreEntryTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.numberOfScoresLabel = new System.Windows.Forms.Label();
            this.sumOutputLabel = new System.Windows.Forms.Label();
            this.averageOutputLabel = new System.Windows.Forms.Label();
            this.minScoreOutputLabel = new System.Windows.Forms.Label();
            this.maxScoreOuputLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(257, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Please enter a test score, or enter 999 to end entries.";
            // 
            // testScoreEntryTextBox
            // 
            this.testScoreEntryTextBox.Location = new System.Drawing.Point(35, 45);
            this.testScoreEntryTextBox.Name = "testScoreEntryTextBox";
            this.testScoreEntryTextBox.Size = new System.Drawing.Size(117, 20);
            this.testScoreEntryTextBox.TabIndex = 1;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(35, 92);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 2;
            this.calculateButton.Text = "Enter";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // numberOfScoresLabel
            // 
            this.numberOfScoresLabel.AutoSize = true;
            this.numberOfScoresLabel.Location = new System.Drawing.Point(35, 164);
            this.numberOfScoresLabel.Name = "numberOfScoresLabel";
            this.numberOfScoresLabel.Size = new System.Drawing.Size(0, 13);
            this.numberOfScoresLabel.TabIndex = 3;
            // 
            // sumOutputLabel
            // 
            this.sumOutputLabel.AutoSize = true;
            this.sumOutputLabel.Location = new System.Drawing.Point(35, 191);
            this.sumOutputLabel.Name = "sumOutputLabel";
            this.sumOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.sumOutputLabel.TabIndex = 4;
            // 
            // averageOutputLabel
            // 
            this.averageOutputLabel.AutoSize = true;
            this.averageOutputLabel.Location = new System.Drawing.Point(35, 217);
            this.averageOutputLabel.Name = "averageOutputLabel";
            this.averageOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.averageOutputLabel.TabIndex = 5;
            // 
            // minScoreOutputLabel
            // 
            this.minScoreOutputLabel.AutoSize = true;
            this.minScoreOutputLabel.Location = new System.Drawing.Point(35, 245);
            this.minScoreOutputLabel.Name = "minScoreOutputLabel";
            this.minScoreOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.minScoreOutputLabel.TabIndex = 6;
            // 
            // maxScoreOuputLabel
            // 
            this.maxScoreOuputLabel.AutoSize = true;
            this.maxScoreOuputLabel.Location = new System.Drawing.Point(35, 274);
            this.maxScoreOuputLabel.Name = "maxScoreOuputLabel";
            this.maxScoreOuputLabel.Size = new System.Drawing.Size(0, 13);
            this.maxScoreOuputLabel.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 375);
            this.Controls.Add(this.maxScoreOuputLabel);
            this.Controls.Add(this.minScoreOutputLabel);
            this.Controls.Add(this.averageOutputLabel);
            this.Controls.Add(this.sumOutputLabel);
            this.Controls.Add(this.numberOfScoresLabel);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.testScoreEntryTextBox);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Test Scores";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox testScoreEntryTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label numberOfScoresLabel;
        private System.Windows.Forms.Label sumOutputLabel;
        private System.Windows.Forms.Label averageOutputLabel;
        private System.Windows.Forms.Label minScoreOutputLabel;
        private System.Windows.Forms.Label maxScoreOuputLabel;
    }
}

