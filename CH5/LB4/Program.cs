﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace LB4
{
    class Program
    {
        static void Main(string[] args)
        {
            int row, col;
            string userInput;

            WriteLine("How many rows should the table have?");
            userInput = ReadLine();
            row = Convert.ToInt32(userInput);

            WriteLine("How many columns should the table have?");
            userInput = ReadLine();
            col = Convert.ToInt32(userInput);

            int[,] arr = new int[row, col];
            for (int i = 0; i < row; i++)
            {

                for (int j = 0; j < col; j++)
                {
                    arr[i, j] = (i + 1) * (j + 1);
                }
            }

            WriteLine();
            WriteLine("-----------------------------------------------------");
            for (int i = 0; i < row; i++)
            {

                for (int j = 0; j < col; j++)
                {
                    Write(String.Format("{0,5}", arr[i, j]));
                }
                WriteLine();
            }
            ReadLine();
        }
    }
    }
