﻿namespace EX1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblBalance = new System.Windows.Forms.Label();
            this.lblLoanOutput = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.txtboxName = new System.Windows.Forms.TextBox();
            this.txtboxAmount = new System.Windows.Forms.TextBox();
            this.radShortTerm = new System.Windows.Forms.RadioButton();
            this.radLongTerm = new System.Windows.Forms.RadioButton();
            this.btnCreateLoan = new System.Windows.Forms.Button();
            this.btnMakePayment = new System.Windows.Forms.Button();
            this.lblInterestAccumulation = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCreateLoan);
            this.groupBox1.Controls.Add(this.radLongTerm);
            this.groupBox1.Controls.Add(this.radShortTerm);
            this.groupBox1.Controls.Add(this.txtboxAmount);
            this.groupBox1.Controls.Add(this.txtboxName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(31, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(621, 255);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Create Loan";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Amount:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Type:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblInterestAccumulation);
            this.groupBox2.Controls.Add(this.btnMakePayment);
            this.groupBox2.Controls.Add(this.lblBalance);
            this.groupBox2.Controls.Add(this.lblLoanOutput);
            this.groupBox2.Controls.Add(this.lblName);
            this.groupBox2.Location = new System.Drawing.Point(31, 309);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(621, 255);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Manage Loan";
            // 
            // lblBalance
            // 
            this.lblBalance.AutoSize = true;
            this.lblBalance.Location = new System.Drawing.Point(36, 178);
            this.lblBalance.Name = "lblBalance";
            this.lblBalance.Size = new System.Drawing.Size(46, 17);
            this.lblBalance.TabIndex = 2;
            this.lblBalance.Text = "label4";
            // 
            // lblLoanOutput
            // 
            this.lblLoanOutput.AutoSize = true;
            this.lblLoanOutput.Location = new System.Drawing.Point(36, 81);
            this.lblLoanOutput.Name = "lblLoanOutput";
            this.lblLoanOutput.Size = new System.Drawing.Size(46, 17);
            this.lblLoanOutput.TabIndex = 1;
            this.lblLoanOutput.Text = "label5";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(35, 45);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(66, 24);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "label6";
            // 
            // txtboxName
            // 
            this.txtboxName.Location = new System.Drawing.Point(163, 37);
            this.txtboxName.Name = "txtboxName";
            this.txtboxName.Size = new System.Drawing.Size(443, 22);
            this.txtboxName.TabIndex = 3;
            // 
            // txtboxAmount
            // 
            this.txtboxAmount.Location = new System.Drawing.Point(163, 92);
            this.txtboxAmount.Name = "txtboxAmount";
            this.txtboxAmount.Size = new System.Drawing.Size(443, 22);
            this.txtboxAmount.TabIndex = 4;
            // 
            // radShortTerm
            // 
            this.radShortTerm.AutoSize = true;
            this.radShortTerm.Location = new System.Drawing.Point(163, 148);
            this.radShortTerm.Name = "radShortTerm";
            this.radShortTerm.Size = new System.Drawing.Size(137, 21);
            this.radShortTerm.TabIndex = 5;
            this.radShortTerm.TabStop = true;
            this.radShortTerm.Text = "Short-Term Loan";
            this.radShortTerm.UseVisualStyleBackColor = true;
            // 
            // radLongTerm
            // 
            this.radLongTerm.AutoSize = true;
            this.radLongTerm.Location = new System.Drawing.Point(163, 175);
            this.radLongTerm.Name = "radLongTerm";
            this.radLongTerm.Size = new System.Drawing.Size(135, 21);
            this.radLongTerm.TabIndex = 6;
            this.radLongTerm.TabStop = true;
            this.radLongTerm.Text = "Long-Term Loan";
            this.radLongTerm.UseVisualStyleBackColor = true;
            // 
            // btnCreateLoan
            // 
            this.btnCreateLoan.Location = new System.Drawing.Point(323, 201);
            this.btnCreateLoan.Name = "btnCreateLoan";
            this.btnCreateLoan.Size = new System.Drawing.Size(117, 30);
            this.btnCreateLoan.TabIndex = 7;
            this.btnCreateLoan.Text = "Create Loan";
            this.btnCreateLoan.UseVisualStyleBackColor = true;
            this.btnCreateLoan.Click += new System.EventHandler(this.btnCreateLoan_Click);
            // 
            // btnMakePayment
            // 
            this.btnMakePayment.Location = new System.Drawing.Point(39, 125);
            this.btnMakePayment.Name = "btnMakePayment";
            this.btnMakePayment.Size = new System.Drawing.Size(117, 30);
            this.btnMakePayment.TabIndex = 8;
            this.btnMakePayment.Text = "Make Payment";
            this.btnMakePayment.UseVisualStyleBackColor = true;
            this.btnMakePayment.Click += new System.EventHandler(this.btnMakePayment_Click);
            // 
            // lblInterestAccumulation
            // 
            this.lblInterestAccumulation.AutoSize = true;
            this.lblInterestAccumulation.Location = new System.Drawing.Point(36, 213);
            this.lblInterestAccumulation.Name = "lblInterestAccumulation";
            this.lblInterestAccumulation.Size = new System.Drawing.Size(46, 17);
            this.lblInterestAccumulation.TabIndex = 9;
            this.lblInterestAccumulation.Text = "label5";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 599);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Bank Loan";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCreateLoan;
        private System.Windows.Forms.RadioButton radLongTerm;
        private System.Windows.Forms.RadioButton radShortTerm;
        private System.Windows.Forms.TextBox txtboxAmount;
        private System.Windows.Forms.TextBox txtboxName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblInterestAccumulation;
        private System.Windows.Forms.Button btnMakePayment;
        private System.Windows.Forms.Label lblBalance;
        private System.Windows.Forms.Label lblLoanOutput;
        private System.Windows.Forms.Label lblName;
    }
}

