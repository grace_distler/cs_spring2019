﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            btnMakePayment.Enabled = false;
        }

        string name;
        double amount;
        string loan;
        double currentBalance;

        private void btnCreateLoan_Click(object sender, EventArgs e)
        {
            name = txtboxName.Text;
            amount = Convert.ToDouble(txtboxAmount.Text);
           
            if (radShortTerm.Checked)
            {
                loan = "Short-Term loan";
                ShortTermLoan stl = new ShortTermLoan(name, amount);
            }
            else if (radLongTerm.Checked)
            {
                loan = "Long-Term loan";
                LongTermLoan stl = new LongTermLoan(name, amount);

            }

            lblName.Text = name;
            if (amount < 0)
            {
                lblLoanOutput.Text = "Enter a valid amount";
            }
            else
            {
                lblLoanOutput.Text = loan + " of " + amount.ToString("C");
            }
            currentBalance = amount;
            btnMakePayment.Enabled = true;            
        }

        private void btnMakePayment_Click(object sender, EventArgs e)
        {
            if(radShortTerm.Checked)
            {
                double loanBalance = (currentBalance -= 450);
                double loanInterest = loanBalance + (currentBalance * 0.10);
                lblBalance.Text = "After the last payment the loan is down to " + (loanBalance).ToString("C");
                lblInterestAccumulation.Text = "After the last interest accumulation the loan is now " + loanInterest.ToString("C");
            }
            else if (radLongTerm.Checked)
            {
                double loanBalance = (currentBalance -= 200);
                double loanInterest = loanBalance + (currentBalance * 0.05);
                lblBalance.Text = "After the last payment the loan is down to " + (loanBalance).ToString("C");
                lblInterestAccumulation.Text = "After the last interest accumulation the loan is now " + loanInterest.ToString("C");
            }
        }
    }
}
