﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX1
{
    public class LongTermLoan : ILoan
    {
        public string _name;
        public double _balance;

        public LongTermLoan(string name, double balance)
        {
            _name = name;
            _balance = balance;
        }

        double ILoan.MakePayment()
        {
            int payment = 200;
            _balance -= payment;

            return _balance;
        }

        double ILoan.ApplyInterest()
        {
            double interest = .05;
            _balance = (_balance * interest) + _balance;

            return _balance;
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }
        public double Balance
        {
            get
            {
                return _balance;
            }
        }
        public string Type { get; }
    }
}
