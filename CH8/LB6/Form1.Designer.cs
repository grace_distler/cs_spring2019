﻿namespace LB6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.integerEntryTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numbersEnteredOutputLabel = new System.Windows.Forms.Label();
            this.lowestNumberOutputLabel = new System.Windows.Forms.Label();
            this.highestNumberOutputLabel = new System.Windows.Forms.Label();
            this.sumOutputLabel = new System.Windows.Forms.Label();
            this.averageOutputLabel = new System.Windows.Forms.Label();
            this.errorMessageLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // integerEntryTextBox
            // 
            this.integerEntryTextBox.Location = new System.Drawing.Point(29, 25);
            this.integerEntryTextBox.Name = "integerEntryTextBox";
            this.integerEntryTextBox.Size = new System.Drawing.Size(150, 20);
            this.integerEntryTextBox.TabIndex = 0;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(29, 52);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 1;
            this.calculateButton.Text = "Enter";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Numbers Entered:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 157);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Lowest:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 192);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Highest:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 223);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Sum:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 253);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Average:";
            // 
            // numbersEnteredOutputLabel
            // 
            this.numbersEnteredOutputLabel.AutoSize = true;
            this.numbersEnteredOutputLabel.Location = new System.Drawing.Point(164, 125);
            this.numbersEnteredOutputLabel.Name = "numbersEnteredOutputLabel";
            this.numbersEnteredOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.numbersEnteredOutputLabel.TabIndex = 7;
            // 
            // lowestNumberOutputLabel
            // 
            this.lowestNumberOutputLabel.AutoSize = true;
            this.lowestNumberOutputLabel.Location = new System.Drawing.Point(164, 157);
            this.lowestNumberOutputLabel.Name = "lowestNumberOutputLabel";
            this.lowestNumberOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.lowestNumberOutputLabel.TabIndex = 8;
            // 
            // highestNumberOutputLabel
            // 
            this.highestNumberOutputLabel.AutoSize = true;
            this.highestNumberOutputLabel.Location = new System.Drawing.Point(164, 192);
            this.highestNumberOutputLabel.Name = "highestNumberOutputLabel";
            this.highestNumberOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.highestNumberOutputLabel.TabIndex = 9;
            // 
            // sumOutputLabel
            // 
            this.sumOutputLabel.AutoSize = true;
            this.sumOutputLabel.Location = new System.Drawing.Point(164, 223);
            this.sumOutputLabel.Name = "sumOutputLabel";
            this.sumOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.sumOutputLabel.TabIndex = 10;
            // 
            // averageOutputLabel
            // 
            this.averageOutputLabel.AutoSize = true;
            this.averageOutputLabel.Location = new System.Drawing.Point(164, 253);
            this.averageOutputLabel.Name = "averageOutputLabel";
            this.averageOutputLabel.Size = new System.Drawing.Size(0, 13);
            this.averageOutputLabel.TabIndex = 11;
            // 
            // errorMessageLabel
            // 
            this.errorMessageLabel.AutoSize = true;
            this.errorMessageLabel.Location = new System.Drawing.Point(29, 82);
            this.errorMessageLabel.Name = "errorMessageLabel";
            this.errorMessageLabel.Size = new System.Drawing.Size(0, 13);
            this.errorMessageLabel.TabIndex = 12;
            // 
            // Form1
            // 
            this.AcceptButton = this.calculateButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(288, 297);
            this.Controls.Add(this.errorMessageLabel);
            this.Controls.Add(this.averageOutputLabel);
            this.Controls.Add(this.sumOutputLabel);
            this.Controls.Add(this.highestNumberOutputLabel);
            this.Controls.Add(this.lowestNumberOutputLabel);
            this.Controls.Add(this.numbersEnteredOutputLabel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.integerEntryTextBox);
            this.Name = "Form1";
            this.Text = "Integer Stats";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox integerEntryTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label numbersEnteredOutputLabel;
        private System.Windows.Forms.Label lowestNumberOutputLabel;
        private System.Windows.Forms.Label highestNumberOutputLabel;
        private System.Windows.Forms.Label sumOutputLabel;
        private System.Windows.Forms.Label averageOutputLabel;
        private System.Windows.Forms.Label errorMessageLabel;
    }
}

