﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB6
{
    public partial class Form1 : Form
    {
        int[] numbers = new int[20];
        int count = 0;
        int enteredNumber;

        public Form1()
        {
            InitializeComponent();
        }        

        private void calculateButton_Click(object sender, EventArgs e)
        {
            if(int.TryParse(integerEntryTextBox.Text, out enteredNumber))
            {
                if (count < 20)
                {
                    numbers[count] = enteredNumber;
                    count++;
                    numbersEnteredOutputLabel.Text = String.Format($"{count}/20");
                    integerEntryTextBox.Text = "";
                    errorMessageLabel.Text = "";
                }
            }
            else
            {
                errorMessageLabel.Text = "Enter Valid Number";
            }

            if (count == 20)
            {
                CalculateStats(out int min, out int max, out double sum, out double avg, numbers);
                lowestNumberOutputLabel.Text = String.Format($"{min}");
                highestNumberOutputLabel.Text = String.Format($"{max}");
                sumOutputLabel.Text = sum.ToString("0.00");
                averageOutputLabel.Text = avg.ToString("0.00");
            }
        }

        public void CalculateStats(out int min, out int max, out double sum, out double avg, int[] array)
        {
            min = array.Min();
            max = array.Max();
            sum = array.Sum();
            avg = array.Average();
        }
    }
}
