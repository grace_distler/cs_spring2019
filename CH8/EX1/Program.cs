﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX1
{
    class Program
    {
        static void Main(string[] args)
        {
            
            float result;
            string value;

            do
            {
                Console.WriteLine("Enter a number");
                value = Console.ReadLine();
            } while (!(float.TryParse(value, out result)));

            if(float.TryParse(value, out result))
            {
                Console.WriteLine("The result is {0}", result);
            }
            else
            {
                Console.WriteLine("Please enter a number");
            }

            //Console.WriteLine(returnValue);
            //Console.WriteLine(result);
        }
    }
}
