﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int firstNumber;
        int secondNumber;
        int thirdNumber;
        int fourthNumber;

        private void calculateButton_Click(object sender, EventArgs e)
        {
            firstNumber = Convert.ToInt32(firstNumberTextBox.Text);
            secondNumber = Convert.ToInt32(secondNumberTextBox.Text);
            thirdNumber = Convert.ToInt32(thirdNumberTextBox.Text);
            fourthNumber = Convert.ToInt32(fourthNumberTextBox.Text);

            Reverse4(ref firstNumber, ref secondNumber, ref thirdNumber, ref fourthNumber);
        }

        public void Reverse4(ref int firstNumber, ref int secondNumber, ref int thirdNumber, ref int fourthNumber)
        {
            fourthNumberTextBox.Text = Convert.ToString(firstNumber);
            thirdNumberTextBox.Text = Convert.ToString(secondNumber);
            secondNumberTextBox.Text = Convert.ToString(thirdNumber);
            firstNumberTextBox.Text = Convert.ToString(fourthNumber);
        }
    }
}
