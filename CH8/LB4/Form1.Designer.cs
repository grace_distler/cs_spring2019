﻿namespace LB4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.phraseEntryTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.outputPhraseLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // phraseEntryTextBox
            // 
            this.phraseEntryTextBox.Location = new System.Drawing.Point(32, 26);
            this.phraseEntryTextBox.Name = "phraseEntryTextBox";
            this.phraseEntryTextBox.Size = new System.Drawing.Size(378, 20);
            this.phraseEntryTextBox.TabIndex = 0;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(32, 62);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 1;
            this.calculateButton.Text = "Sort";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // outputPhraseLabel
            // 
            this.outputPhraseLabel.AutoSize = true;
            this.outputPhraseLabel.Location = new System.Drawing.Point(29, 109);
            this.outputPhraseLabel.Name = "outputPhraseLabel";
            this.outputPhraseLabel.Size = new System.Drawing.Size(0, 13);
            this.outputPhraseLabel.TabIndex = 2;
            // 
            // Form1
            // 
            this.AcceptButton = this.calculateButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 150);
            this.Controls.Add(this.outputPhraseLabel);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.phraseEntryTextBox);
            this.Name = "Form1";
            this.Text = "Sorted Phrase";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox phraseEntryTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label outputPhraseLabel;
    }
}

