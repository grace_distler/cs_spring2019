﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            string phraseEntered = phraseEntryTextBox.Text;
            string[] split = phraseEntered.Split(new Char[] { });
            string[] sortedWordsArray = SortWords(split);

            outputPhraseLabel.Text = "";
            foreach (string word in sortedWordsArray)
            {
                outputPhraseLabel.Text += word + " ";
            }
        }

        public string[] SortWords(params string[] split)
        {
            Array.Sort(split);
            return split;
        }
    }
}
