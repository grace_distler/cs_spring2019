﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        double minimumBid = 10;
        string enteredBid = "";

        private void calculateButton_Click(object sender, EventArgs e)
        {
            enteredBid = bidEntryTextBox.Text;
            bidAcceptanceLabel.Text = ParseBid(enteredBid);
        }

        public string ParseBid(string enteredBid)
        {
            double bidAsDouble;
            string bidAsString;
            string acceptanceMessage = "";
            bool acceptBid = false;

            if (double.TryParse(enteredBid, out bidAsDouble))
            {
                acceptBid = true;
            }
            else
            {
                bidAsString = new String(enteredBid.Where(Char.IsDigit).ToArray());
                if (double.TryParse(bidAsString, out bidAsDouble))
                {
                    acceptBid = true;
                }
                else
                {
                    acceptanceMessage = "Invalid Bid";
                }
            }

            if (acceptBid == true)
            {
                if (bidAsDouble < minimumBid)
                {
                    acceptanceMessage = "Bid must be at least $10";
                }
                else if (bidAsDouble >= minimumBid)
                {
                    acceptanceMessage = "Bid of " + bidAsDouble.ToString("C") + " accepted!";
                }
            }
            return acceptanceMessage;
        }
    }
}
