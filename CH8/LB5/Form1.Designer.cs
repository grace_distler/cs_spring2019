﻿namespace LB5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bidEntryTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.bidAcceptanceLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bidEntryTextBox
            // 
            this.bidEntryTextBox.Location = new System.Drawing.Point(30, 34);
            this.bidEntryTextBox.Name = "bidEntryTextBox";
            this.bidEntryTextBox.Size = new System.Drawing.Size(170, 20);
            this.bidEntryTextBox.TabIndex = 0;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(30, 61);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 1;
            this.calculateButton.Text = "Bid";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // bidAcceptanceLabel
            // 
            this.bidAcceptanceLabel.AutoSize = true;
            this.bidAcceptanceLabel.Location = new System.Drawing.Point(30, 121);
            this.bidAcceptanceLabel.Name = "bidAcceptanceLabel";
            this.bidAcceptanceLabel.Size = new System.Drawing.Size(0, 13);
            this.bidAcceptanceLabel.TabIndex = 2;
            // 
            // Form1
            // 
            this.AcceptButton = this.calculateButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 180);
            this.Controls.Add(this.bidAcceptanceLabel);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.bidEntryTextBox);
            this.Name = "Form1";
            this.Text = "Auction";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox bidEntryTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label bidAcceptanceLabel;
    }
}

