﻿namespace EX2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ipAddressEntryTextBox = new System.Windows.Forms.TextBox();
            this.scanButton = new System.Windows.Forms.Button();
            this.computerNameLabel = new System.Windows.Forms.Label();
            this.ipAddressOutputLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.serviceOutputLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "IP Address";
            // 
            // ipAddressEntryTextBox
            // 
            this.ipAddressEntryTextBox.Location = new System.Drawing.Point(42, 58);
            this.ipAddressEntryTextBox.Name = "ipAddressEntryTextBox";
            this.ipAddressEntryTextBox.Size = new System.Drawing.Size(351, 22);
            this.ipAddressEntryTextBox.TabIndex = 1;
            // 
            // scanButton
            // 
            this.scanButton.Location = new System.Drawing.Point(438, 53);
            this.scanButton.Name = "scanButton";
            this.scanButton.Size = new System.Drawing.Size(84, 32);
            this.scanButton.TabIndex = 2;
            this.scanButton.Text = "Scan";
            this.scanButton.UseVisualStyleBackColor = true;
            this.scanButton.Click += new System.EventHandler(this.scanButton_Click);
            // 
            // computerNameLabel
            // 
            this.computerNameLabel.AutoSize = true;
            this.computerNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.computerNameLabel.Location = new System.Drawing.Point(38, 117);
            this.computerNameLabel.Name = "computerNameLabel";
            this.computerNameLabel.Size = new System.Drawing.Size(66, 24);
            this.computerNameLabel.TabIndex = 3;
            this.computerNameLabel.Text = "label2";
            // 
            // ipAddressOutputLabel
            // 
            this.ipAddressOutputLabel.AutoSize = true;
            this.ipAddressOutputLabel.Location = new System.Drawing.Point(39, 141);
            this.ipAddressOutputLabel.Name = "ipAddressOutputLabel";
            this.ipAddressOutputLabel.Size = new System.Drawing.Size(46, 17);
            this.ipAddressOutputLabel.TabIndex = 4;
            this.ipAddressOutputLabel.Text = "label3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(39, 217);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 18);
            this.label4.TabIndex = 5;
            this.label4.Text = "Services";
            // 
            // serviceOutputLabel
            // 
            this.serviceOutputLabel.AutoSize = true;
            this.serviceOutputLabel.Location = new System.Drawing.Point(39, 235);
            this.serviceOutputLabel.Name = "serviceOutputLabel";
            this.serviceOutputLabel.Size = new System.Drawing.Size(46, 17);
            this.serviceOutputLabel.TabIndex = 6;
            this.serviceOutputLabel.Text = "label5";
            // 
            // Form1
            // 
            this.AcceptButton = this.scanButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 450);
            this.Controls.Add(this.serviceOutputLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ipAddressOutputLabel);
            this.Controls.Add(this.computerNameLabel);
            this.Controls.Add(this.scanButton);
            this.Controls.Add(this.ipAddressEntryTextBox);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Network Scanner";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ipAddressEntryTextBox;
        private System.Windows.Forms.Button scanButton;
        private System.Windows.Forms.Label computerNameLabel;
        private System.Windows.Forms.Label ipAddressOutputLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label serviceOutputLabel;
    }
}

