﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX2
{
    public partial class Form1 : Form
    {
        private Computer[] computers;
        private int count = 3;
        private string userEntry;
        private int computerIndex;

        public Form1()
        {
            InitializeComponent();

            PortNumber type1;
            type1 = PortNumber.FTP_DATA;
            PortNumber type2;
            type2 = PortNumber.FTP_CONTROL;
            PortNumber type3;
            type3 = PortNumber.TELNET;
            PortNumber type4;
            type4 = PortNumber.SMPT;
            PortNumber type5;
            type5 = PortNumber.DNS;
            PortNumber type6;
            type6 = PortNumber.HTTP;
            PortNumber type7;
            type7 = PortNumber.NTP;
            PortNumber type8;
            type8 = PortNumber.LDAP;
            PortNumber type9;
            type9 = PortNumber.HTTPS;

            computers = new Computer[count];
            computers[0] = new Computer("My-Desktop", "127.0.0.1", [type1, type2, type3, type4, type5, type6, type7, type8, type9]);
            computers[1] = new Computer("google-public-dns-a", "8.8.8.8", [type5]);
            computers[2] = new Computer("ranken.edu", "47.44.246.80", [type1]);

        }

        private void scanButton_Click(object sender, EventArgs e)
        {
            userEntry = ipAddressEntryTextBox.Text;
            ScanIpAddress(userEntry);
            ShowComputer(ScanIpAddress(userEntry));
        }

        public int ScanIpAddress(string userEntry)
        {
            bool found = false;

            for (int i = 0; i < count; i++)
            {
                if (userEntry.Equals(computers[i].IpAddress))
                {
                    computerIndex = i;
                    found = true;
                }
            }

            if (found == false)
            {
                return -1;
            }
            else
            {
                return computerIndex;
            }
        }

        public void ShowComputer(int computerIndex)
        {
            if (computerIndex >= 0)
            {
                computerNameLabel.Text = computers[computerIndex].Name;
                ipAddressOutputLabel.Text = computers[computerIndex].IpAddress;
                serviceOutputLabel.Text = computers[computerIndex].Services.ToString();
            }
            else
            {
                computerNameLabel.Text = "Request Timed Out";
                ipAddressOutputLabel.Text = ipAddressEntryTextBox.Text;
                serviceOutputLabel.Text = "NONE";
            }
        }
    }
}
