﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX2
{
    public enum PortNumber
    {
        FTP_DATA=20,
        FTP_CONTROL=21,
        TELNET=23,
        SMPT=25,
        DNS=53,
        HTTP=80,
        NTP=123,
        LDAP=389,
        HTTPS=443
    }

    class Computer
    {
        private string _name;
        private string _ipAddress;
        private PortNumber[] _services;

        public Computer(string name, string ipAddress, PortNumber[] services)
        {
            _name = name;
            _ipAddress = ipAddress;
            _services = services;
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public string IpAddress
        {
            get
            {
                return _ipAddress;
            }
        }

        public PortNumber[] Services
        {
            get
            {
                return _services;
            }
        }
    }
}
