﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX1
{
    public partial class Form1 : Form
    {
        private Password[] passwords;
        private int count = 10;
        private string userEntry;
        private string returnValue;

        public Form1()
        {
            InitializeComponent();

            passwords = new Password[count];
            passwords[0] = new Password("123456", "e10adc3949ba59abbe56e057f20f883e");
            passwords[1] = new Password("123456789", "25f9e794323b453885f5181f1b624d0b");
            passwords[2] = new Password("qwerty", "d8578edf8458ce06fbc5bb76a58c5ca4");
            passwords[3] = new Password("111111", "96e79218965eb72c92a549dd5a330112");
            passwords[4] = new Password("password", "5f4dcc3b5aa765d61d8327deb882cf99");
            passwords[5] = new Password("qwertyuiop", "6eea9b7ef19179a06954edd0f6c05ceb");
            passwords[6] = new Password("123321", "c8837b23ff8aaa8a2dde915473ce0991");
            passwords[7] = new Password("google", "c822c1b63853ed273b89687ac505f9fa");
            passwords[8] = new Password("P@ssw0rd", "161ebd7d45089b3446ee4e0d86dbcf92");
            passwords[9] = new Password("Tr0ub4dor&3", "4ece57a61323b52ccffdbef021956754");
        }
        
        private void crackButton_Click(object sender, EventArgs e)
        {
            userEntry = md5HashEntryTextBox.Text;
            CrackPassword(userEntry);

            if (CrackPassword(userEntry) == null)
            {
                outputLabel.Text = "* FAIL *";
            }
            else
            {
                outputLabel.Text = returnValue;
            }

        }

        public string CrackPassword(string userEntry)
        {
            bool found = false;
            for (int i = 0; i < count; i++)
            {
                if (userEntry.Equals(passwords[i].Hash))
                {
                    found = true;
                    returnValue = passwords[i].Raw;
                }
            }

            if (found == false)
            {
                return null;
            }
            else
            {
                return returnValue;
            }
        }
    }
}
