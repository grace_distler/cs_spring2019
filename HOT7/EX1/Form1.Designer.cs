﻿namespace EX1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.md5HashEntryTextBox = new System.Windows.Forms.TextBox();
            this.crackButton = new System.Windows.Forms.Button();
            this.outputLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "MD5 Hash";
            // 
            // md5HashEntryTextBox
            // 
            this.md5HashEntryTextBox.Location = new System.Drawing.Point(35, 52);
            this.md5HashEntryTextBox.Name = "md5HashEntryTextBox";
            this.md5HashEntryTextBox.Size = new System.Drawing.Size(453, 22);
            this.md5HashEntryTextBox.TabIndex = 1;
            // 
            // crackButton
            // 
            this.crackButton.Location = new System.Drawing.Point(530, 49);
            this.crackButton.Name = "crackButton";
            this.crackButton.Size = new System.Drawing.Size(92, 29);
            this.crackButton.TabIndex = 2;
            this.crackButton.Text = "Crack";
            this.crackButton.UseVisualStyleBackColor = true;
            this.crackButton.Click += new System.EventHandler(this.crackButton_Click);
            // 
            // outputLabel
            // 
            this.outputLabel.AutoSize = true;
            this.outputLabel.Location = new System.Drawing.Point(32, 123);
            this.outputLabel.Name = "outputLabel";
            this.outputLabel.Size = new System.Drawing.Size(0, 17);
            this.outputLabel.TabIndex = 3;
            // 
            // Form1
            // 
            this.AcceptButton = this.crackButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(670, 178);
            this.Controls.Add(this.outputLabel);
            this.Controls.Add(this.crackButton);
            this.Controls.Add(this.md5HashEntryTextBox);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Password Cracker";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox md5HashEntryTextBox;
        private System.Windows.Forms.Button crackButton;
        private System.Windows.Forms.Label outputLabel;
    }
}

